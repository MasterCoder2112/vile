package com.vile.launcher;

import java.awt.Choice;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.FloatControl;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.vile.Display;
import com.vile.Game;
import com.vile.RunGame;

/**
 * @title  FPSLauncher
 * @author Alex Byrd
 * @modified 9/10/16
 * Description:
 * Starts the program off with the main menu. This holds the title
 * theme music, the Start New Game button, the Options menu to
 * set how you want your game to be, the Controls, the Read Me
 * file, and the Quit button. 
 *
 */
public class FPSLauncher extends JFrame
{
	private static final long serialVersionUID = 1L;

	private static final double versionNumber = 0.6;
	
	private static Clip clip;
	private static Clip[] click    =  new Clip[20];
	
	private static boolean audioOn = false;
	
	//Controls Gain (Volume) for both music and sounds
	private static FloatControl musicControl;
	private static FloatControl[] soundControl = new FloatControl[20];
	
	private JLayeredPane panel = new JLayeredPane();
	
	private JButton play;
	private JButton playGame;
	private JButton options;
	private JButton controls;
	private JButton quit;
	private JButton back1;
	private JButton back2;
	private JButton mouse;
	private JButton returnToGame;
	
	private JLabel  resolutionTitle;
	private JLabel  themeTitle;
	private JLabel  levelSizeTitle;
	private JLabel  modeTitle;
	private JLabel  musicTitle;
	private JLabel  musicVolumeTitle;
	private JLabel  soundVolumeTitle;
	private JLabel  title;
	
	private JTextField readMeText1;
	private JTextField readMeText2;
	private JTextField readMeText3;
	private JTextField readMeText4;
	private JTextField readMeText5;
	
	private Rectangle rplay;
	private Rectangle rplayGame;
	private Rectangle roptions;
	private Rectangle rcontrols;
	private Rectangle rquit;
	private Rectangle rback;
	private Rectangle rback2;
	private Rectangle resRect;
	private Rectangle themeRect;
	private Rectangle levelSizeRect;
	private Rectangle mouseRect;
	private Rectangle modeRect;
	private Rectangle musicRect;
	private Rectangle returnToGameRect;
	
	private ImageIcon titleImage;
	
	//Has scroll box you can put choices on
	private Choice resolution = new Choice();
	private Choice theme      = new Choice();
	private Choice levelSize  = new Choice();
	private Choice mode       = new Choice();
	private Choice music      = new Choice();
	
	//Volume Knobs
	public JSlider musicVolume;
	public JSlider soundVolume;
	
   /*
    * Level of sound in decibals above or below the original set amount 
    * by the computer. 
    */
	public static float  musicVolumeLevel = -20.0f;
	public static float  soundVolumeLevel = -20.0f;
	
	private static boolean opened      = false;
	private static boolean mouseStatus = true;
	
	private static AudioInputStream input;
	
	private static int clickNum    = -1;
	
	public static final int WIDTH  = 800;
	public static final int HEIGHT = 400;
	
   /*
    * Instantiates a new ActionListener that listens to whether Buttons 
    * are clicked or not.
    */
	ActionListener aL = new ActionListener()
	{
		public void actionPerformed(ActionEvent e)
		{
			//Call the play audio method to play the sound
			Display.playAudioFile(input, click, clickNum);
			
			//Update sound clip being played
			clickNum++;
			
			//If the clipNum is past the length, reset it
			if(clickNum == click.length)
			{
				clickNum = 0;
			}		
			
			if(e.getSource() == play)
			{
				clip.close();
				audioOn = false;
				dispose();
				Game.setMap = false;
				new RunGame();
			}
			
			if(e.getSource() == playGame)
			{
				clip.close();
				audioOn = false;
				dispose();
				Game.setMap = true;
				Display.themeNum = 2;
				new RunGame();
			}
			
			if(e.getSource() == options)
			{
				dispose();
				new Options();
			}
			
			if(e.getSource() ==  controls)
			{
				dispose();
				new Controls();
			}
			
			if(e.getSource() == quit)
			{
				System.exit(0);
			}
			
			if(e.getSource() == back1)
			{
				Display.graphicsSelection = resolution.getSelectedIndex();
				Display.themeNum          = theme.getSelectedIndex();
				Display.levelSize         = levelSize.getSelectedIndex();
				Display.mouseOn           = mouseStatus;
				Display.skillMode          = mode.getSelectedIndex();
				Display.musicTheme        = music.getSelectedIndex();
				dispose();
				new FPSLauncher(0);
			}
			
			if(e.getSource() == returnToGame)
			{
				Display.resetGame = true;
				clip.close();
				audioOn = false;
				dispose();
				new RunGame();
			}
			
			if(e.getSource() == back2)
			{
				dispose();
				new FPSLauncher(0);
			}
			
			if(e.getSource() == mouse)
			{
				if(mouseStatus)
				{
					mouseStatus = false;
					mouse.setText("Mouse Status: Off");
				}
				else
				{
					mouseStatus = true;
					mouse.setText("Mouse Status: On");
				}
			}
		}
	};
	
   /*
    * A listener that listens for any change of the volume knob. If
    * the state of the knob is changed, the method is passed an event
    * which then performs a serious of operations that the user 
    * programs it to do. In this case, the clip is played at a new
    * volume, and the tool tip on the volume knob is updated to show the
    * new volume level.
    */
	ChangeListener change = new ChangeListener()
	{
		@Override
		public void stateChanged(ChangeEvent e) 
		{
			if(e.getSource() == musicVolume)
			{
				float newVolume = musicVolume.getValue();
				musicVolumeLevel     = newVolume;
				
				//If newVolume is less than the volume limit, set it to limit
				if(newVolume < -80)
				{
					newVolume = -80;
				}
				
				musicControl.setValue(newVolume);	
				
				//Only do if there is music playing
				if(Display.audioOn)
				{
					   /*
					    * If the music volume is as lower than it can go (Meaning off) 
					    * then just set it to the limit. Otherwise just set the
					    * music volume to the correct level.
					    */
						if(musicVolumeLevel < -80)
						{
							Display.musicControl.setValue(-80.0f);
						}
						else
						{
							Display.musicControl.setValue(musicVolumeLevel);
						}
				}
				
				int temp  = (int) musicVolumeLevel + 94;
				musicVolume.setToolTipText("Music Volume Level: "+temp);
			}

			
			if(e.getSource() == soundVolume)
			{
				float newVolume2 = soundVolume.getValue();
				soundVolumeLevel     = newVolume2;
			
				//If newVolume is less than the volume limit, set it to limit
				if(newVolume2 < -80)
				{
					newVolume2 = -80;
				}
			
				for(int i = 0; i < click.length; i++)
				{
					soundControl[i].setValue(newVolume2);
				}
			
				int temp2  = (int) soundVolumeLevel + 94;
				soundVolume.setToolTipText("Sound Volume Level: "+temp2);
			}
		}
	};
	
   /**
    * Starts the specific launcher depending on its idNum. It could be
    * the pause menu, the main menu, the options menu, etc...
    * @param idNum
    */
	public FPSLauncher(int idNum)
	{
	   /*
	    * If the game was running, dispose of its frame so that the
	    * pause menu can show
	    */
		try
		{
			RunGame.frame.dispose();
		}
		catch(Exception e)
		{
			
		}
		
		if(!Display.pauseGame)
		{
			setTitle("Vile Launcher (Version: "+versionNumber+")");
		}
		else
		{
			setTitle("Pause Menu");
		}
		
		setSize(new Dimension(WIDTH, HEIGHT));
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setContentPane(panel);
		setLocationRelativeTo(null);
		
		//Gets a random int between 0 and 1
		int random = (int) (Math.random() * 2);

		if(random == 0)
		{
			//Set background to black
			panel.setBackground(new Color(0, 0, 0));
		}
		else
		{
			//Set background to red
			panel.setBackground(new Color(225, 0, 0));
		}
		
		setResizable(false);
		setVisible(true);
		
		panel.setLayout(null);
		
		if(idNum == 0)
		{
			drawLauncherButtons();
			drawBackground();
		}	
	
		
	   /*
	    * Try to open up the title theme audio clip
	    */
		try
		{
			if(!audioOn && !Display.pauseGame)
			{
				clip = AudioSystem.getClip();
				AudioInputStream input = AudioSystem.getAudioInputStream(this.getClass().getResource("/test/title.wav"));
				clip.open(input);
				clip.loop(Clip.LOOP_CONTINUOUSLY);
				clip.start();
				audioOn = true;
			}
		}
		catch (Exception e)
		{
			System.out.println(e);
		}
		
		
	   /*
	    * A FloatControl object is something that allows you to control 
	    * certain values of a sound object such as a clip, but in float
	    * form so that it is more accurate. The type MASTER_GAIN gets the
	    * volume level in decibals of the sound clip. So basically this
	    * just gains control of the decibal level of the sound clip.
	    */
		musicControl = (FloatControl) clip.getControl
			    		(FloatControl.Type.MASTER_GAIN);
		
	   /*
	    * Sets the value of a given control. In this case it is a 
	    * FloatControl that determines the volume level of the clip in
	    * decibals. The f after the number determines that the number
	    * is indeed a float and not a double. The highest the number 
	    * can be raised in terms of decibals is 6. The decibals can
	    * pretty much be decreased infinitely low though. 80 is usually
	    * all you need to complete get rid of the sound.
	    * 
	    * If the volume level is less than -80, just set the volume to
	    * -80. Otherwise, set the volume to the correct volume level.
	    */
		if(musicVolumeLevel < -80)
		{
			musicControl.setValue(-80.0f);
		}
		else
		{
			musicControl.setValue(musicVolumeLevel);
		}
		
		try
		{
			if (!opened) 
			{
				clickNum = 0;
				
				for (int i = 0; i < click.length; i++) {
					input = AudioSystem
							.getAudioInputStream(this
									.getClass()
									.getResource(
											"/test/titleClick.wav"));
	
					click[i] = AudioSystem.getClip();
	
					click[i].open(input);
				}
	
				opened = true;
			}
		}
		catch(Exception e)
		{
			
		}

		for(int i = 0; i < click.length; i++)
		{
		   /*
			* A FloatControl object is something that allows you to control 
			* certain values of a sound object such as a clip, but in float
			* form so that it is more accurate. The type MASTER_GAIN gets the
			* volume level in decibals of the sound clip. So basically this
			* just gains control of the decibal level of the sound clip.
			*/
			soundControl[i] = (FloatControl) click[i].getControl
						    (FloatControl.Type.MASTER_GAIN);
					
		   /*
			* Sets the value of a given control. In this case it is a 
			* FloatControl that determines the volume level of the clip in
			* decibals. The f after the number determines that the number
			* is indeed a float and not a double. The highest the number 
			* can be raised in terms of decibals is 6. The decibals can
			* pretty much be decreased infinitely low though. 80 is usually
			* all you need to complete get rid of the sound.
			* 
			* If the volume level is less than -80, just set the volume to
			* -80. Otherwise, set the volume to the correct volume level.
			*/
			if(soundVolumeLevel < -80)
			{
				soundControl[i].setValue(-80.0f);
			}
			else
			{
				soundControl[i].setValue(soundVolumeLevel);
			}
		}
	}
  
   /**
    * Draws all the buttons that show when the launcher first opens
    * so that the play can choose to play the game, or open up the
    * options menu, or the controls menu, etc...
    */
	private void drawLauncherButtons()
	{		
		play = new JButton("Survival Mode");
		
		rplay    = new Rectangle(0, 250, 295, 40);
		play.setBounds(rplay);
		
		//Button blends with background
		play.setOpaque(false);
		play.setContentAreaFilled(false);
		play.setBorderPainted(false);
		
		//Removes textbox focus so the textbox border is removed
		play.setFocusPainted(false);
		
		//Sets font type, mode, and size
		play.setFont(new Font("Nasalization", Font.BOLD, 24));
		
		//Uses a bitwise operator to merge the fonts of bold and italic
		//for the text
		play.setFont(play.getFont()
				.deriveFont(Font.BOLD | Font.ITALIC));
		
		play.setForeground(new Color(0,0,0));
		
		//Listens for whether the mouse has entered or exited the button
		//area and if it has or has not, change color accordingly
		play.addMouseListener(new java.awt.event.MouseAdapter() {
		    public void mouseEntered(java.awt.event.MouseEvent evt) {
		        play.setForeground(Color.RED);
		    }

		    public void mouseExited(java.awt.event.MouseEvent evt) {
		        play.setForeground(Color.BLACK);
		    }
		});
		
		play.addActionListener(aL);
		panel.add(play);

		playGame   = new JButton("New Game");
		rplayGame  = new Rectangle(0, 100, 250, 40);
		playGame.setBounds(rplayGame);
		
		playGame.setOpaque(false);
		playGame.setContentAreaFilled(false);
		playGame.setBorderPainted(false);
		
		//Removes textbox focus so the textbox border is removed
		playGame.setFocusPainted(false);
		
		playGame.setFont(new Font("Nasalization", Font.BOLD, 24));
		playGame.setFont(playGame.getFont()
				.deriveFont(Font.BOLD | Font.ITALIC));
		
		playGame.setForeground(new Color(0,0,0));
		
		playGame.addMouseListener(new java.awt.event.MouseAdapter() {
		    public void mouseEntered(java.awt.event.MouseEvent evt) {
		        playGame.setForeground(Color.RED);
		    }

		    public void mouseExited(java.awt.event.MouseEvent evt) {
		        playGame.setForeground(Color.BLACK);
		    }
		});
		
		playGame.addActionListener(aL);
		panel.add(playGame);
		
		returnToGame     = new JButton("Return to game");
		returnToGameRect = new Rectangle(500, 240, 300, 50);
		returnToGame.setBounds(returnToGameRect);
		
		//Button blends with background
		returnToGame.setOpaque(false);
		returnToGame.setContentAreaFilled(false);
		returnToGame.setBorderPainted(false);
		
		//Removes textbox focus so the textbox border is removed
		returnToGame.setFocusPainted(false);
		
		//Sets font type, mode, and size
		returnToGame.setFont(new Font("Nasalization", Font.BOLD, 24));
		
		returnToGame.setForeground(new Color(0,0,0));
		
		//Listens for whether the mouse has entered or exited the button
		//area and if it has or has not, change color accordingly
		returnToGame.addMouseListener(new java.awt.event.MouseAdapter() {
		    public void mouseEntered(java.awt.event.MouseEvent evt) {
		    	returnToGame.setForeground(Color.RED);
		    }

		    public void mouseExited(java.awt.event.MouseEvent evt) {
		    	returnToGame.setForeground(Color.BLACK);
		    }
		});
				
		returnToGame.addActionListener(aL);
		
		//If game is paused, show that you can return to the game
		if(Display.pauseGame)
		{
			panel.add(returnToGame);
		}
		
		options  = new JButton("Options");
		roptions = new Rectangle(0, 150, 200, 40);
		options.setBounds(roptions);
		
		//Button blends with background
		options.setOpaque(false);
		options.setContentAreaFilled(false);
		options.setBorderPainted(false);
		
		//Removes textbox focus so the textbox border is removed
		options.setFocusPainted(false);
		
		//Sets font type, mode, and size
		options.setFont(new Font("Nasalization", Font.BOLD, 24));
		
		//Uses a bitwise operator to merge the fonts of bold and italic
		//for the text
		options.setFont(options.getFont()
				.deriveFont(Font.BOLD | Font.ITALIC));
		
		options.setForeground(new Color(0,0,0));
		
		//Listens for whether the mouse has entered or exited the button
		//area and if it has or has not, change color accordingly
		options.addMouseListener(new java.awt.event.MouseAdapter() {
		    public void mouseEntered(java.awt.event.MouseEvent evt) {
		        options.setForeground(Color.RED);
		    }

		    public void mouseExited(java.awt.event.MouseEvent evt) {
		        options.setForeground(Color.BLACK);
		    }
		});
		
		options.addActionListener(aL);
		panel.add(options);
		
		controls  = new JButton("Controls");
		rcontrols = new Rectangle(0, 200, 220, 40);
		controls.setBounds(rcontrols);
		
		//Button blends with background
		controls.setOpaque(false);
		controls.setContentAreaFilled(false);
		controls.setBorderPainted(false);
		
		//Removes textbox focus so the textbox border is removed
		controls.setFocusPainted(false);
		
		//Sets font type, mode, and size
		controls.setFont(new Font("Nasalization", Font.BOLD, 24));
		
		//Uses a bitwise operator to merge the fonts of bold and italic
		//for the text
		controls.setFont(controls.getFont()
				.deriveFont(Font.BOLD | Font.ITALIC));
		
		controls.setForeground(new Color(0,0,0));
		
		//Listens for whether the mouse has entered or exited the button
		//area and if it has or has not, change color accordingly
		controls.addMouseListener(new java.awt.event.MouseAdapter() {
		    public void mouseEntered(java.awt.event.MouseEvent evt) {
		        controls.setForeground(Color.RED);
		    }

		    public void mouseExited(java.awt.event.MouseEvent evt) {
		        controls.setForeground(Color.BLACK);
		    }
		});
		
		controls.addActionListener(aL);
		panel.add(controls);
		
		quit      = new JButton("Quit");
		rquit     = new Rectangle(0, 300, 140, 40);
		quit.setBounds(rquit);
		
		//Button blends with background
		quit.setOpaque(false);
		quit.setContentAreaFilled(false);
		quit.setBorderPainted(false);
		
		//Removes textbox focus so the textbox border is removed
		quit.setFocusPainted(false);
		
		//Sets font type, mode, and size
		quit.setFont(new Font("Nasalization", Font.BOLD, 24));
		
		//Uses a bitwise operator to merge the fonts of bold and italic
		//for the text
		quit.setFont(quit.getFont()
				.deriveFont(Font.BOLD | Font.ITALIC));
		
		quit.setForeground(new Color(0,0,0));
		
		//Listens for whether the mouse has entered or exited the button
		//area and if it has or has not, change color accordingly
		quit.addMouseListener(new java.awt.event.MouseAdapter() {
		    public void mouseEntered(java.awt.event.MouseEvent evt) {
		        quit.setForeground(Color.RED);
		    }

		    public void mouseExited(java.awt.event.MouseEvent evt) {
		        quit.setForeground(Color.BLACK);
		    }
		});
		
		quit.addActionListener(aL);
		panel.add(quit);
		
		//Refreashes title screen to show all buttons and title perfectly
		panel.repaint();
	}
	
   /**
    * Draws up all the JButtons, Choice boxes, and textfields 
    * within the options menu when you pull it up.
    */
	public void drawOptionsMenu()
	{
		back1     = new JButton("Back To Main Menu");
		rback     = new Rectangle(0, 275, 300, 40);
		back1.setBounds(rback);
		
		//Button blends with background
		back1.setOpaque(false);
		back1.setContentAreaFilled(false);
		back1.setBorderPainted(false);
		
		//Removes textbox focus so the textbox border is removed
		back1.setFocusPainted(false);
		
		//Sets font type, mode, and size
		back1.setFont(new Font("Nasalization", Font.BOLD, 18));
		
		//Uses a bitwise operator to merge the fonts of bold and italic
		//for the text
		back1.setFont(back1.getFont()
				.deriveFont(Font.BOLD | Font.ITALIC));
		
		back1.setForeground(new Color(0,0,0));
		
		//Listens for whether the mouse has entered or exited the button
		//area and if it has or has not, change color accordingly
		back1.addMouseListener(new java.awt.event.MouseAdapter() {
		    public void mouseEntered(java.awt.event.MouseEvent evt) {
		        back1.setForeground(Color.RED);
		    }

		    public void mouseExited(java.awt.event.MouseEvent evt) {
		        back1.setForeground(Color.BLACK);
		    }
		});
		
		back1.addActionListener(aL);
		panel.add(back1);
		
		if(mouseStatus)
		{
			mouse     = new JButton("Mouse Status: On");
		}
		else
		{
			mouse     = new JButton("Mouse Status: Off");
		}
		
		mouseRect     = new Rectangle(300, 275, 250, 40);
		mouse.setBounds(mouseRect);
		
		//Button blends with background
		mouse.setOpaque(false);
		mouse.setContentAreaFilled(false);
		mouse.setBorderPainted(false);
		
		//Removes textbox focus so the textbox border is removed
		mouse.setFocusPainted(false);
		
		//Sets font type, mode, and size
		mouse.setFont(new Font("Nasalization", Font.BOLD, 18));
		
		//Uses a bitwise operator to merge the fonts of bold and italic
		//for the text
		mouse.setFont(mouse.getFont()
				.deriveFont(Font.BOLD | Font.ITALIC));
		
		mouse.setForeground(new Color(0,0,0));
		
		//Listens for whether the mouse has entered or exited the button
		//area and if it has or has not, change color accordingly
		mouse.addMouseListener(new java.awt.event.MouseAdapter() {
		    public void mouseEntered(java.awt.event.MouseEvent evt) {
		        mouse.setForeground(Color.RED);
		    }

		    public void mouseExited(java.awt.event.MouseEvent evt) {
		        mouse.setForeground(Color.BLACK);
		    }
		});
		
		mouse.addActionListener(aL);
		panel.add(mouse);
		
		resolutionTitle = new JLabel("Resolution:");
		resolutionTitle.setBounds(50, 25, 200, 10); 
		resolutionTitle.setForeground(Color.BLACK);
		panel.add(resolutionTitle);
		
		resRect  = new Rectangle(50, 50, 200, 40);
		resolution.setBounds(resRect);
		resolution.add("640 x 480");
		resolution.add("800 x 600");
		resolution.add("1020 x 768");
		resolution.select(Display.graphicsSelection);
		panel.add(resolution);	
		
		themeTitle = new JLabel("Themes:");
		themeTitle.setBounds(250, 100, 200, 10);
		themeTitle.setForeground(Color.BLACK);
		panel.add(themeTitle);
		
		themeRect  = new Rectangle(175, 125, 200, 40);
		theme.setBounds(themeRect);
		theme.add("Outdoors");
		theme.add("Night Time");
		theme.add("Level Default");
		theme.add("Nick Cage");
		theme.add("Moon");
		theme.add("MLG");
		theme.select(Display.themeNum);
		panel.add(theme);
		
		levelSizeTitle = new JLabel("Level Size (Survival only):");
		levelSizeTitle.setBounds(300, 25, 250, 15); 
		levelSizeTitle.setForeground(Color.BLACK);
		panel.add(levelSizeTitle);
		
		levelSizeRect  = new Rectangle(300, 50, 200, 40);
		levelSize.setBounds(levelSizeRect);
		levelSize.add("10x10");
		levelSize.add("25x25");
		levelSize.add("50x50");
		levelSize.add("100x100");
		levelSize.add("250x250");
		levelSize.add("500x500");
		levelSize.add("1000x1000");
		levelSize.select(Display.levelSize);
		panel.add(levelSize);
		
		modeTitle = new JLabel("Game Mode:");
		modeTitle.setBounds(50, 175, 250, 15); 
		modeTitle.setForeground(Color.BLACK);
		panel.add(modeTitle);
		
		modeRect  = new Rectangle(50, 200, 200, 40);
		mode.setBounds(modeRect);
		mode.add("Peaceful");
		mode.add("Go easy on me!");
		mode.add("I can handle this!");
		mode.add("Bring it on!");
		mode.add("Death cannot Touch me!");
		mode.select(Display.skillMode);
		panel.add(mode);
		
		musicTitle = new JLabel("Music Options:");
		musicTitle.setBounds(300, 175, 200, 15); 
		musicTitle.setForeground(Color.BLACK);
		panel.add(musicTitle);
		
		musicRect  = new Rectangle(300, 200, 200, 40);
		music.setBounds(musicRect);
		music.add("8 Bit Music");
		music.add("Doom heavy metal");
		music.add("Original Doom music");
		
	   /*
	    * If musicTheme is not one of the normal themes (because the 
	    * game theme is mlg or koester) then just have it select the
	    * 2nd music choice. If the music theme is one of the first
	    * 4 themes available (including music off) then have that
	    * selected as is.
	    */
		if(Display.musicTheme > 3)
		{
			music.select(2);
		}
		else
		{
			music.select(Display.musicTheme);
		}
		
		panel.add(music);
		
		musicVolumeTitle = new JLabel("Music Volume:");
		musicVolumeTitle.setBounds(550, 180, 200, 10); 
		musicVolumeTitle.setForeground(Color.BLACK);
		panel.add(musicVolumeTitle);
		
		musicVolume     = new JSlider();
		musicVolume.setBounds(550, 200, 200, 40);
		musicVolume.setMaximum(6);
		musicVolume.setMinimum(-94);
		musicVolume.setValue((int) musicVolumeLevel); 
		musicVolume.setOpaque(true);
		
		int temp  = (int) musicVolumeLevel + 94;
		musicVolume.setToolTipText("Music Volume Level: "+temp);
		
		musicVolume.addChangeListener(change);
		panel.add(musicVolume);
		
		
		soundVolumeTitle = new JLabel("Sound Volume:");
		soundVolumeTitle.setBounds(550, 80, 200, 10); 
		soundVolumeTitle.setForeground(Color.BLACK);
		panel.add(soundVolumeTitle);
		
		soundVolume     = new JSlider();
		soundVolume.setBounds(550, 100, 200, 40);
		soundVolume.setMaximum(6);
		soundVolume.setMinimum(-94);
		soundVolume.setValue((int) soundVolumeLevel); 
		soundVolume.setOpaque(true);
		
		int temp2  = (int) soundVolumeLevel + 94;
		soundVolume.setToolTipText("Sound Volume Level: "+temp2);
		
		soundVolume.addChangeListener(change);
		panel.add(soundVolume);
	}
	
   /**
    * Draw all the Textfields and JButtons that will appear and work
    * in the controls menu.
    */
	public void drawControlsButtons()
	{
		readMeText1 = new JTextField();
		readMeText1.setBounds(0, 0, WIDTH, 25);
		readMeText1.setText("Foward = W, Backwards = S, Strafe Left = A,"
				+ " Strafe Right = D, Turn left = Left arrow,"
				+ " Turn right = Right arrow, Run = Shift, Crouch = C");
		readMeText1.setEditable(false);
		panel.add(readMeText1);
		
		readMeText2 = new JTextField();
		readMeText2.setBounds(0, 25, WIDTH, 25);
		readMeText2.setText("Jump = Space, Show FPS = F,"
				+ " Look up = Up Arrow, Look down = Down arrow,"
				+ " Noclip = N, Super Speed = P,"
				+ " Reload = R, Fly mode = O");
		readMeText2.setEditable(false);
		panel.add(readMeText2);
		
		readMeText3 = new JTextField();
		readMeText3.setBounds(0, 50, WIDTH, 25);
		readMeText3.setText("God Mode = G, Shoot = V, Pause = ESC, E = Use"
				+ "    If using the mouse, moving it any direction will"
				+ " move the camera, and clicking shoots.");
		readMeText3.setEditable(false);
		panel.add(readMeText3);
		
		readMeText4 = new JTextField();
		readMeText4.setBounds(0, 75, WIDTH, 25);
		readMeText4.setText("There will eventually be a way to change"
				+ " the controls you use in game, but not yet...");
		readMeText4.setEditable(false);
		panel.add(readMeText4);
		
		back2     = new JButton("Back To Main Menu");
		rback2    = new Rectangle(300, 300, 200, 40);
		back2.setBounds(rback2);
		back2.addActionListener(aL);
		panel.add(back2);
	}
	
   /**
    * Draws background of menu. Always draws it last so that it is behind
    * all the buttons and text every time.
    */
	public void drawBackground()
	{
		title = new JLabel();
		title.setBounds(0, 0, WIDTH, HEIGHT);
		titleImage = new ImageIcon("Images/title.png");
		title.setIcon(titleImage);
		panel.add(title, new Integer(0));
		
		panel.repaint();
	}
}
