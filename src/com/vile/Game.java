package com.vile;

import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import java.util.Scanner;

import com.vile.entities.*;
import com.vile.input.Controller;
import com.vile.levelGenerator.*;

/**
 * Title: Game
 * @author Alex Byrd
 * Date Updated: 9/16/2016
 *
 * Keeps track of all the in game controls (using keyListener), also
 * keeps track of game time using the time variable and adding to it
 * in the tick method. It also sets up the random level generated each
 * time the game is ran with a given size (determined by the launcher).
 * 
 * It also calls the performActions method in controller to perform all  
 * the actions within the game that the player activates using the keys.
 * 
 * It also keeps track of all the entities in the game by having 3
 * array list that keep track of each bullet, each item) and
 * each enemy. Also keeps track of all the ammo that is placed into
 * the map for the player as well.
 */
public class Game 
{
	public static int time;
	public Controller controls;
	
	public static boolean setMap = false;
	
	public static valueHolder[][] map;
	
	public static Level level;
	public static int levelSize;
	
	public static int skillMode = 2;
	public static int mapNum   = 1;
	public static String mapName = "";
	
	public static int secretsInMap = 0;
	public static int enemiesInMap = 0;
	public static int secretsFound = 0;
	
	public static ArrayList<Enemy> enemies    
				= new ArrayList<Enemy>();
	public static ArrayList<Item> items 
				= new ArrayList<Item>();
	public static ArrayList<Bullet> bullets   
				= new ArrayList<Bullet>();
	public static ArrayList<EnemyFire> enemyProjectiles  
				= new ArrayList<EnemyFire>();
	public static ArrayList<Button> buttons   
				= new ArrayList<Button>();
	public static ArrayList<Door>  doors      
				= new ArrayList<Door>();
	public static ArrayList<Elevator> elevators 
				=  new ArrayList<Elevator>();
	public static ArrayList<HurtingBlock> hurtingBlocks 
				= new ArrayList<HurtingBlock>();
	public static ArrayList<Corpse> corpses
				= new ArrayList<Corpse>();
	public static ArrayList<ExplosiveCanister> canisters
				= new ArrayList<ExplosiveCanister>();
	public static ArrayList<Item> solidItems
				= new ArrayList<Item>();
	
	private static boolean key[];
	private int calculatedSize;
	
	private Display display;
	
	/* All Game Actions ***********************************************/
	public static boolean foward;
	public static boolean back;
	public static boolean left;
	public static boolean right;
	public static boolean turnLeft;
	public static boolean turnRight;
	public static boolean turnUp;
	public static boolean turnDown;
	public static boolean shoot;
	public static boolean pause;
	public static boolean run;
	public static boolean crouch;
	public static boolean jump;
	public static boolean fpsShow;
	public static boolean reloading;
	public static boolean noClip;
	public static boolean superSpeed;
	public static boolean fly;
	public static boolean godMode;
	public static boolean restock;
	public static boolean use;
	public static boolean weaponSlot0;
	public static boolean weaponSlot1;
	public static boolean weaponSlot2;
	/* End Game Actions ***********************************************/
	
   /**
    * Sets up the level and the Controller object. The level size is
    * determined by the Display.levelSize variable which is determined
    * by the  launcher. Also adds the initial enemy to the game and 
    * adds healthPacks for the player. The number of health packs is
    * equal to the size of the level.
    */
	public Game(Display display)
	{
		int healAmount = 15;
		
		//Set the games display object to the display sent in
		this.display = display;
		
	   /*
		* Determine amount of health a health pack will give the player
		* based on the difficulty
		*/
		if(skillMode == 0)
		{
			healAmount = 20;
		}
		else if(skillMode == 1)
		{
			healAmount = 10;
		}
		else if(skillMode == 2)
		{
			healAmount = 5;
		}
		else
		{
			healAmount = 3;
		}
		
		//Everything is reset
		enemies       = new ArrayList<Enemy>();
		items         = new ArrayList<Item>();
		bullets       = new ArrayList<Bullet>();
		enemyProjectiles = new ArrayList<EnemyFire>();
		buttons    	  = new ArrayList<Button>();
		hurtingBlocks = new ArrayList<HurtingBlock>();
		doors         = new ArrayList<Door>();
		elevators     = new ArrayList<Elevator>();
		corpses       = new ArrayList<Corpse>();
		canisters     = new ArrayList<ExplosiveCanister>();
		solidItems    = new ArrayList<Item>();
			
		if(setMap == false)
		{
			calculatedSize = 100;
			
			if(Display.levelSize == 0)
			{
				calculatedSize = 10;
			}
			else if(Display.levelSize == 1)
			{
				calculatedSize = 25;
			}
			else if(Display.levelSize == 2)
			{
				calculatedSize = 50;
			}
			else if(Display.levelSize == 3)
			{
				calculatedSize = 100;
			}
			else if(Display.levelSize == 4)
			{
				calculatedSize   = 250;
			}
			else if(Display.levelSize == 5)
			{
				calculatedSize   = 500;
			}
			else if(Display.levelSize == 6)
			{
				calculatedSize   = 1000;
			}
			else
			{
				calculatedSize   = 100;
			}
			
			levelSize = calculatedSize;
			level    = new Level(levelSize, levelSize);
			
			if(skillMode != 0)
			{
				addEnemy();
			}
			
		   /*
		    * Depending on the calculated size of the level, put that many
		    * health packs (items) into the level.
		    */
			for(int i = 0; i < 250; i++)
			{
				Random rand = new Random();
				items.add(new Item(healAmount, 
						0.5 + rand.nextInt(calculatedSize), 
						0, 0.5 + rand.nextInt(calculatedSize), 2));
			}
			
		   /*
		    * Also depending on the calculated size of the level, put that
		    * many shell cases into the level.
		    */
			for(int i = 0; i < 250; i++)
			{
				Random rand = new Random();
				
				int ammoType = rand.nextInt(3);
				
				//Place random ammo type in map
				if(ammoType == 0)
				{
					ammoType = 3;
				}
				else if(ammoType == 1)
				{
					ammoType = 50;
				}
				else if(ammoType == 2)
				{
					ammoType = 56;
				}
				
				items.add(new Item(2, 0.5 + rand.nextInt(calculatedSize), 
						0, 1.5 + rand.nextInt(calculatedSize), ammoType));
			}
			
			//Player has all weapons
			Player.weapons[0].dualWield = true;
			Player.weapons[1].canBeEquipped = true;
			Player.weapons[2].canBeEquipped = true;
			
		}
		else
		{
			mapNum = 1;
			loadNextMap();
		}

		controls = new Controller();
	}
	
   /**
    * Ticks each time the game renders, therefore keeping track of game
    * time, and all the key events that happen within the game.
    * @param key2
    */
	public void tick(boolean key2[])
	{
		time++;
		
		//Just in case you go above a billion ticks
		if(time > 1000000000)
		{
			time = 0;
		}
		
		key = key2;
		
		foward      = key[KeyEvent.VK_W];
		back        = key[KeyEvent.VK_S];
		left        = key[KeyEvent.VK_A];
		right       = key[KeyEvent.VK_D];
		turnLeft    = key[KeyEvent.VK_LEFT];
		turnRight   = key[KeyEvent.VK_RIGHT];
		pause       = key[KeyEvent.VK_ESCAPE];
		run         = key[KeyEvent.VK_SHIFT];
		crouch      = key[KeyEvent.VK_C];
		jump        = key[KeyEvent.VK_SPACE];
		fpsShow     = key[KeyEvent.VK_F];
		turnUp      = key[KeyEvent.VK_UP];
		turnDown    = key[KeyEvent.VK_DOWN];
		reloading   = key[KeyEvent.VK_R];
		noClip      = key[KeyEvent.VK_N];
		superSpeed  = key[KeyEvent.VK_P];
		fly         = key[KeyEvent.VK_O];
		godMode     = key[KeyEvent.VK_G];
		shoot       = key[KeyEvent.VK_V];
		restock     = key[KeyEvent.VK_L];
		use         = key[KeyEvent.VK_E];
		weaponSlot0 = key[KeyEvent.VK_1];
		weaponSlot1 = key[KeyEvent.VK_2];
		weaponSlot2 = key[KeyEvent.VK_3];
		
		Collections.sort(enemies);
		
		//IF a button is activated
		if(buttons.size() > 0)
		{
			for(int i = 0; i < buttons.size(); i++)
			{
				Button temp = buttons.get(i);
				
				//If an end button, load the next map
				if(temp.ID == 0 && temp.activated)
				{
					mapNum++;
					loadNextMap();
				}
				
				//If a normal button
				if(temp.ID == 1 && temp.activated)
				{
					//De-activate the button
					temp.activated = false;
					
					//Holds all the items to be deleted
					ArrayList<Item> tempItems = new ArrayList<Item>();
					
					//Scan all items
					for(int j = 0; j < items.size(); j++)
					{
						Item item = items.get(j);
						
						//If Item is a satillite dish, activate it and
						//state that it is activated
						if(item.itemID == 52)
						{
							item.activated = true;
							Display.itemPickup = "COM SYSTEM ACTIVATED!";
							Display.itemPickupTime = 1;
							Player.hasYellowKey = true;
						}
						
						//If item is enemy spawnpoint, then spawn the
						//enemy, and add the item to the arraylist of
						//items to be deleted
						if(item.itemID == 54)
						{
							tempItems.add(item);
							enemiesInMap++;
							addEnemy(item.x, item.z);
						}
					}
					
					//Delete all items that need to be deleted.
					for(Item tempItem: tempItems)
					{
						items.remove(tempItem);
					}
				}
			}
		}
		
		//Go through all toxic waste, lava, and spike blocks
		//in the game and update them.
		for(int i = 0; i < Game.hurtingBlocks.size(); i++)
		{
			HurtingBlock temp = Game.hurtingBlocks.get(i);
			
			//Only update once every tick
			if(i == 0)
			{
				HurtingBlock.time++;
			}
			
		   /*
		    * Determine whether the player is on the block or not. If
		    * the player is on the block, hurt him/her a certain 
		    * amount.
		    */
			temp.activate();
		}
		
	   /*
	    * Check all door entities each tick through the game,
	    * and if the door is activated, continue to move the
	    * door in whatever way it was moving before. Otherwise
	    * do nothing to it.
	    */
		for(int i = 0; i < Game.doors.size(); i++)
		{
			Door door = Game.doors.get(i);
			
			if(door.activated)
			{
				door.move();
			}
		}
		
		//Update all elevator entities
		for(int i = 0; i < Game.elevators.size(); i++)
		{
			Elevator elevator = Game.elevators.get(i);
			
			if(elevator.activated)
			{
				elevator.move();
			}
		}
		
		//Update players current weapon
		if(Player.weaponEquipped == 0)
		{
			((Pistol)(Player.weapons[0])).updateValues();
		}
		else if(Player.weaponEquipped == 1)
		{
			((Shotgun)(Player.weapons[1])).updateValues();
		}
		else
		{
			((PhaseCannon)(Player.weapons[2])).updateValues();
		}
		
	   /*
	    * If in Death cannot hurt me mode, the corpses will resurrect
	    * on their own after 2500 ticks.
	    */
		if(Display.skillMode == 4)
		{
			ArrayList<Corpse> temp = new ArrayList<Corpse>();
			
			for(Corpse corpse: Game.corpses)
			{
				corpse.tick();
				
				if(corpse.time > 2500)
				{
				   /*
				    * Reconstruct enemy depending on the ID the corpse
				    * was before it died. Also activate the new resurrected enemy.
				    */
					Enemy newEnemy = new Enemy(corpse.x, 0, corpse.z,
							corpse.enemyID);
					
					newEnemy.activated = true;
					
					Game.enemies.add(newEnemy);
					
					temp.add(corpse);
					
					Game.enemiesInMap++;
				}
				
			}
			
			//Every corpse that is resurrected, remove from the
			//corpses list
			for(Corpse corpse: temp)
			{
				Game.corpses.remove(corpse);
			}
		}
		
		//Perform all actions depending on keys pressed.
		controls.performActions(this);
	}
	
   /**
    * Adds a bullet to the game starting at the players x, y and z 
    * position. This entity is used to hit enemies and hurt them.
    */
	public static void addBullet(int damage, int ID, double speed, 
			double rotation)
	{
		bullets.add(new Bullet(damage, speed, Player.x,
				((-Player.y) * 0.085), Player.z, ID, rotation));
	}
	
   /**
    * Adds a random enemy to the game if the map is random, and places
    * it in a random location within the map, depending on the maps 
    * size.
    */
	public void addEnemy()
	{
		Random rand  = new Random();
		int yPos     = 0;
		int ID       = 0;

		
		int randomNum = rand.nextInt(100);
		
	   /*
	    * Spawns a random type of enemy of the 4 types
	    */
		if(randomNum <= 20)
		{
			//Normal enemy, don't change anything but ID
			ID = 1;
		}
		else if(randomNum <= 40)
		{
			//The flyerdemon
			yPos   = -5;
			ID     = 2;
		}
		else if(randomNum <= 50)
		{
			//3rd type of enemy, the crocGaurdian
			ID      = 3;
		}
		//Reaper enemy
		else if(randomNum <= 70)
		{
			ID      = 4;
		}
		//Vile Civilian
		else if(randomNum <= 98)
		{
			ID      = 7;
		}
		//Resurrector
		else
		{
			ID      = 5;
		}
		
		
		enemies.add(new Enemy( 
				0.5 + rand.nextInt(calculatedSize), yPos,
				0.5 + rand.nextInt(calculatedSize), ID));
	}
	
	/**
    * Adds a random enemy to the game to a designated place in
    * the map that is sent in through the parameters.
    */
	public void addEnemy(double x, double z)
	{
		Random rand  = new Random();
		int yPos     = 0;
		int ID       = 0;

		
		int randomNum = rand.nextInt(100);
		
	   /*
	    * Spawns a random type of enemy of the 4 types
	    */
		if(randomNum <= 24)
		{
			//Normal enemy, don't change anything but ID
			ID = 1;
		}
		else if(randomNum <= 49)
		{
			//The flyerdemon
			yPos   = -5;
			ID     = 2;
		}
		else if(randomNum <= 74)
		{
			//3rd type of enemy, the crocGaurdian
			ID      = 3;
		}
		//Reaper enemy
		else if(randomNum <= 98)
		{
			ID      = 4;
		}
		else
		{
			ID      = 5;
		}
		
		
		enemies.add(new Enemy( 
				x, yPos,
				z, ID));
	}
	
   /**
    * Loads the next map up for the game, and sets up the new level and
    * the positions of the entities.
    */
	public void loadNextMap()
	{
	   /*
	    * The player does not keep keys from level to level.
	    */
		Player.hasRedKey = false;
		Player.hasBlueKey = false;
		Player.hasGreenKey = false;
		Player.hasYellowKey = false;
		Player.immortality = 0;
		Player.environProtectionTime = 0;
		
	   /*
	    * Every map starts with 0 enemies, 0 secrets, and no secrets
	    * found on default.
	    */
		secretsInMap = 0;
		secretsFound = 0;
		enemiesInMap = 0;
		
		//Player has no kills on the map when the map is first started
		Display.kills = 0;
		
		//Try and read the file correctly, if not catch exception
		try
		{
			String temp = "";
			int row = 0;
			int col = 0;
			
			enemies       = new ArrayList<Enemy>();
			items         = new ArrayList<Item>();
			bullets       = new ArrayList<Bullet>();
			buttons    	  = new ArrayList<Button>();
			hurtingBlocks = new ArrayList<HurtingBlock>();
			doors         = new ArrayList<Door>();
			elevators     = new ArrayList<Elevator>();
			corpses       = new ArrayList<Corpse>();
			canisters     = new ArrayList<ExplosiveCanister>();
			solidItems    = new ArrayList<Item>();
			enemyProjectiles = new ArrayList<EnemyFire>();
			
			//Sets up the file reader that will read the file
			Scanner sc = new Scanner(new BufferedReader
				(new FileReader("map"+mapNum+".txt")));
			
			//The very first part of any map file now is the name
			mapName = sc.nextLine();
			
		   /*
		    * While the whole file has not been read, keep scanning each 
		    * line of the file into string format and adding it to the
		    * temporary string.
		    */
			while(sc.hasNext())
			{
				temp += sc.next();
			}
			
		   /*
		    * At each point there is a , in the string, split that up
		    * as a seperate string and add it as a new slot in the array
		    * of strings called splitedParts
		    */
			String[] splitedParts = temp.split(",");
			
		   /*
		    * Because apparently map has to be instantiated correctly with
		    * a size that will fit all the walls into it, therefore I
		    * instantiated it with a size in both rows and columns that may
		    * be too big, but will fit all the walls and entities in the
		    * map for sure.
		    */
			valueHolder[][] tempMap = 
					new valueHolder[splitedParts.length][splitedParts.length];
			
		   /*
		    * Stores the actual number of cloumns the 2D array of the map
		    * has so that the 2D array that "map" is instantiated with is
		    * the correct size.
		    */
			int storeColumn = 0;
			
		   /*
		    * Will stop adding to storeColumn when set to true. After one
		    * row of columns has been counted, the number is stored, and
		    * the counting needs to stop.
		    */
			boolean stop = false;
			
		   /*
		    * For each slot of the splited parts array, convert the
		    * part into an actual integer using the parseInt method, 
		    * then put that int into that slot of the 2D array map.
		    */
			for(int i = 0; i < splitedParts.length; i++)
			{
				String[] blockValues = splitedParts[i].split(":");

				double height = Integer.parseInt(blockValues[0]);
				int wallID = Integer.parseInt(blockValues[1]);
				int entityID = Integer.parseInt(blockValues[2]);
				
				if(wallID == 100)
				{
					row++;
					col = 0;
					stop = true;
				}
				else
				{
					tempMap[row][col] = 
							new valueHolder(height, wallID, entityID);
					col++;
					
					if(!stop)
					{
						storeColumn++;
					}
				}
			}
			
		   /*
		    * For now, all maps are made square in the text file so that
		    * the program works every time with these values. So the
		    * actual map loaded will be its correct size and not too
		    * small or large based on limitations that are manually set.
		    */
			map = new valueHolder[row][storeColumn];
			
		   /*
		    * Puts the values that actually matter in the temp map into
		    * the actual map loaded.
		    */
			for(int i = 0; i < map.length; i++)
			{
				for(int j = 0; j < map[0].length; j++)
				{
					map[i][j] = tempMap[i][j];
				}
			}

		   /*
		    * Figures out how many blocks in both the x and z
		    * direction that the game needs to render. It uses the 
		    * farthest direction as the amount to render in every
		    * direction
		    */
			if(map.length > map[0].length)
			{
				levelSize = map.length;
			}
			else
			{
				levelSize = map[0].length;
			}
			
			//Constructs the new level
			level    = new Level(map);
			
			//Calculates the size of the new level
			calculatedSize = map.length * map[0].length;
			
			display.resetMusic();
			
			//Close the scanner
			sc.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
}
