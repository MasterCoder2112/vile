package com.vile;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.io.File;
import java.util.Collections;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.FloatControl;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.vile.Game;
import com.vile.entities.Bullet;
import com.vile.entities.Corpse;
import com.vile.entities.Door;
import com.vile.entities.Enemy;
import com.vile.entities.EnemyFire;
import com.vile.entities.ExplosiveCanister;
import com.vile.entities.Item;
import com.vile.entities.Player;
import com.vile.entities.Weapon;
//Imports files within mineFront package holding Render and Screen Objects
import com.vile.graphics.Render3D;
import com.vile.graphics.Screen;
import com.vile.graphics.Textures;
import com.vile.input.Controller;
import com.vile.input.InputHandler;
import com.vile.launcher.FPSLauncher;
import com.vile.levelGenerator.Block;

/**
 * Title: Display
 * 
 * @author Alex Byrd 
 * Date Updated: 9/23/2016
 *
 * Runs the Game initially, displays everything on the screen, keeps
 * track of fps, and renders the screen. Also creates thread to keep
 * track of all events and such.
 */
public class Display extends Canvas implements Runnable 
{
	private static final long serialVersionUID = 1L;

	private static double versionNumber = 1.5;
	public static int HEIGHT   = 600;
	public static int WIDTH    = 800;
	public static int clipSize = 10;
	public static final String TITLE = "Vile Alpha " + versionNumber
			+" dev 2";

	// Is music audio on?
	public static boolean audioOn = false;

	// Audio clip that is loaded in for music
	private static Clip music;

	// The enemy is killed clip that plays
	private static Clip[] kill = new Clip[clipSize];

	// Clip that is played when the player picks up a health pack
	private static Clip[] health = new Clip[clipSize];

	// When the player picks up an ammo clip
	private static Clip[] clip = new Clip[clipSize];
	
	private static Clip[] bossDeath = new Clip[1];
	private static Clip[] barrelExplosion = new Clip[clipSize];
	private static Clip[] secret = new Clip[clipSize];
	private static Clip[] megaPickUp = new Clip[clipSize];
	private static Clip[] keyPickUp = new Clip[clipSize];
	private static Clip[] weaponPickUp = new Clip[clipSize];
	private static Clip[] playerDeath = new Clip[1];
	
	//These clips are public because they are used in Controller
	public static Clip[] reload  = new Clip[1];
	public static Clip[] ammoOut = new Clip[clipSize];
	public static Clip[] shoot   = new Clip[clipSize];
	public static Clip[] phaseShot = new Clip[clipSize];
	public static Clip[] tryToUse = new Clip[clipSize];
	public static Clip[] buttonPress = new Clip[clipSize];
	
	// If player is injured by an enemy
	public static Clip[] playerHurt = new Clip[1];
	
	//This is public because it used by the door and lift classes
	public static Clip[] lifting = new Clip[clipSize];
	
	// When the enemy is hit by the players bullet
	public static Clip[] enemyHit = new Clip[clipSize];
	
	public static Clip[] bossHit = new Clip[clipSize];
	public static Clip[] bossActivate = new Clip[clipSize];
	public static Clip[] enemyActivate = new Clip[clipSize];
	public static Clip[] pistol = new Clip[clipSize];
	public static Clip[] enemy3Activate = new Clip[clipSize];
	public static Clip[] enemy4Activate = new Clip[clipSize];
	public static Clip[] enemy5Activate = new Clip[clipSize];
	public static Clip[] enemy7Activate = new Clip[clipSize];
	public static Clip[] enemyFire = new Clip[clipSize];
	public static Clip[] enemyFireHit = new Clip[clipSize];
	public static Clip[] reaperHurt = new Clip[clipSize];
	public static Clip[] vileCivHurt = new Clip[clipSize];
	public static Clip[] tankHurt = new Clip[clipSize];
	public static Clip[] wallHit = new Clip[clipSize];
	
	//Keeps track of the gain controller for all the sound clips
	private static FloatControl[] soundControl1 =  new FloatControl[enemyHit.length];
	private static FloatControl[] soundControl2 =  new FloatControl[kill.length];
	private static FloatControl[] soundControl3 =  new FloatControl[health.length];
	private static FloatControl[] soundControl4 =  new FloatControl[clip.length];
	private static FloatControl[] soundControl5 =  new FloatControl[playerHurt.length];
	private static FloatControl[] soundControl6 =  new FloatControl[shoot.length];
	private static FloatControl[] soundControl7 =  new FloatControl[ammoOut.length];
	private static FloatControl[] soundControl8 =  new FloatControl[reload.length];
	private static FloatControl[] soundControl9 =  new FloatControl[tryToUse.length];
	private static FloatControl[] soundControl10 =  new FloatControl[buttonPress.length];
	private static FloatControl[] soundControl11 =  new FloatControl[lifting.length];
	private static FloatControl[] soundControl12 =  new FloatControl[secret.length];
	private static FloatControl[] soundControl13 =  new FloatControl[megaPickUp.length];
	private static FloatControl[] soundControl14 =  new FloatControl[wallHit.length];
	private static FloatControl[] soundControl15 =  new FloatControl[keyPickUp.length];
	private static FloatControl[] soundControl16 =  new FloatControl[weaponPickUp.length];
	private static FloatControl[] soundControl17 =  new FloatControl[playerDeath.length];
	private static FloatControl[] soundControl18 =  new FloatControl[bossHit.length];
	private static FloatControl[] soundControl19 =  new FloatControl[bossActivate.length];
	private static FloatControl[] soundControl20 =  new FloatControl[enemyActivate.length];
	private static FloatControl[] soundControl21 =  new FloatControl[bossDeath.length];
	private static FloatControl[] soundControl22 =  new FloatControl[phaseShot.length];
	private static FloatControl[] soundControl23 =  new FloatControl[barrelExplosion.length];
	private static FloatControl[] soundControl24 =  new FloatControl[pistol.length];
	private static FloatControl[] soundControl25 =  new FloatControl[enemy3Activate.length];
	private static FloatControl[] soundControl26 =  new FloatControl[enemy4Activate.length];
	private static FloatControl[] soundControl27 =  new FloatControl[enemy5Activate.length];
	private static FloatControl[] soundControl28 =  new FloatControl[enemy7Activate.length];
	private static FloatControl[] soundControl29 =  new FloatControl[enemyFire.length];
	private static FloatControl[] soundControl30 =  new FloatControl[enemyFireHit.length];
	private static FloatControl[] soundControl31 =  new FloatControl[reaperHurt.length];
	private static FloatControl[] soundControl32 =  new FloatControl[vileCivHurt.length];
	private static FloatControl[] soundControl33 =  new FloatControl[tankHurt.length];
	
	//Keeps track of gain controller for the music
	public static FloatControl musicControl;
	
	// Keep track of index numbers of clip arrays
	private static int clipNum2;
	private static int clipNum3;
	private static int clipNum4;
	private static int clipNum6;
	private static int clipNum7;
	private static int clipNum9;
	private static int clipNum10;
	private static int clipNum11;
	private static int clipNum15;
	private static int clipNum16;
	
	public static int clipNum;
	public static int clipNum5;
	public static int clipNum8;
	public static int clipNum12;
	public static int clipNum13;
	public static int clipNum14;
	public static int clipNum17;
	public static int clipNum18;
	public static int clipNum19;
	public static int clipNum20;
	public static int clipNum21;
	public static int clipNum22;
	public static int clipNum23;
	public static int clipNum24;
	public static int clipNum25;
	

	public static int mouseSpeedHorizontal;
	public static int mouseSpeedVertical;

	// Thread of events
	private static Thread thread;

	// Determines if program is running
	private boolean isRunning = false;

	// Determines whether the game is paused
	public static boolean pauseGame = false;

	// New screen object
	private Screen screen;

	// The BufferedImage that is displayed
	private BufferedImage img;

	// Creates a new game (Handles all key events and ticks)
	public static Game game;

	// Handles input events
	private InputHandler input;

	/*
	 * Used in determining the direction the mouse has moved
	 */
	public static int newX;
	public static int oldX;
	public static int newY;
	public static int oldY;

	// Whether the audio clips have been opened or not yet
	private static boolean opened = false;

	// The audio input streams for the clips
	private static AudioInputStream input2;
	private static AudioInputStream input3;
	private static AudioInputStream input4;	
	private static AudioInputStream input12;
	private static AudioInputStream input13;
	private static AudioInputStream input15;
	private static AudioInputStream input16;
	private static AudioInputStream input17;	
	private static AudioInputStream input22;
	private static AudioInputStream input24;
	
	//Used elsewhere outside class, I've kinda lost track
	public static AudioInputStream input1;
	public static AudioInputStream input5;
	public static AudioInputStream input14;
	public static AudioInputStream input19;
	public static AudioInputStream input20;
	public static AudioInputStream input21;
	public static AudioInputStream input26;
	public static AudioInputStream input27;
	public static AudioInputStream input28;
	public static AudioInputStream input29;
	public static AudioInputStream input30;
	public static AudioInputStream input31;
	public static AudioInputStream input32;
	public static AudioInputStream input33;
	public static AudioInputStream input34;
	
	//These are public because theyre used in Controller
	public static AudioInputStream input7;
	public static AudioInputStream input8;
	public static AudioInputStream input9;
	public static AudioInputStream input10;
	public static AudioInputStream input11;
	
	//Used in weapons
	public static AudioInputStream input23;
	public static AudioInputStream input6;
	public static AudioInputStream input25;
	
	//Music input stream
	private static AudioInputStream input18;

	// Used to display frames per second
	public static int fps = 0;

	// How many kills the player has
	public static int kills = 0;

	/*
	 * Gotten from the options menu to make changes in the game
	 */
	public static int graphicsSelection = 1;
	public static int themeNum          = 0;
	public static int levelSize         = 3;
	public static boolean mouseOn       = true;
	public static int skillMode         = 2;
	
	//Current music theme
	public static int musicTheme = 2;
	
	//Last music theme
	public static int oldMusicTheme = 2;
	
	private BufferStrategy bs;
	public static boolean resetGame = false;
	
	//Continues to change the face direction and look.
	private int facePhase = 0;
	
   /*
    * All different HUD images to load up
    */
	private static BufferedImage yellowKey;
	private static BufferedImage redKey;
	private static BufferedImage blueKey;
	private static BufferedImage greenKey;
	private static BufferedImage HUD;
	private static BufferedImage healthyFace1;
	private static BufferedImage healthyFace2;
	private static BufferedImage healthyFace3;
	private static BufferedImage hurtFace1;
	private static BufferedImage hurtFace2;
	private static BufferedImage hurtFace3;
	private static BufferedImage veryHurtFace1;
	private static BufferedImage veryHurtFace2;
	private static BufferedImage veryHurtFace3;
	private static BufferedImage almostDead1;
	private static BufferedImage almostDead2;
	private static BufferedImage almostDead3;
	private static BufferedImage dead;
	private static BufferedImage godMode;
	private static BufferedImage gunNormal;
	private static BufferedImage gunShot;
	private static BufferedImage gunShot2;
	private static BufferedImage gunShot3;
	private static BufferedImage gunShot4;
	private static BufferedImage phaseCannon1;
	private static BufferedImage phaseCannon2;
	private static BufferedImage phaseCannon3;
	private static BufferedImage phaseCannon4;
	private static BufferedImage pistolLeft1;
	private static BufferedImage pistolLeft2;
	private static BufferedImage pistolLeft3;
	private static BufferedImage pistolLeft4;
	private static BufferedImage pistolLeft5;
	private static BufferedImage pistolRight1;
	private static BufferedImage pistolRight2;
	private static BufferedImage pistolRight3;
	private static BufferedImage pistolRight4;
	private static BufferedImage pistolRight5;
	
   /*
    * Sets up a string that can change and disappears after 100 ticks
    * about the most recent pick up you got, or if you are trying to
    * open a door that requires a key you don't have.
    */
	public static String itemPickup = "";
	public static int itemPickupTime = 0;
	
	private int ticks = 0;

	/**
	 * Creates a new display by setting up the screen and its dimensions, sets
	 * up the image the screen displays, how the pixels will render when set,
	 * and adds all the input stuff so that the program will read input from the
	 * mouse, keyboard, and can set a focus on them if needed.
	 */
	public Display() 
	{
		JFrame loading = new JFrame("Loading Variables... 0% Loaded");
		loading.setSize(WIDTH, HEIGHT);
		loading.setLocationRelativeTo(null);
		loading.setResizable(false);
		loading.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		loading.setVisible(true);
		
		//Resets mouse pointer values
		newX = 0;
		oldX = 0;
		newY = 0;
		oldY = 0;
		
		// Keep track of index numbers of clip arrays
		clipNum = 0;
		clipNum2 = 0;
		clipNum3 = 0;
		clipNum4 = 0;
		
		//public because also used in HurtingBlock
		clipNum5 = 0;
		
		clipNum6 = 0;
		clipNum7 = 0;
		clipNum8 = 0;
		clipNum9 = 0;
		clipNum10 = 0;
		clipNum11 = 0;
		
		clipNum12 = 0;
		clipNum13 = 0;
		clipNum14 = 0;
		clipNum15 = 0;
		clipNum16 = 0;
		clipNum17 = 0;
		clipNum18 = 0;
		clipNum19 = 0;
		clipNum20 = 0;
		
		Controller.clipNum = 0;
		Controller.clipNum2 = 0;
		Controller.clipNum3 = 0;
		Controller.clipNum4 = 0;
		Controller.clipNum5 = 0;
		
		// The game is not ending yet
		Controller.quitGame = false;
		
		Display.pauseGame = false;

		//Only resets game values if the game needs to be restarted
		if(!Display.resetGame)
		{
			loading.setTitle("Loading graphics... 5% Loaded");
			//Try to load all the HUD images. If not catch the exception
			try
			{
				loadHUD();
			}
			catch(Exception e)
			{
				
			}
			
			loading.setTitle("Setting up game entities... 15% Loaded");
			
			//If new game, reset the gameMode
			Game.skillMode = Display.skillMode;
			
			// Resets player values (health, ammo, etc...)
			new Player();
			
			// New Game
			game = new Game(this);
			
			// Reset debug values
			Controller.showFPS = false;
			Controller.flyOn = false;
			Controller.noClipOn = false;
			Controller.superSpeedOn = false;
			Controller.godModeOn = false;
			
			//Reset kills
			kills = 0;
			
			loading.setTitle("Setting up game audio... 25% Loaded");
	       /*
	        * If audio is already on from a previous game, close it
	        * so that the new audio can run.
	        */
			if(audioOn)
			{
				music.close();
			}
			
			//Music is not on
			audioOn = false;
			
			//If mlg mode, change theme music to mlg theme
			if(themeNum == 5)
			{
				musicTheme = 3;
			}
			
		   /*
		    * When there is a new game, there is no previous music theme
		    * so oldMusicTheme is set equal to the current music theme.
		    */
			oldMusicTheme = musicTheme;
			
		   /*
		    * If the audio clips have not been opened yet (Meaning this is
		    * the first New Game) then open them all.
		    */
			if(!opened)
			{
				try
				{
					for (int i = 0; i < clipSize; i++) 
					{
						//Sets and opens the enemy hit sound clip
						input1 = AudioSystem
								.getAudioInputStream(this
										.getClass()
										.getResource(
												"/test/enemyHit.wav"));

						enemyHit[i] = AudioSystem.getClip();

						enemyHit[i].open(input1);
						///////////////////////////////////
						
						//Sets and opens enemy kill sound clip
						input2 = AudioSystem
								.getAudioInputStream(this
										.getClass()
										.getResource(
												"/test/enemyDeath.wav"));
	
						kill[i] = AudioSystem.getClip();
	
						kill[i].open(input2);
						///////////////////////////////////////
						
						//Sets and opens health kit sound clip
						input3 = AudioSystem
								.getAudioInputStream(this
										.getClass().getResource(
												"/test/health.wav"));
	
						health[i] = AudioSystem.getClip();
	
						health[i].open(input3);
						///////////////////////////////////////
						
						//Sets and opens picking up a shell clip
						input4 = AudioSystem
								.getAudioInputStream(this
										.getClass().getResource(
												"/test/shell.wav"));
	
						clip[i] = AudioSystem.getClip();
	
						clip[i].open(input4);
						///////////////////////////////////////
						
						//Sets and opens the player hurt sound clip
						input5 = AudioSystem
								.getAudioInputStream(this
										.getClass()
										.getResource(
												"/test/playerHit.wav"));

						playerHurt[0] = AudioSystem.getClip();

						playerHurt[0].open(input5);
						
						///////////////////////////////////////
						
						//Sets and opens the shooting sound clip
						input6 = AudioSystem.getAudioInputStream
								(this.getClass().getResource
										("/test/shot.wav"));
						
						shoot[i] = AudioSystem.getClip();
					
						shoot[i].open(input6);
						///////////////////////////////////////
						
						//Sets and opens the out of ammo sound clip
						input7 = AudioSystem.getAudioInputStream
								(this.getClass().getResource
										("/test/ammoOut.wav"));
						
						ammoOut[i] = AudioSystem.getClip();
					
						ammoOut[i].open(input7);
						///////////////////////////////////////
						
						//Sets and opens the reloading sound clip
						input8 = AudioSystem.getAudioInputStream
								(this.getClass().getResource
										("/test/reload.wav"));
						
						reload[0] = AudioSystem.getClip();
					
						reload[0].open(input8);
						///////////////////////////////////////
						
						//Sets and opens the reloading sound clip
						input9 = AudioSystem.getAudioInputStream
								(this.getClass().getResource
										("/test/oomf.wav"));
						
						tryToUse[i] = AudioSystem.getClip();
					
						tryToUse[i].open(input9);
						///////////////////////////////////////
						
						//Sets and opens the reloading sound clip
						input10 = AudioSystem.getAudioInputStream
								(this.getClass().getResource
										("/test/endSwitch.wav"));
						
						buttonPress[i] = AudioSystem.getClip();
					
						buttonPress[i].open(input10);
						///////////////////////////////////////
						
						//Sets and opens the reloading sound clip
						input11 = AudioSystem.getAudioInputStream
								(this.getClass().getResource
										("/test/wallMove.wav"));
						
						lifting[i] = AudioSystem.getClip();
					
						lifting[i].open(input11);
						///////////////////////////////////////
						
						//Sets and opens the reloading sound clip
						input12 = AudioSystem.getAudioInputStream
								(this.getClass().getResource
										("/test/secretFound.wav"));
						
						secret[i] = AudioSystem.getClip();
					
						secret[i].open(input12);
						///////////////////////////////////////
						
						//Sets and opens the reloading sound clip
						input13 = AudioSystem.getAudioInputStream
								(this.getClass().getResource
										("/test/megaPickUp.wav"));
						
						megaPickUp[i] = AudioSystem.getClip();
					
						megaPickUp[i].open(input13);
						///////////////////////////////////////
						
						//Sets and opens the reloading sound clip
						input14 = AudioSystem.getAudioInputStream
								(this.getClass().getResource
										("/test/wallHit.wav"));
						
						wallHit[i] = AudioSystem.getClip();
					
						wallHit[i].open(input14);
						///////////////////////////////////////
						
						//Sets and opens the reloading sound clip
						input15 = AudioSystem.getAudioInputStream
								(this.getClass().getResource
										("/test/keyPickUp.wav"));
						
						keyPickUp[i] = AudioSystem.getClip();
					
						keyPickUp[i].open(input15);
						///////////////////////////////////////
						
						//Sets and opens the reloading sound clip
						input16 = AudioSystem.getAudioInputStream
								(this.getClass().getResource
										("/test/weaponPickUp.wav"));
						
						weaponPickUp[i] = AudioSystem.getClip();
					
						weaponPickUp[i].open(input16);
						///////////////////////////////////////
						
						//Sets and opens the reloading sound clip
						input17 = AudioSystem.getAudioInputStream
								(this.getClass().getResource
										("/test/playerDeath.wav"));
						
						playerDeath[0] = AudioSystem.getClip();
					
						playerDeath[0].open(input17);
						///////////////////////////////////////
						
						//Sets and opens the bossHit sound clip
						input19 = AudioSystem.getAudioInputStream
								(this.getClass().getResource
										("/test/bossHit.wav"));
						
						bossHit[i] = AudioSystem.getClip();
					
						bossHit[i].open(input19);
						///////////////////////////////////////
						
						//Sets and opens the bossActivate sound clip
						input20 = AudioSystem.getAudioInputStream
								(this.getClass().getResource
										("/test/bossActivate.wav"));
						
						bossActivate[i] = AudioSystem.getClip();
					
						bossActivate[i].open(input20);
						///////////////////////////////////////
						
						//Sets and opens the enemyActivate sound clip
						input21 = AudioSystem.getAudioInputStream
								(this.getClass().getResource
										("/test/enemyActivate.wav"));
						
						enemyActivate[i] = AudioSystem.getClip();
					
						enemyActivate[i].open(input21);
						///////////////////////////////////////
						
						//Sets and opens the bossDeath sound clip
						input22 = AudioSystem.getAudioInputStream
								(this.getClass().getResource
										("/test/bossDeath.wav"));
						
						bossDeath[0] = AudioSystem.getClip();
					
						bossDeath[0].open(input22);
						
						opened = true;
						///////////////////////////////////////
						
						//Sets and opens the phaseShot sound clip
						input23 = AudioSystem.getAudioInputStream
								(this.getClass().getResource
										("/test/phaseShot.wav"));
						
						phaseShot[i] = AudioSystem.getClip();
					
						phaseShot[i].open(input23);
						///////////////////////////////////////
						
						//Sets and opens the barrel Explosion sound clip
						input24 = AudioSystem.getAudioInputStream
								(this.getClass().getResource
										("/test/barrelexplosion.wav"));
						
						barrelExplosion[i] = AudioSystem.getClip();
					
						barrelExplosion[i].open(input24);
						///////////////////////////////////////
						
						//Sets and opens the pistol sound clip
						input25 = AudioSystem.getAudioInputStream
								(this.getClass().getResource
										("/test/pistol.wav"));
						
						pistol[i] = AudioSystem.getClip();
					
						pistol[i].open(input25);	
						///////////////////////////////////////
						
						//Sets and opens the enemy3Activate sound clip
						input26 = AudioSystem.getAudioInputStream
								(this.getClass().getResource
										("/test/enemy3Activate.wav"));
						
						enemy3Activate[i] = AudioSystem.getClip();
					
						enemy3Activate[i].open(input26);
						///////////////////////////////////////
						
						//Sets and opens the enemy4Activate sound clip
						input27 = AudioSystem.getAudioInputStream
								(this.getClass().getResource
										("/test/enemy4Activate.wav"));
						
						enemy4Activate[i] = AudioSystem.getClip();
					
						enemy4Activate[i].open(input27);
						///////////////////////////////////////
						
						//Sets and opens the enemy5Activate sound clip
						input28 = AudioSystem.getAudioInputStream
								(this.getClass().getResource
										("/test/enemy5Activate.wav"));
						
						enemy5Activate[i] = AudioSystem.getClip();
					
						enemy5Activate[i].open(input28);
						///////////////////////////////////////
						
						//Sets and opens the enemy7Activate sound clip
						input29 = AudioSystem.getAudioInputStream
								(this.getClass().getResource
										("/test/enemy7Activate.wav"));
						
						enemy7Activate[i] = AudioSystem.getClip();
					
						enemy7Activate[i].open(input29);
						///////////////////////////////////////
						
						//Sets and opens the enemyFire sound clip
						input30 = AudioSystem.getAudioInputStream
								(this.getClass().getResource
										("/test/enemyFire.wav"));
						
						enemyFire[i] = AudioSystem.getClip();
					
						enemyFire[i].open(input30);
						///////////////////////////////////////
						
						//Sets and opens the enemyFireHit sound clip
						input31 = AudioSystem.getAudioInputStream
								(this.getClass().getResource
										("/test/fireballHit.wav"));
						
						enemyFireHit[i] = AudioSystem.getClip();
					
						enemyFireHit[i].open(input31);
						///////////////////////////////////////
						
						//Sets and opens the reaperHurt sound clip
						input32 = AudioSystem.getAudioInputStream
								(this.getClass().getResource
										("/test/reaperHarm.wav"));
						
						reaperHurt[i] = AudioSystem.getClip();
					
						reaperHurt[i].open(input32);
						///////////////////////////////////////
						
						//Sets and opens the enemyFireHit sound clip
						input33 = AudioSystem.getAudioInputStream
								(this.getClass().getResource
										("/test/defaultHurt.wav"));
						
						vileCivHurt[i] = AudioSystem.getClip();
					
						vileCivHurt[i].open(input33);	
						///////////////////////////////////////
						
						//Sets and opens the enemyFireHit sound clip
						input34 = AudioSystem.getAudioInputStream
								(this.getClass().getResource
										("/test/tankHurt.wav"));
						
						tankHurt[i] = AudioSystem.getClip();
					
						tankHurt[i].open(input34);
						
					}
				}
				catch (Exception e)
				{
					
				}
			}
				
			//Only do if there is music playing
			if(audioOn)
			{
				   /*
				    * If the music volume is as lower than it can go (Meaning off) 
				    * then just set it to the limit. Otherwise just set the
				    * music volume to the correct level.
				    */
					if(FPSLauncher.musicVolumeLevel < -80)
					{
						musicControl.setValue(-80.0f);
					}
					else
					{
						musicControl.setValue(FPSLauncher.musicVolumeLevel);
					}
			}
			
		   /*
		    * First set the FloatControl arrays so that each slot of the
		    * array takes care of each sound clip in the sound clip
		    * arrays. Make sure the control type is Master gain so that
		    * it controls volume.
		    * 
			* Then for each sound clip in each array 
			* (clip size is the size all the sound clip arrays) 
			* set its new value depending on the value of the 
			* volume knob in the options menu of the
			* launcher.
			*/
			for(int i = 0; i < clipSize; i++)
			{
				soundControl1[i] = (FloatControl) enemyHit[i].getControl
						(FloatControl.Type.MASTER_GAIN);
				
				soundControl2[i] = (FloatControl) kill[i].getControl
						(FloatControl.Type.MASTER_GAIN);
				
				soundControl3[i] = (FloatControl) health[i].getControl
						(FloatControl.Type.MASTER_GAIN);
				
				soundControl4[i] = (FloatControl) clip[i].getControl
						(FloatControl.Type.MASTER_GAIN);
				
				soundControl5[0] = (FloatControl) playerHurt[0].getControl
						(FloatControl.Type.MASTER_GAIN);
				
				soundControl6[i] = (FloatControl) shoot[i].getControl
						(FloatControl.Type.MASTER_GAIN);
				
				soundControl7[i] = (FloatControl) ammoOut[i].getControl
						(FloatControl.Type.MASTER_GAIN);
				
				soundControl8[0] = (FloatControl) reload[0].getControl
						(FloatControl.Type.MASTER_GAIN);
				
				soundControl9[i] = (FloatControl) tryToUse[i].getControl
						(FloatControl.Type.MASTER_GAIN);
				
				soundControl10[i] = (FloatControl) buttonPress[i].getControl
						(FloatControl.Type.MASTER_GAIN);
				
				soundControl11[i] = (FloatControl) lifting[i].getControl
						(FloatControl.Type.MASTER_GAIN);
				
				soundControl12[i] = (FloatControl) secret[i].getControl
						(FloatControl.Type.MASTER_GAIN);
				
				soundControl13[i] = (FloatControl) megaPickUp[i].getControl
						(FloatControl.Type.MASTER_GAIN);
				
				soundControl14[i] = (FloatControl) wallHit[i].getControl
						(FloatControl.Type.MASTER_GAIN);
				
				soundControl15[i] = (FloatControl) keyPickUp[i].getControl
						(FloatControl.Type.MASTER_GAIN);
				
				soundControl16[i] = (FloatControl) weaponPickUp[i].getControl
						(FloatControl.Type.MASTER_GAIN);
				
				soundControl17[0] = (FloatControl) playerDeath[0].getControl
						(FloatControl.Type.MASTER_GAIN);
				
				soundControl18[i] = (FloatControl) bossHit[i].getControl
						(FloatControl.Type.MASTER_GAIN);
				
				soundControl19[i] = (FloatControl) bossActivate[i].getControl
						(FloatControl.Type.MASTER_GAIN);
				
				soundControl20[i] = (FloatControl) enemyActivate[i].getControl
						(FloatControl.Type.MASTER_GAIN);
				
				soundControl21[0] = (FloatControl) bossDeath[0].getControl
						(FloatControl.Type.MASTER_GAIN);
				
				soundControl22[i] = (FloatControl) phaseShot[i].getControl
						(FloatControl.Type.MASTER_GAIN);
				
				soundControl23[i] = (FloatControl) barrelExplosion[i].getControl
						(FloatControl.Type.MASTER_GAIN);	
				
				soundControl24[i] = (FloatControl) pistol[i].getControl
						(FloatControl.Type.MASTER_GAIN);
				
				soundControl25[i] = (FloatControl) enemy3Activate[i].getControl
						(FloatControl.Type.MASTER_GAIN);
				
				soundControl26[i] = (FloatControl) enemy4Activate[i].getControl
						(FloatControl.Type.MASTER_GAIN);
				
				soundControl27[i] = (FloatControl) enemy5Activate[i].getControl
						(FloatControl.Type.MASTER_GAIN);
				
				soundControl28[i] = (FloatControl) enemy7Activate[i].getControl
						(FloatControl.Type.MASTER_GAIN);
				
				soundControl29[i] = (FloatControl) enemyFire[i].getControl
						(FloatControl.Type.MASTER_GAIN);
				
				soundControl30[i] = (FloatControl) enemyFireHit[i].getControl
						(FloatControl.Type.MASTER_GAIN);
				
				soundControl31[i] = (FloatControl) reaperHurt[i].getControl
						(FloatControl.Type.MASTER_GAIN);
				
				soundControl32[i] = (FloatControl) vileCivHurt[i].getControl
						(FloatControl.Type.MASTER_GAIN);
				
				soundControl33[i] = (FloatControl) tankHurt[i].getControl
						(FloatControl.Type.MASTER_GAIN);
			}	
		}
		else
		{
			Display.resetGame = false;
			
			loading.setTitle("Setting up game audio... 25% Loaded");
			
			//If mlg theme, make musicTheme the mlg theme
			if(themeNum == 5)
			{
				musicTheme = 3;
			}
			
		   /*
		    * If you changed the music theme while the game was paused,
		    * then close the old music playing, turn audioOn to false, and
		    * then set the oldMusicTheme to the current theme. The music
		    * will then switch to playing the new audio.
		    */
			if(musicTheme != oldMusicTheme)
			{
				music.close();
				audioOn = false;
				oldMusicTheme =  musicTheme;
			}
			
			//Only do if there is music playing
			if(audioOn)
			{
				/*
				    * If the music volume is as lower than it can go (Meaning off) 
				    * then just set it to the limit. Otherwise just set the
				    * music volume to the correct level.
				    */
					if(FPSLauncher.musicVolumeLevel < -80)
					{
						musicControl.setValue(-80.0f);
					}
					else
					{
						musicControl.setValue(FPSLauncher.musicVolumeLevel);
					}
			}			   
		}
		
		//Resets all sound clips
		for(int i = 0; i < clipSize; i++)
		{
		   /*
		    * If the sound is as lower than it can go (Meaning off) 
		    * then just set it to the limit. Otherwise just set the
		    * sound to the correct level.
		    */
			if(FPSLauncher.soundVolumeLevel < -80)
			{
				soundControl1[i].setValue(-80.0f);
				
				soundControl2[i].setValue(-80.0f);
				
				soundControl3[i].setValue(-80.0f);
				
				soundControl4[i].setValue(-80.0f);
				
				soundControl5[0].setValue(-80.0f);
				
				soundControl6[i].setValue(-80.0f);
				
				soundControl7[i].setValue(-80.0f);
				
				soundControl8[0].setValue(-80.0f);
				
				soundControl9[i].setValue(-80.0f);
				
				soundControl10[i].setValue(-80.0f);
				
				soundControl11[i].setValue(-80.0f);
				
				soundControl12[i].setValue(-80.0f);
				
				soundControl13[i].setValue(-80.0f);
				
				soundControl14[i].setValue(-80.0f);
				
				soundControl15[i].setValue(-80.0f);
				
				soundControl16[i].setValue(-80.0f);
				
				soundControl17[0].setValue(-80.0f);
				
				soundControl18[i].setValue(-80.0f);
				
				soundControl19[i].setValue(-80.0f);
				
				soundControl20[i].setValue(-80.0f);
				
				soundControl21[0].setValue(-80.0f);
				
				soundControl22[i].setValue(-80.0f);
				
				soundControl23[i].setValue(-80.0f);
				
				soundControl24[i].setValue(-80.0f);
				
				soundControl25[i].setValue(-80.0f);
				
				soundControl26[i].setValue(-80.0f);
				
				soundControl27[i].setValue(-80.0f);
				
				soundControl28[i].setValue(-80.0f);
				
				soundControl29[i].setValue(-80.0f);
				
				soundControl30[i].setValue(-80.0f);
				
				soundControl31[i].setValue(-80.0f);
				
				soundControl32[i].setValue(-80.0f);
				
				soundControl33[i].setValue(-80.0f);
			}
			else
			{
				soundControl1[i].setValue(FPSLauncher.soundVolumeLevel);
				
				soundControl2[i].setValue(FPSLauncher.soundVolumeLevel);
				
				soundControl3[i].setValue(FPSLauncher.soundVolumeLevel);
				
				soundControl4[i].setValue(FPSLauncher.soundVolumeLevel);
				
				soundControl5[0].setValue(FPSLauncher.soundVolumeLevel);
				
				soundControl6[i].setValue(FPSLauncher.soundVolumeLevel);
				
				soundControl7[i].setValue(FPSLauncher.soundVolumeLevel);
				
				soundControl8[0].setValue(FPSLauncher.soundVolumeLevel);
				
				soundControl9[i].setValue(FPSLauncher.soundVolumeLevel);
				
				soundControl10[i].setValue(FPSLauncher.soundVolumeLevel);
				
				soundControl11[i].setValue(FPSLauncher.soundVolumeLevel);
				
				soundControl12[i].setValue(FPSLauncher.soundVolumeLevel);
				
				soundControl13[i].setValue(FPSLauncher.soundVolumeLevel);
				
				soundControl14[i].setValue(FPSLauncher.soundVolumeLevel);
				
				soundControl15[i].setValue(FPSLauncher.soundVolumeLevel);
				
				soundControl16[i].setValue(FPSLauncher.soundVolumeLevel);
				
				soundControl17[0].setValue(FPSLauncher.soundVolumeLevel);
				
				soundControl18[i].setValue(FPSLauncher.soundVolumeLevel);
				
				soundControl19[i].setValue(FPSLauncher.soundVolumeLevel);
				
				soundControl20[i].setValue(FPSLauncher.soundVolumeLevel);
				
				soundControl21[0].setValue(FPSLauncher.soundVolumeLevel);
				
				soundControl22[i].setValue(FPSLauncher.soundVolumeLevel);
				
				soundControl23[i].setValue(FPSLauncher.soundVolumeLevel);
				
				soundControl24[i].setValue(FPSLauncher.soundVolumeLevel);
				
				soundControl25[i].setValue(FPSLauncher.soundVolumeLevel);
				
				soundControl26[i].setValue(FPSLauncher.soundVolumeLevel);
				
				soundControl27[i].setValue(FPSLauncher.soundVolumeLevel);
				
				soundControl28[i].setValue(FPSLauncher.soundVolumeLevel);
				
				soundControl29[i].setValue(FPSLauncher.soundVolumeLevel);
				
				soundControl30[i].setValue(FPSLauncher.soundVolumeLevel);
				
				soundControl31[i].setValue(FPSLauncher.soundVolumeLevel);
				
				soundControl32[i].setValue(FPSLauncher.soundVolumeLevel);
				
				soundControl33[i].setValue(FPSLauncher.soundVolumeLevel);
			}
			
			// The enemy is killed clip that plays
			kill[i].setMicrosecondPosition(0);

			// Clip that is played when the player picks up a health pack
			health[i].setMicrosecondPosition(0);

			// When the player picks up an ammo clip
			clip[i].setMicrosecondPosition(0);

			// When the enemy is hit by the players bullet
			enemyHit[i].setMicrosecondPosition(0);

			// If player is injured by an enemy
			playerHurt[0].setMicrosecondPosition(0);;
			
			//These clips are public because they are used in Controller
			reload[0].setMicrosecondPosition(0);
			ammoOut[i].setMicrosecondPosition(0);
			shoot[i].setMicrosecondPosition(0);
			phaseShot[i].setMicrosecondPosition(0);
			tryToUse[i].setMicrosecondPosition(0);
			buttonPress[i].setMicrosecondPosition(0);
			
			//This is public because it used by the door and lift classes
			lifting[i].setMicrosecondPosition(0);
			
			secret[i].setMicrosecondPosition(0);
			megaPickUp[i].setMicrosecondPosition(0);
			wallHit[i].setMicrosecondPosition(0);
			keyPickUp[i].setMicrosecondPosition(0);
			weaponPickUp[i].setMicrosecondPosition(0);
			playerDeath[0].setMicrosecondPosition(0);;
			bossHit[i].setMicrosecondPosition(0);
			bossActivate[i].setMicrosecondPosition(0);
			enemyActivate[i].setMicrosecondPosition(0);
			bossDeath[0].setMicrosecondPosition(0);
			barrelExplosion[i].setMicrosecondPosition(0);
			pistol[i].setMicrosecondPosition(0);
			enemy3Activate[i].setMicrosecondPosition(0);
			enemy4Activate[i].setMicrosecondPosition(0);
			enemy5Activate[i].setMicrosecondPosition(0);
			enemy7Activate[i].setMicrosecondPosition(0);
			enemyFire[i].setMicrosecondPosition(0);
			enemyFireHit[i].setMicrosecondPosition(0);
			reaperHurt[i].setMicrosecondPosition(0);
			vileCivHurt[i].setMicrosecondPosition(0);
			tankHurt[i].setMicrosecondPosition(0);
		}

		loading.setTitle("Resetting textures and GUI... 65% Loaded");
		// Resets game width and height
		setGameWidth();
		setGameHeight();

		// Resets games textures depending on theme
		Textures.resetTextures();

		// Reset ceilingHeight
		Render3D.ceilingDefaultHeight = 15.0;

		/*
		 * Creates different dimensions of the screen that can be used
		 */
		Dimension size = new Dimension(WIDTH, HEIGHT);
		Dimension minSize = new Dimension(0, 0);
		Dimension maxSize = new Dimension(5000, 5000);

		/*
		 * Sets the max, min, and preferred sizes of the screen
		 */
		setPreferredSize(size);
		setMinimumSize(minSize);
		setMaximumSize(maxSize);
		
		// Sets up the new screen used
		screen = new Screen(WIDTH, HEIGHT);

		// Sets up the BufferedImage size, and type
		img = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);

		//Gets data from each int in terms of color?
		screen.PIXELS = ((DataBufferInt) img.getRaster().getDataBuffer()).getData();

		loading.setTitle("Setting up input handling listeners... 90% Loaded");
		
	   /*
		* Sets up all the input stuff.
		*/
		input = new InputHandler();
		addKeyListener(input);
		addMouseListener(input);
		addFocusListener(input);
		addMouseMotionListener(input);
		
		loading.setTitle("Done 100% Loaded");
		loading.dispose();
	}

	/**
	 * Sets Width of frame depending on the the graphics selection from the
	 * launcher.
	 */
	public static void setGameWidth() 
	{
		if (graphicsSelection == 0)
		{
			WIDTH = 640;
		} 
		else if (graphicsSelection == 1) 
		{
			WIDTH = 800;
		} 
		else if (graphicsSelection == 2) 
		{
			WIDTH = 1020;
		} 
		else 
		{
			WIDTH = 800;
		}
	}

	/**
	 * Sets Height of frame depending on the graphics selection from the
	 * launcher.
	 */
	public static void setGameHeight() 
	{
		if (graphicsSelection == 0)
		{
			HEIGHT = 480;
		} 
		else if (graphicsSelection == 1) 
		{
			HEIGHT = 600;
		} 
		else if (graphicsSelection == 2) 
		{
			HEIGHT = 768;
		} 
		else 
		{
			HEIGHT = 600;
		}
	}

	/**
	 * Initiates the game to start running, and sets isRunning to true.
	 * Synchronized helps synchronize the thread to run correctly if the program
	 * is being ran in an applet.
	 */
	public synchronized void start() 
	{
		// If program is running, just return
		if (isRunning == true) 
		{
			return;
		}

		// If not, set the program to start running
		else 
		{
			isRunning = true;

			// Starts a new thread to handle all the events
			thread = new Thread(this);
			thread.start();
		}
	}

	/**
	 * What is called if the program is running. It renders the screen, performs
	 * all the actions in the thread of events, tracks fps, etc.
	 */
	public void run() 
	{
		int frames = 0;
		int tickCount = 0;
		double unprocessedSeconds = 0;
		double secondsPerTick = (1 / 60.0);
		long previousTime = System.nanoTime();

		/*
		 * Focuses your user on the screen so you don't have to click to start
		 */
		requestFocus();

		// While the game is running, keep ticking and rendering
		while (isRunning == true) 
		{			
			long currentTime = System.nanoTime();
			long passedTime = currentTime - previousTime;

			previousTime = currentTime;

			/*
			 * Sets unprocessedSeconds equal to a number with the same number
			 * before the decimal place as secondsPerTick, and equal to
			 * passedTime.
			 */
			unprocessedSeconds += (passedTime / 1000000000.0);

			/*
			 * Basically checks for the difference in seconds between the set
			 * value and calculated value. As long as unprocessedSeconds is more
			 * than secondsPerTick it will continue to loop through. Each loop
			 * it will tick, and unprocessedSeconds will keep going down. Every
			 * 60 loops, it will print out the number of frames that the game
			 * has, set frames back to 0, and add to previousTime. Why it does
			 * all this I am not 100% sure yet, but it works.
			 */
			while (unprocessedSeconds > secondsPerTick) 
			{
				tick();
				unprocessedSeconds -= secondsPerTick;
				tickCount++;

				//Every sixty ticks
				if (tickCount % 60 == 0) 
				{
					fps = frames;
					frames = 0;
					previousTime += 1000;
				}

			}
			
			//Always request focus
			requestFocus();
			
		   /*
		    * Used to correct the players y in case the player is 
		    * crouching and the y goes below the maxHeight the player
		    * can stand on.
		    */
			double yCorrect = Player.y;
			
			if(yCorrect < Player.maxHeight)
			{
				yCorrect = Player.maxHeight;
			}
			
			try
			{
				Collections.sort(Game.enemies);
			}
			catch(Exception e)
			{
				
			}
			
		   /*
		    * Checks if bullets hit a canister, if they do, then activate
		    * that canister and remove the bullet.
		    */
			for (int j = 0; j < Game.bullets.size(); j++) 
			{
				Bullet bullet = Game.bullets.get(j);
				
				bullet.isFreeZ(bullet.x, bullet.z);
				bullet.isFreeX(bullet.x, bullet.z);
				
				for(int k = 0; k < Game.canisters.size(); k++)
				{
					ExplosiveCanister can = Game.canisters.get(k);
					
					double distance2 = Math.sqrt(((Math.abs(can.x - bullet.x))
							* (Math.abs(can.x - bullet.x)))
							+ ((Math.abs(can.z - bullet.z))
									* (Math.abs(can.z - bullet.z))));
					
				   /*
				    * If the bullet is within range of the can, or the
				    * player is above the canister (autoaim) then
				    * blow up the canister.
				    */
					if (distance2 <= 0.3
							&& ((Math.abs(bullet.y + 
									(can.y / can.heightCorrect)) <= 1)
									|| (Math.abs(can.y / can.heightCorrect)
									< Player.y))
							&& !can.exploding) 
					{
						can.health -= bullet.damage;
						Game.bullets.remove(bullet);
						
						if(can.health <= 0)
						{
							can.exploding = true;
						}
					}
				}
			}
			
			//Check to see if bullets hit the floor
			for (int j = 0; j < Game.bullets.size(); j++) 
			{
				Bullet bullet = Game.bullets.get(j);
				
			   /*
			    * If a bullet has hit a wall, then remove that bullet
			    * and play the hit sound.
			    */
				if(bullet.hit || bullet.y > 1)
				{
					//Don't play hit sound a second time.
					if(!bullet.hit)
					{
						bullet.playHitSound();
					}
					
					Game.bullets.remove(bullet);
				}
			}
			
			//Check to see if enemy projectiles hit a wall.
			for (int j = 0; j < Game.enemyProjectiles.size(); j++) 
			{
				EnemyFire enemyFire = Game.enemyProjectiles.get(j);
				
			   /*
			    * If a projectile has hit a wall, then remove that
			    * projectile and play the hit sound.
			    */
				if(enemyFire.hit || enemyFire.y > 1)
				{
					Game.enemyProjectiles.remove(enemyFire);
					
					//Call the play audio method to play the sound
					playAudioFile(input14, wallHit, clipNum8);
					
					//Update sound clip being played
					clipNum8++;
					
					//If the clipNum is past the length, reset it
					if(clipNum8 == wallHit.length)
					{
						clipNum8 = 0;
					}
				}
				
				//Run through all the canisters to see if the
				//Enemy projectiles hit the canister.
				for(int k = 0; k < Game.canisters.size(); k++)
				{
					ExplosiveCanister can = Game.canisters.get(k);
					
					double distance2 = Math.sqrt
							(((Math.abs(can.x - enemyFire.x))
							* (Math.abs(can.x - enemyFire.x)))
							+ ((Math.abs(can.z - enemyFire.z))
								* (Math.abs(can.z - enemyFire.z))));
					
				   /*
				    * If the projectile is within range of the can
				    * then blow up the canister.
				    */
					if (distance2 <= 0.3
							&& ((Math.abs(enemyFire.y + 
									(can.y / can.heightCorrect)) <= 1))
							&& !can.exploding) 
					{
						can.health -= enemyFire.damage;
						Game.enemyProjectiles.remove(enemyFire);
						
						if(can.health <= 0)
						{
							can.exploding = true;
						}
					}
				}
			}

			/*
			 * Loop through all the enemies each time, see if they have been hit
			 * by a bullet, and update both all the bullet movements and enemy
			 * movements.
			 */
			for (int i = 0; i < Game.enemies.size(); i++) 
			{	
				Enemy enemy = Game.enemies.get(i);
				
			   /*
			    * If the player is within 1 block of the enemy, or if
			    * the player is playing survival mode, then activate the
			    * enemy to start moving and acting.
			    * 
			    * Player has to be alive, and the enemy cannot already
			    * be activated for this to occur. Also the gamemode cannot
			    * be peaceful otherwise the sound just gets annoying.
			    * 
			    * Enemy can also be activated if seen by the player
			    */
				if(enemy.distance <= 1 && !enemy.activated
						&& Player.alive && skillMode > 0
						&& enemy.distance >= 0
						|| !Game.setMap && !enemy.activated
						&& Player.alive && skillMode > 0)
				{
					enemy.activated = true;
					
				   /*
				    * Play the audio file that correlates to each enemies
				    * activation sound.
				    */
					if(enemy.ID == 6 || enemy.ID == 8)
					{
						playAudioFile(input20, bossActivate, clipNum13);
						clipNum13++;
						
						if(clipNum13 == bossActivate.length)
						{
							clipNum13 = 0;
						}
					}
					else if(enemy.ID == 1 || enemy.ID == 2)
					{
						Display.playAudioFile(Display.input21,
								Display.enemyActivate,
								Display.clipNum14);
						Display.clipNum14++;
						
						if(Display.clipNum14 == 
								Display.enemyActivate.length)
						{
							Display.clipNum14 = 0;
						}
					}
					else if(enemy.ID == 3)
					{
						Display.playAudioFile(Display.input26,
								Display.enemy3Activate,
								Display.clipNum17);
						Display.clipNum17++;
						
						if(Display.clipNum17 == 
								Display.enemy3Activate.length)
						{
							Display.clipNum17 = 0;
						}
					}
					else if(enemy.ID == 4)
					{
						Display.playAudioFile(Display.input27,
								Display.enemy4Activate,
								Display.clipNum18);
						Display.clipNum18++;
						
						if(Display.clipNum18 == 
								Display.enemy4Activate.length)
						{
							Display.clipNum18 = 0;
						}
					}
					else if(enemy.ID == 5)
					{
						Display.playAudioFile(Display.input28,
								Display.enemy5Activate,
								Display.clipNum19);
						Display.clipNum19++;
						
						if(Display.clipNum19 == 
								Display.enemy5Activate.length)
						{
							Display.clipNum19 = 0;
						}
					}
					else if(enemy.ID == 7)
					{
						Display.playAudioFile(Display.input29,
								Display.enemy7Activate,
								Display.clipNum20);
						Display.clipNum20++;
						
						if(Display.clipNum20 == 
								Display.enemy7Activate.length)
						{
							Display.clipNum20 = 0;
						}
					}
				}
				
				if(Player.health <= 0)
				{
					enemy.activated = false;
				}
				
				//tick the enemy, updating its values and such
				enemy.tick(enemy);

				// Update the enemies movements
				enemy.move();
				
			   /*
			    * If the enemy is within range of the door, open the
			    * door because enemies are smarter than just letting a
			    * door stop them from killing you.
			    */
				if(enemy.ID != 2)
				{
					for(int a = 0; a < Game.doors.size(); a++)
					{
						Door door = Game.doors.get(a);
						
						if(Math.abs(door.getZ() - enemy.zPos) <= 1
								&& Math.abs(door.getX() - enemy.xPos) <= 1)
						{
							if(door.doorType == 0)
							{
								door.activated = true;
							}
						}
					}
				}
				
				
				/*
				 * Attempt to attack the player as long as the player is
				 * alive. The method itself will determine what attack if
				 * any the enemy will use though.
				 */
				if (Player.alive) 
				{
					enemy.attack(yCorrect);
				}
				
				boolean isenemyHit = false;
				boolean isbossHit = false;

				/*
				 * Update all bullets and see if they hit the enemy. bCorrect is
				 * used to get this to work, because if the bullet and the enemy
				 * are moving, they are moving so fast that sometimes the bullet
				 * doesn't read as hitting the enemy because it just passes
				 * through, so therefore the farther the enemy is away, the
				 * larger hit radius it has.
				 */
				for (int j = 0; j < Game.bullets.size(); j++) 
				{
					Bullet bullet = Game.bullets.get(j);
					
					double distance2 = Math.sqrt(((Math.abs(enemy.xPos - bullet.x))
							* (Math.abs(enemy.xPos - bullet.x)))
							+ ((Math.abs(enemy.zPos - bullet.z))
									* (Math.abs(enemy.zPos - bullet.z))));
					
					//If not morgoth
					if(enemy.ID != 6 && enemy.ID != 8)
					{
						// If the hits the borders of the enemy or
						//the enemy is below the player for autoaim
						//to take effect and hit the enemy.
						if (distance2 <= 0.35
								&& ((Math.abs((bullet.y) - (enemy.getY() 
										/ enemy.heightCorrect)) <= 1)
										|| Math.abs(enemy.getY()
												/ enemy.heightCorrect) 
										< Player.y)
								&& !bullet.hit) 
						{
							//Hurt enemy, remove bullet, and activate
							//the enemy if not already.
							enemy.hurt(bullet.damage, isenemyHit);
							enemy.activated = true;
							enemy.targetEnemy = null;
							Game.bullets.remove(j);
							enemy.harmed = 5;
							
							isenemyHit = true;
						}
					}
					else
					{
						if (distance2 <= 3
								&& Math.abs(bullet.y - (enemy.getY() 
										/ enemy.heightCorrect)) <= 5
								&& !bullet.hit) 
						{
							enemy.hurt(bullet.damage, isbossHit);
							enemy.activated = true;
							enemy.targetEnemy = null;
							enemy.harmed = 5;
							Game.bullets.remove(j);
							
							isbossHit = true;
						}
					}
				}
				
				isenemyHit = false;
				isbossHit = false;
				
				/*
				 * See if any enemy Projectile hits another enemy and if
				 * it does, then change the target of the current enemy to
				 * the enemy that shot the target.
				 */
				for (int j = 0; j < Game.enemyProjectiles.size(); j++) 
				{
					EnemyFire projectile = Game.enemyProjectiles.get(j);
					
					double distance2 = Math.sqrt(((Math.abs(enemy.xPos - projectile.x))
							* (Math.abs(enemy.xPos - projectile.x)))
							+ ((Math.abs(enemy.zPos - projectile.z))
									* (Math.abs(enemy.zPos - projectile.z))));
					
					//If not morgoth
					if(enemy.ID != 6 && enemy.ID != 8)
					{
						// If the hits the borders of the enemy or
						//the enemy is below the player for autoaim
						//to take effect and hit the enemy.
						if (distance2 <= 0.35
								&& ((Math.abs((projectile.y) - (enemy.getY() 
										/ enemy.heightCorrect)) <= 1)
										|| Math.abs(enemy.getY()
												/ enemy.heightCorrect) 
										< Player.y)
								&& !projectile.hit) 
						{
							if(!projectile.sourceEnemy.equals(enemy))
							{
								//Hurt enemy, remove bullet, and activate
								//the enemy if not already.
								enemy.hurt(projectile.damage, isenemyHit);
								enemy.activated = true;
								
								//If enemy that shot projectile is still alive
								//Then reset target to that 
								if(projectile.sourceEnemy != null)
								{
									enemy.targetEnemy 
										= projectile.sourceEnemy;
								}
								
								Game.enemyProjectiles.remove(j);
								enemy.harmed = 5;
								
								isenemyHit = true;
							}
						}
					}
					else
					{
						if (distance2 <= 3
								&& Math.abs(projectile.y - (enemy.getY() 
										/ enemy.heightCorrect)) <= 5
								&& !projectile.hit) 
						{
							if(!projectile.sourceEnemy.equals(enemy))
							{
								enemy.hurt(projectile.damage, isbossHit);
								enemy.activated = true;
								
								//If enemy that shot projectile is still alive
								//Then reset target to that 
								if(projectile.sourceEnemy != null)
								{
									enemy.targetEnemy 
										= projectile.sourceEnemy;
								}
								
								Game.enemyProjectiles.remove(j);
								
								isbossHit = true;
							}
						}
					}
				}
			

				/*
				 * If an enemy is killed, update kills, 
				 * remove that enemy, then add a corpse;
				 * In survival mode two more enemies are
				 * added in its place.
				 * 
				 * Also each enemy drops a different item
				 * or items pertaining to that enemies
				 * role in the game.
				 */
				if (enemy.health <= 0) 
				{
					enemyDeath(enemy);
				}
			}
			
			//Cycle through all the canisters
			for(int i = 0; i < Game.canisters.size(); i++)
			{
				ExplosiveCanister can = Game.canisters.get(i);
				
				if(can.exploding && can.phaseTime >= 6
						&& !can.performedAction)
				{
					can.performedAction = true;
				   /*
				    * Any canisters within range of this canister will
				    * be triggered to explode as well.
				    */
					for(int j = 0; j < Game.canisters.size(); j++)
					{
						ExplosiveCanister temp = Game.canisters.get(j);
						
						double distance2 = Math.sqrt(((Math.abs(can.x - temp.x))
								* (Math.abs(can.x - temp.x)))
								+ ((Math.abs(can.z - temp.z))
										* (Math.abs(can.z - temp.z))));
						
						if(!temp.equals(can) && distance2 <= 1.5 &&
								Math.abs((temp.y) - (can.y)) <= 1.5
								&& !temp.exploding)
						{
							temp.exploding = true;
						}
					}
					
				   /*
				    * Any enemies within range of the barrel explosion
				    * will be hurt with 20 damage to them. 
				    */
					for(int j = 0; j < Game.enemies.size(); j++)
					{
						Enemy temp = Game.enemies.get(j);
						
						double distance2 = Math.sqrt(((Math.abs(can.x - temp.xPos))
								* (Math.abs(can.x - temp.xPos)))
								+ ((Math.abs(can.z - temp.zPos))
										* (Math.abs(can.z - temp.zPos))));
						
						if(distance2 <= 2 &&
								Math.abs(temp.yPos - can.y) <= 2)
						{
							temp.hurt(40, false);	
						}
					}
					
					double distance2 = Math.sqrt(((Math.abs(can.x - Player.x))
							* (Math.abs(can.x - Player.x)))
							+ ((Math.abs(can.z - Player.z))
									* (Math.abs(can.z - Player.z))));
					
				   /*
				    * See if player is within range of barrel explosion
				    * and if he/she is, then damage the player
				    * accordingly.
				    */
					if(distance2 <= 2 && Math.abs(can.y - Player.y) <= 2
							&& Player.immortality == 0 
							&& !Controller.godModeOn)
					{
						//Hurt player depending on how much 
						//damage the enemy deals
						//and on the armor the player is wearing.
						if(Player.armor > 150)
						{
							Player.health -= 10;
							Player.armor -= 20;
						}
						else if(Player.armor > 100)
						{
							Player.health -= (int)(20 / 1.75);
							Player.armor -= 20 * 1.5;
						}
						else if(Player.armor > 50)
						{
							Player.health -= (int)(20 / 1.5);
							Player.armor -= 20 * 2;
						}
						else if(Player.armor > 0)
						{
							Player.health -= (int)(20 / 1.25);
							Player.armor -= 20 * 3;
						}
						else
						{
							Player.health -= 20;
						}
						
						if(Player.armor < 0)
						{
							Player.armor = 0;
						}
						
						playAudioFile(input5, playerHurt, clipNum5);
						clipNum5++;
						
						if(clipNum5 == playerHurt.length)
						{
							clipNum5 = 0;
						}
						
					}
				}
				
				//If the barrel just exploded play the barrel explosion
				//sound
				if(can.phaseTime >= 1 && can.exploding
						&& !can.playedSound)
				{
					can.playedSound = true;
					
					playAudioFile(input24, barrelExplosion, clipNum16);
					clipNum16++;
					
					if(clipNum16 == barrelExplosion.length)
					{
						clipNum16 = 0;
					}
				}
				
			   /*
			    * Every tick, update each cans phase. This is put here
			    * because when ticks are updated in the render method,
			    * if the item is not being rendered, then it will tick
			    * faster than if it is, so this allows all the canisters
			    * to be ticking equally, so that no matter whether they
			    * are within sight or not, they will still explode in
			    * order, and the time that they should be exploding in.
			    * 
			    */
				if(ticks % 1 == 0)
				{
					can.phaseTime++;
				}
				
				//After 16 ticks, remove the can
				if(can.phaseTime > 16)
				{
					can.removeCanister();
				}
			}
			
		   /*
		    * Loop through all the enemy Projectiles being thrown at the
		    * player and if they are within a distance of 0.3 all around
		    * the player, then have it hit the player and deal the amount
		    * of damage required.
		    */
			for(int i = 0; i < Game.enemyProjectiles.size(); i++)
			{
				EnemyFire temp = Game.enemyProjectiles.get(i);
				
				//Finds the distance the item is away from the player
				double distance = Math.sqrt(((Math.abs(temp.x - Player.x))
						* (Math.abs(temp.x - Player.x)))
						+ ((Math.abs(temp.z - Player.z))
								* (Math.abs(temp.z - Player.z))));
				
				double tempNum = 6;
				
				//Helps correct where the projectile is detecting where
				//the player actually is. It's weird don't worry about it
				if(Player.y < 20)
				{
					tempNum = 6;
				}
				else if(Player.y < 30)
				{
					tempNum = 7;
				}
				else if(Player.y < 40)
				{
					tempNum = 8;
				}
				else if(Player.y < 50)
				{
					tempNum = 10;
				}
				else
				{
					tempNum = 12;
				}
				
				if(distance <= 0.3 && Math.abs((Player.y / tempNum) + (temp.y)) <= 2.5
						&& !Controller.godModeOn && Player.immortality == 0)
				{
					double damage = temp.damage;
					
					if(Player.armor > 150)
					{
						damage /= 3;
						Player.armor -= damage;
					}
					else if(Player.armor > 100)
					{
						damage /= 2;
						Player.armor -= damage;
					}
					else if(Player.armor > 100)
					{
						damage /= 1.5;
						Player.armor -= damage;
					}
					else if(Player.armor > 50)
					{
						damage /= 1.25;
						Player.armor -= damage;
					}
					else if(Player.armor > 0)
					{
						damage /= 1.1;
						Player.armor -= damage;
					}
					
					if(Player.armor < 0)
					{
						Player.armor = 0;
					}
					
					Player.health -= temp.damage;
					Game.enemyProjectiles.remove(i);
					
					playAudioFile(input5, playerHurt, clipNum5);
					clipNum5++;
					
					if(clipNum5 == playerHurt.length)
					{
						clipNum5 = 0;
					}
				}
			}
			
			//Move all bullets on map
			for(int j = 0; j < Game.bullets.size(); j++)
			{
				Bullet bullet = Game.bullets.get(j);
				
				bullet.move();
			}
			
			//Move all enemy projectiles on the map
			for(int j = 0; j < Game.enemyProjectiles.size(); j++)
			{
				EnemyFire temp = Game.enemyProjectiles.get(j);
				
				temp.move();
			}
			
			//Reset ticks each tick
			if(ticks % 1 == 0)
			{
				ticks = 0;
			}
			
		   /*
		    * Loop through all the corpses and enact gravity on them
		    * basically so there is no floating corpses. They will all
		    * reach the ground after being spawned.
		    */
			for (int i = 0; i < Game.corpses.size(); i++) 
			{
				Corpse corpse = Game.corpses.get(i);

				//Gets the block that the item is over.
				Block temp = Game.level.getBlock
						((int)(corpse.x), (int)(corpse.z));
	

			   /*
			    * If the items y is greater than the blocks height plus
			    * its y value, then decrease the items y until it reaches
			    * the ground.
			    */
				if(corpse.y > temp.height + temp.y + 3 && !temp.isaDoor)
				{
					corpse.y -= 1;
				}
				else
				{
					if(!temp.isaDoor)
					{
						corpse.y = temp.height + temp.y + 3;
					}
					else
					{
						corpse.y = 3;
					}
				}
			}
			
			/*
			 * Loop through all the items and see if they are within radius of the
			 * player. If they are, and the player can use it,
			 * and remove that item from the games list of items.
			 */
			for (int i = 0; i<Game.items.size(); i++) 
			{		
				Item item = Game.items.get(i);
				//Gets the block that the item is over.
				Block temp = Game.level.getBlock
						((int)(item.x), (int)(item.z));
	

			   /*
			    * If the items y is greater than the blocks height plus
			    * its y value, then decrease the items y until it reaches
			    * the ground.
			    */
				if(item.y > temp.height + temp.y && !temp.isaDoor)
				{
					item.y -= 1;
				}
				else
				{
					if(!temp.isaDoor)
					{
						item.y = temp.height + temp.y;
					}
					else
					{
						item.y = 0;
					}
				}
				
				//Finds the distance the item is away from the player
				double distance = Math.sqrt(((Math.abs(item.x - Player.x))
						* (Math.abs(item.x - Player.x)))
						+ ((Math.abs(item.z - Player.z))
								* (Math.abs(item.z - Player.z))));
				
				//If it is an end level linedef, start the next level
				if(item.itemID == 48 && distance <= 0.5)
				{
					game.mapNum++;
					game.loadNextMap();
				}
				
				//If the item is at least 10 units away, and its not
				//a secret, and the player is not in noClip mode
				if (distance <= 0.7
						&& Math.abs(item.y - yCorrect) <= 3
						&& item.itemID != 20
						&& !Controller.noClipOn) 
				{
					//Was the object activated?
					boolean activated = item.activate();
					
				   /*
				    * If the item was activated remove it. Otherwise keep
				    * it in the map.
				    */
					if(activated)
					{
						game.items.remove(i);
					}
					
					
				   /*
				    * If activated, and the item is a megahealth, play
				    * the sound that corresponds to the item
				    * by calling the playAudioFile method and sending
				    * it the corresponding values for that particular
				    * objects sound.
				    */
					if(activated && item.itemID == 1)
					{
						//Call the play audio method to play the sound
						playAudioFile(input13, megaPickUp, clipNum7);
						
						//Update sound clip being played
						clipNum7++;
						
						//If the clipNum is past the length, reset it
						if(clipNum7 == megaPickUp.length)
						{
							clipNum7 = 0;
						}
					}
					//Default pick up sound is played for items
					else if(activated && item.itemID == 2
							|| activated && item.itemID > 7
							&& item.itemID != 21 && item.itemID != 36
							&& item.itemID != 47 && item.itemID < 49)
					{
						//Call the play audio method to play the sound
						playAudioFile(input3, health, clipNum3);
						
						//Update sound clip being played
						clipNum3++;
						
						//If the clipNum is past the length, reset it
						if(clipNum3 == health.length)
						{
							clipNum3 = 0;
						}
					}
					//2nd Default sound for particular items
					else if(activated && item.itemID == 3
							|| activated && item.itemID == 36
							|| activated && item.itemID == 47
							|| activated && item.itemID == 50
							|| activated && item.itemID == 51
							|| activated && item.itemID == 56
							|| activated && item.itemID == 57) 
					{
						//Call the play audio method to play the sound
						playAudioFile(input4, clip, clipNum4);
						
						//Update sound clip being played
						clipNum4++;
						
						//If the clipNum is past the length, reset it
						if(clipNum4 == clip.length)
						{
							clipNum4 = 0;
						}
					}
					//Same but for keys
					else if(activated && item.itemID >= 4 
							&& item.itemID <= 7)
					{
						//Call the play audio method to play the sound
						playAudioFile(input15, keyPickUp, clipNum9);
						
						//Update sound clip being played
						clipNum9++;
						
						//If the clipNum is past the length, reset it
						if(clipNum9 == keyPickUp.length)
						{
							clipNum9 = 0;
						}
					}
					//When you pick up a weapon
					else if(activated && item.itemID == 21
							|| activated && item.itemID == 49
							|| activated && item.itemID == 55)
					{
						//Call the play audio method to play the sound
						playAudioFile(input16, weaponPickUp, clipNum10);
						
						//Update sound clip being played
						clipNum10++;
						
						//If the clipNum is past the length, reset it
						if(clipNum10 == weaponPickUp.length)
						{
							clipNum10 = 0;
						}
					}
				}
				else if(distance <= 0.7 && item.itemID == 20)
				{
					//Was the object activated?
					boolean activated = item.activate();
					
					//Call the play audio method to play the sound
					playAudioFile(input12, secret, clipNum6);
					
					//Update sound clip being played
					clipNum6++;
					
					//If the clipNum is past the length, reset it
					if(clipNum6 == secret.length)
					{
						clipNum6 = 0;
					}
					
				   /*
				    * If the item was activated remove it. Otherwise keep
				    * it in the map.
				    */
					if(activated)
					{
						game.items.remove(i);
					}
				}
			}

		   /*
		    * If player just died, then determine whether he/she can be
		    * resurrected, and if he/she can't then player death sound
		    * and set player status to not alive. 
		    * 
		    * Otherwise resurrect the player with 100 health, but no
		    * armor.
		    */
			if (Player.health <= 0 && Player.alive) 
			{
				if(Player.resurrections == 0)
				{
					Player.alive = false;
					
					//Call the play audio method to play the sound
					playAudioFile(input17, playerDeath, clipNum11);
					
					//Update sound clip being played
					clipNum11++;
					
					//If the clipNum is past the length, reset it
					if(clipNum11 == playerDeath.length)
					{
						clipNum11 = 0;
					}
				}
				else
				{
					Player.resurrections--;
					
					itemPickup = "Resurrected with skull!";
					itemPickupTime = 1;
					
					Player.health = 100;
					Player.armor  = 0;
					Player.immortality = 100;
				}
			}
			
			if(Player.health > Player.maxHealth)
			{
				Player.health = Player.maxHealth;
			}
			
			Player.updateBuffs();

			// Keeps playing audio as long as game is running
			playAudio();

			// Render the game, and add to number of frames rendered
			render();
			frames++;

			// Only do this if the mouse is on
			if (mouseOn) 
			{
				// Reads the x value of the mouse
				newX = InputHandler.MouseX;

				// Reads the y value of the mouse
				newY = InputHandler.MouseY;
				
			   /*
				* Determine what the mouses position is in correlation to what
				* it previously was last tick, and move it in that given
				* direction if it has moved.
				*/
				if (newX > oldX) 
				{
					Controller.mouseRight = true;
					Controller.mouseLeft = false;
				} 
				else if (newX < oldX) 
				{
					Controller.mouseLeft = true;
					Controller.mouseRight = false;
				} 
				else 
				{
					Controller.mouseLeft = false;
					Controller.mouseRight = false;
				}

				/*
				 * Determine what the mouses position is in correlation to what
				 * it previously was last tick, and move it in that given
				 * direction if it has moved.
				 */
				if (newY > oldY)
				{
					Controller.mouseDown = true;
					Controller.mouseUp = false;
				} 
				else if (newY < oldY) 
				{
					Controller.mouseUp = true;
					Controller.mouseDown = false;
				} 
				else 
				{
					Controller.mouseUp = false;
					Controller.mouseDown = false;
				}

			   /*
				* Depending on the difference of how far it moved since the
				* last tick, move it faster in that direction.
				*/
				if (InputHandler.mouseLoop == 0) 
				{
					mouseSpeedHorizontal = Math.abs(oldX - newX);
					mouseSpeedVertical = Math.abs(oldY - newY);
				}

				// Set oldX equal to the x just found
				oldX = newX;

				// Set oldY equal to the y just found
				oldY = newY;
			}
			
			if (Controller.quitGame == true) 
			{
				stop();
			}
		}
	}

	/**
	 * Stop the program if it is still running. Otherwise just return, the
	 * programs already stopped.
	 */
	public synchronized void stop() 
	{
		if (isRunning == false) 
		{
			return;
		} 
		else 
		{
			isRunning = false;

			// Closes the clip, stopping the games music.
			if (musicTheme != 0 && !Display.pauseGame) 
			{
				music.close();
			}
			
			try 
			{
				new FPSLauncher(0);
			}
			catch (Exception e) 
			{
				// If that somehow has an error, end the program anyway
				e.printStackTrace();
				System.exit(0);
			}
		}
	}

	/**
	 * Causes the game to tick, and also sends any keys typed into the game
	 * class to determine what will happen.
	 */
	private void tick() 
	{
		ticks++;
		game.tick(input.key);
	}

	/**
	 * Renders the screen after each new event resetting the graphics. Thats the
	 * simplest way to put it.
	 */
	private void render() 
	{
	   /*
		* Strategy of how the image and pixels are buffered. How it organizes
		* complex memory on a particular canvas or window.
		* 
		* Takes the color integers and renders them using a graphics object
		* that interprets the integers and displays them correctly on the
		* screen.
		*/
		bs = this.getBufferStrategy();

		//If it has not been instantiated yet
		if (bs == null) 
		{
			/*
			 * Creates the 3rd type of BufferStrategy. Method can be called
			 * because this class extends canvas. I'm guessing the 3 is because
			 * this program will be 3D.
			 */
			createBufferStrategy(3);
			return;
		}

		// Call screen and have it render all events that happen in game
		screen.render(game);

		/*
		 * Creates a graphics object based on what the buffered strategy is and
		 * then disposes of the graphics object after it is drawn to the screen.
		 * bs.show(); shows it on the screen.
		 */
		Graphics g = bs.getDrawGraphics();

		// Draws image with offsets, and given WIDTH and HEIGHT
		g.drawImage(img, 0, 0, WIDTH, HEIGHT, null);

		// Sets font of any text drawn on the screen
		g.setFont(new Font("Verdana", 1, 15));

		/*
		 * Depending on how low the players health is, set the text to different
		 * colors on the screen.
		 */
		if (Player.health > 100) 
		{
			g.setColor(Color.BLUE);
			Controller.moveSpeed = 1.5;
		} 
		else if (Player.health > 60) 
		{
			g.setColor(Color.GREEN);
			Controller.moveSpeed = 1.0;
		} 
		else if (Player.health > 20) 
		{
			g.setColor(Color.YELLOW);
			Controller.moveSpeed = 0.75;
		} 
		else 
		{
			g.setColor(Color.RED);
			Controller.moveSpeed = 0.5;
		}
		
	   /*
	    * Tries to load up the gui image for the HUD. If it can't, it will
	    * just pass through this and render the text only.
	    */
		g.drawImage(HUD, (WIDTH / 2) - 400, HEIGHT - 138, 800, 100, null);
		
		//Get the Weapon the player currently has Equipped
		Weapon playerWeapon = Player.weapons[Player.weaponEquipped];
	
	   /*
	    * Tries to load the face corresponding to the players health and
	    * phase of looking, but if it can't load it, it will just not
	    * load and go on.
	    */
		try
		{
			BufferedImage face = healthyFace1; 
			
			int phase = facePhase / 50;
			
			//If player is healthy
			if(Player.health > 75)
			{
				if(phase == 0 || phase == 2)
				{
					face = healthyFace1;
				}
				else if(phase == 1)
				{
					face = healthyFace2;
				}
				else
				{
					face = healthyFace3;
				}
			}
			else if(Player.health > 50)
			{
				if(phase == 0 || phase == 2)
				{
					face = hurtFace1;
				}
				else if(phase == 1)
				{
					face = hurtFace2;
				}
				else
				{
					face = hurtFace3;
				}
			}
			else if(Player.health > 25)
			{
				if(phase == 0 || phase == 2)
				{
					face = veryHurtFace1;
				}
				else if(phase == 1)
				{
					face = veryHurtFace2;
				}
				else
				{
					face = veryHurtFace3;
				}
			}
			else if(Player.health > 0)
			{
				if(phase == 0 || phase == 2)
				{
					face = almostDead1;
				}
				else if(phase == 1)
				{
					face = almostDead2;
				}
				else
				{
					face = almostDead3;
				}
			}
			else
			{
				face = dead;
			}
			
		   /*
		    * If in god mode, override all the faces and make this the
		    * face displayed.
		    */
			if(Controller.godModeOn || Player.immortality > 0)
			{
				face = godMode;
			}
			
			facePhase++;
			
			if(facePhase >= 200)
			{
				facePhase = 0;
			}
			
			g.drawImage(face, (WIDTH / 2) - 50, HEIGHT - 138, 100, 100, null);
		}
		catch(Exception e)
		{
		}
		
	   /*
	    * Display the gun in front of the player and how it looks
	    * depending on what phase of firing the gun is, and
	    * whether the player is alive or not
	    */
		try
		{
			BufferedImage gun = gunNormal;
			
			int x = (WIDTH / 2) - 150;
			int y = HEIGHT - 388;
			
			if(playerWeapon.weaponID == 0)
			{
				x = (WIDTH / 2);
				
				if(playerWeapon.weaponPhase == 1)
				{
					gun = pistolRight2;
				}
				else if(playerWeapon.weaponPhase == 2)
				{
					gun = pistolRight3;
				}
				else if(playerWeapon.weaponPhase == 3)
				{
					gun = pistolRight4;
				}
				else if(playerWeapon.weaponPhase == 4)
				{
					gun = pistolRight5;
				}
				else
				{
					gun = pistolRight1;
				}
				
				if(playerWeapon.dualWield == true)
				{
					BufferedImage gun2 = pistolLeft1;
					
					if(playerWeapon.weaponPhase2 == 1)
					{
						gun2 = pistolLeft2;
					}
					else if(playerWeapon.weaponPhase2 == 2)
					{
						gun2 = pistolLeft3;
					}
					else if(playerWeapon.weaponPhase2 == 3)
					{
						gun2 = pistolLeft4;
					}
					else if(playerWeapon.weaponPhase2 == 4)
					{
						gun2 = pistolLeft5;
					}
					else
					{
						gun2 = pistolLeft1;
					}
					
					g.drawImage(gun2, (WIDTH / 2) - 300, y,
							300, 250, null);
				}
			}
			else if(playerWeapon.weaponID == 1)
			{
				if(playerWeapon.weaponPhase == 1)
				{
					gun = gunShot;
				}
				else if(playerWeapon.weaponPhase == 2)
				{
					gun = gunShot2;
				}
				else if(playerWeapon.weaponPhase == 3)
				{
					gun = gunShot3;
				}
				else if(playerWeapon.weaponPhase == 4)
				{
					gun = gunShot4;
				}
				else
				{
					gun = gunNormal;
				}
			}
			else if(playerWeapon.weaponID == 2)
			{
				if(playerWeapon.weaponPhase == 1)
				{
					gun = phaseCannon2;
				}
				else if(playerWeapon.weaponPhase == 2)
				{
					gun = phaseCannon3;
				}
				else if(playerWeapon.weaponPhase == 3)
				{
					gun = phaseCannon4;
				}
				else
				{
					gun = phaseCannon1;
				}
			}
			
			if(Player.alive)
			{
				g.drawImage(gun, x, y, 300, 250, null);
			}
		}
		catch(Exception e)
		{
		}
		
	   /*
	    * Try to render the keys now depending on whether the player has
	    * them or not.
	    */
		if(Player.hasRedKey)
		{
			g.drawImage(redKey, (WIDTH / 2) + 75, HEIGHT - 115, 10, 20, null);
		}
		
		if(Player.hasBlueKey)
		{
			g.drawImage(blueKey, (WIDTH / 2) + 95, HEIGHT - 115, 10, 20, null);
		}
		
		if(Player.hasGreenKey)
		{
			g.drawImage(greenKey, (WIDTH / 2) + 115, HEIGHT - 115, 10, 20, null);
		}
		
		if(Player.hasYellowKey)
		{
			g.drawImage(yellowKey, (WIDTH / 2) + 135, HEIGHT - 115, 10, 20, null);
		}

		// Shows the FPS on the screen if it is activated to show
		if (Controller.showFPS) {
			g.drawString("FPS: " + fps, 20, 50);
		}

		// Shows if fly mode is on
		if (Controller.flyOn) {
			g.drawString("FlyMode on", 400, 50);
		}

		// Shows up if noClip is on
		if (Controller.noClipOn) {
			g.drawString("noClip On", 200, 50);
		}

		// If SuperSpeed is activated
		if (Controller.superSpeedOn) {
			g.drawString("Super Speed On", 200, 100);
		}

		// If GodMode is activated
		if (Controller.godModeOn) {
			g.drawString("God Mode On", 400, 100);
		}

		/*
		 * If the player is alive, show the number of cartridges of ammo the
		 * player has, the amount of ammo the player currently has loaded, and
		 * how much is in the current cartridge. Also show the players health,
		 * and keycards. Eventually this will show secrets found too.
		 */
		if (Player.alive) 
		{
			g.drawString("Health: "+Player.health, (WIDTH / 2) - 170,
					HEIGHT - 120);
			
			g.drawString("Armor: "+Player.armor, (WIDTH / 2) - 170,
					HEIGHT - 95);
			
			if(playerWeapon.weaponID == 0)
			{
				g.drawString("Pistol Cartridges: " 
						+playerWeapon.cartridges.size(),
						(WIDTH / 2) - 390, HEIGHT - 95);
				
				g.drawString("Bullets: " + playerWeapon.ammo, 
						(WIDTH / 2) - 390,HEIGHT - 70);
			}
			else if(playerWeapon.weaponID == 1)
			{
				g.drawString("Shell Cartridges: " 
						+playerWeapon.cartridges.size(),
						(WIDTH / 2) - 390, HEIGHT - 95);
				
				g.drawString("Shells: " + playerWeapon.ammo, 
						(WIDTH / 2) - 390,HEIGHT - 70);
			}
			else
			{
				g.drawString("Batteries: " 
						+playerWeapon.cartridges.size(),
						(WIDTH / 2) - 390, HEIGHT - 95);
				
				g.drawString("Charges: " + playerWeapon.ammo, 
						(WIDTH / 2) - 390,HEIGHT - 70);
			}

			/*
			 * If there are cartridges, show the ammo of the one being currently
			 * used, if not just display that there is 0 available.
			 */
			if (playerWeapon.cartridges.size() != 0) 
			{
				if(playerWeapon.weaponID == 0)
				{
					g.drawString("Bullets in Cartridge: "
							+playerWeapon.cartridges.get(0).ammo, 
							(WIDTH / 2) - 390,HEIGHT - 120);
				}
				else if(playerWeapon.weaponID == 1)
				{
					g.drawString("Shells in Cartridge: "
						+playerWeapon.cartridges.get(0).ammo, 
						(WIDTH / 2) - 390,HEIGHT - 120);
				}
				else
				{
					g.drawString("Charge in Battery: "
							+playerWeapon.cartridges.get(0).ammo, 
							(WIDTH / 2) - 390,HEIGHT - 120);
				}
			} 
			else 
			{
				if(playerWeapon.weaponID == 0)
				{
					g.drawString("Bullets in Cartridge: 0", 
							(WIDTH / 2) - 390,HEIGHT - 120);
				}
				else if(playerWeapon.weaponID == 1)
				{
					g.drawString("Shells in Cartridge: 0", 
						(WIDTH / 2) - 390,HEIGHT - 120);
				}
				else
				{
					g.drawString("Charge in Battery: 0", 
							(WIDTH / 2) - 390,HEIGHT - 120);
				}
			}
		}
		else
		{
			g.drawString("DEAD...", (WIDTH / 2) - 170, HEIGHT - 120);
			
			g.drawString("Armor: "+Player.armor, (WIDTH / 2) - 170,
					HEIGHT - 95);
			
			Controller.moveSpeed = 0.0;
			Player.y = -6;
			
			itemPickup = "Press E to restart level";
			itemPickupTime = 1;
		}

	   /*
	    * In survival show the number of kills the player has total.
	    * It will never reset like in the actual game.
	    * 
	    * Otherwise show secrets found, and the actual map name.
	    */
		if(!Game.setMap)
		{
			g.drawString("Kills: " + kills, 
					(WIDTH / 2) + 200, HEIGHT - 120);
			
			g.drawString("Survival Map", 
					(WIDTH / 2) - 390, HEIGHT - 45);
			
			g.drawString("Enemies: "+Game.enemies.size(), 
					(WIDTH / 2) + 200, HEIGHT - 95);
		}
		else
		{
			g.drawString("Secrets found: "
					+Game.secretsFound+" / "+Game.secretsInMap, 
					(WIDTH / 2) + 200, HEIGHT - 120);
			
			g.drawString(Game.mapName, (WIDTH / 2) - 390, HEIGHT - 45);
			
			
			g.drawString("Kills: "+kills+" / "+Game.enemiesInMap, 
					(WIDTH / 2) + 200, HEIGHT - 95);
		}
		
		
		
	   /*
	    * If an item has been picked up within the last 100 ticks, show 
	    * the item name that was picked up. Or show that a player can't
	    * open a door without the given key if the player tries to 
	    * open a door he can't open.
	    * 
	    * Also adds to itemPickupTime each time this is rendered.
	    */
		if(itemPickupTime != 0)
		{
			g.drawString(itemPickup,(WIDTH / 2) + 75, HEIGHT - 45);
			itemPickupTime++;
		}
		
		if(itemPickupTime > 100)
		{
			itemPickupTime = 0;
		}
		
	   /*
	    * Draw other needed texts
	    */
		g.drawString("Keys:", (WIDTH / 2) + 75, HEIGHT - 120);
		
		//g.drawString("Weapon: Shotgun", (WIDTH / 2) + 75, HEIGHT - 75);

		/*
		 * Disposes of this graphics object, and show what it was on the screen
		 */
		g.dispose();
		bs.show();
	}

	public static void main(String[] args) 
	{
		// Start the launcher
		new FPSLauncher(0);
	}

	/**
	 * Plays the game audio, depending on the audio the player chose for the
	 * game to play. This gets an audio input stream from an audio file that is
	 * opened using the AudioSystem objects getAudioInput Stream method. After
	 * getting the audio stream, and opening it, it then tells the Clip to keep
	 * looping as long as the clip is playing and then it starts the clip, and
	 * set audioOn equal to true. It will only start the clip once therefore.
	 */
	public synchronized void playAudio() 
	{
		try
		{
			if (!audioOn) 
			{
				music = AudioSystem.getClip();

				// I instantiate it up here so the code below doesn't squak
				input18 = AudioSystem.getAudioInputStream(this
						.getClass().getResource("/test/gameAudio.wav"));
				

				if (musicTheme == 0)
				{
					input18 = AudioSystem.getAudioInputStream(this.getClass()
							.getResource("/test/gameAudio.wav"));
				} 
				else if (musicTheme == 1) 
				{
					input18 = AudioSystem.getAudioInputStream(this.getClass()
							.getResource("/test/gameAudio2.wav"));
				}
				else if (musicTheme == 2) 
				{
					input18 = AudioSystem.getAudioInputStream(this.getClass()
							.getResource("/test/e1m3.wav"));
				}
				else if (musicTheme == 3)
				{
					input18 = AudioSystem.getAudioInputStream(this.getClass()
							.getResource("/test/gameAudio3.wav"));
				} 
				else 
				{
					input18 = AudioSystem.getAudioInputStream(this.getClass()
							.getResource("/test/gameAudio4.wav"));
				}
				
			   /*
			    * If the actual maps are being played instead of the
			    * randomly generated survival maps.
			    */
				if(Game.setMap)
				{
					input18 = AudioSystem.getAudioInputStream(this.getClass()
							.getResource("/test/level"+Game.mapNum+".wav"));		
				}

				music.open(input18);
				music.loop(Clip.LOOP_CONTINUOUSLY);
				music.start();
				audioOn = true;
				
				musicControl= (FloatControl) music.getControl
						(FloatControl.Type.MASTER_GAIN);
				
			   /*
				* If the music volume is as lower than it can go (Meaning off) 
			    * then just set it to the limit. Otherwise just set the
			    * music volume to the correct level.
				*/
				if(FPSLauncher.musicVolumeLevel < -80)
				{
					musicControl.setValue(-80.0f);
				}
				else
				{
					musicControl.setValue(FPSLauncher.musicVolumeLevel);
				}
				
				input18.close();
			}
		} 
		catch (Exception e) 
		{
			System.out.println(e);
		}
	}
	
	public void resetMusic()
	{
		try
		{
			music.close();
		}
		catch(Exception e)
		{
			
		}
		
		audioOn = false;

		playAudio();
	}

   /**
    * Loads all the images up for the HUD, and if one fails, it throws
    * an exception.
    * @throws Exception
    */
	public void loadHUD() throws Exception
	{
		yellowKey = ImageIO.read
				(new File("Images/yellowKey.png"));
		
		greenKey = ImageIO.read
				(new File("Images/greenKey.png"));
		
		redKey = ImageIO.read
				(new File("Images/redKey.png"));
		
		blueKey = ImageIO.read
				(new File("Images/blueKey.png"));
		
		HUD = ImageIO.read
				(new File("Images/userGUI.png"));
		
		healthyFace1 = ImageIO.read
				(new File("Images/healthyface1.png"));
		
		healthyFace2 = ImageIO.read
				(new File("Images/healthyface2.png"));
		
		healthyFace3 = ImageIO.read
				(new File("Images/healthyface3.png"));
		
		hurtFace1 = ImageIO.read
				(new File("Images/hurtFace1.png"));
		
		hurtFace2 = ImageIO.read
				(new File("Images/hurtFace2.png"));
		
		hurtFace3 = ImageIO.read
				(new File("Images/hurtFace3.png"));
		
		veryHurtFace1 = ImageIO.read
				(new File("Images/veryHurtFace1.png"));
		
		veryHurtFace2 = ImageIO.read
				(new File("Images/veryHurtFace2.png"));
		
		veryHurtFace3 = ImageIO.read
				(new File("Images/veryHurtFace3.png"));
		
		almostDead1 = ImageIO.read
				(new File("Images/almostDeadFace1.png"));
		
		almostDead2 = ImageIO.read
				(new File("Images/almostDeadFace2.png"));
		
		almostDead3 = ImageIO.read
				(new File("Images/almostDeadFace3.png"));
		
		dead = ImageIO.read
				(new File("Images/deadFace.png"));
		
		godMode = ImageIO.read
				(new File("Images/godModeFace.png"));
		
		gunNormal = ImageIO.read
				(new File("Images/gunNormal.png"));
		
		for (int x = 0; x < gunNormal.getWidth(); ++x)
		{
			for (int y = 0; y < gunNormal.getHeight(); ++y)
			{
			   if ((gunNormal.getRGB(x, y) & 0x00FFFFFF) == 0xFFFFFF) 
			   {
			       gunNormal.setRGB(x, y, 0);
			   }
			}
		}
		
		gunShot = ImageIO.read
				(new File("Images/gunShot.png"));
		
		for (int x = 0; x < gunShot.getWidth(); ++x)
		{
			for (int y = 0; y < gunShot.getHeight(); ++y)
			{
			   if ((gunShot.getRGB(x, y) & 0x00FFFFFF) == 0xFFFFFF) 
			   {
			       gunShot.setRGB(x, y, 0);
			   }
			}
		}
		
		gunShot2 = ImageIO.read
				(new File("Images/gunShot2.png"));
		
		for (int x = 0; x < gunShot2.getWidth(); ++x)
		{
			for (int y = 0; y < gunShot2.getHeight(); ++y)
			{
			   if ((gunShot2.getRGB(x, y) & 0x00FFFFFF) == 0xFFFFFF) 
			   {
			       gunShot2.setRGB(x, y, 0);
			   }
			}
		}
		
		gunShot3 = ImageIO.read
				(new File("Images/gunShot3.png"));
		
		for (int x = 0; x < gunShot3.getWidth(); ++x)
		{
			for (int y = 0; y < gunShot3.getHeight(); ++y)
			{
			   if ((gunShot3.getRGB(x, y) & 0x00FFFFFF) == 0xFFFFFF) 
			   {
			       gunShot3.setRGB(x, y, 0);
			   }
			}
		}
		
		gunShot4 = ImageIO.read
				(new File("Images/gunShot4.png"));
		
		for (int x = 0; x < gunShot4.getWidth(); ++x)
		{
			for (int y = 0; y < gunShot4.getHeight(); ++y)
			{
			   if ((gunShot4.getRGB(x, y) & 0x00FFFFFF) == 0xFFFFFF) 
			   {
			       gunShot4.setRGB(x, y, 0);
			   }
			}
		}
		
		phaseCannon1 = ImageIO.read
				(new File("Images/phaseCannon1.png"));
		
		for (int x = 0; x < phaseCannon1.getWidth(); ++x)
		{
			for (int y = 0; y < phaseCannon1.getHeight(); ++y)
			{
			   if ((phaseCannon1.getRGB(x, y) & 0x00FFFFFF) == 0xFFFFFF) 
			   {
			       phaseCannon1.setRGB(x, y, 0);
			   }
			}
		}
		
		phaseCannon2 = ImageIO.read
				(new File("Images/phaseCannon2.png"));
		
		for (int x = 0; x < phaseCannon2.getWidth(); ++x)
		{
			for (int y = 0; y < phaseCannon2.getHeight(); ++y)
			{
			   if ((phaseCannon2.getRGB(x, y) & 0x00FFFFFF) == 0xFFFFFF) 
			   {
			       phaseCannon2.setRGB(x, y, 0);
			   }
			}
		}
		
		phaseCannon3 = ImageIO.read
				(new File("Images/phaseCannon3.png"));
		
		for (int x = 0; x < phaseCannon3.getWidth(); ++x)
		{
			for (int y = 0; y < phaseCannon3.getHeight(); ++y)
			{
			   if ((phaseCannon3.getRGB(x, y) & 0x00FFFFFF) == 0xFFFFFF) 
			   {
			       phaseCannon3.setRGB(x, y, 0);
			   }
			}
		}
		
		phaseCannon4 = ImageIO.read
				(new File("Images/phaseCannon4.png"));
		
		for (int x = 0; x < phaseCannon4.getWidth(); ++x)
		{
			for (int y = 0; y < phaseCannon4.getHeight(); ++y)
			{
			   if ((phaseCannon4.getRGB(x, y) & 0x00FFFFFF) == 0xFFFFFF) 
			   {
			       phaseCannon4.setRGB(x, y, 0);
			   }
			}
		}
		
		pistolLeft1 = ImageIO.read
				(new File("Images/pistolLeft1.png"));
		
		for (int x = 0; x < pistolLeft1.getWidth(); ++x)
		{
			for (int y = 0; y < pistolLeft1.getHeight(); ++y)
			{
			   if ((pistolLeft1.getRGB(x, y) & 0x00FFFFFF) == 0xFFFFFF) 
			   {
			       pistolLeft1.setRGB(x, y, 0);
			   }
			}
		}
		
		pistolLeft2 = ImageIO.read
				(new File("Images/pistolLeft2.png"));
		
		for (int x = 0; x < pistolLeft2.getWidth(); ++x)
		{
			for (int y = 0; y < pistolLeft2.getHeight(); ++y)
			{
			   if ((pistolLeft2.getRGB(x, y) & 0x00FFFFFF) == 0xFFFFFF) 
			   {
			       pistolLeft2.setRGB(x, y, 0);
			   }
			}
		}
		
		pistolLeft3 = ImageIO.read
				(new File("Images/pistolLeft3.png"));
		
		for (int x = 0; x < pistolLeft3.getWidth(); ++x)
		{
			for (int y = 0; y < pistolLeft3.getHeight(); ++y)
			{
			   if ((pistolLeft3.getRGB(x, y) & 0x00FFFFFF) == 0xFFFFFF) 
			   {
			       pistolLeft3.setRGB(x, y, 0);
			   }
			}
		}
		
		pistolLeft4 = ImageIO.read
				(new File("Images/pistolLeft4.png"));
		
		for (int x = 0; x < pistolLeft4.getWidth(); ++x)
		{
			for (int y = 0; y < pistolLeft4.getHeight(); ++y)
			{
			   if ((pistolLeft4.getRGB(x, y) & 0x00FFFFFF) == 0xFFFFFF) 
			   {
			       pistolLeft4.setRGB(x, y, 0);
			   }
			}
		}
		
		pistolLeft5 = ImageIO.read
				(new File("Images/pistolLeft5.png"));
		
		for (int x = 0; x < pistolLeft5.getWidth(); ++x)
		{
			for (int y = 0; y < pistolLeft5.getHeight(); ++y)
			{
			   if ((pistolLeft5.getRGB(x, y) & 0x00FFFFFF) == 0xFFFFFF) 
			   {
			       pistolLeft5.setRGB(x, y, 0);
			   }
			}
		}
		
		pistolRight1 = ImageIO.read
				(new File("Images/pistolRight1.png"));
		
		for (int x = 0; x < pistolRight1.getWidth(); ++x)
		{
			for (int y = 0; y < pistolRight1.getHeight(); ++y)
			{
			   if ((pistolRight1.getRGB(x, y) & 0x00FFFFFF) == 0xFFFFFF) 
			   {
			       pistolRight1.setRGB(x, y, 0);
			   }
			}
		}
		
		pistolRight2 = ImageIO.read
				(new File("Images/pistolRight2.png"));
		
		for (int x = 0; x < pistolRight2.getWidth(); ++x)
		{
			for (int y = 0; y < pistolRight2.getHeight(); ++y)
			{
			   if ((pistolRight2.getRGB(x, y) & 0x00FFFFFF) == 0xFFFFFF) 
			   {
			       pistolRight2.setRGB(x, y, 0);
			   }
			}
		}
		
		pistolRight3 = ImageIO.read
				(new File("Images/pistolRight3.png"));
		
		for (int x = 0; x < pistolRight3.getWidth(); ++x)
		{
			for (int y = 0; y < pistolRight3.getHeight(); ++y)
			{
			   if ((pistolRight3.getRGB(x, y) & 0x00FFFFFF) == 0xFFFFFF) 
			   {
			       pistolRight3.setRGB(x, y, 0);
			   }
			}
		}
		
		pistolRight4 = ImageIO.read
				(new File("Images/pistolRight4.png"));
		
		for (int x = 0; x < pistolRight4.getWidth(); ++x)
		{
			for (int y = 0; y < pistolRight4.getHeight(); ++y)
			{
			   if ((pistolRight4.getRGB(x, y) & 0x00FFFFFF) == 0xFFFFFF) 
			   {
			       pistolRight4.setRGB(x, y, 0);
			   }
			}
		}
		
		pistolRight5 = ImageIO.read
				(new File("Images/pistolRight1.png"));
		
		for (int x = 0; x < pistolRight5.getWidth(); ++x)
		{
			for (int y = 0; y < pistolRight5.getHeight(); ++y)
			{
			   if ((pistolRight5.getRGB(x, y) & 0x00FFFFFF) == 0xFFFFFF) 
			   {
			       pistolRight5.setRGB(x, y, 0);
			   }
			}
		}
	}
	
	public static void playAudioFile(AudioInputStream input, Clip[] clip, int clipNumTemp)
	{
	   /*
		* Basically, try to open the audio file, and throw an
		* exception if you can't. If the audio file is opened,
		* then set all 20 sound clips to that audio stream.
		* Once they are set (meaning they have been opened)
		* then play them one by one each time the sound is
		* played. After a clip finishes, its sound is restarted
		* at the begininning and is ready to play again, and
		* its audio Stream is closed to eliminate too many
		* threads opening causing the game to slow and
		* eventually crash.
		*/
		try 
		{
			input.close();
			
		   /*
		    * Ok so get this bit of bullcrap here. If you try to reset
		    * the microsecond position of a sound clip, and you don't 
		    * try to print something to the screen, it will continue to
		    * not reset the sound file sometimes. For this I have no clue
		    * in hell why it requires this, or why it does what it does,
		    * but it does it. This is only with sound clips with 1 sound 
		    * being played luckily, but it still sucks.
		    * 
		    * Like try it. If you try to delete the system.out.println();
		    * and you either try to reload twice, or get hurt on like a
		    * toxic waste block twice, it will not play the sound the
		    * second time, but skip and do it the third time. I have no
		    * idea why it does this, or why, but it does. Even with this
		    * method, every 100 times or so it will skip, and it makes
		    * no sense whatsoever! Programming really pisses me off in
		    * times like this.
		    */
			if(clip.length == 1)
			{			
				//If the clip wasn't reset, try to reset it again.
				if(clip[clipNumTemp].getMicrosecondPosition() != 0)
				{
					clip[clipNumTemp].setMicrosecondPosition(0);
				}
				
				//So the gosh darn thing will work
				System.out.println();
				System.out.println();
				
				//Stop the sound clip, this is a backup in case above
				//doesn't work. 
				clip[clipNumTemp].stop();
			}
			
			clip[clipNumTemp].start();

			clipNumTemp++;

			if (clipNumTemp == (clip.length / 2) - 1)
			{
				for (int i = (int)(clip.length * 0.5); i < clip.length; i++) 
				{
					clip[i].setMicrosecondPosition(0);
				}
			}

			if (clipNumTemp == clip.length) 
			{
				clipNumTemp = 0;

				for (int i = 0; i < clip.length * 0.5; i++) 
				{
					clip[i].setMicrosecondPosition(0);
				}
				
				if(clip.length == 1)
				{
					clip[0].setMicrosecondPosition(0);
				}
			}
		} 
		catch (Exception e) 
		{
			System.out.println(e);
		}
	}
	
   /**
    * If an enemy dies, the items corresponding to that enemy (including
    * random drops) are spawned in the enemy death location. And 2 enemies
    * are spawned when in survival mode. Also kills are added to and 
    * everything else is taken care of.
    * 
    * A corpse is also dropped in the enemies death location.
    * 
    * @param enemy
    */
	public void enemyDeath(Enemy enemy)
	{
		Random random = new Random();
		
		if(enemy.ID == 1)
		{
			game.items.add(new Item(2, enemy.xPos, 
				Math.abs(enemy.yPos) + 4, 
				enemy.zPos, 3));
		}
		else if(enemy.ID == 2)
		{
			game.items.add(new Item(2, enemy.xPos, 
					Math.abs(enemy.yPos) + 4, 
					enemy.zPos, 3));
			
			game.items.add(new Item(2, enemy.xPos, 
					Math.abs(enemy.yPos) + 4, 
					enemy.zPos, 36));
			
			game.items.add(new Item(2, enemy.xPos, 
					Math.abs(enemy.yPos) + 4, 
					enemy.zPos, 37));
		}
		else if(enemy.ID == 3)
		{
			for(int z = 0; z < 5; z++)
			{
				game.items.add(new Item(2, enemy.xPos, 
					Math.abs(enemy.yPos) + 4, 
					enemy.zPos, 36));
			}

		}
		else if(enemy.ID == 4)
		{
			game.items.add(new Item(2, enemy.xPos, 
					Math.abs(enemy.yPos) + 4, 
					enemy.zPos, 37));
		}
		else if(enemy.ID == 5)
		{
			game.items.add(new Item(2, enemy.xPos, 
					Math.abs(enemy.yPos) + 4, 
					enemy.zPos, 2));
			
			game.items.add(new Item(2, enemy.xPos, 
					Math.abs(enemy.yPos) + 4, 
					enemy.zPos, 2));	
		}
		else if(enemy.ID == 6)
		{
			game.items.add(new Item(2, enemy.xPos, 
					Math.abs(enemy.yPos) + 4, 
					enemy.zPos, 4));
		}
		else if(enemy.ID == 7)
		{
			game.items.add(new Item(2, enemy.xPos, 
					Math.abs(enemy.yPos) + 4, 
					enemy.zPos, 56));
		}
		else if(enemy.ID == 8)
		{
			game.items.add(new Item(2, enemy.xPos, 
					Math.abs(enemy.yPos) + 4, 
					enemy.zPos, 6));
		}
		
		
		int temp = random.nextInt(100);
		
		//Random drops. Rare chances that they'll drop with 
		//Everything else.
		if(temp == 10)
		{
			game.items.add(new Item(2, enemy.xPos, 
					Math.abs(enemy.yPos) + 4, 
					enemy.zPos, 33));
		}
		else if(temp == 20)
		{
			game.items.add(new Item(2, enemy.xPos, 
					Math.abs(enemy.yPos) + 4, 
					enemy.zPos, 50));
		}
		else if(temp == 30)
		{
			game.items.add(new Item(2, enemy.xPos, 
					Math.abs(enemy.yPos) + 4, 
					enemy.zPos, 37));
		}
		else if(temp == 40)
		{
			game.items.add(new Item(2, enemy.xPos, 
					Math.abs(enemy.yPos) + 4, 
					enemy.zPos, 27));
		}
		else if(temp == 50)
		{
			game.items.add(new Item(2, enemy.xPos, 
					Math.abs(enemy.yPos) + 4, 
					enemy.zPos, 36));
		}
		else if(temp == 60)
		{
			game.items.add(new Item(2, enemy.xPos, 
					Math.abs(enemy.yPos) + 4, 
					enemy.zPos, 55));
		}
		else if(temp == 70)
		{
			game.items.add(new Item(2, enemy.xPos, 
					Math.abs(enemy.yPos) + 4, 
					enemy.zPos, 3));
		}
		else if(temp == 80)
		{
			game.items.add(new Item(2, enemy.xPos, 
					Math.abs(enemy.yPos) + 4, 
					enemy.zPos, 2));
		}
		else if(temp == 90)
		{
			game.items.add(new Item(2, enemy.xPos, 
					Math.abs(enemy.yPos) + 4, 
					enemy.zPos, 21));
		}
		
		game.corpses.add(new Corpse(enemy.xPos,
				enemy.zPos,
				-enemy.yPos, enemy.ID));
		
		int enemyID = enemy.ID;
		
		game.enemies.remove(enemy);
		
		//If the map doesn't already have set enemies
		if(!Game.setMap)
		{
			game.addEnemy();
			game.addEnemy();
		}
		
		kills++;
		
		if(enemyID != 6 && enemyID != 8)
		{
			//Call the play audio method to play the sound
			playAudioFile(input2, kill, clipNum2);
			
			//Update sound clip being played
			clipNum2++;
			
			//If the clipNum is past the length, reset it
			if(clipNum2 == kill.length)
			{
				clipNum2 = 0;
			}
		}
		else
		{
			playAudioFile(input22, bossDeath, clipNum15);
			clipNum15++;
			
			if(clipNum15 == bossDeath.length)
			{
				clipNum15 = 0;
			}
		}
	}
}
