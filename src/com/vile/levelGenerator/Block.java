package com.vile.levelGenerator;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import com.vile.graphics.Sprite;

/**
 * Title: Block
 * @author Alex Byrd
 * Date Updated: 9/16/2016
 * 
 * Description:
 * Each Block is used as a way to construct the walls in the map. A single
 * block is rendered in the game as 4 walls surround an empty space in the
 * center which shouldn't be seen unless the player jumps high enough to
 * see it. If the block isSolid this is what is seen, but if it is not
 * solid (Defaultly all blocks are not solid) then it is just air, and the
 * block cannot be seen by the player. Each block has a given height and
 * ID (used for texturing purposes, and action purposes) and y value.
 * 
 * Each block is also labeled as seeThrough if you can see other blocks
 * through it, under it, or above it.
 * 
 * wallPhase is used for animated walls as their textures will go through
 * phases to seem like the walls are animated.
 * 
 * Implements Comparable method so that when sorted in Render3D it can
 * sort Block objects by using the quickSort to sort the blocks in order
 * of height.
 *
 */
public class Block implements Comparable
{
	public boolean isSolid = false;
	public boolean seeThrough = false;
	
   /*
    * Only is used for when a player walks through a doorway and his/her
    * collision detection must be altered correctly to work within a
    * doorway or elevator. 
    */
	public boolean isaDoor = false;
	
	public int wallID = 0;
	public int wallPhase = 0;
	
	public double height = 12;
	
	public double y = 0;
	
	public int x = 0;
	public int z = 0;
	
	public List<Sprite> sprites = new ArrayList<Sprite>();
	
   /**
    * Constructs block
    * @param h
    */
	public Block(double h, int wallID, double y, int x, int z)
	{
		height = h;
		this.wallID = wallID;
		this.y = y;
		this.x = x;
		this.z = z;
		
	   /*
	    * If the block is glass, a door, or a lift it can
	    * be seen through depending on what state the block
	    * is, therefore it is rendered differently.
	    * 
	    * Doors are only seen as "seenThrough" if they are
	    * activated.
	    */
		if(wallID == 4)
		{
			seeThrough = true;
		}
		
	   /*
	    * Sets whether the wall is solid or not directly related to the
	    * ID of the wall.
	    */
		if(wallID == 0)
		{
			isSolid = false;
		}
		else
		{
			isSolid = true;
		}
	}
	
   /**
    * Adds sprites to this blocks sprites.
    * @param s
    */
	public void addSprites(Sprite s)
	{
		sprites.add(s);
	}

	@Override
   /**
	* Compares two blocks together depending on their height.
	* Compares in terms of descending height so that higher walls are
	* rendered before shorter blocks.
	*/
	public int compareTo(Object block) 
	{
		//Gets Integer height of the block being compared
		int comparedInteger = (int)((Block)(block)).height;
		
	   /*
	    * Compares the two blocks being compared, and sends back the
	    * result. This particular comparison would cause the list
	    * to be sorted in decending order of height. If you switched
	    * this.height and comparedInteger it would be sorted in
	    * ascending order.
	    */
		return comparedInteger - (int)this.height;
	}
	
   /**
    * Compare both blocks and sort the blocks in terms of descending x
    * values. If the x values are the same, then compare them in
    * descending order in terms of z.
    * 
    * Only is called with the player is looking in the first quadrant
    * of the map.
    */
	public static Comparator<Block> ninetyDegrees 
		= new Comparator<Block>() 
	{
			public int compare(Block b1, Block b2) 
			{			   
			    if(b1.x == b2.x)
			    {
			    	return b2.z - b1.z; 
			    }

			   		/*Descending*/
			    return b2.x-b1.x;
			   
			}
	};
	
   /**
    * Compare both blocks and sort the blocks in terms of descending x
    * values. If the x values are the same, then compare them in
    * ascending order in terms of z.
    * 
    * Only is called with the player is looking in the second quadrant
    * of the map.
    */
	public static Comparator<Block> oneEightyDegrees 
		= new Comparator<Block>() 
	{
			public int compare(Block b1, Block b2) 
			{			   
			    if(b1.x == b2.x)
			    {
			    	return b1.z - b2.z; 
			    }

			   	/*Descending*/
			   	return b2.x-b1.x;
			   
			}
	};
	
   /**
    * Compare both blocks and sort the blocks in terms of ascending x
    * values. If the x values are the same, then compare them in
    * ascending order in terms of z.
    * 
    * Only is called with the player is looking in the third quadrant
    * of the map.
    */
	public static Comparator<Block> twoSeventyDegrees 
		= new Comparator<Block>() 
	{
			public int compare(Block b1, Block b2) 
			{			   
			    if(b1.x == b2.x)
			    {
			    	return b1.z - b2.z; 
			    }

			   	/*Descending*/
			   	return b1.x-b2.x;
			   
			}
	};
	
   /**
    * Compare both blocks and sort the blocks in terms of ascending x
    * values. If the x values are the same, then compare them in
    * descending order in terms of z.
    * 
    * Only is called with the player is looking in the fourth quadrant
    * of the map.
    */
	public static Comparator<Block> threeSixtyDegrees 
		= new Comparator<Block>() 
	{
			public int compare(Block b1, Block b2) 
			{			   
			    if(b1.x == b2.x)
			    {
			    	return b2.z - b1.z; 
			    }

			   	return b1.x-b2.x;
			   
			}
	};
}
