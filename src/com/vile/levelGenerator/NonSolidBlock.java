package com.vile.levelGenerator;

/**
 * Title: NonSolidBlock
 * @author Alex Byrd
 * Date Updated: 7/26/2016
 * 
 * Description:
 * Creates a non solid block. At the moment all this does is make sure
 * that the block is not solid.
 *
 */
public class NonSolidBlock extends Block
{
   /**
    * Constructs the non solid block
    * @param h
    * @param wallID
    */
	public NonSolidBlock(int h, int wallID, double y, int x, int z)
	{
		super(h, wallID, y, x, z);
		isSolid = false;
	}
}
