package com.vile.levelGenerator;

import java.util.Random;

import com.vile.Display;
import com.vile.Game;
import com.vile.valueHolder;
import com.vile.entities.Button;
import com.vile.entities.Corpse;
import com.vile.entities.Door;
import com.vile.entities.Elevator;
import com.vile.entities.Enemy;
import com.vile.entities.ExplosiveCanister;
import com.vile.entities.HurtingBlock;
import com.vile.entities.Item;
import com.vile.entities.Player;
import com.vile.graphics.Sprite;
import com.vile.input.Controller;

/**
 * Title: Level
 * @author Alex Byrd
 * Date Updated: 9/16/2016
 *
 * Generates a level of blocks with set positions for all of them.
 * Blocks can be nonSolid meaning they are not seen and can be
 * walked through or have entities spawn in them, or solid where
 * they will have a certain texture but can't be walked through.
 */
public class Level 
{
	public static Block[] blocks;
	public static int width = 0;
	public static int height = 0;
	
   /**
    * First type of level constructor used to construct a random level.
    * @param w
    * @param h
    */
	public Level(int w, int h)
	{
		this.width = w;
		this.height = h;
		blocks = new Block[width * height];
		
		Random random = new Random();
		
		for(int z = 0; z < height; z++)
		{
			for(int x = 0; x < width; x++)
			{
				Block block = null;
				
				int randomWall = random.nextInt(18);
				
				if(randomWall == 0)
				{
					randomWall = 1;
				}
				
				//One in 5 chance the block becomes solid
				if(random.nextInt(100) == 0)
				{

					block = new Block(24, randomWall, 0, x, z);
				}
				else
				{
					block = new Block(0, 0, 0, x, z);
				}
				
			   /*
			    * Generates a new block at a certain position in the
			    * level.
			    */
				blocks[x + z * width] = block;
			}
		}
		
	}
	
   /**
    * Uses a 2 dimensional array sent in to create a map of different
    * types of walls.
    * @param map
    */
	public Level(valueHolder[][] map)
	{
		//Get height of map from 2D array column
		height = map[0].length;
		
		//Get width from 2D array row
		width  = map.length;
		
		//Array of blocks that make up map
		blocks = new Block[width * height];
		
		//For each element in the map
		for(int i = 0; i < width; i++)
		{
			for(int j = 0; j < height; j++)
			{
				//Each block is null until instantiated
				Block block = null;
				
				//Gets the itemID at each location
				int itemID = map[i][j].entityID;
				
				//Gets each blocks height
				block = new Block(map[i][j].height,
						map[i][j].wallID, 0, i, j);
				
				//Sets the block at that location in the level.
				blocks[i + j * width] = block;
				
				//Normal items (Keys, healthpacks, ammo, etc...)
				if(itemID > 0 && itemID <= 7 || itemID == 21
						|| itemID > 23 && itemID < 44
						|| itemID >= 47 && itemID != 53
						&& itemID != 58 && itemID != 59)
				{
					if(itemID != 32)
					{
						Game.items.add(new Item(5, 
							i + 0.5, 
							block.height - block.y, 
							j + 0.5, itemID));
					}
					else
					{
						new ExplosiveCanister(5, 
								i + 0.5, 
								block.height - block.y, 
								j + 0.5, itemID);
					}
				}
				//Players spawn
				else if(itemID == 8)
				{
				   /*
				    * Corrects for position in map, and places the player
				    * directly in the center of the block that he/she
				    * is placed at in the map file.
				    */
					Player.x = i + 0.5;
					Player.z = j + 0.5;
					Player.y = block.y + block.height;
				}
				//End button or normal button
				else if(itemID == 9 || itemID == 53)
				{
					Game.buttons.add(new Button( 
							i + 0.5, 
							0.77, j + 0.5, itemID));
				}
				//Lift/elevator
				else if(itemID == 10)
				{
					Game.elevators.add(new Elevator( 
							i + 0.5, 
							0.77, j + 0.5, i, j));
				}
				//Normal door
				else if(itemID == 11)
				{
					Game.doors.add(new Door( 
							i + 0.5, 
							0.77, j + 0.5, i, j, 0));
				}
				//Red door
				else if(itemID == 12)
				{
					Game.doors.add(new Door( 
							i + 0.5, 
							0.77, j + 0.5, i, j, 1));
				}
				//Blue door
				else if(itemID == 13)
				{
					Game.doors.add(new Door( 
							i + 0.5, 
							0.77, j + 0.5, i, j, 2));
				}
				//Green Door
				else if(itemID == 14)
				{
					Game.doors.add(new Door( 
							i + 0.5, 
							0.77, j + 0.5, i, j, 3));
				}
				//Yellow door
				else if(itemID == 15)
				{
					Game.doors.add(new Door( 
							i + 0.5, 
							0.77, j + 0.5, i, j, 4));
				}
				//Adds brainstormer enemy
				else if(itemID == 16)
				{
					Game.enemies.add(new Enemy(
							i + 0.5, 
							0, 
							j + 0.5, 1));
				}
				//Adds flyerdemon at this location
				else if(itemID == 17)
				{
					Game.enemies.add(new Enemy(
							i + 0.5, 0.77, 
							j + 0.5, 2));
				}
				//Adds CrocGaurdian at this location
				else if(itemID == 18)
				{
					Game.enemies.add(new Enemy(
							i + 0.5, 0.77, 
							j + 0.5, 3));
				}
				//Adds Reaper at this location
				else if(itemID == 19)
				{
					Game.enemies.add(new Enemy(
							i + 0.5, 0.77, 
							j + 0.5, 4));
				}
				//Adds Vile Civilian at this location
				else if(itemID == 58)
				{
					Game.enemies.add(new Enemy(
							i + 0.5, 0.77, 
							j + 0.5, 7));
				}
				//BELEGOTH is added
				else if(itemID == 59)
				{
					Game.enemies.add(new Enemy(
							i + 0.5, 0.77, 
							j + 0.5, 8));
				}
				//Adds secret at this location
				else if(itemID == 20)
				{
					Game.items.add(new Item(5, 
							i + 0.5, 
							0, j + 0.5, itemID));
				}
				//Toxic waste
				else if(itemID == 22)
				{
					Game.hurtingBlocks.add(new HurtingBlock( 
							i + 0.5, 
							0.77, j + 0.5, i, j, 0));
				}
				//Lava
				else if(itemID == 23)
				{
					Game.hurtingBlocks.add(new HurtingBlock( 
							i + 0.5, 
							0.77, j + 0.5, i, j, 1));
				}
				//Corpse
				else if(itemID == 44)
				{
					Game.corpses.add(new Corpse( 
							i + 0.5,  
							j + 0.5,
							block.height - block.y, 0));
				}
				//Adds resurrecter at this location
				else if(itemID == 45)
				{
					Game.enemies.add(new Enemy(
							i + 0.5, 0.77, 
							j + 0.5, 5));
				}
				//The boss MORGOTH
				else if(itemID == 46)
				{
					Game.enemies.add(new Enemy(
							i + 0.5, 0.77, 
							j + 0.5, 6));
				}
			}
		}
		
		//Sets the amount of enemies in the map
		Game.enemiesInMap = Game.enemies.size();
	}
	
   /**
    * Get a block at a given cooridinate
    * @param x
    * @param z
    * @return
    */
	public static Block getBlock(int x, int z)
	{
	   /*
	    * If the x and y are larger than the size of the level, then
	    * create it as a solid block to create a solid block border
	    * around the level that will be impassable.
	    */
		if(x < 0 || x >= width || z < 0 || z >= height)
		{
			return new SolidBlock(12, 1, 0, x, z);
		}
		
		//Return block to be generated.
		return blocks[x + (z * width)];
	}
	
	
}
