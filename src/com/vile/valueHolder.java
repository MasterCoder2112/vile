package com.vile;

/**
 * Title: valueHolder
 * 
 * @author Alex Byrd 
 * Date Updated: 9/16/2016
 *
 * Purpose:
 * To hold each slot of the map text files values for wallHeight, wallID,
 * and entityID on the block.
 */
public class valueHolder 
{
	public double height = 12;
	public int wallID    = 1;
	public int entityID  = 0;
	
   /**
    * Sets and stores the values in each value holder.
    * @param height
    * @param wallID
    * @param ID
    */
	public valueHolder(double height, int wallID, int ID) 
	{
		this.height = height;
		this.wallID = wallID;
		entityID = ID;
	}

}
