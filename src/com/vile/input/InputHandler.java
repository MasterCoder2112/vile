package com.vile.input;

import java.awt.Dimension;
import java.awt.Robot;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import com.vile.Display;
import com.vile.entities.Player;
import com.vile.launcher.FPSLauncher;

/**
 * Title: InputHandler
 * @author Alex Byrd
 * Date Updated: 7/26/2016
 * 
 * Description:
 * Deals with all the input devices such as the keyboard and mouse, using
 * listeners. It then instantiates all the required listener
 * methods and deals with events accordingly.
 * 
 * The listeners are interfaces.
 *
 */
public class InputHandler implements KeyListener, MouseListener, MouseMotionListener, FocusListener 
{
	//All the possible combinations on the keyboard im guessing
	public boolean[] key = new boolean[68836];
	
   /*
    * Mouse position on screen
    */
	public static int MouseX;
	public static int MouseY;
	
	/*
	 * Whether the mouse just exited the screen and was reset. If it
	 * was this will count to 5, once at 5 it resets to 0. During these 5
	 * quick ticks, the game will not make the player look because if it
	 * allowed that, the movement of the mouse would be so great within 
	 * one tick that the player would look extremely fast in a certain
	 * direction, distorting gameplay.
	 */
	public static int mouseLoop = 0;
	
	private Robot robot;
	
	
	@Override
   /**
    * Checks if keyCode is valid, and then sets that key to true if it
    * is.
    * @param e
    */
	public void keyPressed(KeyEvent e) 
	{
		int keyCode = e.getKeyCode();
		
		if(keyCode > 0 && keyCode < key.length)
		{
			key[keyCode] = true;
		}
		
	}

	@Override
   /**
    * Checks if the keyCode of the key released is valid, then sets that
    * key to false if it is.
    * @param e
    */
	public void keyReleased(KeyEvent e) 
	{
		int keyCode = e.getKeyCode();
		
		if(keyCode > 0 && keyCode < key.length)
		{
			key[keyCode] = false;
		}
		
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void focusGained(FocusEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void focusLost(FocusEvent arg0)
	{
		for(int i = 0; i < key.length; i++)
		{
			key[i] = false;
		}
		
	}

	@Override
	public void mouseClicked(MouseEvent e) 
	{
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	/**
	 * Is called when the mouse exits the frame,
	 * and it uses a Robot objects to move the cursor
	 * back to a certain position on the screen.
	 */
	public void mouseExited(MouseEvent e) 
	{
		try 
	    {
			robot = new Robot();
	    } 
	    catch (Exception ex) 
	    {
	    	ex.printStackTrace();
	    }
		
		if (robot != null) 
		{
			if(e.getX() > Display.WIDTH - 50 || e.getX() < 0)
			{
				robot.mouseMove(600, e.getYOnScreen());
			}
			
			if(e.getY() > Display.HEIGHT - 50 || e.getY() < 0)
			{
				robot.mouseMove(e.getXOnScreen(), 400);
			}
        
        }
		
		mouseLoop++;
	}

	@Override
	public void mousePressed(MouseEvent arg0) 
	{
		Controller.shot = true;	
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseDragged(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	/**
	 * Checks if mouse was moved, If it was,
	 *  get the new mouse x and y coordinates on the screen
	 */
	public void mouseMoved(MouseEvent e) 
	{
		if(mouseLoop > 0)
		{
			mouseLoop++;
		}
		
		if(mouseLoop > 5)
		{
			mouseLoop = 0;
		}
		
        MouseX = e.getX();
        MouseY = e.getY();  
	}

}
