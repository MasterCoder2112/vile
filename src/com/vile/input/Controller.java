package com.vile.input;

import com.vile.Display;
import com.vile.Game;
import com.vile.entities.Button;
import com.vile.entities.Cartridge;
import com.vile.entities.Door;
import com.vile.entities.Elevator;
import com.vile.entities.Enemy;
import com.vile.entities.Item;
import com.vile.entities.Player;
import com.vile.graphics.Render3D;
import com.vile.levelGenerator.Block;
import com.vile.levelGenerator.Level;

/**
 * Title: Controller
 * @author Alex Byrd
 * Date Updated: 5/10/2016
 *
 * Takes all the input from the controls sent from game, and performs all
 * the actions of the game and helps set the variables used by Render3D
 * to render the 3D environment of the game correctly as the Player moves.
 * 
 * Also it updates movement, and detects collision.
 */
public class Controller 
{
   /*
    * Position and movement variables.
    */
	private double xa;
	private double za;
	private double upRotationa;
	private double rotationa;
	
   /*
    * Variables used to check certain cases, so that some actions do not
    * activate if another one is activated. For instance, if your in a 
    * jump you cannot jump again, and etc...
    */
	private boolean inJump            = false;
	private boolean crouching         = false;
	
	//If crouching while falling, only increase damage once
	private boolean once              = false;
	
   /*
    * Handles time between when you can activate certain events
    */
	private int     time;
	private int     time2;
	
	//Used to keep track of each Clip in the clip arrays
	public static int     clipNum;
	public static int     clipNum2;
	public static int     clipNum3;
	public static int     clipNum4;
	public static int     clipNum5;
	
	//Handles maximum jumpHeight for each jump
	private double jumpHeight = 8;
	
   /*
    * Variables used in falling acceleration (gravity)
    */
	private double  fallSpeed         = 0.4;
	private double  acceleration      = 0.03;
	private double  fallHeight        = 0;
	
   /*
    * Debugging variables
    */
	public static boolean noClipOn;
	public static boolean flyOn;
	public static boolean superSpeedOn;
	public static boolean godModeOn;
	
   /*
    * Other variables
    */
	public static boolean mouseLeft      = false;
	public static boolean mouseRight     = false;
	public static boolean mouseUp        = false;
	public static boolean mouseDown      = false;
	public static boolean showFPS        = false;
	public static boolean quitGame       = false;
	public static double moveSpeed       = 1.0;
	public static double rotationUpSpeed = 0.01;
	public static int 	 timeAfterShot;
	public static boolean shot        = false;
	
	private boolean firstCheck = false;
	private int useTime = 0;
	
	
   /**
    * Takes all the values in from reading the keyboard, and
    * activates certain things depending on what values are true
    * or not. This method also the x, z, and rotation speeds so that
    * they can be used by the Render3D method to move the player 
    * the corresponding ways.
    * 
    * Also takes care of shooting, flying, and other such events.
    */
	public void performActions(Game game)
	{
	   /*
	    * Reset these variables each time it runs through.
	    */
		double rotationSpeed = 0.03;
		double horizontalMouseSpeed = 0.005 * Display.mouseSpeedHorizontal;
		double verticalMouseSpeed = 0.001 * Display.mouseSpeedVertical;
		
		//System.out.println("H"+horizontalMouseSpeed);
		//System.out.println("V"+verticalMouseSpeed);
		
		//Amount your moving in x or z direction each time it loops
		double moveX         = 0;
		double moveZ         = 0;
		
	   /*
	    * If the player spawns in a block, put the player on top of it.
	    * Just invoking this method will determine the height the player
	    * will spawn at anyway.
	    */
		if(!isFreeZ(Player.x, Player.z))
		{
		}
		
	   /*
	    * If on the moon, change the jumpHeight to 20, and reduce the
	    * acceleration due to gravity.
	    */
		if(Display.themeNum == 4)
		{
			acceleration = 0.005;
		}
		
	   /*
	    * Only allow these actions if the player is alive
	    */
		if(Player.alive)
		{
		   /*
		    * If you shoot, and there is some delay between this shot and
		    * the last one, reduce ammo by 1, set shot = true, and add to
		    * time to keep track of when you last shot.
		    */
			if(Game.shoot)
			{
				shot = true;
			}
			
			//If moving foward
			if(Game.foward)
			{
				moveZ += 1/21.3;
			}
		
			//If moving back
			if(Game.back)
			{
				moveZ -= 1/21.3;
			}
		
			//If strafing left
			if(Game.left)
			{
				moveX -= 1/21.3;
			}
		
			//If strafing right
			if(Game.right)
			{
				moveX += 1/21.3;
			}
		
			//If turning left
			if(Game.turnLeft)
			{
				rotationa -= rotationSpeed;
			}
		
			//If turning right
			if(Game.turnRight)
			{
				rotationa += rotationSpeed;
			}
			
			//If looking up
			if(Game.turnUp)
			{
				upRotationa += rotationUpSpeed;
			}
		
			//If looking down
			if(Game.turnDown)
			{
				upRotationa -= rotationUpSpeed;
			}
		
			//If mouse goes left
			if(mouseLeft)
			{
				rotationa -= horizontalMouseSpeed;
				
				if(rotationa < -0.6)
				{
					rotationa = 0;
				}
			}
		
			//If mouse moves right
			if(mouseRight)
			{
				rotationa += horizontalMouseSpeed;
				
				if(rotationa < 0 || rotationa > 0.3)
				{
					rotationa = 0;
				}
			}
			
			//If mouse moves up
			if(mouseUp)
			{
				upRotationa -= verticalMouseSpeed;
				
				if(upRotationa < -0.1)
				{
					upRotationa = 0;
				}
			}
		
			//If mouse moves down
			if(mouseDown)
			{
				upRotationa += verticalMouseSpeed;
				
				if(upRotationa > 0.1)
				{
					upRotationa = 0;
				}
			}
		
			//If running, set moveSpeed to 1.5 times the value it was
			if(Game.run)
			{
				moveSpeed *= 1.5;
			}
			
		   /*
		    * If you are pressing crouch, and not going up because of
		    * jumping. Half the moveSpeed, set crouching = true, and keep
		    * performing a given operation while you are crouching.
		    */
			if(Game.crouch && !inJump)
			{
				moveSpeed = 0.5;
				crouching = true;
				
				//If still not as low as you can go
				if(Player.y > -6.0 + Player.maxHeight) 
				{
					//If flying, just fly down
					if(flyOn)
					{
						Player.y  -= 0.8;
					}
					//If falling from getting out of flyMode
					else if(fallHeight > 0)
					{
					   /*
					    * If you are falling, your not crouching, but
					    * you are increasing your speed downward due to
					    * becoming smaller, and so your speed increases.
					    */
						crouching   = false;
						fallSpeed  *= 1.005;
						
					   /*
					    * Only increase the acceleration once when falling
					    * and hunkering down while falling
					    */
						if(!once)
						{
							fallHeight *= 1.1;
							once = true;
						}
					}
					//If not falling, just crouch at the normal speed
					else
					{
						Player.y  -= 0.5;
						Player.height -= 0.17;
					}
				}
				
			
				//If there is some decimal error, round to -6.0
				if(Player.y <= -6.0 + Player.maxHeight)
				{
					Player.y = -6.0 + Player.maxHeight;
					Player.height = 0;
				}
			}
			//If not crouching
			else
			{
				crouching = false;
				
			   /*
			    * If you're not standing at your normal height yet,
			    * keep rising till your back at your normal height
			    */
				if(Player.y < 0 + Player.maxHeight)
				{
					Player.y += 0.4;
					Player.height += 1.7;
				}
				
				if(Player.height > 2)
				{
					Player.height = 2;
				}
			}
		
		   /*
		    * If fly mode is not on, jump like normal
		    */
			if(!flyOn)
			{
				//Gets the block to check if the blocks an elevator
				Block temp = Level.getBlock((int)Player.x, (int)Player.z);
				
			   /*
			    * If the Player is trying to jump, and the player is on
			    * the ground, then jump. If the Player is on an elevator
			    * then add some error to being on "the ground" because
			    * the elevator is moving making it so that technically
			    * the player is never "on the ground" because what was
			    * ground zero is now higher because the elevator is
			    * constantly moving.
			    */
				if(Game.jump && Player.y == 0 + Player.maxHeight
						|| Game.jump && 
						Player.y <= 0.5 + Player.maxHeight &&
						Player.y >= 0 + Player.maxHeight)
				{
					inJump = true;
					jumpHeight = 8 + Player.maxHeight;
					
					//If on the moon, add to maximum jumpHeight
					if(Display.themeNum == 4)
					{
						jumpHeight  += 12;
					}
				}
			
			   /*
			    * If you haven't reached your max jumpHeight yet then keep
			    * going up, but if you have, then stop going up.
			    */
				if(inJump && Player.y < jumpHeight)
				{
					Player.y += 0.4;
				}
				else if(inJump && Player.y >= jumpHeight
						&& Player.y < jumpHeight + 1)
				{
					inJump = false;
				}
				
				//If you are above the ceiling, get back to the ceiling
				if(Player.y >= Render3D.ceilingDefaultHeight)
				{
					Player.y = Render3D.ceilingDefaultHeight;
				}
				
				//If you're falling, accelerate down
				if(Player.y > 0 + Player.maxHeight && !crouching && !inJump)
				{
					Player.y -= fallSpeed;
					fallSpeed = fallSpeed + (fallSpeed * acceleration);
				}
			}
			else
			{
				if(Game.jump)
				{
					Player.y += 0.8;
				}
				
				//If you are above the ceiling, get back to the ceiling
				if(Player.y >= Render3D.ceilingDefaultHeight)
				{
					Player.y = Render3D.ceilingDefaultHeight;
				}
			}
		
			//If Player.y is negligibly close to 0, set it to 0
			if(Player.y < 0.4 + Player.maxHeight && Player.y > -0.4 + Player.maxHeight)
			{
				Player.y = 0 + Player.maxHeight;
				inJump     = false;
				fallSpeed  = 0.4;
			}
			
		   /*
		    * If you fell so hard you went below the normal Player.y for
		    * crouching, then set the Player.y equal to -7 to
		    * not go below the map.
		    */
			if(Player.y <= -7 + Player.maxHeight && !crouching)
			{
				Player.y     = -7 + Player.maxHeight;
				inJump         = false;
				fallSpeed      = 0.4;
			}
			
		   /*
		    * If you are on the ground, calculate whether you are getting
		    * fall damage or not. If you havent fallen, then you recieve
		    * no damage.
		    */
			if(Player.y <= 0 + Player.maxHeight)
			{
				if(!godModeOn && Player.immortality == 0)
				{
					Player.health -= (int) (5 * (fallHeight / 25));
				}
				
				fallHeight     = 0;
			}
			
			//If gun shot
			if(shot)
			{
				shot = false;
				//See if weapon can fire
				if(!Player.weapons[Player.weaponEquipped].shoot())
				{
					//Only play sound if weapon can't fire and its out of ammo
					if(Player.weapons[Player.weaponEquipped].ammo <= 0)
					{
						//Call the play audio method to play the sound
						Display.playAudioFile(Display.input7, Display.ammoOut, clipNum2);
						
						//Update sound clip being played
						clipNum2++;
						
						//If the clipNum is past the length, reset it
						if(clipNum2 == Display.ammoOut.length)
						{
							clipNum2 = 0;
						}
					}
				}
			}
			
			//If the player chooses the first weapon slot
			//Set that weapon as being equipped
			if(Game.weaponSlot0)
			{
				Player.weaponEquipped = 0;
			}
			
			//If Player chooses the second weapon slot
			//Set that weapon as being equipped
			if(Game.weaponSlot1 && Player.weapons[1].canBeEquipped)
			{
				Player.weaponEquipped = 1;
			}
			
			//If Player chooses the second weapon slot
			//Set that weapon as being equipped
			if(Game.weaponSlot2 && Player.weapons[2].canBeEquipped)
			{
				Player.weaponEquipped = 2;
			}
			
		   /*
		    * If you try to reload, and there are still cartridges with
		    * ammo in them, and some time has passed since the last
		    * reload, then reload.
		    */
			if(Game.reloading &&  time == 0)
			{
			    if(Player.weapons[Player.weaponEquipped].reload())
			    {
			    	time++;
			    	
					//Call the play audio method to play the sound
					Display.playAudioFile(Display.input8, Display.reload, clipNum3);
					
					//Update sound clip being played
					clipNum3++;
					
					//If the clipNum is past the length, reset it
					if(clipNum3 == Display.reload.length)
					{
						clipNum3 = 0;
					}
			    }
			}
		}
		
		//If time is not 0, add to it
		if(time != 0)
		{
			time++;
		}
		
		//If 25 ticks have passed, set time to 0
		if(time == 25)
		{
			time = 0;
		}
		
		//Do the same thing with time2 for shooting
		if(time2 != 0)
		{
			time2++;
		}
		
		//Allow shooting to be faster though, so 11 instead of 25 ticks
		if(time2 == 11)
		{
			time2 = 0;
		}
		
	   /*
	    * If you want to show or not show fps, and some time has passed
	    * since you last performed the action, then flip the status of
	    * showFPS, and then start the ticks again since this action was
	    * last performed.
	    */
		if(Game.fpsShow && time == 0)
		{
			if(showFPS)
			{
				showFPS = false;
			}
			else
			{
				showFPS = true;
			}
			
			time++;
		}
		
		//If you want to quit the game, set quitGame to true
		if(Game.pause)
		{
			//Game is paused
			Display.pauseGame =  true;
			
			quitGame = true;		
		}
		
	   /*
	    * If the player is trying to use an entity such as a button,
	    * elevator or door, if the player is within a small distance
	    * of that particular entity then activate that entity.
	    * 
	    * Each time check all of these entities in the game to see if
	    * the player is within range of them.
	    */
		if(Game.use && Player.alive)
		{
			//If nothing is activated, this will activate the oomf sound
			boolean anyActivated = false;
			
			//Activate button is button pressed
			if(Game.buttons.size() > 0)
			{
				for(int i = 0; i < Game.buttons.size(); i++)
				{
					Button button = Game.buttons.get(i);
					
					if(Math.abs(button.getZ() - Player.z) <= 0.95
							&& Math.abs(button.getX() - Player.x) <= 0.95)
					{
						button.activated = true;
						anyActivated = true;
						
						//Call the play audio method to play the sound
						Display.playAudioFile
						(Display.input10, Display.buttonPress, clipNum4);
						
						//Update sound clip being played
						clipNum4++;
						
						//If the clipNum is past the length, reset it
						if(clipNum4 == Display.buttonPress.length)
						{
							clipNum4 = 0;
						}
						
						//Reset useTime and start it again
						useTime = 0;
						useTime++;
					}
				}
			}
			
			for(int i = 0; i < Game.doors.size(); i++)
			{
				Door door = Game.doors.get(i);
				
				if(Math.abs(door.getZ() - Player.z) <= 0.95
						&& Math.abs(door.getX() - Player.x) <= 0.95)
				{
					if(door.doorType == 0 ||
							door.doorType == 1 && Player.hasRedKey ||
							door.doorType == 2 && Player.hasBlueKey ||
							door.doorType == 3 && Player.hasGreenKey ||
							door.doorType == 4 && Player.hasYellowKey)
					{
						door.activated = true;
						anyActivated = true;
					}
					else
					{
						if(door.doorType == 1 && !Player.hasRedKey)
						{
						   /*
						    * Displays that the player cannot open the
						    * door without the red key.
						    */
							Display.itemPickup = 
									"You need the red keycard!";
							Display.itemPickupTime = 1;
						}
						else if(door.doorType == 2 && !Player.hasBlueKey)
						{
						   /*
						    * Displays that the player cannot open the
						    * door without the blue key.
						    */
							Display.itemPickup = 
									"You need the blue keycard!";
							Display.itemPickupTime = 1;
						}
						else if(door.doorType == 3 && !Player.hasGreenKey)
						{
						   /*
						    * Displays that the player cannot open the
						    * door without the green key.
						    */
							Display.itemPickup = 
									"You need the green keycard!";
							Display.itemPickupTime = 1;
						}
						else
						{
						   /*
						    * Displays that the player cannot open the
						    * door without the yellow key.
						    */
							Display.itemPickup = 
									"You need the yellow keycard!";
							Display.itemPickupTime = 1;
						}
					}
				}
			}
			
			for(int i = 0; i < Game.elevators.size(); i++)
			{
				Elevator elevator = Game.elevators.get(i);
				
				if(Math.abs(elevator.getZ() - Player.z) <= 0.95
						&& Math.abs(elevator.getX() - Player.x) <= 0.95)
				{
					elevator.activated = true;
					anyActivated = true;
					
					//Reset useTime and start it again
					useTime = 0;
					useTime++;
				}
			}
			
			if(!anyActivated && useTime == 0)
			{
				//Call the play audio method to play the sound
				Display.playAudioFile(Display.input9, Display.tryToUse, clipNum5);
				
				//Update sound clip being played
				clipNum5++;
				
				//If the clipNum is past the length, reset it
				if(clipNum5 == Display.tryToUse.length)
				{
					clipNum5 = 0;
				}
				
				//Begin the next waiting cycle to play sound again
				useTime++;
			}
			
			//Keep ticking the wait time until it reaches 101
			if(useTime < 21 && useTime > 0)
			{
				useTime++;
			}
			//At 21 reset it to 0
			else
			{
				useTime = 0;
			}
		}
	   /*
	    * If player is dead, allow player to respawn at beginning of the 
	    * map so he/she does not have to replay through the whole game,
	    * but start them out with their default values, kind of like in
	    * DOOM when you die. Usually if your skilled enough you can
	    * survive and continue on going.
	    */
		else if(Game.use && !Player.alive)
		{
			new Player();
			
			if(Game.setMap)
			{
				game.loadNextMap();
			}
			else
			{
				new Display();
			}
		}
		else
		{
			//If the player is not trying to use a wall, reset useTime
			useTime = 0;
		}
		
	   /*
		* If you want to noclip, and some time has passed
		* since you last performed the action, then flip the status of
		* noClip, and then start the ticks again since this action was
		* last performed.
		*/
		if(Game.noClip && time == 0)
		{
			time++;
			
			if(!noClipOn)
			{
				noClipOn = true;
			}
			else
			{
				noClipOn = false;
			}
		}
		
		//Same as above just for godMode
		if(Game.godMode && time == 0)
		{
			time++;
			
			if(!godModeOn)
			{
				godModeOn = true;
			}
			else
			{
				godModeOn = false;
			}
		}
		
	   /*
	    * Same as above, except for this time if flyMode is off
	    * reset the fallSpeed, and turn inJump (meaning you are currently
	    * jumping), and up (Means your going up in the jump) off, while
	    * also turning flyMode on. 
	    * 
	    * If flyMode is on, then turn it off, but then set the fallHeight
	    * to your current height indicated by Player.y to start your
	    * decent through the air. If you are on the moon, in order to
	    * decrease the damage you recieve when hitting the ground, it
	    * decreases the fallHeight you are at (even though technically
	    * you are at the same height). 
	    */
		if(Game.fly && time == 0)
		{
			if(!flyOn)
			{
				fallSpeed = 0.4;
				flyOn     = true;
				
				//No longer in a jump
				inJump    = false;		
			}
			else
			{
				flyOn      = false;
				fallHeight = Player.y - Player.maxHeight;
				
				//If on the moon, falling has less of a health impact
				if(Display.themeNum == 4)
				{
					fallHeight /= 6;
				}
			}
			
			time++;
		}
		
	   /*
	    * Same as other debug modes.
	    */
		if(Game.superSpeed && time == 0)
		{
			if(!superSpeedOn)
			{
				superSpeedOn = true;
			}
			else
			{
				superSpeedOn = false;
			}
			
			time++;
		}
		
		//If super speed is on, multiply players speed by 4
		if(superSpeedOn)
		{
			moveSpeed *= 4;
		}
		
		if(Game.restock)
		{
			Player.weapons[0].ammo = 40;
			Player.weapons[1].ammo = 40;
			Player.weapons[2].ammo = 20;
		}
			
	   /*
	    * Calculates the change of x and z depending on which direction
	    * the player is moving, and the players rotation. The players 
	    * rotation is used to determine how the graphics are rotated and
	    * drawn around the player using sin and cos to make a complete
	    * circle around the player.
	    */
		xa += ((moveX * Math.cos(Player.rotation)) +
				(moveZ * Math.sin(Player.rotation))) * moveSpeed;
		za += ((moveZ * Math.cos(Player.rotation)) -
				(moveX * Math.sin(Player.rotation))) * moveSpeed;
		
	   /*
	    * These determine if the space to the side of the player
	    * is a solid block or not. If its not then move the player,
	    * if it is then don't execute movement in that direction.
	    * 
	    * If noclip is on, you can clip through walls, so you can
	    * do this anyway.
	    */
		if(isFreeX(Player.x + xa, Player.z) || noClipOn)
		{
			Player.x += xa;
		}
		
	   /*
		* These determine if the space in front or back of the player
		* is a solid block or not. If its not then move the player,
		* if it is then don't execute movement in that direction.
		* 
		* If noclip is on, you can clip through walls, so you can
		* do this anyway.
		*/
		if(isFreeZ(Player.x, Player.z + za) || noClipOn)
		{
			Player.z += za;
		}

	   /*
	    * Causes the players movement to quickly (but not 
	    * instantaniously) move to 0 if there is not reset in
	    * the players movement.
	    */
		xa *= 0.1;
		za *= 0.1;
		
		//Increase the players rotation based on change in rotation
		Player.rotation += rotationa;
		
		//Decreases rotation steadily if you stop rotating
		rotationa = 0.0d;
		
		//Does the same things with upRotation
		Player.upRotate += upRotationa;
		
		upRotationa = 0.0d;
		
		//Sets lower bound on upRotation
		if(Player.upRotate >= 2.8)
		{
			Player.upRotate = 2.8;
		}
				
		//Sets upper bound on upRotation
		if(Player.upRotate <= 0.3)
		{
			Player.upRotate = 0.3;
		}		
	}
	
   /**
    * Is called with a given x and z value to determine if the block the
    * player is about to move to is free to move to or not. If it is
    * determined to be solid, you cannot move into it, and it stops your
    * movement, if not obviously you can move through it.
    * 
    * In terms of recent updates, if the block is higher than the players
    * position and height in the y direction, then the player can also
    * move under it. This is mainly used for doors, but will also come
    * in handy later for multi floored maps.
    * 
    * Since the 1.4 Update there are two methods, each is called 
    * individually depending on the direction the player is moving so
    * that at any height the walls will detect your collision correctly
    * no matter what the situation is. 
    * 
    * @param xx
    * @param yy
    * @return
    */
	public boolean isFreeZ(double xx, double zz)
	{
		// Number used for how far away from block, block detects
		double z = -0.25;
		
		if(zz < Player.z)
		{
			z = -0.25;
		}
		else
		{
			z = 0.25;
		}
		
		Block block5 = Game.level.getBlock((int)
				(Math.round(Player.x)), (int)(Math.round(Player.z)));
		
		//Go through all the solid items in the game
		for(int i = 0; i < Game.solidItems.size(); i++)
		{
			Item temp = Game.solidItems.get(i);
			
			//Distance between item and player
			double distance = Math.sqrt(((Math.abs(temp.x - xx))
					* (Math.abs(temp.x - xx)))
					+ ((Math.abs(temp.z - zz))
							* (Math.abs(temp.z - zz))));
			
			double playerY = Player.y;
			
			if(playerY < 0)
			{
				playerY = 0;
			}
			
			//Difference in y
			double yDifference = playerY - Math.abs(temp.y);
			
			//If close enough, don't allow the player to move into it.
			if(distance <= 0.3 && yDifference <= temp.height
					&& yDifference >= -temp.height)
			{
				return false;
			}	
			else if(distance <= 0.3 && yDifference > temp.height)
			{
				Player.maxHeight 
					= temp.height + Math.abs(temp.y) + 5;
				return true;
			}	
		}
		
		//Go through all the enemies in the game
		for(int i = 0; i < Game.enemies.size(); i++)
		{
			Enemy temp = Game.enemies.get(i);
			
			//Distance between enemy and player
			double distance = Math.sqrt(((Math.abs(temp.xPos - xx))
					* (Math.abs(temp.xPos - xx)))
					+ ((Math.abs(temp.zPos - zz))
							* (Math.abs(temp.zPos - zz))));
			
			double playerY = Player.y;
			
			if(playerY < 0)
			{
				playerY = 0;
			}
			
			double yDifference = playerY - Math.abs(temp.yPos);
			
			//If close enough, don't allow the player to move into it.
			if(distance <= 0.3 && yDifference <= 8
					&& yDifference >= -8)
			{
				return false;
			}	
			else if(distance <= 0.3 && yDifference > 8)
			{
				Player.maxHeight 
					= temp.height + Math.abs(temp.yPos) + 5;
				return true;
			}
			
			//If a boss, have the distance be farther the hitbox expands
			//out of
			if(distance <= 1 && temp.isABoss)
			{
				return false;
			}
		}
		
		//West Block
		int x0 = (int)((xx - z));

		//East Block
		int x1  = (int)((xx + z));
		
		//North block
		int z0   = (int)((zz - z));
		
		//South Block
		int z1   = (int)((zz + z));
			
		Block block1 = Game.level.getBlock(x0,z0);
		Block block2 = Game.level.getBlock(x1,z0);
		Block block3 = Game.level.getBlock(x0,z1);
		Block block4 = Game.level.getBlock(x1,z1);
		
		//Adds the plus 2 to go up steps
		double yCorrect = Player.y + 2;
		
	   /*
	    * After first check, no longer set the players height as being
	    * the block that he/she is in. This is just to set the player
	    * up initially.
	    */
		if(!firstCheck)
		{
			Player.y = block5.height + block5.y;
			Player.maxHeight = Player.y;
			firstCheck = true;
		}
		
	   /*
	    * If y value is less than 0 due to crouching, just
	    * keep it at 0 so that you cannot go under walls,
	    * and set it to what the player y would be if
	    * it was not negative.s
	    */
		if(Player.y < Player.maxHeight)
		{
			yCorrect = Player.maxHeight + 2;
		}
		
	   /*
	    * Because the blocks Y in correlation to the players Y is 4 times
	    * less than what the players y would be at that height visually.
	    * (Ex. When a wall is at a y of 3, to the player it looks like it
	    * is at a y of 12, though it is not) This corrects it so the
	    * wall height being checked is the same height the player thinks
	    * it is.
	    */
		double block1Y = block1.y * 4;
		double block2Y = block2.y * 4;
		double block3Y = block3.y * 4;
		double block4Y = block4.y * 4;
		
		if(block3.isSolid)
		{
			boolean temp = checkCollision(block3, block3Y, yCorrect);
			
			return temp;
		}
		
		if(block2.isSolid)
		{
			boolean temp = checkCollision(block2, block2Y, yCorrect);

			return temp;
		}
		
		if(block4.isSolid)
		{
			boolean temp = checkCollision(block4, block4Y, yCorrect);

			return temp;
		}

		if(block1.isSolid)
		{
			boolean temp = checkCollision(block1, block1Y, yCorrect);
			
			return temp;
		}
		
		Player.maxHeight = 0;
		return true;
	}
	
   /**
    * Is called with a given x and z value to determine if the block the
    * player is about to move to is free to move to or not. If it is
    * determined to be solid, you cannot move into it, and it stops your
    * movement, if not obviously you can move through it.
    * 
    * In terms of recent updates, if the block is higher than the players
    * position and height in the y direction, then the player can also
    * move under it. This is mainly used for doors, but will also come
    * in handy later for multi floored maps.
    * 
    * Since the 1.4 Update there are two methods, each is called 
    * individually depending on the direction the player is moving so
    * that at any height the walls will detect your collision correctly
    * no matter what the situation is. 
    * 
    * @param xx
    * @param yy
    * @return
    */
	public boolean isFreeX(double xx, double zz)
	{
		// Number used for how far away from block, block detects
		double z = -0.25;
		
		if(xx < Player.x)
		{
			z = 0.25;
		}
		else
		{
			z = -0.25;
		}
		
		Block block5 = Game.level.getBlock((int)
				(Math.round(Player.x)), (int)(Math.round(Player.z)));
		
		//Go through all the solid items in the game
		for(int i = 0; i < Game.solidItems.size(); i++)
		{
			Item temp = Game.solidItems.get(i);
			
			//Distance between item and player
			double distance = Math.sqrt(((Math.abs(temp.x - xx))
					* (Math.abs(temp.x - xx)))
					+ ((Math.abs(temp.z - zz))
							* (Math.abs(temp.z - zz))));
			
			double playerY = Player.y;
			
			if(playerY < 0)
			{
				playerY = 0;
			}
			
			//Difference in y
			double yDifference = playerY - Math.abs(temp.y);
			
			//If close enough, don't allow the player to move into it.
			if(distance <= 0.3 && yDifference <= temp.height
					&& yDifference >= -temp.height)
			{
				return false;
			}	
			else if(distance <= 0.3 && yDifference > temp.height)
			{
				Player.maxHeight 
					= temp.height + Math.abs(temp.y) + 5;
				return true;
			}	
		}
		
		//Go through all the enemies in the game
		for(int i = 0; i < Game.enemies.size(); i++)
		{
			Enemy temp = Game.enemies.get(i);
			
			//Distance between enemy and player
			double distance = Math.sqrt(((Math.abs(temp.xPos - xx))
					* (Math.abs(temp.xPos - xx)))
					+ ((Math.abs(temp.zPos - zz))
							* (Math.abs(temp.zPos - zz))));
			
			double playerY = Player.y;
			
			if(playerY < 0)
			{
				playerY = 0;
			}
			
			double yDifference = playerY - Math.abs(temp.yPos);
			
			//If close enough, don't allow the player to move into it.
			if(distance <= 0.3 && yDifference <= 8
					&& yDifference >= -8)
			{
				return false;
			}	
			else if(distance <= 0.3 && yDifference > 8)
			{
				Player.maxHeight = 
						temp.height + Math.abs(temp.yPos) + 5;
				return true;
			}
			
			//If a boss, have the distance be farther the hitbox expands
			//out of
			if(distance <= 1 && temp.isABoss)
			{
				return false;
			}
		}
		
		//West Block
		int x0 = (int)((xx - z));

		//East Block
		int x1  = (int)((xx + z));
		
		//North block
		int z0   = (int)((zz - z));
		
		//South Block
		int z1   = (int)((zz + z));
			
		Block block1 = Game.level.getBlock(x0,z0);
		Block block2 = Game.level.getBlock(x1,z0);
		Block block3 = Game.level.getBlock(x0,z1);
		Block block4 = Game.level.getBlock(x1,z1);
		
		//Adds the plus 2 to go up steps
		double yCorrect = Player.y + 2;
		
	   /*
	    * After first check, no longer set the players height as being
	    * the block that he/she is in. This is just to set the player
	    * up initially.
	    */
		if(!firstCheck)
		{
			Player.y = block5.height + block5.y;
			Player.maxHeight = Player.y;
			firstCheck = true;
		}
		
	   /*
	    * If y value is less than 0 due to crouching, just
	    * keep it at 0 so that you cannot go under walls,
	    * and set it to what the player y would be if
	    * it was not negative.s
	    */
		if(Player.y < Player.maxHeight)
		{
			yCorrect = Player.maxHeight + 2;
		}
		
	   /*
	    * Because the blocks Y in correlation to the players Y is 4 times
	    * less than what the players y would be at that height visually.
	    * (Ex. When a wall is at a y of 3, to the player it looks like it
	    * is at a y of 12, though it is not) This corrects it so the
	    * wall height being checked is the same height the player thinks
	    * it is.
	    */
		double block1Y = block1.y * 4;
		double block2Y = block2.y * 4;
		double block3Y = block3.y * 4;
		double block4Y = block4.y * 4;
		
		if(block3.isSolid)
		{
			boolean temp = checkCollision(block3, block3Y, yCorrect);
			
			return temp;
		}
		
		if(block2.isSolid)
		{
			boolean temp = checkCollision(block2, block2Y, yCorrect);

			return temp;
		}
		
		if(block4.isSolid)
		{
			boolean temp = checkCollision(block4, block4Y, yCorrect);

			return temp;
		}

		if(block1.isSolid)
		{
			boolean temp = checkCollision(block1, block1Y, yCorrect);
			
			return temp;
		}
		
		Player.maxHeight = 0;
		return true;
	}
	
	public boolean checkCollision(Block block, double blockY,
			double yCorrect)
	{
		if(block.height + (blockY / 2) > yCorrect
				&& yCorrect + Player.height >= blockY / 2)
		{
			return false;
		}
		else if(yCorrect >= block.height + blockY / 2
				|| block.isSolid && yCorrect + Player.height <= blockY / 2)
		{
			if(yCorrect >= block.height + blockY)
			{
				Player.maxHeight = block.height + blockY;
			}
			
			return true;
		}
		
		return true;
	}
}
