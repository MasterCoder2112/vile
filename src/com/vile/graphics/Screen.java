package com.vile.graphics;

//import java.util.Random;
import com.vile.Display;
import com.vile.Game;
import com.vile.entities.Player;

/**
 * Title: Screen
 * @author Alex Byrd
 * Date Updated: Long ago
 * 
 * Description:
 * Actually calls what is to be rendered on the screen and then renders 
 * them using the methods in the Render class. The methods called just
 * manipulate the bits in the PIXELS array to manipulate how the screen
 * looks.
 *
 */
public class Screen extends Render
{
	//private Render test;
	public static Render3D render3D;
	
	public Screen(int width, int height)
	{
		//Constructs a Render object with given width and height
		super(width, height);
		
		//New random that randomizes pixel colors
		//Random random = new Random();
		
		render3D      = new Render3D(WIDTH, HEIGHT);
		
		//Test is a new render with width and height 256
		//test          =  new Render(256, 256);
		
	   /*
	    * Fill each of the pixels in test with a random color using random
	    */
		//for(int i = 0; i < 256*256; i++)
		//{
			//test.PIXELS[i] = random.nextInt();
		//}
		
	}
	
   /**
    * This method uses Renders draw method to draw the graphics on the screen
    */
	public void render(Game game)
	{
		/*
		for(int i = 0; i < WIDTH * HEIGHT; i++)
		{
			//Sets all pixels made to blank every time the screen renders
			PIXELS[i] = 0;
		}
		/*
		for(int i = 0; i < 50; i++)
		{
		   /*
		    * Causes the images offset to change very quickly in coordination with the system time in milliseconds.
		    * The +1 allows the sin to work. The number after the modulus % determines its left and rightishness.
		    * The number after the / determines its speed. The ratio of the numbers has to be the same for it to
		    * make a complete circle though. 2pi makes the circumference, and the number after it all determines the
		    * radius.
		    */
			//int animationX = (int)(Math.sin((System.currentTimeMillis() + i * 5) % 1000.0 / 1000 * Math.PI * 2) * 100);
			//int animationY = (int)(Math.cos((System.currentTimeMillis() + i * 5) % 1000.0 / 1000 * Math.PI * 2) * 100);
			
			//draw(test, ((WIDTH - 256)/2) + animationX, ((HEIGHT - 256)/2) + animationY);
		//}
		
		render3D.floor(game);
		
		if(Display.themeNum != 0 && Display.themeNum != 4)
		{
			render3D.renderDistanceLimiter();
		}
		
		draw(render3D, 0, 0);
	}
}
