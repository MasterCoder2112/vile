package com.vile.graphics;

import java.awt.image.BufferedImage;

import javax.imageio.ImageIO;

import com.vile.Display;

/**
 * Title: Textures
 * @author Alex Byrd
 * Date Updated: 7/26/2016
 *
 * Descriptions:
 * Loads the textures into the game from the textures file, and makes them
 * into Render objects with given rgb values and width and heights so that
 * they are rendered correctly on screen.
 */
public class Textures 
{
	public static Render floor   = loadBitMap
			("/textures/theme"+(Display.themeNum + 1)+"/ground.png");
	public static Render floor1   = loadBitMap
			("/textures/theme"+3+"/floor1.png");
	public static Render floor2   = loadBitMap
			("/textures/theme"+3+"/floor2.png");
	public static Render floor3   = loadBitMap
			("/textures/theme"+3+"/floor3.png");
	public static Render floor4   = loadBitMap
			("/textures/theme"+3+"/floor4.png");
	public static Render enemy1   = loadBitMap
			("/textures/enemy1.png");
	public static Render enemy2   = loadBitMap
			("/textures/enemy2.png");
	public static Render enemy3   = loadBitMap
			("/textures/enemy3.png");
	public static Render enemy4   = loadBitMap
			("/textures/enemy4.png");
	public static Render wall1 = loadBitMap
			("/textures/theme"+(Display.themeNum + 1)
					+"/walls/wall1.png");
	public static Render wall2 = loadBitMap
			("/textures/theme"+(Display.themeNum + 1)
					+"/walls/wall2.png");
	public static Render wall3 = loadBitMap
			("/textures/theme"+(Display.themeNum + 1)
					+"/walls/wall3.png");
	public static Render wall4 = loadBitMap
			("/textures/theme"+(Display.themeNum + 1)
					+"/walls/wall4.png");
	public static Render wall5 = loadBitMap
			("/textures/theme"+(Display.themeNum + 1)
					+"/walls/wall5.png");
	public static Render wall6 = loadBitMap
			("/textures/theme"+(Display.themeNum + 1)
					+"/walls/wall6.png");
	public static Render wall7 = loadBitMap
			("/textures/theme"+(Display.themeNum + 1)
					+"/walls/wall7.png");
	public static Render wall8 = loadBitMap
			("/textures/theme"+(Display.themeNum + 1)
					+"/walls/wall8.png");
	public static Render wall9 = loadBitMap
			("/textures/theme"+(Display.themeNum + 1)
					+"/walls/wall9.png");
	public static Render wall10 = loadBitMap
			("/textures/theme"+(Display.themeNum + 1)
					+"/walls/wall10.png");
	public static Render wall11 = loadBitMap
			("/textures/theme"+(Display.themeNum + 1)
					+"/walls/wall11.png");
	public static Render wall12 = loadBitMap
			("/textures/theme"+(Display.themeNum + 1)
					+"/walls/wall12.png");
	public static Render wall13Phase1 = loadBitMap
			("/textures/theme"+(Display.themeNum + 1)
					+"/walls/wall13phase1.png");
	public static Render wall13Phase2 = loadBitMap
			("/textures/theme"+(Display.themeNum + 1)
					+"/walls/wall13phase2.png");
	public static Render wall14 = loadBitMap
			("/textures/theme"+(Display.themeNum + 1)
					+"/walls/wall14.png");
	public static Render wall15Phase5 = loadBitMap
			("/textures/theme"+(Display.themeNum + 1)
					+"/walls/wall15phase5.png");
	public static Render wall15Phase4 = loadBitMap
			("/textures/theme"+(Display.themeNum + 1)
					+"/walls/wall15phase4.png");
	public static Render wall15Phase3 = loadBitMap
			("/textures/theme"+(Display.themeNum + 1)
					+"/walls/wall15phase3.png");
	public static Render wall15Phase2 = loadBitMap
			("/textures/theme"+(Display.themeNum + 1)
					+"/walls/wall15phase2.png");
	public static Render wall15Phase1 = loadBitMap
			("/textures/theme"+(Display.themeNum + 1)
					+"/walls/wall15phase1.png");
	
	//Toxic Waste/Lava. Has many phases
	public static Render toxic1 = loadBitMap
			("/textures/theme"+(Display.themeNum + 1)
					+"/walls/toxicWaste16.png");
	public static Render toxic2 = loadBitMap
			("/textures/theme"+(Display.themeNum + 1)
					+"/walls/toxicWaste15.png");
	public static Render toxic3 = loadBitMap
			("/textures/theme"+(Display.themeNum + 1)
					+"/walls/toxicWaste14.png");
	public static Render toxic4 = loadBitMap
			("/textures/theme"+(Display.themeNum + 1)
					+"/walls/toxicWaste13.png");
	public static Render toxic5 = loadBitMap
			("/textures/theme"+(Display.themeNum + 1)
					+"/walls/toxicWaste12.png");
	public static Render toxic6 = loadBitMap
			("/textures/theme"+(Display.themeNum + 1)
					+"/walls/toxicWaste11.png");
	public static Render toxic7 = loadBitMap
			("/textures/theme"+(Display.themeNum + 1)
					+"/walls/toxicWaste10.png");
	public static Render toxic8 = loadBitMap
			("/textures/theme"+(Display.themeNum + 1)
					+"/walls/toxicWaste9.png");
	public static Render toxic9 = loadBitMap
			("/textures/theme"+(Display.themeNum + 1)
					+"/walls/toxicWaste8.png");
	public static Render toxic10 = loadBitMap
			("/textures/theme"+(Display.themeNum + 1)
					+"/walls/toxicWaste7.png");
	public static Render toxic11 = loadBitMap
			("/textures/theme"+(Display.themeNum + 1)
					+"/walls/toxicWaste6.png");
	public static Render toxic12 = loadBitMap
			("/textures/theme"+(Display.themeNum + 1)
					+"/walls/toxicWaste5.png");
	public static Render toxic13 = loadBitMap
			("/textures/theme"+(Display.themeNum + 1)
					+"/walls/toxicWaste4.png");
	public static Render toxic14 = loadBitMap
			("/textures/theme"+(Display.themeNum + 1)
					+"/walls/toxicWaste3.png");
	public static Render toxic15 = loadBitMap
			("/textures/theme"+(Display.themeNum + 1)
					+"/walls/toxicWaste2.png");
	public static Render toxic16 = loadBitMap
			("/textures/theme"+(Display.themeNum + 1)
					+"/walls/toxicWaste1.png");
	////////////////////////////////////////////////////////
	
	public static Render ceiling = loadBitMap
			("/textures/theme"+(Display.themeNum + 1)+"/sky.png");
	public static Render health  = loadBitMap
			("/textures/health.png");
	public static Render shell   = loadBitMap
			("/textures/shell.png");
	public static Render megaPhase1   = loadBitMap
			("/textures/megahealthphase1.png");
	public static Render megaPhase2   = loadBitMap
			("/textures/megahealthphase2.png");
	public static Render megaPhase3   = loadBitMap
			("/textures/megahealthphase3.png");
	public static Render megaPhase4   = loadBitMap
			("/textures/megahealthphase4.png");
	public static Render redKey = loadBitMap
			("/textures/redKey.png");
	public static Render blueKey = loadBitMap
			("/textures/blueKey.png");
	public static Render greenKey = loadBitMap
			("/textures/greenKey.png");
	public static Render yellowKey = loadBitMap
			("/textures/yellowKey.png");
	public static Render bullet  = loadBitMap
			("/textures/bullet.png");
	public static Render shotgun  = loadBitMap
			("/textures/shotgun.png");
	public static Render resurrect1  = loadBitMap
			("/textures/resurrectSkull1.png");
	public static Render resurrect2  = loadBitMap
			("/textures/resurrectSkull2.png");
	public static Render resurrect3  = loadBitMap
			("/textures/resurrectSkull3.png");
	public static Render environSuit  = loadBitMap
			("/textures/environSuit.png");
	public static Render goblet1  = loadBitMap
			("/textures/goblet1.png");
	public static Render goblet2  = loadBitMap
			("/textures/goblet2.png");
	public static Render goblet3  = loadBitMap
			("/textures/goblet3.png");
	public static Render adrenaline  = loadBitMap
			("/textures/adrenaline.png");
	public static Render glasses  = loadBitMap
			("/textures/glasses.png");
	public static Render torch1   = loadBitMap
			("/textures/torch1.png");
	public static Render torch2   = loadBitMap
			("/textures/torch2.png");
	public static Render torch3   = loadBitMap
			("/textures/torch3.png");
	public static Render torch4   = loadBitMap
			("/textures/torch4.png");
	public static Render lamp1   = loadBitMap
			("/textures/lamp1.png");
	public static Render lamp2   = loadBitMap
			("/textures/lamp2.png");
	public static Render lamp3   = loadBitMap
			("/textures/lamp3.png");
	public static Render tree   = loadBitMap
			("/textures/tree.png");
	public static Render canister   = loadBitMap
			("/textures/canister.png");
	public static Render canExplode1   = loadBitMap
			("/textures/canExplode1.png");
	public static Render canExplode2   = loadBitMap
			("/textures/canExplode2.png");
	public static Render canExplode3   = loadBitMap
			("/textures/canExplode3.png");
	public static Render canExplode4   = loadBitMap
			("/textures/canExplode4.png");
	public static Render chainmeal   = loadBitMap
			("/textures/chainmealArmor.png");
	public static Render combat   = loadBitMap
			("/textures/combatArmor.png");
	public static Render argent   = loadBitMap
			("/textures/argentArmor.png");
	public static Render shard   = loadBitMap
			("/textures/armorShard.png");
	public static Render vial   = loadBitMap
			("/textures/healthVial.png");
	public static Render baronTreasure   = loadBitMap
			("/textures/baronTreasure.png");
	public static Render holyWater1   = loadBitMap
			("/textures/holyWater1.png");
	public static Render holyWater2   = loadBitMap
			("/textures/holyWater2.png");
	public static Render table   = loadBitMap
			("/textures/table.png");
	public static Render lampTable = loadBitMap
			("/textures/lampTable.png");
	public static Render scepter   = loadBitMap
			("/textures/decietScepter.png");
	public static Render morgothHead   = loadBitMap
			("/textures/headOfMorgoth.png");
	public static Render morgoth   = loadBitMap
			("/textures/morgoth.png");
	public static Render enemy5   = loadBitMap
			("/textures/enemy5.png");
	public static Render corpse   = loadBitMap
			("/textures/corpse.png");
	public static Render shellBox = loadBitMap
			("/textures/shellBox.png");
	
	//Spine wall phases
	public static Render spine1 = loadBitMap
			("/textures/theme"+(Display.themeNum + 1)
					+"/walls/spineWall1.png");
	public static Render spine2 = loadBitMap
			("/textures/theme"+(Display.themeNum + 1)
					+"/walls/spineWall2.png");
	public static Render spine3 = loadBitMap
			("/textures/theme"+(Display.themeNum + 1)
					+"/walls/spineWall3.png");
	public static Render spine4 = loadBitMap
			("/textures/theme"+(Display.themeNum + 1)
					+"/walls/spineWall4.png");
	public static Render spine5 = loadBitMap
			("/textures/theme"+(Display.themeNum + 1)
					+"/walls/spineWall5.png");
	public static Render spine6 = loadBitMap
			("/textures/theme"+(Display.themeNum + 1)
					+"/walls/spineWall6.png");
	public static Render spine7 = loadBitMap
			("/textures/theme"+(Display.themeNum + 1)
					+"/walls/spineWall7.png");
	public static Render spine8 = loadBitMap
			("/textures/theme"+(Display.themeNum + 1)
					+"/walls/spineWall8.png");
	
	public static Render mlg = loadBitMap
			("/textures/mlg.png");
	public static Render chargePack = loadBitMap
			("/textures/chargePack.png");
	public static Render largeChargePack = loadBitMap
			("/textures/chargePackLarge.png");
	public static Render phaseCannon = loadBitMap
			("/textures/phaseCannonWeapon.png");
	public static Render phaser = loadBitMap
			("/textures/phaser.png");
	public static Render box = loadBitMap
			("/textures/boxWall.png");
	public static Render woodenWall = loadBitMap
			("/textures/woodWall.png");
	public static Render bloodWall = loadBitMap
			("/textures/bloodWall.png");
	public static Render marble = loadBitMap
			("/textures/marble.png");
	public static Render sat1 = loadBitMap
			("/textures/SatDish1.png");
	public static Render sat2 = loadBitMap
			("/textures/SatDish2.png");
	public static Render normButton = loadBitMap
			("/textures/normButton.png");
	public static Render pistol = loadBitMap
			("/textures/pistol.png");
	public static Render clip = loadBitMap
			("/textures/clip.png");
	public static Render bullets = loadBitMap
			("/textures/boxOfBullets.png");
	public static Render vileCiv1 = loadBitMap
			("/textures/vileCivilian.png");
	public static Render vileCiv2 = loadBitMap
			("/textures/vileCivilian2.png");
	public static Render vileCiv3 = loadBitMap
			("/textures/vileCivilian3.png");
	public static Render vileCiv4 = loadBitMap
			("/textures/vileCivilian4.png");
	public static Render vileCivAttack1 = loadBitMap
			("/textures/vileCivilianAttack1.png");
	public static Render vileCivAttack2 = loadBitMap
			("/textures/vileCivilianAttack2.png");
	public static Render vileCivHurt = loadBitMap
			("/textures/vileCivilianHurt.png");
	public static Render belegoth = loadBitMap
			("/textures/belegoth.png");
	public static Render corpse1 = loadBitMap
			("/textures/corpse1.png");
	public static Render corpse2 = loadBitMap
			("/textures/corpse2.png");
	public static Render corpse3 = loadBitMap
			("/textures/corpse3.png");
	public static Render corpse4 = loadBitMap
			("/textures/corpse4.png");
	public static Render corpse5 = loadBitMap
			("/textures/corpse5.png");
	public static Render corpse6 = loadBitMap
			("/textures/corpse6.png");
	public static Render corpse7 = loadBitMap
			("/textures/corpse7.png");
	public static Render corpse8 = loadBitMap
			("/textures/corpse8.png");
	public static Render corpseType2 = loadBitMap
			("/textures/corpseType2.png");
	public static Render enemy4b = loadBitMap
			("/textures/enemy4-2.png");
	public static Render enemy3b = loadBitMap
			("/textures/enemy3-2.png");
	public static Render enemy3c = loadBitMap
			("/textures/enemy3-3.png");
	public static Render enemy3d = loadBitMap
			("/textures/enemy3-4.png");
	public static Render enemy3e = loadBitMap
			("/textures/enemy3-5.png");
	public static Render enemy3f = loadBitMap
			("/textures/enemy3-6.png");
	public static Render defaultFireball = loadBitMap
			("/textures/fireball.png");
	public static Render giantFireball = loadBitMap
			("/textures/giantFireball.png");
	public static Render electricShock = loadBitMap
			("/textures/electricShock.png");
	public static Render electroBall = loadBitMap
			("/textures/electroBall.png");
	public static Render enemy1b   = loadBitMap
			("/textures/enemy1-2.png");
	public static Render enemy1corpse1   = loadBitMap
			("/textures/enemy1corpse1.png");
	public static Render enemy1corpse2   = loadBitMap
			("/textures/enemy1corpse2.png");
	public static Render enemy1corpse3   = loadBitMap
			("/textures/enemy1corpse3.png");
	public static Render enemy1corpse   = loadBitMap
			("/textures/enemy1corpse.png");
	public static Render enemy1hurt   = loadBitMap
			("/textures/enemy1hurt.png");
	public static Render enemy1fire1   = loadBitMap
			("/textures/enemy1fire1.png");
	public static Render enemy1fire2   = loadBitMap
			("/textures/enemy1fire2.png");
	public static Render enemy1fire3   = loadBitMap
			("/textures/enemy1fire3.png");
	public static Render enemy1fire4   = loadBitMap
			("/textures/enemy1fire4.png");
	public static Render enemy5b   = loadBitMap
			("/textures/enemy5-2.png");
	public static Render enemy5corpse1   = loadBitMap
			("/textures/enemy5corpse1.png");
	public static Render enemy5corpse2   = loadBitMap
			("/textures/enemy5corpse2.png");
	public static Render enemy5corpse3   = loadBitMap
			("/textures/enemy5corpse3.png");
	public static Render enemy5corpse4   = loadBitMap
			("/textures/enemy5corpse4.png");
	public static Render enemy5corpse5   = loadBitMap
			("/textures/enemy5corpse5.png");
	public static Render enemy5corpse6   = loadBitMap
			("/textures/enemy5corpse6.png");
	public static Render enemy5corpse7   = loadBitMap
			("/textures/enemy5corpse7.png");
	public static Render enemy5corpse   = loadBitMap
			("/textures/enemy5corpse.png");
	public static Render enemy5fire1   = loadBitMap
			("/textures/enemy5fire1.png");
	public static Render enemy5fire2   = loadBitMap
			("/textures/enemy5fire2.png");
	public static Render enemy5fire3   = loadBitMap
			("/textures/enemy5fire3.png");
	public static Render enemy5fire4   = loadBitMap
			("/textures/enemy5fire4.png");
	public static Render enemy5fire5   = loadBitMap
			("/textures/enemy5fire5.png");
	public static Render enemy3corpse   = loadBitMap
			("/textures/enemy3corpse.png");
	public static Render enemy3fire1   = loadBitMap
			("/textures/enemy3fire1.png");
	public static Render enemy3fire2   = loadBitMap
			("/textures/enemy3fire2.png");
	public static Render enemy3fire3   = loadBitMap
			("/textures/enemy3fire3.png");
	public static Render enemy3hurt   = loadBitMap
			("/textures/enemy3hurt.png");
	public static Render enemy2hurt   = loadBitMap
			("/textures/enemy2hurt.png");
	public static Render enemy2corpse1   = loadBitMap
			("/textures/enemy2corpse1.png");
	public static Render enemy2corpse2   = loadBitMap
			("/textures/enemy2corpse2.png");
	public static Render enemy2corpse3   = loadBitMap
			("/textures/enemy2corpse3.png");
	public static Render enemy2corpse4   = loadBitMap
			("/textures/enemy2corpse4.png");
	public static Render enemy2corpse5   = loadBitMap
			("/textures/enemy2corpse5.png");
	public static Render enemy2corpse6   = loadBitMap
			("/textures/enemy2corpse6.png");
	public static Render enemy2corpse   = loadBitMap
			("/textures/enemy2corpse.png");
	public static Render enemy2fire1   = loadBitMap
			("/textures/enemy2fire.png");
	public static Render belegothCorpse1   = loadBitMap
			("/textures/belegothCorpse1.png");
	public static Render belegothCorpse2   = loadBitMap
			("/textures/belegothCorpse2.png");
	public static Render belegothCorpse3   = loadBitMap
			("/textures/belegothCorpse3.png");
	public static Render belegothCorpse4   = loadBitMap
			("/textures/belegothCorpse4.png");
	public static Render belegothCorpse5   = loadBitMap
			("/textures/belegothCorpse5.png");
	public static Render belegothCorpse6   = loadBitMap
			("/textures/belegothCorpse6.png");
	public static Render belegothCorpse7   = loadBitMap
			("/textures/belegothCorpse7.png");
	public static Render belegothCorpse8   = loadBitMap
			("/textures/belegothCorpse8.png");
	public static Render belegothCorpse9   = loadBitMap
			("/textures/belegothCorpse9.png");
	public static Render belegothCorpse10   = loadBitMap
			("/textures/belegothCorpse10.png");
	public static Render belegothCorpse11   = loadBitMap
			("/textures/belegothCorpse11.png");
	public static Render belegothCorpse12   = loadBitMap
			("/textures/belegothCorpse12.png");
	public static Render belegothCorpse13   = loadBitMap
			("/textures/belegothCorpse13.png");
	public static Render belegothCorpse   = loadBitMap
			("/textures/belegothCorpse14.png");
	public static Render belegothHurt   = loadBitMap
			("/textures/belegothHurt.png");
	public static Render belegothMelee   = loadBitMap
			("/textures/belegothMelee.png");
	public static Render belegoth2   = loadBitMap
			("/textures/belegoth2.png");
	public static Render belegoth3   = loadBitMap
			("/textures/belegoth3.png");
	public static Render belegoth4   = loadBitMap
			("/textures/belegoth4.png");
	public static Render belegoth5   = loadBitMap
			("/textures/belegoth5.png");
	public static Render belegoth6   = loadBitMap
			("/textures/belegoth6.png");
	public static Render belegothAttack1   = loadBitMap
			("/textures/belegothAttack1.png");
	public static Render belegothAttack2   = loadBitMap
			("/textures/belegothAttack2.png");
	public static Render belegothAttack3   = loadBitMap
			("/textures/belegothAttack3.png");
	public static Render belegothAttack4   = loadBitMap
			("/textures/belegothAttack4.png");
	public static Render morgothFire1   = loadBitMap
			("/textures/morgothFire1.png");
	public static Render morgothFire2   = loadBitMap
			("/textures/morgothFire2.png");
	public static Render morgothMelee   = loadBitMap
			("/textures/morgothMelee.png");
	public static Render morgoth2   = loadBitMap
			("/textures/morgoth2.png");
	public static Render morgoth3   = loadBitMap
			("/textures/morgoth3.png");
	public static Render morgoth4   = loadBitMap
			("/textures/morgoth4.png");
	public static Render morgothHurt   = loadBitMap
			("/textures/morgothHurt.png");
	
   /**
    * Finds the texture called for in the file, then creates a Render 
    * object out of it so that it renders on the screen.
    * @param fileName
    * @return
    */
	public static Render loadBitMap(String fileName)
	{
		try
		{
			BufferedImage image = ImageIO.read(Textures.class.getResource(fileName));
			
			int width = image.getWidth();
			int height = image.getHeight();
		
			Render result = new Render(width, height);
		
			image.getRGB(0, 0, width, height, result.PIXELS, 0, width);
		
			return result;
		}
		catch (Exception e)
		{
			System.out.println(fileName);
			throw new RuntimeException(e);
		}
	}
	
   /**
    * Resets the textures for each game so that textures do not stay
    * the same from theme to theme.
    */
	public static void resetTextures()
	{
		floor   = loadBitMap
				("/textures/theme"+(Display.themeNum + 1)+"/ground.png");
		floor1   = loadBitMap
				("/textures/theme"+3+"/floor1.png");
		floor2   = loadBitMap
				("/textures/theme"+3+"/floor2.png");
		floor3   = loadBitMap
				("/textures/theme"+3+"/floor3.png");
		floor4   = loadBitMap
				("/textures/theme"+3+"/floor4.png");
		enemy1   = loadBitMap
				("/textures/enemy1.png");
		enemy2   = loadBitMap
				("/textures/enemy2.png");
		enemy3   = loadBitMap
				("/textures/enemy3.png");
		enemy4   = loadBitMap
				("/textures/enemy4.png");
		wall1 = loadBitMap
				("/textures/theme"+(Display.themeNum + 1)
						+"/walls/wall1.png");
		wall2 = loadBitMap
				("/textures/theme"+(Display.themeNum + 1)
						+"/walls/wall2.png");
		wall3 = loadBitMap
				("/textures/theme"+(Display.themeNum + 1)
						+"/walls/wall3.png");
		wall4 = loadBitMap
				("/textures/theme"+(Display.themeNum + 1)
						+"/walls/wall4.png");
		wall5 = loadBitMap
				("/textures/theme"+(Display.themeNum + 1)
						+"/walls/wall5.png");
		wall6 = loadBitMap
				("/textures/theme"+(Display.themeNum + 1)
						+"/walls/wall6.png");
		wall7 = loadBitMap
				("/textures/theme"+(Display.themeNum + 1)
						+"/walls/wall7.png");
		wall8 = loadBitMap
				("/textures/theme"+(Display.themeNum + 1)
						+"/walls/wall8.png");
		wall9 = loadBitMap
				("/textures/theme"+(Display.themeNum + 1)
						+"/walls/wall9.png");
		wall10 = loadBitMap
				("/textures/theme"+(Display.themeNum + 1)
						+"/walls/wall10.png");
		wall11 = loadBitMap
				("/textures/theme"+(Display.themeNum + 1)
						+"/walls/wall11.png");
		wall12 = loadBitMap
				("/textures/theme"+(Display.themeNum + 1)
						+"/walls/wall12.png");
		wall13Phase1 = loadBitMap
				("/textures/theme"+(Display.themeNum + 1)
						+"/walls/wall13phase1.png");
		wall13Phase2 = loadBitMap
				("/textures/theme"+(Display.themeNum + 1)
						+"/walls/wall13phase2.png");
		wall14 = loadBitMap
				("/textures/theme"+(Display.themeNum + 1)
						+"/walls/wall14.png");
		wall15Phase5 = loadBitMap
				("/textures/theme"+(Display.themeNum + 1)
						+"/walls/wall15phase5.png");
		wall15Phase4 = loadBitMap
				("/textures/theme"+(Display.themeNum + 1)
						+"/walls/wall15phase4.png");
		wall15Phase3 = loadBitMap
				("/textures/theme"+(Display.themeNum + 1)
						+"/walls/wall15phase3.png");
		wall15Phase2 = loadBitMap
				("/textures/theme"+(Display.themeNum + 1)
						+"/walls/wall15phase2.png");
		wall15Phase1 = loadBitMap
				("/textures/theme"+(Display.themeNum + 1)
						+"/walls/wall15phase1.png");
		ceiling = loadBitMap
				("/textures/theme"+(Display.themeNum + 1)+"/sky.png");
		health  = loadBitMap
				("/textures/health.png");
		shell   = loadBitMap
				("/textures/shell.png");
		bullet  = loadBitMap
				("/textures/bullet.png");
		megaPhase1   = loadBitMap
				("/textures/megahealthphase1.png");
		megaPhase2   = loadBitMap
				("/textures/megahealthphase2.png");
		megaPhase3   = loadBitMap
				("/textures/megahealthphase3.png");
		megaPhase4   = loadBitMap
				("/textures/megahealthphase4.png");
		redKey = loadBitMap
				("/textures/redKey.png");
		blueKey = loadBitMap
				("/textures/blueKey.png");
		greenKey = loadBitMap
				("/textures/greenKey.png");
		yellowKey = loadBitMap
				("/textures/yellowKey.png");
		shotgun  = loadBitMap
				("/textures/shotgun.png");
		
		toxic1 = loadBitMap
				("/textures/theme"+(Display.themeNum + 1)
						+"/walls/toxicWaste16.png");
		toxic2 = loadBitMap
				("/textures/theme"+(Display.themeNum + 1)
						+"/walls/toxicWaste15.png");
		toxic3 = loadBitMap
				("/textures/theme"+(Display.themeNum + 1)
						+"/walls/toxicWaste14.png");
		toxic4 = loadBitMap
				("/textures/theme"+(Display.themeNum + 1)
						+"/walls/toxicWaste13.png");
		toxic5 = loadBitMap
				("/textures/theme"+(Display.themeNum + 1)
						+"/walls/toxicWaste12.png");
		toxic6 = loadBitMap
				("/textures/theme"+(Display.themeNum + 1)
						+"/walls/toxicWaste11.png");
		toxic7 = loadBitMap
				("/textures/theme"+(Display.themeNum + 1)
						+"/walls/toxicWaste10.png");
		toxic8 = loadBitMap
				("/textures/theme"+(Display.themeNum + 1)
						+"/walls/toxicWaste9.png");
		toxic9 = loadBitMap
				("/textures/theme"+(Display.themeNum + 1)
						+"/walls/toxicWaste8.png");
		toxic10 = loadBitMap
				("/textures/theme"+(Display.themeNum + 1)
						+"/walls/toxicWaste7.png");
		toxic11 = loadBitMap
				("/textures/theme"+(Display.themeNum + 1)
						+"/walls/toxicWaste6.png");
		toxic12 = loadBitMap
				("/textures/theme"+(Display.themeNum + 1)
						+"/walls/toxicWaste5.png");
		toxic13 = loadBitMap
				("/textures/theme"+(Display.themeNum + 1)
						+"/walls/toxicWaste4.png");
		toxic14 = loadBitMap
				("/textures/theme"+(Display.themeNum + 1)
						+"/walls/toxicWaste3.png");
		toxic15 = loadBitMap
				("/textures/theme"+(Display.themeNum + 1)
						+"/walls/toxicWaste2.png");
		toxic16 = loadBitMap
				("/textures/theme"+(Display.themeNum + 1)
						+"/walls/toxicWaste1.png");
		
		resurrect1  = loadBitMap
				("/textures/resurrectSkull1.png");
		resurrect2  = loadBitMap
				("/textures/resurrectSkull2.png");
		resurrect3  = loadBitMap
				("/textures/resurrectSkull3.png");
		environSuit  = loadBitMap
			("/textures/environSuit.png");
		goblet1  = loadBitMap
				("/textures/goblet1.png");
		goblet2  = loadBitMap
				("/textures/goblet2.png");
		goblet3  = loadBitMap
				("/textures/goblet3.png");
		adrenaline  = loadBitMap
				("/textures/adrenaline.png");
		glasses  = loadBitMap
				("/textures/glasses.png");
		torch1   = loadBitMap
				("/textures/torch1.png");
		torch2   = loadBitMap
				("/textures/torch2.png");
		torch3   = loadBitMap
				("/textures/torch3.png");
		torch4   = loadBitMap
				("/textures/torch4.png");
		lamp1   = loadBitMap
				("/textures/lamp1.png");
		lamp2   = loadBitMap
				("/textures/lamp2.png");
		lamp3   = loadBitMap
				("/textures/lamp3.png");
		tree   = loadBitMap
				("/textures/tree.png");
		canister   = loadBitMap
				("/textures/canister.png");
		canExplode1   = loadBitMap
				("/textures/canExplode1.png");
		canExplode2   = loadBitMap
				("/textures/canExplode2.png");
		canExplode3   = loadBitMap
				("/textures/canExplode3.png");
		canExplode4   = loadBitMap
				("/textures/canExplode4.png");
		combat   = loadBitMap
				("/textures/combatArmor.png");
		argent   = loadBitMap
				("/textures/argentArmor.png");
		shard   = loadBitMap
				("/textures/armorShard.png");
		vial   = loadBitMap
				("/textures/healthVial.png");
		baronTreasure   = loadBitMap
				("/textures/baronTreasure.png");
		holyWater1   = loadBitMap
				("/textures/holyWater1.png");
		holyWater2   = loadBitMap
				("/textures/holyWater2.png");
		scepter   = loadBitMap
				("/textures/decietScepter.png");
		morgothHead   = loadBitMap
				("/textures/headOfMorgoth.png");
		table   = loadBitMap
				("/textures/table.png");
		lampTable = loadBitMap
				("/textures/lampTable.png");
		enemy5   = loadBitMap
				("/textures/enemy5.png");
		corpse   = loadBitMap
				("/textures/corpse.png");
		morgoth   = loadBitMap
				("/textures/morgoth.png");
		shellBox = loadBitMap
				("/textures/shellBox.png");
		spine1 = loadBitMap
				("/textures/theme"+(Display.themeNum + 1)
						+"/walls/spineWall1.png");
		spine2 = loadBitMap
				("/textures/theme"+(Display.themeNum + 1)
						+"/walls/spineWall2.png");
		spine3 = loadBitMap
				("/textures/theme"+(Display.themeNum + 1)
						+"/walls/spineWall3.png");
		spine4 = loadBitMap
				("/textures/theme"+(Display.themeNum + 1)
						+"/walls/spineWall4.png");
		spine5 = loadBitMap
				("/textures/theme"+(Display.themeNum + 1)
						+"/walls/spineWall5.png");
		spine6 = loadBitMap
				("/textures/theme"+(Display.themeNum + 1)
						+"/walls/spineWall6.png");
		spine7 = loadBitMap
				("/textures/theme"+(Display.themeNum + 1)
						+"/walls/spineWall7.png");
		spine8 = loadBitMap
				("/textures/theme"+(Display.themeNum + 1)
						+"/walls/spineWall8.png");
		
		mlg = loadBitMap
				("/textures/mlg.png");
		chargePack = loadBitMap
				("/textures/chargePack.png");
		largeChargePack = loadBitMap
				("/textures/chargePackLarge.png");
		phaseCannon = loadBitMap
				("/textures/phaseCannonWeapon.png");
		phaser = loadBitMap
				("/textures/phaser.png");
		box = loadBitMap
				("/textures/boxWall.png");
		woodenWall = loadBitMap
				("/textures/woodWall.png");
		bloodWall = loadBitMap
				("/textures/bloodWall.png");
		marble = loadBitMap
				("/textures/marble.png");
		sat1 = loadBitMap
				("/textures/SatDish1.png");
		sat2 = loadBitMap
				("/textures/SatDish2.png");
		normButton = loadBitMap
				("/textures/normButton.png");
		pistol = loadBitMap
				("/textures/pistol.png");
		clip = loadBitMap
				("/textures/clip.png");
		bullets = loadBitMap
				("/textures/boxOfBullets.png");
		vileCiv1 = loadBitMap
				("/textures/vileCivilian.png");
		vileCiv2 = loadBitMap
				("/textures/vileCivilian2.png");
		vileCiv3 = loadBitMap
				("/textures/vileCivilian3.png");
		vileCiv4 = loadBitMap
				("/textures/vileCivilian4.png");
		vileCivAttack1 = loadBitMap
				("/textures/vileCivilianAttack1.png");
		vileCivAttack2 = loadBitMap
				("/textures/vileCivilianAttack2.png");
		vileCivHurt = loadBitMap
				("/textures/vileCivilianHurt.png");
		belegoth = loadBitMap
				("/textures/belegoth.png");
		corpse1 = loadBitMap
				("/textures/corpse1.png");
		corpse2 = loadBitMap
				("/textures/corpse2.png");
		corpse3 = loadBitMap
				("/textures/corpse3.png");
		corpse4 = loadBitMap
				("/textures/corpse4.png");
		corpse5 = loadBitMap
				("/textures/corpse5.png");
		corpse6 = loadBitMap
				("/textures/corpse6.png");
		corpse7 = loadBitMap
				("/textures/corpse7.png");
		corpse8 = loadBitMap
				("/textures/corpse8.png");
		corpseType2 = loadBitMap
				("/textures/corpseType2.png");
		enemy4b = loadBitMap
				("/textures/enemy4-2.png");
		enemy3b = loadBitMap
				("/textures/enemy3-2.png");
		enemy3c = loadBitMap
				("/textures/enemy3-3.png");
		enemy3d = loadBitMap
				("/textures/enemy3-4.png");
		enemy3e = loadBitMap
				("/textures/enemy3-5.png");
		enemy3f = loadBitMap
				("/textures/enemy3-6.png");
		defaultFireball = loadBitMap
				("/textures/fireball.png");
		giantFireball = loadBitMap
				("/textures/giantFireball.png");
		electricShock = loadBitMap
				("/textures/electricShock.png");
		electroBall = loadBitMap
				("/textures/electroBall.png");
		enemy1b   = loadBitMap
				("/textures/enemy1-2.png");
		enemy1corpse1   = loadBitMap
				("/textures/enemy1corpse1.png");
		enemy1corpse2   = loadBitMap
				("/textures/enemy1corpse2.png");
		enemy1corpse3   = loadBitMap
				("/textures/enemy1corpse3.png");
		enemy1corpse   = loadBitMap
				("/textures/enemy1corpse.png");
		enemy1hurt   = loadBitMap
				("/textures/enemy1hurt.png");
		enemy1fire1   = loadBitMap
				("/textures/enemy1fire1.png");
		enemy1fire2   = loadBitMap
				("/textures/enemy1fire2.png");
		enemy1fire3   = loadBitMap
				("/textures/enemy1fire3.png");
		enemy1fire4   = loadBitMap
				("/textures/enemy1fire4.png");
		enemy5b   = loadBitMap
				("/textures/enemy5-2.png");
		enemy5corpse1   = loadBitMap
				("/textures/enemy5corpse1.png");
		enemy5corpse2   = loadBitMap
				("/textures/enemy5corpse2.png");
		enemy5corpse3   = loadBitMap
				("/textures/enemy5corpse3.png");
		enemy5corpse4   = loadBitMap
				("/textures/enemy5corpse4.png");
		enemy5corpse5   = loadBitMap
				("/textures/enemy5corpse5.png");
		enemy5corpse6   = loadBitMap
				("/textures/enemy5corpse6.png");
		enemy5corpse7   = loadBitMap
				("/textures/enemy5corpse7.png");
		enemy5corpse   = loadBitMap
				("/textures/enemy5corpse.png");
		enemy5fire1   = loadBitMap
				("/textures/enemy5fire1.png");
		enemy5fire2   = loadBitMap
				("/textures/enemy5fire2.png");
		enemy5fire3   = loadBitMap
				("/textures/enemy5fire3.png");
		enemy5fire4   = loadBitMap
				("/textures/enemy5fire4.png");
		enemy5fire5   = loadBitMap
				("/textures/enemy5fire5.png");
		enemy3corpse   = loadBitMap
				("/textures/enemy3corpse.png");
		enemy3fire1   = loadBitMap
				("/textures/enemy3fire1.png");
		enemy3fire2   = loadBitMap
				("/textures/enemy3fire2.png");
		enemy3fire3   = loadBitMap
				("/textures/enemy3fire3.png");
		enemy3hurt   = loadBitMap
				("/textures/enemy3hurt.png");
		enemy2hurt   = loadBitMap
				("/textures/enemy2hurt.png");
		enemy2corpse1   = loadBitMap
				("/textures/enemy2corpse1.png");
		enemy2corpse2   = loadBitMap
				("/textures/enemy2corpse2.png");
		enemy2corpse3   = loadBitMap
				("/textures/enemy2corpse3.png");
		enemy2corpse4   = loadBitMap
				("/textures/enemy2corpse4.png");
		enemy2corpse5   = loadBitMap
				("/textures/enemy2corpse5.png");
		enemy2corpse6   = loadBitMap
				("/textures/enemy2corpse6.png");
		enemy2corpse   = loadBitMap
				("/textures/enemy2corpse.png");
		enemy2fire1   = loadBitMap
				("/textures/enemy2fire.png");
		belegothCorpse1   = loadBitMap
				("/textures/belegothCorpse1.png");
		belegothCorpse2   = loadBitMap
				("/textures/belegothCorpse2.png");
		belegothCorpse3   = loadBitMap
				("/textures/belegothCorpse3.png");
		belegothCorpse4   = loadBitMap
				("/textures/belegothCorpse4.png");
		belegothCorpse5   = loadBitMap
				("/textures/belegothCorpse5.png");
		belegothCorpse6   = loadBitMap
				("/textures/belegothCorpse6.png");
		belegothCorpse7   = loadBitMap
				("/textures/belegothCorpse7.png");
		belegothCorpse8   = loadBitMap
				("/textures/belegothCorpse8.png");
		belegothCorpse9   = loadBitMap
				("/textures/belegothCorpse9.png");
		belegothCorpse10   = loadBitMap
				("/textures/belegothCorpse10.png");
		belegothCorpse11   = loadBitMap
				("/textures/belegothCorpse11.png");
		belegothCorpse12   = loadBitMap
				("/textures/belegothCorpse12.png");
		belegothCorpse13   = loadBitMap
				("/textures/belegothCorpse13.png");
		belegothCorpse   = loadBitMap
				("/textures/belegothCorpse14.png");
		belegothHurt   = loadBitMap
				("/textures/belegothHurt.png");
		belegothMelee   = loadBitMap
				("/textures/belegothMelee.png");
		belegoth2   = loadBitMap
				("/textures/belegoth2.png");
		belegoth3   = loadBitMap
				("/textures/belegoth3.png");
		belegoth4   = loadBitMap
				("/textures/belegoth4.png");
		belegoth5   = loadBitMap
				("/textures/belegoth5.png");
		belegoth6   = loadBitMap
				("/textures/belegoth6.png");
		belegothAttack1   = loadBitMap
				("/textures/belegothAttack1.png");
		belegothAttack2   = loadBitMap
				("/textures/belegothAttack2.png");
		belegothAttack3   = loadBitMap
				("/textures/belegothAttack3.png");
		belegothAttack4   = loadBitMap
				("/textures/belegothAttack4.png");
		morgothFire1   = loadBitMap
				("/textures/morgothFire1.png");
		morgothFire2   = loadBitMap
				("/textures/morgothFire2.png");
		morgothMelee   = loadBitMap
				("/textures/morgothMelee.png");
		morgoth2   = loadBitMap
				("/textures/morgoth2.png");
		morgoth3   = loadBitMap
				("/textures/morgoth3.png");
		morgoth4   = loadBitMap
				("/textures/morgoth4.png");
		morgothHurt   = loadBitMap
				("/textures/morgothHurt.png");
	}
}
