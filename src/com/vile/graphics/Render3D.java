package com.vile.graphics;

import java.util.ArrayList;
import java.util.Collections;

import com.vile.Display;
import com.vile.Game;
import com.vile.entities.Bullet;
import com.vile.entities.Corpse;
import com.vile.entities.Enemy;
import com.vile.entities.EnemyFire;
import com.vile.entities.ExplosiveCanister;
import com.vile.entities.Item;
import com.vile.entities.Player;
import com.vile.input.Controller;
import com.vile.levelGenerator.*;

/**
 * Title: Render3D
 * @author Alex Byrd
 * Date Updated: 9/16/2016
 *
 * Renders the graphics on the screen for all the entities, walls,
 * etc... It renders all these things in a raycasting environment that
 * simulates a 3D environment without using "actual" 3D.  
 */
public class Render3D extends Render
{
	
	public boolean once = true;
	
   /*
    * Basically an array of values used for the pixels rendered on the
    * screen to get darker as they fade into the distance. The lower the
    * value, the darker that pixel is. The pixel stays the same color, but
    * its brightness value is set by these arrays. zBufferWall is for walls
    * and zBuffer is for everything else.
    */
	private double[] zBuffer;
	private double[] zBufferWall;
	
   /*
    * Sets how many pixels away in the z direction the render distance
    * limiter begins. 
    */
	private double renderDistanceDefault = 100000.0;
	private double renderDistance = renderDistanceDefault;
	
   /*
    * Corrects the movement of walls to the players movement. If the 
    * player moves, the walls look as if they are staying in place though
    * they are truly not, they are just moving away at enough speed to
    * simulate that they are staying in place with respect to the player.
    */
	private double forwardCorrect = 3;
	private double rightCorrect   = 3;

	//Speeds and turning values
	private double fowardSpeed, rightSpeed, cosine, sine;
	
	//Default ceiling height
	public static double ceilingDefaultHeight = 200;
	
	//Stores the id of the current wall being rendered to keep track
	public static int currentWallID = 0;
	
	public Render3D(int width, int height)
	{
		super(width, height);
		zBuffer     = new double[WIDTH * HEIGHT];
		zBufferWall = new double[WIDTH];
	}
	
	public void floor(Game game)
	{		
	   /*
	    * Determines speed values based on what is read from the keyboard.
	    */
	    cosine        = Math.cos(Player.rotation);
		sine          = Math.sin(Player.rotation);
		fowardSpeed   = Player.z * 21.3;
		rightSpeed    = Player.x * 21.3;
		
		double ceilingHeight = ceilingDefaultHeight -
				Player.y;
		double floorDepth    = 8 +
				Player.y;
		
		if(Display.themeNum == 2)
		{
			renderDistanceDefault = 50000;
		}
		else
		{
			renderDistanceDefault = 100000;
		}
		
		if(Player.weapons[Player.weaponEquipped].weaponShootTime == 0)
		{
			renderDistance = renderDistanceDefault;
		}
		else
		{
			renderDistance = 3000000.0;	
		}
		
		for (int y = 0; y < HEIGHT; y++)
		{	
		   /*
		    * Sets the ceiling and the floor basically, ceiling is both
		    * of them. they are horizontal lines drawn at certain y coordinates
		    * that basically decrease in distance between by a factor of 2.0 each
		    * time they are drawn, then they are divided by height to get the squares
		    * to look right.
		    */
			double ceiling = (y - HEIGHT / Math.sin(Player.upRotate) 
					* Math.cos(Player.upRotate)) / HEIGHT;
			
		
			
		   /*
			* Sets the distance the floor is below the player.
			*/
			double z       = floorDepth / ceiling;
			
		   /*
		    * Makes the actual ceiling of the frame move the same direction as the
		    * floor instead of being the inverse of the of the floor. Also sets the
		    * distance the ceiling is above the player.
		    */
			if (ceiling < 0)
			{
				z       = ceilingHeight / -ceiling;
			}
					
			for (int x = 0; x < WIDTH; x++)
			{
				zBufferWall[x] = 0;
				
			   /*
			    * Sets the bars along the x that go up in y. As
			    * the bars go into the distance, the distance between them
			    * decreases by a factor of 2.0. Then the distance between
			    * them is / HEIGHT to set them equal to the distance between
			    * the y ones, to make squares.
			    */
				double depth = (x - WIDTH / 2.0) / HEIGHT;
				
				//Sets the depth relative to z
				depth *= z;
				
				double xx = ((depth * (cosine)) + (z * (sine))) + rightSpeed;
				double yy = ((z * cosine) - (depth * sine)) + fowardSpeed;
				
				double textureCorrect = 16;
				
				if(Display.themeNum == 4)
				{
					textureCorrect = 2;
				}
				
				if(Display.themeNum == 2 && ceiling < 0)
				{
					textureCorrect = 0.01;
				}
				
			   /*
			    * Converts xx and yy into int form, and multiplies the bits
			    * by 255 (Aka, 011111111)
			    */
				int xPix = (int) ((xx) * textureCorrect) & 255;
				int yPix = (int) ((yy) * textureCorrect) & 255;
				
				
				zBuffer[x + y * WIDTH] = z;
				
			   /*
			    * Sets the colors of the pixels on the screen at each width.
			    * The << shifts the bits to the left make the number larger
			    * which makes the green color instead of the blue color for the 
			    * yPix. 			
			    */
				if(Display.themeNum == 0)
				{	
					ceilingDefaultHeight  = 1000.0;
				}
				else if(Display.themeNum == 1)
				{
					//renderDistanceDefault = 8000.0;

					ceilingDefaultHeight  = 50.0;
				}
				else if(Display.themeNum == 2)
				{
					ceilingDefaultHeight = 100.0;
				}
				else if(Display.themeNum == 4)
				{
					ceilingDefaultHeight  = 100.0;
				}
				else
				{
					ceilingDefaultHeight  = 50.0;
				}
				
			   /*
			    * The center of the screen is considered 0, above that is
			    * negative because computers are weird like that, and moving
			    * up in y is actually like moving down. Anyway, if the
			    * celing pixel is less that 0, then texture them with the 
			    * ceiling texture correlating to the theme.
			    * 
			    * Otherwise, it is the floor, so texture the floor with
			    * the corresponding floor texture.
			    */
				if(Display.themeNum != 0)
				{
					if(Display.themeNum == 2)
					{
						if(Game.mapNum == 1 || Game.mapNum == 9)
						{
							if(ceiling < 0)
							{
								PIXELS[x + y * WIDTH] = //(xPix + yPix) / 4;
										Textures.ceiling.PIXELS
										[(xPix & 255) + (yPix & 255) * 256];
							}
							else
							{
								PIXELS[x + y * WIDTH] = //((xPix) << 8 | (yPix) << 16);
										Textures.floor1.PIXELS
										[(xPix & 255) + (yPix & 255) * 256];
							}
						}
						
						if(Game.mapNum == 3 || Game.mapNum == 8
								|| Game.mapNum == 4)
						{
							if(ceiling < 0)
							{
								PIXELS[x + y * WIDTH] = //(xPix + yPix) / 4;
										Textures.ceiling.PIXELS
										[(xPix & 255) + (yPix & 255) * 256];
							}
							else
							{
								PIXELS[x + y * WIDTH] = //((xPix) << 8 | (yPix) << 16);
										Textures.floor2.PIXELS
										[(xPix & 255) + (yPix & 255) * 256];
							}
						}
						
						if(Game.mapNum == 2 || Game.mapNum == 7)
						{
							if(ceiling < 0)
							{
								PIXELS[x + y * WIDTH] = //(xPix + yPix) / 4;
										Textures.ceiling.PIXELS
										[(xPix & 255) + (yPix & 255) * 256];
							}
							else
							{
								PIXELS[x + y * WIDTH] = //((xPix) << 8 | (yPix) << 16);
										Textures.floor3.PIXELS
										[(xPix & 255) + (yPix & 255) * 256];
							}
						}
						
						if(Game.mapNum == 5 || Game.mapNum == 6
								|| Game.mapNum >= 10)
						{
							if(ceiling < 0)
							{
								PIXELS[x + y * WIDTH] = //(xPix + yPix) / 4;
										Textures.ceiling.PIXELS
										[(xPix & 255) + (yPix & 255) * 256];
							}
							else
							{
								PIXELS[x + y * WIDTH] = //((xPix) << 8 | (yPix) << 16);
										Textures.floor4.PIXELS
										[(xPix & 255) + (yPix & 255) * 256];
							}
						}
					}
					else
					{
						if(ceiling < 0)
						{
							PIXELS[x + y * WIDTH] = //(xPix + yPix) / 4;
									Textures.ceiling.PIXELS
									[(xPix & 255) + (yPix & 255) * 256];
						}
						else
						{
							PIXELS[x + y * WIDTH] = //((xPix) << 8 | (yPix) << 16);
									Textures.floor.PIXELS
									[(xPix & 255) + (yPix & 255) * 256];
						}
					}
				}
				else
				{				
					if(ceiling < 0)
					{
						PIXELS[x + y * WIDTH] = 255 << 7 | 255 << 8 | 255;	
					}
					else
					{
						PIXELS[x + y * WIDTH] = //((xPix) << 8 | (yPix) << 16);
								Textures.floor.PIXELS[(xPix & 255) + (yPix & 255) * 256];
					}
				}
			}
		}
		
		once = true;
		
		//Gets the level from game
		Level level = Game.level;
		
		//Array list that holds all non see through blocks
		ArrayList <Block> blocks 	   = new ArrayList<Block>();
		
		//Holds glass and other blocks that can be seen though
		ArrayList <Block> entityBlocks = new ArrayList<Block>();
		
	   /*
	    * Go through all the blocks in a given level, and check each
	    * block through the loop. If the block is of height 12 or less
	    * it is put into the block1 array list. If less than or equal to
	    * 36, put it into block2. If it is any more than that, put it
	    * into block3.
	    */
		for (int i = (level.width * level.height) - 1; i >= 0; i--)
		{
			Block temp = level.blocks[i];

			if(!temp.seeThrough && temp.isSolid)
			{
				blocks.add(temp);
			}
			else if(temp.seeThrough)
			{
				entityBlocks.add(temp);
			}
					
		}
		
	   /*
	    * Sorts the blocks array list using quicksort and the compareTo
	    * method that is overridden in the Block class. Sorts blocks in
	    * descending order of height so higher blocks are rendered before
	    * smaller blocks.
	    */
		Collections.sort(blocks);

		
		//Renders blocks from tallest to shortest
		for(Block temp: blocks)
		{			
			renderWallsCorrect(temp);
		}
		
	   /*
	    * Using bubble sort, and casts, this organizes the blocks based
	    * on where the players looking. 
	    * 
	    * In this case the player is looking at quadrant 1 (southeast) and
	    * therefore blocks are rendered from pos z to neg, and from pos x
	    * to neg x.
	    */
		if(Math.sin(Player.rotation) >= 0 && Math.cos(Player.rotation) >= 0)
		{
			Collections.sort(entityBlocks, Block.ninetyDegrees);
		}
	   /*
	    * Player is looking Northeast at quadrant 2, and blocks are rendered
	    * from negative z to positive, and positive x to negative x
	    */
		else if(Math.sin(Player.rotation) >= 0 && Math.cos(Player.rotation) < 0)
		{
			Collections.sort(entityBlocks, Block.oneEightyDegrees);
		}
	   /*
	    * Quadrant 3 (Northwest): Blocks rendered from negative x and z to
	    * postive x and z.
	    */
		else if(Math.sin(Player.rotation) < 0 && Math.cos(Player.rotation) < 0)
		{
			Collections.sort(entityBlocks, Block.twoSeventyDegrees);
		}
	   /*
	    * Quandrant 4 (Southwest): Positive z to negative, and
	    * negative x to positive x.
	    */
		else
		{
			Collections.sort(entityBlocks, Block.threeSixtyDegrees);
		}		
		
	   /*
	    * Renders all the entityBlocks in the order that they are in the
	    * array. 
	    */
		for(Block block: entityBlocks)
		{			
			renderWallsCorrect(block);
		}
		
	   /*
	    * For every enemy on the map, render each
	    */
		for(int i = 0; i < game.enemies.size(); i++)
		{
			Enemy enemy = game.enemies.get(i);
			Block temp = Level.getBlock((int)enemy.xPos, (int)enemy.zPos);
			
			double yCorrect = enemy.getY();
			
		   /*
		    * Because the boss is so gosh darn big, correct his graphical
		    * height of the ground so it looks like he is touching the
		    * ground.
		    */
			if(enemy.ID == 6 || enemy.ID == 8)
			{
				yCorrect -= 42;
			}
			
			renderEnemy(enemy.getX(), yCorrect, enemy.getZ(), 0.2,
					enemy.ID, enemy);
		}
		
	   /*
	    * For each item, render correctly
	    */
		for(int i = 0; i < game.items.size(); i++)
		{
			Item item = game.items.get(i);
			
			double distance = 
					Math.sqrt(((Math.abs(Player.x - item.x))
					* (Math.abs(Player.x - item.x)))
					+ ((Math.abs(Player.z - item.z))
							* (Math.abs(Player.z - item.z))));
			
		   /*
		    * Again because the units visually are different from the
		    * actual units, this corrects the height so that it matches
		    * the height that the players can visually pick it up.
		    * 
		    * For instance if an item is at height 12. It is going to
		    * look like it is at like a height of 36. The player can
		    * still pick it up, but it will look like its not 
		    * supposed to be picked up. This corrects that.
		    */
			double yCorrect = 0 - ((item.y/10)) -(item.y/12);
			
			if(item.y >= 12)
			{
				yCorrect = 0.2 - (item.y/11.7) - 1;
			}
			
		   /*
		    * The bigger the sprite needs to be for the item being
		    * displayed, then lift it higher off the ground so that
		    * it does not seem to clip through the ground.
		    */
			if(item.itemID == 1 
					|| item.itemID == 21
					|| item.itemID == 24 || item.itemID == 25
					|| item.itemID >= 33)
			{
				yCorrect -= 0.5;
			}
			if(item.itemID == 39 ||
					item.itemID == 42 || item.itemID == 43)
			{
				yCorrect -= 0.3;
			}
			if(item.itemID >= 29 && item.itemID < 33)
			{
				yCorrect -= 0.8;
			}
			if(item.itemID == 26)
			{
				yCorrect -= 0.3;
			}
			//If Satillite Dish
			if(item.itemID == 52)
			{
				yCorrect -= 3;
			}
			
			yCorrect -= (distance / 2) * 0.04;
			
			//Only render item if it is seeable (can be seen by player).
			if(item.isSeeable && item.itemID != 32)
			{
				renderItems(item.x, yCorrect + 0.77, item.z,
					0, item.itemID, item);
			}
		}
		
		//Stores all the canisters that are exploded completely
		ArrayList<ExplosiveCanister> toDelete 
			= new ArrayList<ExplosiveCanister>();
		
		//Go through all the canisters and render them
		for(int i = 0; i < game.canisters.size(); i++)
		{
			ExplosiveCanister temp = game.canisters.get(i);
			
			double distance = 
					Math.sqrt(((Math.abs(Player.x - temp.x))
					* (Math.abs(Player.x - temp.x)))
					+ ((Math.abs(Player.z - temp.z))
							* (Math.abs(Player.z - temp.z))));
			
		   /*
		    * Again because the units visually are different from the
		    * actual units, this corrects the height so that it matches
		    * the height that the players can visually pick it up.
		    * 
		    * For instance if an item is at height 12. It is going to
		    * look like it is at like a height of 36. The player can
		    * still pick it up, but it will look like its not 
		    * supposed to be picked up. This corrects that.
		    */
			double yCorrect = 0 - ((temp.y/10)) -(temp.y/12);
			
			if(temp.y >= 12)
			{
				yCorrect = 0.2 - (temp.y/11.7) - 1;
			}
			
			yCorrect -= 0.8;
			
			yCorrect -= (distance / 2) * 0.04;
			
			renderCanister(temp.x, yCorrect + 0.77, temp.z,
					0, temp);
		}
		
		for(int i = 0; i < game.corpses.size(); i++)
		{
			Corpse corpse = game.corpses.get(i);
			
			double distance = 
					Math.sqrt(((Math.abs(Player.x - corpse.x))
					* (Math.abs(Player.x - corpse.x)))
					+ ((Math.abs(Player.z - corpse.z))
							* (Math.abs(Player.z - corpse.z))));
			
			Block temp = Level.getBlock((int)corpse.x, (int)corpse.z);
			
			double totalHeight = temp.height + temp.y;
			double heightCorrect = 0;

			if(totalHeight >= 0 && totalHeight < 18)
			{
				heightCorrect = 8;
			}
			else if(totalHeight >= 18 && totalHeight < 30)
			{
				heightCorrect = 9;
			}
			else if(totalHeight >= 30 && totalHeight <= 36)
			{
				heightCorrect = 9;
			}
			else if(totalHeight > 36 && totalHeight <= 48)
			{
				heightCorrect = 10;
			}
			else if(totalHeight <= 79)
			{
				heightCorrect = 10;
			}
			else
			{
				double addCorrect;
				totalHeight -= 60;
				
				addCorrect = totalHeight / 20;
				heightCorrect = 10 + (0.5 * addCorrect);
				
			}
			
			double yCorrect = (-corpse.y / heightCorrect) + 0.5;
			
			yCorrect -= (distance / 2) * 0.04;
				
			renderCorpse(corpse.x, yCorrect, corpse.z,
				0, corpse);
			
		}
		
	   /*
	    * Renders all the bullet objects
	    */
		for(int i = 0; i < game.bullets.size(); i++)
		{
			Bullet bullet = game.bullets.get(i);
			renderProjectiles(bullet.x, bullet.y, bullet.z,
					0.2, bullet.ID);
		}	
		
	   /*
	    * Renders all the enemy projectiles
	    */
		for(int i = 0; i < game.enemyProjectiles.size(); i++)
		{
			EnemyFire temp = game.enemyProjectiles.get(i);
			renderProjectiles(temp.x, temp.y, temp.z,
					0.2, temp.ID);
		}
	}
	
   /*
    * Renders each enemy on the map as a sprite. Each sprites position is
    * corrected for the players movement so that it does not move (other
    * than its own movement) in correlation to the players movement.
    */
	public void renderEnemy(double x, double y, double z, double hOffSet, int ID, Enemy enemy)
	{
	   /*
	    * The change in x, y, and z coordinates in correlation to the player to
	    * correct them in accordance to the players position so that they don't
	    * seem to move. Casts a vector between the item and Player to
	    * figure out the difference in distance.
	    */
		double xC 		        = (x - Player.x) * 1.9;
		double yC 			    = (y / enemy.heightCorrect) + (Player.y * 0.1);
		double zC 			    = (z - Player.z) * 1.9;
		
		//Size of sprite in terms of pixels. 512 x 512 is typical 
		int spriteSize          = 512;
		
		//If Morgoth
		if(ID == 6 || ID == 8)
		{
			spriteSize = 4096;
		}
		
	   /*
	    * Corrects for rotation of the player. Allows the texture to
	    * still appear even if the Player does a complete circle
	    * around the object.
	    */
		double rotX      =  
				xC * cosine - zC * sine;
		double rotY      =  
				yC;
		double rotZ      =  
				zC * cosine + xC * sine;
		
	   /*
	    * Centers the way the sprites are rendered towards the center
	    * of the screen as well.
	    */
		double xCenter   = WIDTH / 2;
		//double yCenter   = HEIGHT / 2;
		
		//Draws vector from the player to the object
		double xPixel    = ((rotX / rotZ) * HEIGHT) + xCenter;
		
		//Draws a vector from the players height to the object
		//Which also depends where the player is looking up or down
		double yPixel    = ((rotY / rotZ) * HEIGHT) + 
				(HEIGHT / Math.sin(Player.upRotate) 
						* Math.cos(Player.upRotate));
		
	   /*
	    * Figures out the sides of the object being rendered by drawing
	    * vectors to them based on the vecters figured out above.
	    */
		double xPixelL   = xPixel - (spriteSize / rotZ);
		double xPixelR   = xPixel + (spriteSize / rotZ);
		double yPixelL   = yPixel - (spriteSize / rotZ);
		double yPixelR   = yPixel + (spriteSize / rotZ);
		
		//Converts them into ints
		int xPixL = (int)(xPixelL);
		int xPixR = (int)(xPixelR);
		int yPixL = (int)(yPixelL);
		int yPixR = (int)(yPixelR);
		
	   /*
	    * If they are off the screen, cut off the sprite from rendering
	    * outstide of the frame.
	    */
		if(xPixL < 0)
		{
			xPixL = 0;
		}
		
		if(xPixR > WIDTH)
		{
			xPixR = WIDTH;
		}
		
		if(yPixL < 0)
		{
			yPixL = 0;
		}
		
		if(yPixR > HEIGHT)
		{
			yPixR = HEIGHT;
		}
		
		//I used this because it didn't work without this.
		rotZ *= 8;
		
		int imageDimensions = 256;
		
		enemy.enemyPhase++;
		enemy.inSight = false;
		
	   /*
	    * Render each pixel in the sprite correctly, and with the 
	    * (zBuffer) as well so that sprites behind other sprites will
	    * not clip through sprites in front of them. 
	    * 
	    * For each sprite also texture them correctly
	    * and correct the textures for rotation and movement.
	    */
		for(int yy = yPixL + 1; yy < yPixR; yy++)
		{
			//How the sprite rotates up or down based on players up and
			//down rotation
			double pixelRotationY = -((yy - yPixelR) / (yPixelL - yPixelR));
			
			//Sets the current pixel to an int, and multiplies it by
			//256 since the image is 256 by 256
			int yTexture = (int)(pixelRotationY * imageDimensions);
			
			for(int xx = xPixL + 1; xx < xPixR; xx++)
			{
				//Same as above but for x position of pixel
				double pixelRotationX = -(xx - xPixelR) / (xPixelL - xPixelR);
				int xTexture = (int)(pixelRotationX * imageDimensions);
				
				//Finds exact pixel on screen
				int temp = xx + yy * WIDTH;
				
			   /*
			    * In case a rare bug continues to occur.
			    */
				if(temp < 0)
				{
					temp = 0;
				}
				
				if(zBuffer[temp] > rotZ)
				{
					enemy.inSight = true;
					
					int color = 0;
					
					//If in sight, activate the enemy
					if(!enemy.activated && Display.skillMode > 0
							&& Player.alive)
					{					
						boolean canActivate = true;
						
					   /*
					    * Because the flying enemies can see the player
					    * from much farther away than any other normal
					    * enemy standing on the ground, this limits how
					    * far away the flying enemy can activate so
					    * that all the flying enemies in the map do not
					    * activate at once causing issues.
					    */
						if(enemy.ID == 2 && enemy.distance <= 10)
						{
							
						}
						else if(enemy.ID == 2 && enemy.distance > 10)
						{
							canActivate = false;
						}
						
						if(canActivate)
						{
							enemy.activated = true;
							
							if(enemy.ID == 6 || enemy.ID == 8)
							{
								Display.playAudioFile(Display.input20, 
										Display.bossActivate, 
										Display.clipNum13);
								Display.clipNum13++;
								
								if(Display.clipNum13 == 
										Display.bossActivate.length)
								{
									Display.clipNum13 = 0;
								}
							}
							else if(enemy.ID == 1 || enemy.ID == 2)
							{
								Display.playAudioFile(Display.input21,
										Display.enemyActivate,
										Display.clipNum14);
								Display.clipNum14++;
								
								if(Display.clipNum14 == 
										Display.enemyActivate.length)
								{
									Display.clipNum14 = 0;
								}
							}
							else if(enemy.ID == 3)
							{
								Display.playAudioFile(Display.input26,
										Display.enemy3Activate,
										Display.clipNum17);
								Display.clipNum17++;
								
								if(Display.clipNum17 == 
										Display.enemy3Activate.length)
								{
									Display.clipNum17 = 0;
								}
							}
							else if(enemy.ID == 4)
							{
								Display.playAudioFile(Display.input27,
										Display.enemy4Activate,
										Display.clipNum18);
								Display.clipNum18++;
								
								if(Display.clipNum18 == 
										Display.enemy4Activate.length)
								{
									Display.clipNum18 = 0;
								}
							}
							else if(enemy.ID == 5)
							{
								Display.playAudioFile(Display.input28,
										Display.enemy5Activate,
										Display.clipNum19);
								Display.clipNum19++;
								
								if(Display.clipNum19 == 
										Display.enemy5Activate.length)
								{
									Display.clipNum19 = 0;
								}
							}
							else if(enemy.ID == 7)
							{
								Display.playAudioFile(Display.input29,
										Display.enemy7Activate,
										Display.clipNum20);
								Display.clipNum20++;
								
								if(Display.clipNum20 == 
										Display.enemy7Activate.length)
								{
									Display.clipNum20 = 0;
								}
							}
						}
					}
					
					//Brain demon
					if(ID == 1)
					{
						//If enemy was recently hurt
						if(enemy.harmed > 0)
						{
							color = Textures.enemy1hurt.PIXELS
							[(xTexture & 255) + (yTexture & 255) * 256];
							
							enemy.enemyPhase = 0;
						}
						else
						{
							//If enemy is firing, then show enemy
							//firing phases
							if(enemy.isFiring)
							{
								if(enemy.tick <= 2 * ((Display.fps / 30) + 1))
								{
									color = Textures.enemy1fire1.PIXELS
									[(xTexture & 255) + (yTexture & 255) * 256];
								}
								else if(enemy.tick <= 5 * ((Display.fps / 30) + 1))
								{
									color = Textures.enemy1fire2.PIXELS
									[(xTexture & 255) + (yTexture & 255) * 256];
								}
								else if(enemy.tick <= 8 * ((Display.fps / 30) + 1))
								{
									color = Textures.enemy1fire3.PIXELS
									[(xTexture & 255) + (yTexture & 255) * 256];
								}
								else if(enemy.tick >= 8 * ((Display.fps / 30) + 1))
								{
									color = Textures.enemy1fire4.PIXELS
									[(xTexture & 255) + (yTexture & 255) * 256];
								}
							}
							//If enemy is attacking, then show the
							//phases of that
							else if(enemy.isAttacking)
							{
								if(enemy.tick <= 3 * ((Display.fps / 30) + 1))
								{
									color = Textures.enemy1fire2.PIXELS
									[(xTexture & 255) + (yTexture & 255) * 256];
								}
								else if(enemy.tick <= 6 * ((Display.fps / 30) + 1))
								{
									color = Textures.enemy1fire3.PIXELS
									[(xTexture & 255) + (yTexture & 255) * 256];
								}
								else if(enemy.tick <= 9 * ((Display.fps / 30) + 1))
								{
									color = Textures.enemy1fire2.PIXELS
									[(xTexture & 255) + (yTexture & 255) * 256];
								}
								else if(enemy.tick >= 9 * ((Display.fps / 30) + 1))
								{
									color = Textures.enemy1fire1.PIXELS
									[(xTexture & 255) + (yTexture & 255) * 256];
								}
							}
							else
							{
								//Sets color to the color int of a pixel at a
								//given location in the image to be rendered
								if(enemy.enemyPhase <= 14)
								{
									color = Textures.enemy1.PIXELS
										[(xTexture & 255) + (yTexture & 255) * 256];
								}
								else if(enemy.enemyPhase <= 28)
								{
									color = Textures.enemy1b.PIXELS
										[(xTexture & 255) + (yTexture & 255) * 256];
								}
								
								//If done with phases, start over again
								if(enemy.enemyPhase >= 28)
								{
									enemy.enemyPhase = 0;
								}
								
							}
						}
					}
					//Flyerdemon
					else if(ID == 2)
					{
						//If enemy was recently hurt
						if(enemy.harmed > 0)
						{
							color = Textures.enemy2hurt.PIXELS
							[(xTexture & 255) + (yTexture & 255) * 256];
							
							enemy.enemyPhase = 0;
						}
						else
						{
							//If enemy is firing, then show enemy
							//firing phases
							if(enemy.isFiring)
							{
								if(enemy.tick >= 5 * ((Display.fps / 30) + 1))
								{
									color = Textures.enemy2fire1.PIXELS
									[(xTexture & 255) + (yTexture & 255) * 256];
								}
								else
								{
									color = Textures.enemy2.PIXELS
									[(xTexture & 255) + (yTexture & 255) * 256];
								}
							}
							//If enemy is attacking, then show the
							//phases of that
							else if(enemy.isAttacking)
							{
								if(enemy.tick <= 3 * ((Display.fps / 30) + 1))
								{
									color = Textures.enemy2fire1.PIXELS
									[(xTexture & 255) + (yTexture & 255) * 256];
								}
								else if(enemy.tick <= 6 * ((Display.fps / 30) + 1))
								{
									color = Textures.enemy2.PIXELS
									[(xTexture & 255) + (yTexture & 255) * 256];
								}
								else if(enemy.tick <= 9 * ((Display.fps / 30) + 1))
								{
									color = Textures.enemy2fire1.PIXELS
									[(xTexture & 255) + (yTexture & 255) * 256];
								}
								else if(enemy.tick >= 9 * ((Display.fps / 30) + 1))
								{
									color = Textures.enemy2.PIXELS
									[(xTexture & 255) + (yTexture & 255) * 256];
								}
							}
							else
							{
								color = Textures.enemy2.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
						
								enemy.enemyPhase = 0;
							}
						}
					}
					//Mutated biologist
					else if(ID == 3)
					{
						//If enemy was recently hurt
						if(enemy.harmed > 0)
						{
							color = Textures.enemy3hurt.PIXELS
							[(xTexture & 255) + (yTexture & 255) * 256];
							
							enemy.enemyPhase = 0;
						}
						else
						{
							//If enemy is firing, then show enemy
							//firing phases
							if(enemy.isFiring)
							{
								if(enemy.tick <= 4 * ((Display.fps / 30) + 1))
								{
									color = Textures.enemy3fire1.PIXELS
									[(xTexture & 255) + (yTexture & 255) * 256];
								}
								else if(enemy.tick <= 8 * ((Display.fps / 30) + 1))
								{
									color = Textures.enemy3fire2.PIXELS
									[(xTexture & 255) + (yTexture & 255) * 256];
								}
								else if(enemy.tick >= 8 * ((Display.fps / 30) + 1))
								{
									color = Textures.enemy3fire3.PIXELS
									[(xTexture & 255) + (yTexture & 255) * 256];
								}
							}
							else
							{
								if(enemy.enemyPhase <= 14)
								{
									color = Textures.enemy3.PIXELS
										[(xTexture & 255) + (yTexture & 255) * 256];
								}
								else if(enemy.enemyPhase <= 28)
								{
									color = Textures.enemy3b.PIXELS
										[(xTexture & 255) + (yTexture & 255) * 256];
								}
								else if(enemy.enemyPhase <= 42)
								{
									color = Textures.enemy3c.PIXELS
										[(xTexture & 255) + (yTexture & 255) * 256];
								}
								else if(enemy.enemyPhase <= 56)
								{
									color = Textures.enemy3d.PIXELS
										[(xTexture & 255) + (yTexture & 255) * 256];
								}
								else if(enemy.enemyPhase <= 70)
								{
									color = Textures.enemy3e.PIXELS
										[(xTexture & 255) + (yTexture & 255) * 256];
								}
								else if(enemy.enemyPhase <= 84)
								{
									color = Textures.enemy3f.PIXELS
										[(xTexture & 255) + (yTexture & 255) * 256];
								}
								
								if(enemy.enemyPhase >= 84)
								{
									enemy.enemyPhase = 0;
								}
							}
						}
					}
					//Reaper
					else if(ID == 4)
					{
						if(enemy.enemyPhase <= 4)
						{
							color = Textures.enemy4.PIXELS
							[(xTexture & 255) + (yTexture & 255) * 256];
						}
						else
						{
							color = Textures.enemy4b.PIXELS
							[(xTexture & 255) + (yTexture & 255) * 256];
						}
						
						if(enemy.enemyPhase > 9)
						{
							enemy.enemyPhase = 0;
						}
					}
					//Resurrector
					else if(ID == 5)
					{
						//If enemy is firing, then show enemy
						//firing phases
						if(enemy.isFiring)
						{
							if(enemy.tick <= 3 * ((Display.fps / 30) + 1))
							{
								color = Textures.enemy5fire1.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
							}
							else if(enemy.tick <= 6 * ((Display.fps / 30) + 1))
							{
								color = Textures.enemy5fire2.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
							}
							else if(enemy.tick <= 12 * ((Display.fps / 30) + 1))
							{
								color = Textures.enemy5fire3.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
							}
							else if(enemy.tick <= 18 * ((Display.fps / 30) + 1))
							{
								color = Textures.enemy5fire4.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
							}
							else if(enemy.tick >= 18 * ((Display.fps / 30) + 1))
							{
								color = Textures.enemy5fire5.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
							}
						}
						else
						{
							//Sets color to the color int of a pixel at a
							//given location in the image to be rendered
							if(enemy.enemyPhase <= 7)
							{
								color = Textures.enemy5.PIXELS
									[(xTexture & 255) + (yTexture & 255) * 256];
							}
							else if(enemy.enemyPhase <= 14)
							{
								color = Textures.enemy5b.PIXELS
									[(xTexture & 255) + (yTexture & 255) * 256];
							}
							
							//If done with phases, start over again
							if(enemy.enemyPhase >= 14)
							{
								enemy.enemyPhase = 0;
							}
						}
					}
					//Morgoth
					else if(ID == 6)
					{
						//If enemy was recently hurt
						if(enemy.harmed > 0)
						{
							color = Textures.morgothHurt.PIXELS
							[(xTexture & 255) + (yTexture & 255) * 256];
							
							enemy.enemyPhase = 0;
						}
						else
						{
							//If enemy is firing, then show enemy
							//firing phases
							if(enemy.isFiring)
							{
								if(enemy.tick <= 6 * ((Display.fps / 30) + 1))
								{
									color = Textures.morgothFire1.PIXELS
									[(xTexture & 255) + (yTexture & 255) * 256];
								}
								else if(enemy.tick >= 6 * ((Display.fps / 30) + 1))
								{
									color = Textures.morgothFire2.PIXELS
									[(xTexture & 255) + (yTexture & 255) * 256];
								}
							}
							//If enemy is attacking, then show the
							//phases of that
							else if(enemy.isAttacking)
							{
								if(enemy.tick <= 3 * ((Display.fps / 30) + 1))
								{
									color = Textures.morgothMelee.PIXELS
									[(xTexture & 255) + (yTexture & 255) * 256];
								}
								else if(enemy.tick <= 6 * ((Display.fps / 30) + 1))
								{
									color = Textures.morgoth.PIXELS
									[(xTexture & 255) + (yTexture & 255) * 256];
								}
								else if(enemy.tick <= 9 * ((Display.fps / 30) + 1))
								{
									color = Textures.morgothMelee.PIXELS
									[(xTexture & 255) + (yTexture & 255) * 256];
								}
								else if(enemy.tick >= 9 * ((Display.fps / 30) + 1))
								{
									color = Textures.morgoth.PIXELS
									[(xTexture & 255) + (yTexture & 255) * 256];
								}
							}
							else
							{
								//Runs through movement phases of the 
								//enemies textures.
								if(enemy.enemyPhase <= 7)
								{
									color = Textures.morgoth.PIXELS
										[(xTexture & 255) + (yTexture & 255) * 256];
								}
								else if(enemy.enemyPhase <= 14)
								{
									color = Textures.morgoth2.PIXELS
										[(xTexture & 255) + (yTexture & 255) * 256];
								}
								else if(enemy.enemyPhase <= 21)
								{
									color = Textures.morgoth3.PIXELS
										[(xTexture & 255) + (yTexture & 255) * 256];
								}
								else if(enemy.enemyPhase <= 28)
								{
									color = Textures.morgoth4.PIXELS
										[(xTexture & 255) + (yTexture & 255) * 256];
								}
								
								if(enemy.enemyPhase >= 28)
								{
									enemy.enemyPhase = 0;
								}
							}
						}
					}
					//Vile Civilian
					else if(ID == 7)
					{
						//If enemy was recently hurt
						if(enemy.harmed > 0)
						{
							color = Textures.vileCivHurt.PIXELS
							[(xTexture & 255) + (yTexture & 255) * 256];
							
							enemy.enemyPhase = 0;
						}
						else
						{
							//If enemy is attacking, then show the
							//phases of that
							if(enemy.isAttacking)
							{
								if(enemy.tick <= 6 * ((Display.fps / 30) + 1))
								{
									color = Textures.vileCivAttack1.PIXELS
									[(xTexture & 255) + (yTexture & 255) * 256];
								}
								else if(enemy.tick >= 6 * ((Display.fps / 30) + 1))
								{
									color = Textures.vileCivAttack2.PIXELS
									[(xTexture & 255) + (yTexture & 255) * 256];
								}
							}
							else
							{
								//Runs through movement phases of the 
								//enemies textures.
								if(enemy.enemyPhase <= 7)
								{
									color = Textures.vileCiv1.PIXELS
										[(xTexture & 255) + (yTexture & 255) * 256];
								}
								else if(enemy.enemyPhase <= 14)
								{
									color = Textures.vileCiv2.PIXELS
										[(xTexture & 255) + (yTexture & 255) * 256];
								}
								else if(enemy.enemyPhase <= 21)
								{
									color = Textures.vileCiv3.PIXELS
										[(xTexture & 255) + (yTexture & 255) * 256];
								}
								else if(enemy.enemyPhase <= 28)
								{
									color = Textures.vileCiv4.PIXELS
										[(xTexture & 255) + (yTexture & 255) * 256];
								}
								
								if(enemy.enemyPhase >= 28)
								{
									enemy.enemyPhase = 0;
								}
							}
						}			
					}
					//Belegoth
					else if(ID == 8)
					{
						//If enemy was recently hurt
						if(enemy.harmed > 0)
						{
							color = Textures.belegothHurt.PIXELS
							[(xTexture & 255) + (yTexture & 255) * 256];
							
							enemy.enemyPhase = 0;
						}
						else
						{
							//If enemy is firing, then show enemy
							//firing phases
							if(enemy.isFiring)
							{
								if(enemy.tick <= 2 * ((Display.fps / 30) + 1))
								{
									color = Textures.belegothAttack1.PIXELS
									[(xTexture & 255) + (yTexture & 255) * 256];
								}
								else if(enemy.tick <= 5 * ((Display.fps / 30) + 1))
								{
									color = Textures.belegothAttack2.PIXELS
									[(xTexture & 255) + (yTexture & 255) * 256];
								}
								else if(enemy.tick <= 8 * ((Display.fps / 30) + 1))
								{
									color = Textures.belegothAttack3.PIXELS
									[(xTexture & 255) + (yTexture & 255) * 256];
								}
								else if(enemy.tick >= 8 * ((Display.fps / 30) + 1))
								{
									color = Textures.belegothAttack4.PIXELS
									[(xTexture & 255) + (yTexture & 255) * 256];
								}
							}
							//If enemy is attacking, then show the
							//phases of that
							else if(enemy.isAttacking)
							{
								if(enemy.tick <= 3 * ((Display.fps / 30) + 1))
								{
									color = Textures.belegothMelee.PIXELS
									[(xTexture & 255) + (yTexture & 255) * 256];
								}
								else if(enemy.tick <= 6 * ((Display.fps / 30) + 1))
								{
									color = Textures.belegoth4.PIXELS
									[(xTexture & 255) + (yTexture & 255) * 256];
								}
								else if(enemy.tick <= 9 * ((Display.fps / 30) + 1))
								{
									color = Textures.belegothMelee.PIXELS
									[(xTexture & 255) + (yTexture & 255) * 256];
								}
								else if(enemy.tick >= 9 * ((Display.fps / 30) + 1))
								{
									color = Textures.belegoth4.PIXELS
									[(xTexture & 255) + (yTexture & 255) * 256];
								}
							}
							else
							{
								//Runs through movement phases of the 
								//enemies textures.
								if(enemy.enemyPhase <= 7)
								{
									color = Textures.belegoth.PIXELS
										[(xTexture & 255) + (yTexture & 255) * 256];
								}
								else if(enemy.enemyPhase <= 14)
								{
									color = Textures.belegoth2.PIXELS
										[(xTexture & 255) + (yTexture & 255) * 256];
								}
								else if(enemy.enemyPhase <= 21)
								{
									color = Textures.belegoth3.PIXELS
										[(xTexture & 255) + (yTexture & 255) * 256];
								}
								else if(enemy.enemyPhase <= 28)
								{
									color = Textures.belegoth4.PIXELS
										[(xTexture & 255) + (yTexture & 255) * 256];
								}
								else if(enemy.enemyPhase <= 35)
								{
									color = Textures.belegoth5.PIXELS
										[(xTexture & 255) + (yTexture & 255) * 256];
								}
								else if(enemy.enemyPhase <= 42)
								{
									color = Textures.belegoth6.PIXELS
										[(xTexture & 255) + (yTexture & 255) * 256];
								}
								
								if(enemy.enemyPhase >= 42)
								{
									enemy.enemyPhase = 0;
								}
							}
						}
					}
					
				
					
				   /*
				    * If color is not white, render it, otherwise don't to
				    * have transparency around your image. First two ff's
				    * are the images alpha, 2nd two are red, 3rd two are
				    * green, 4th two are blue. ff is 255.
				    */
					if(color != 0xffffffff)
					{
						try
						{
							//Sets pixel in 2D array to that color
							PIXELS[xx + yy * WIDTH] = color;
							zBuffer[xx + yy * WIDTH] = rotZ;
						}
						catch(Exception e)
						{
							
						}
					}
					
					//Reapers are translucent because theyre ghost 
					//technically
					if(ID == 4)
					{
						xx++;
					}
				}
			}
		}
	}
	
   /**
    * Renders items the same way as the enemies
    * @param x
    * @param y
    * @param z
    * @param hOffSet
    * @param ID
    */
	public void renderItems(double x, double y, double z, double hOffSet,
			int ID, Item item)
	{
		double yCorrect = Player.y;
		
	   /*
	    * If the player is crouching, this corrects the item graphics so
	    * that you can still see them as if you were actually crawling
	    * and not below the map as it would be if it used the negative
	    * Player.y as the actually y.
	    */
		if(Player.y < Player.maxHeight - 2)
		{
			yCorrect = Player.y - 1.77;
		}
		
		double xC 		        = ((x) - Player.x) * 1.9;
		double yC 			    = y + (yCorrect / 10);
		double zC 			    = ((z) - Player.z) * 1.9;
		
		int spriteSize          = 160;
		
	   /*
	    * Both Megahealths and shotguns need to be blown up bigger because
	    * the textures in the file themselves are smaller than most.
	    */
		if(ID == 1 || ID == 24 || ID == 26
				|| ID >= 33)
		{
			spriteSize          = 240;
		}
		
		if(ID == 21 || ID == 25)
		{
			spriteSize          = 480;
		}
		
		if(ID >= 29 && ID < 33
				|| ID == 39 || ID == 42
				|| ID == 43)
		{
			spriteSize          = 600;
		}
		
		//If Satillite
		if(ID == 52)
		{
			spriteSize = 2048;
		}
		
		double rotX      =  
				xC * cosine - zC * sine;
		double rotY      =  
				yC;
		double rotZ      =  
				zC * cosine + xC * sine;
		
		double xCenter   = WIDTH / 2;
		
		double xPixel    = ((rotX / rotZ) * HEIGHT) + xCenter;
		double yPixel    = ((rotY / rotZ) * HEIGHT) + 
				(HEIGHT / Math.sin(Player.upRotate) 
						* Math.cos(Player.upRotate));
		
		double xPixelL   = xPixel - (spriteSize / rotZ);
		double xPixelR   = xPixel + (spriteSize / rotZ);
		
		double yPixelL   = yPixel - (spriteSize / rotZ);
		double yPixelR   = yPixel + (spriteSize / rotZ);
		
		int xPixL = (int)(xPixelL);
		int xPixR = (int)(xPixelR);
		int yPixL = (int)(yPixelL);
		int yPixR = (int)(yPixelR);
		
		if(xPixL < 0)
		{
			xPixL = 0;
		}
		
		if(xPixR > WIDTH)
		{
			xPixR = WIDTH;
		}
		
		if(yPixL < 0)
		{
			yPixL = 0;
		}
		
		if(yPixR > HEIGHT)
		{
			yPixR = HEIGHT;
		}
		
		rotZ *= 8;
		
	   /*
	    * Because the rendering loop is longer the farther you are
	    * from the item, this updates the item no matter how far you
	    * are from the item, making the phase speed the same for the
	    * item
	    */
		item.phaseTime++;
		
		int imageDimensions = 256;
		
		//If chainmeal, render the picture as 32 by 32 since it cannot
		//be resized due to color blending in paint.
		if(ID == 33)
		{
			imageDimensions = 32;
		}
		
		
		for(int yy = yPixL; yy < yPixR; yy++)
		{
			double pixelRotationY = -(yy - yPixelR) / (yPixelL - yPixelR);
			int yTexture = (int)(pixelRotationY * imageDimensions);
			
			for(int xx = xPixL; xx < xPixR; xx++)
			{
				double pixelRotationX = -(xx - xPixelR) / (xPixelL - xPixelR);
				int xTexture = (int)(pixelRotationX * imageDimensions);
				
				if(zBuffer[xx + yy * WIDTH] > rotZ)
				{
					int color = 0;

					//If health pack
					if(ID == 2)
					{
						color = Textures.health.PIXELS
							[(xTexture & 255) + (yTexture & 255) * 256];
					}
					//If megahealth. Continue to switch phases
					else if(ID == 1)
					{
						if(item.phaseTime <= 20)
						{
							color = Textures.megaPhase1.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
						}
						else if(item.phaseTime <= 40)
						{
							color = Textures.megaPhase2.PIXELS
									[(xTexture & 255) + (yTexture & 255) * 256];
						}
						else if(item.phaseTime <= 60)
						{
							color = Textures.megaPhase3.PIXELS
									[(xTexture & 255) + (yTexture & 255) * 256];
						}
						else
						{
							color = Textures.megaPhase4.PIXELS
									[(xTexture & 255) + (yTexture & 255) * 256];
						}
						
						if(item.phaseTime > 80)
						{
							item.phaseTime = 0;
						}
					}
					else if(ID == 3)
					{
						item.phaseTime = 0;
						color = Textures.shell.PIXELS
							[(xTexture & 255) + (yTexture & 255) * 256];
					}
					else if(ID == 4)
					{
						item.phaseTime = 0;
						color = Textures.redKey.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
					}
					else if(ID == 5)
					{
						item.phaseTime = 0;
						color = Textures.blueKey.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
					}
					else if(ID == 6)
					{
						item.phaseTime = 0;
						color = Textures.greenKey.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
					}
					else if(ID == 7)
					{
						item.phaseTime = 0;
						color = Textures.yellowKey.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
					}
					else if(ID == 21)
					{
						item.phaseTime = 0;
						color = Textures.shotgun.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
					}
					else if(ID == 24)
					{
						int brightness = 255;
						
						if(item.phaseTime <= 20)
						{
							brightness = 150;
							
							color = Textures.resurrect1.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
						}
						else if(item.phaseTime <= 40)
						{
							brightness = 200;
							
							color = Textures.resurrect2.PIXELS
									[(xTexture & 255) + (yTexture & 255) * 256];
						}
						else if(item.phaseTime <= 60)
						{
							color = Textures.resurrect3.PIXELS
									[(xTexture & 255) + (yTexture & 255) * 256];
						}
						
						if(item.phaseTime > 60)
						{
							item.phaseTime = 0;
						}
						
						color = adjustBrightness(brightness, color, 0);
					}
					else if(ID == 25)
					{
						item.phaseTime = 0;
						color = Textures.environSuit.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
					}
					else if(ID == 26)
					{
						int brightness = 0;
						
						if(item.phaseTime <= 20)
						{
							brightness = 255;
							color = Textures.goblet1.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
						}
						else if(item.phaseTime <= 40)
						{
							brightness = 150;
							color = Textures.goblet2.PIXELS
									[(xTexture & 255) + (yTexture & 255) * 256];
						}
						else if(item.phaseTime <= 60)
						{
							brightness = 255;
							color = Textures.goblet3.PIXELS
									[(xTexture & 255) + (yTexture & 255) * 256];
						}
						
						if(item.phaseTime > 60)
						{
							item.phaseTime = 0;
						}
						
						color = adjustBrightness(brightness, color, 0);
					}
					else if(ID == 27)
					{
						item.phaseTime = 0;
						color = Textures.adrenaline.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
					}
					else if(ID == 28)
					{
						int brightness = 0;
						
						if(item.phaseTime <= 20)
						{
							brightness = 255;
							color = Textures.glasses.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
						}
						else if(item.phaseTime <= 40)
						{
							brightness = 150;
							color = Textures.glasses.PIXELS
									[(xTexture & 255) + (yTexture & 255) * 256];
						}	
						
						if(item.phaseTime > 40)
						{
							item.phaseTime = 0;
						}
						
						color = adjustBrightness(brightness, color, 0);
					}
					//Torch
					else if(ID == 29)
					{
						int brightness = 255;
						
						if(item.phaseTime <= 5)
						{
							brightness = 200;
							color = Textures.torch1.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
						}
						else if(item.phaseTime <= 10)
						{
							color = Textures.torch2.PIXELS
									[(xTexture & 255) + (yTexture & 255) * 256];
						}
						else if(item.phaseTime <= 15)
						{
							brightness = 200;
							color = Textures.torch4.PIXELS
									[(xTexture & 255) + (yTexture & 255) * 256];
						}
						else if(item.phaseTime <= 20)
						{
							color = Textures.torch3.PIXELS
									[(xTexture & 255) + (yTexture & 255) * 256];
						}
						
						if(item.phaseTime > 20)
						{
							item.phaseTime = 0;
						}
						
						color = adjustBrightness(brightness, color, 0);
					}
					//Lamp
					else if(ID == 30)
					{
						int brightness = 255;
						
						if(item.phaseTime <= 5)
						{
							brightness = 175;
							color = Textures.lamp3.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
						}
						else if(item.phaseTime <= 10)
						{
							brightness = 200;
							color = Textures.lamp2.PIXELS
									[(xTexture & 255) + (yTexture & 255) * 256];
						}
						else if(item.phaseTime <= 15)
						{
							brightness = 255;
							color = Textures.lamp1.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
						}
						else if(item.phaseTime <= 20)
						{
							brightness = 200;
							color = Textures.lamp2.PIXELS
									[(xTexture & 255) + (yTexture & 255) * 256];
						}
						
						if(item.phaseTime > 20)
						{
							item.phaseTime = 0;
						}
						
						color = adjustBrightness(brightness, color, 0);			
					}
					//Tree
					else if(ID == 31)
					{
						color = Textures.tree.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
						item.phaseTime = 0;
					}
					//Canister (Eventually explosive)
					else if(ID == 32)
					{
						
						color = Textures.canister.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
						item.phaseTime = 0;
					}
					//Chainmeal Armor
					else if(ID == 33)
					{
						color = Textures.chainmeal.PIXELS
								[(xTexture & 31) + (yTexture & 31) * 32];
						item.phaseTime = 0;
					}
					//Combat Armor
					else if(ID == 34)
					{
						color = Textures.combat.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
						item.phaseTime = 0;
					}
					//Argent Armor
					else if(ID == 35)
					{
						color = Textures.argent.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
						item.phaseTime = 0;
					}
					//Armor Shard/chip
					else if(ID == 36)
					{
						color = Textures.shard.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
						item.phaseTime = 0;
					}
					//Health vial
					else if(ID == 37)
					{
						color = Textures.vial.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
						item.phaseTime = 0;
					}
					//Barons Treasure
					else if(ID == 38)
					{
						color = Textures.baronTreasure.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
						item.phaseTime = 0;
					}
					//Dark Spellbook
					else if(ID == 39)
					{
						int brightness = 255;
						
						if(item.phaseTime <= 7)
						{
							brightness = 200;
							
							color = Textures.holyWater1.PIXELS
							[(xTexture & 255) + (yTexture & 255) * 256];
						}
						else if(item.phaseTime <= 14)
						{
							color = Textures.holyWater2.PIXELS
							[(xTexture & 255) + (yTexture & 255) * 256];
						}
						
						if(item.phaseTime > 14)
						{
							item.phaseTime = 0;
						}
						
						color = adjustBrightness(brightness, color, 0);
					}
					//Scepter of Deciet
					else if(ID == 40)
					{
						color = Textures.scepter.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
						item.phaseTime = 0;
					}
					//The head of Morgoth
					else if(ID == 41)
					{
						color = Textures.morgothHead.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
						item.phaseTime = 0;
					}
					//Table (empty)
					else if(ID == 42)
					{
						color = Textures.table.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
						item.phaseTime = 0;
					}
					//Lamp table
					else if(ID == 43)
					{
						color = Textures.lampTable.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
						item.phaseTime = 0;
					}
					//Shell Box
					else if(ID == 47)
					{
						color = Textures.shellBox.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
						item.phaseTime = 0;
					}
					//Phase Cannon
					else if(ID == 49)
					{
						color = Textures.phaseCannon.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
						item.phaseTime = 0;
					}
					//Charge Pack
					else if(ID == 50)
					{
						color = Textures.chargePack.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
						item.phaseTime = 0;
					}
					//Large Charge Pack
					else if(ID == 51)
					{
						color = Textures.largeChargePack.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
						item.phaseTime = 0;
					}
					//Satillite
					else if(ID == 52)
					{
						int brightness = 100;
						
						if(item.activated)
						{
							brightness = 255;
							
							if(item.phaseTime <= 7)
							{
								brightness = 200;
								
								color = Textures.sat1.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
							}
							else if(item.phaseTime <= 14)
							{
								color = Textures.sat2.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
							}
							
							if(item.phaseTime > 14)
							{
								item.phaseTime = 0;
							}
						}
						else
						{
							item.phaseTime = 0;
							
							color = Textures.sat1.PIXELS
							[(xTexture & 255) + (yTexture & 255) * 256];
						}
							
						color = adjustBrightness(brightness, color, 0);
					}
					//Pistol
					else if(ID == 55)
					{
						color = Textures.pistol.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
						item.phaseTime = 0;
					}
					//Bullet clip
					else if(ID == 56)
					{
						color = Textures.clip.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
						item.phaseTime = 0;
					}
					//Box of Bullets
					else if(ID == 57)
					{
						color = Textures.bullets.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
						item.phaseTime = 0;
					}
					
				   /*
				    * If color is not white, render it, otherwise don't to
				    * have transparency around your image. First two ff's
				    * are the images alpha, 2nd two are red, 3rd two are
				    * green, 4th two are blue. ff is 255.
				    */
					if(color != 0xffffffff)
					{
						PIXELS[xx + yy * WIDTH] = color;
						zBuffer[xx + yy * WIDTH] = rotZ;
					}
				}
			}
		}
	}
	
	public void renderCanister(double x, double y, double z,
			double hOffSet, ExplosiveCanister item)
	{	
		double yCorrect = Player.y;
		
	   /*
	    * If the player is crouching, this corrects the item graphics so
	    * that you can still see them as if you were actually crawling
	    * and not below the map as it would be if it used the negative
	    * Player.y as the actually y.
	    */
		if(Player.y < Player.maxHeight - 2)
		{
			yCorrect = Player.y - 1.77;
		}
		
		double xC 		        = ((x) - Player.x) * 1.9;
		double yC 			    = y + (yCorrect / 10);
		double zC 			    = ((z) - Player.z) * 1.9;
		
		int spriteSize          = 600;
		
		double rotX      =  
				xC * cosine - zC * sine;
		double rotY      =  
				yC;
		double rotZ      =  
				zC * cosine + xC * sine;
		
		double xCenter   = WIDTH / 2;
		
		double xPixel    = ((rotX / rotZ) * HEIGHT) + xCenter;
		double yPixel    = ((rotY / rotZ) * HEIGHT) + 
				(HEIGHT / Math.sin(Player.upRotate) 
						* Math.cos(Player.upRotate));
		
		double xPixelL   = xPixel - (spriteSize / rotZ);
		double xPixelR   = xPixel + (spriteSize / rotZ);
		
		double yPixelL   = yPixel - (spriteSize / rotZ);
		double yPixelR   = yPixel + (spriteSize / rotZ);
		
		int xPixL = (int)(xPixelL);
		int xPixR = (int)(xPixelR);
		int yPixL = (int)(yPixelL);
		int yPixR = (int)(yPixelR);
		
		if(xPixL < 0)
		{
			xPixL = 0;
		}
		
		if(xPixR > WIDTH)
		{
			xPixR = WIDTH;
		}
		
		if(yPixL < 0)
		{
			yPixL = 0;
		}
		
		if(yPixR > HEIGHT)
		{
			yPixR = HEIGHT;
		}
		
		rotZ *= 8;
		
		if(!item.exploding)
		{
			item.phaseTime = 0;
		}
		
		int imageDimensions = 256;	
		
		for(int yy = yPixL; yy < yPixR; yy++)
		{
			double pixelRotationY = -(yy - yPixelR) / (yPixelL - yPixelR);
			int yTexture = (int)(pixelRotationY * imageDimensions);
			
			for(int xx = xPixL; xx < xPixR; xx++)
			{
				double pixelRotationX = -(xx - xPixelR) / (xPixelL - xPixelR);
				int xTexture = (int)(pixelRotationX * imageDimensions);
				
				//If within field of view
				if(zBuffer[xx + yy * WIDTH] > rotZ)
				{
					int color = 0;
					
					if(!item.exploding)
					{
						color = Textures.canister.PIXELS
							[(xTexture & 255) + (yTexture & 255) * 256];
						item.phaseTime = 0;
					}
					else
					{						
						if(item.phaseTime <= 4)
						{
							color = Textures.canExplode1.PIXELS
							[(xTexture & 255) + (yTexture & 255) * 256];
						}
						else if(item.phaseTime <= 8)
						{
							color = Textures.canExplode2.PIXELS
							[(xTexture & 255) + (yTexture & 255) * 256];
						}
						else if(item.phaseTime <= 12)
						{
							color = Textures.canExplode3.PIXELS
							[(xTexture & 255) + (yTexture & 255) * 256];
						}
						else if(item.phaseTime <= 16)
						{
							color = Textures.canExplode4.PIXELS
							[(xTexture & 255) + (yTexture & 255) * 256];
						}
						
						if(item.phaseTime >= 16)
						{
							color = Textures.canExplode4.PIXELS
							[(xTexture & 255) + (yTexture & 255) * 256];
							item.removeCanister();
							return;
						}
					}
								
				   /*
				    * If color is not white, render it, otherwise don't to
				    * have transparency around your image. First two ff's
				    * are the images alpha, 2nd two are red, 3rd two are
				    * green, 4th two are blue. ff is 255.
				    */
					if(color != 0xffffffff)
					{
						PIXELS[xx + yy * WIDTH] = color;
						zBuffer[xx + yy * WIDTH] = rotZ;
					}
				}
			}
		}
	}
	
   /**
    * Render all the corpses and their different phases as they fall
    * to the ground after being killed.
    * @param x
    * @param y
    * @param z
    * @param hOffSet
    * @param corpse
    */
	public void renderCorpse(double x, double y, double z,
			double hOffSet, Corpse corpse)
	{
		double yCorrect = Player.y;
		
		int spriteSize          = 600;
		
		if(corpse.enemyID == 6 || corpse.enemyID == 8)
		{
			spriteSize = 4096;
			y -= 3;
		}
		
	   /*
	    * If the player is crouching, this corrects the item graphics so
	    * that you can still see them as if you were actually crawling
	    * and not below the map as it would be if it used the negative
	    * Player.y as the actually y.
	    */
		if(Player.y < Player.maxHeight - 2)
		{
			yCorrect = Player.y - 1.77;
		}
		
		double xC 		        = ((x) - Player.x) * 1.9;
		double yC 			    = y + (yCorrect / 10);
		double zC 			    = ((z) - Player.z) * 1.9;
		
		double rotX      =  
				xC * cosine - zC * sine;
		double rotY      =  
				yC;
		double rotZ      =  
				zC * cosine + xC * sine;
		
		double xCenter   = WIDTH / 2;
		
		double xPixel    = ((rotX / rotZ) * HEIGHT) + xCenter;
		double yPixel    = ((rotY / rotZ) * HEIGHT) + 
				(HEIGHT / Math.sin(Player.upRotate) 
						* Math.cos(Player.upRotate));
		
		double xPixelL   = xPixel - (spriteSize / rotZ);
		double xPixelR   = xPixel + (spriteSize / rotZ);
		
		double yPixelL   = yPixel - (spriteSize / rotZ);
		double yPixelR   = yPixel + (spriteSize / rotZ);
		
		int xPixL = (int)(xPixelL);
		int xPixR = (int)(xPixelR);
		int yPixL = (int)(yPixelL);
		int yPixR = (int)(yPixelR);
		
		if(xPixL < 0)
		{
			xPixL = 0;
		}
		
		if(xPixR > WIDTH)
		{
			xPixR = WIDTH;
		}
		
		if(yPixL < 0)
		{
			yPixL = 0;
		}
		
		if(yPixR > HEIGHT)
		{
			yPixR = HEIGHT;
		}
		
		rotZ *= 8;
		
		if(corpse.phaseTime > 0)
		{
			corpse.phaseTime--;
		}
		
		int imageDimensions = 256;	
		
		for(int yy = yPixL; yy < yPixR; yy++)
		{
			double pixelRotationY = -(yy - yPixelR) / (yPixelL - yPixelR);
			int yTexture = (int)(pixelRotationY * imageDimensions);
			
			for(int xx = xPixL; xx < xPixR; xx++)
			{
				double pixelRotationX = -(xx - xPixelR) / (xPixelL - xPixelR);
				int xTexture = (int)(pixelRotationX * imageDimensions);
				
				//If within field of view
				if(zBuffer[xx + yy * WIDTH] > rotZ)
				{
					int color = 0;
					
					//Depending on corpse, have a different sprite
					//depending on the enemy killed.
					if(corpse.phaseTime == 0)
					{
						if(corpse.enemyID == 7)
						{
							color = Textures.corpseType2.PIXELS
							[(xTexture & 255) + (yTexture & 255) * 256];
						}
						else if(corpse.enemyID == 5)
						{
							color = Textures.enemy5corpse.PIXELS
							[(xTexture & 255) + (yTexture & 255) * 256];
						}
						else if(corpse.enemyID == 1)
						{
							color = Textures.enemy1corpse.PIXELS
							[(xTexture & 255) + (yTexture & 255) * 256];
						}
						else if(corpse.enemyID == 2)
						{
							color = Textures.enemy2corpse.PIXELS
							[(xTexture & 255) + (yTexture & 255) * 256];
						}
						else if(corpse.enemyID == 3)
						{
							color = Textures.enemy3corpse.PIXELS
							[(xTexture & 255) + (yTexture & 255) * 256];
						}
						else if(corpse.enemyID == 8)
						{
							color = Textures.belegothCorpse.PIXELS
							[(xTexture & 255) + (yTexture & 255) * 256];
						}
						else
						{
							color = Textures.corpse.PIXELS
							[(xTexture & 255) + (yTexture & 255) * 256];
						}
						
						corpse.phaseTime = 0;
					}
					//As corpse falls, make its animation look realistic
					else
					{
						if(corpse.enemyID != 1 && corpse.enemyID != 5
								&& corpse.enemyID != 2 
								&& corpse.enemyID != 8)
						{
							if(corpse.phaseTime >= 21)
							{
								color = Textures.corpse1.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
							}
							else if(corpse.phaseTime >= 18)
							{
								color = Textures.corpse2.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
							}
							else if(corpse.phaseTime >= 15)
							{
								color = Textures.corpse3.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
							}
							else if(corpse.phaseTime >= 12)
							{
								color = Textures.corpse4.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
							}
							else if(corpse.phaseTime >= 9)
							{
								color = Textures.corpse5.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
							}
							else if(corpse.phaseTime >= 6)
							{
								color = Textures.corpse6.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
							}
							else if(corpse.phaseTime >= 3)
							{
								color = Textures.corpse7.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
							}
							else if(corpse.phaseTime >= 0)
							{
								color = Textures.corpse8.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
							}
						}
						else if(corpse.enemyID == 1)
						{
							if(corpse.phaseTime >= 12)
							{
								color = Textures.enemy1corpse1.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
							}
							else if(corpse.phaseTime >= 6)
							{
								color = Textures.enemy1corpse2.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
							}
							else if(corpse.phaseTime >= 0)
							{
								color = Textures.enemy1corpse3.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
							}
						}
						else if(corpse.enemyID == 2)
						{
							if(corpse.phaseTime >= 21)
							{
								color = Textures.enemy2corpse1.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
							}
							else if(corpse.phaseTime >= 18)
							{
								color = Textures.enemy2corpse2.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
							}
							else if(corpse.phaseTime >= 15)
							{
								color = Textures.enemy2corpse3.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
							}
							else if(corpse.phaseTime >= 12)
							{
								color = Textures.enemy2corpse4.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
							}
							else if(corpse.phaseTime >= 9)
							{
								color = Textures.enemy2corpse5.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
							}
							else if(corpse.phaseTime >= 6)
							{
								color = Textures.enemy2corpse6.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
							}
							else if(corpse.phaseTime >= 3)
							{
								color = Textures.enemy2corpse.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
							}
							else if(corpse.phaseTime >= 0)
							{
								color = Textures.enemy2corpse.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
							}
						}
						else if(corpse.enemyID == 8)
						{
							if(corpse.phaseTime >= 41)
							{
								color = Textures.belegothCorpse1.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
							}
							else if(corpse.phaseTime >= 37)
							{
								color = Textures.belegothCorpse2.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
							}
							else if(corpse.phaseTime >= 33)
							{
								color = Textures.belegothCorpse3.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
							}
							else if(corpse.phaseTime >= 29)
							{
								color = Textures.belegothCorpse4.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
							}
							else if(corpse.phaseTime >= 25)
							{
								color = Textures.belegothCorpse5.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
							}
							else if(corpse.phaseTime >= 21)
							{
								color = Textures.belegothCorpse6.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
							}
							else if(corpse.phaseTime >= 18)
							{
								color = Textures.belegothCorpse7.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
							}
							else if(corpse.phaseTime >= 15)
							{
								color = Textures.belegothCorpse8.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
							}
							else if(corpse.phaseTime >= 12)
							{
								color = Textures.belegothCorpse9.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
							}
							else if(corpse.phaseTime >= 9)
							{
								color = Textures.belegothCorpse10.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
							}
							else if(corpse.phaseTime >= 6)
							{
								color = Textures.belegothCorpse11.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
							}
							else if(corpse.phaseTime >= 3)
							{
								color = Textures.belegothCorpse12.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
							}
							else if(corpse.phaseTime >= 0)
							{
								color = Textures.belegothCorpse13.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
							}
						}
						else
						{
							if(corpse.phaseTime >= 21)
							{
								color = Textures.enemy5corpse1.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
							}
							else if(corpse.phaseTime >= 18)
							{
								color = Textures.enemy5corpse2.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
							}
							else if(corpse.phaseTime >= 15)
							{
								color = Textures.enemy5corpse3.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
							}
							else if(corpse.phaseTime >= 12)
							{
								color = Textures.enemy5corpse4.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
							}
							else if(corpse.phaseTime >= 9)
							{
								color = Textures.enemy5corpse5.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
							}
							else if(corpse.phaseTime >= 6)
							{
								color = Textures.enemy5corpse6.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
							}
							else if(corpse.phaseTime >= 3)
							{
								color = Textures.enemy5corpse7.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
							}
							else if(corpse.phaseTime >= 0)
							{
								color = Textures.enemy5corpse.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
							}
						}
					}
								
				   /*
				    * If color is not white, render it, otherwise don't to
				    * have transparency around your image. First two ff's
				    * are the images alpha, 2nd two are red, 3rd two are
				    * green, 4th two are blue. ff is 255.
				    */
					if(color != 0xffffffff)
					{
						PIXELS[xx + yy * WIDTH] = color;
						zBuffer[xx + yy * WIDTH] = rotZ;
					}
				}
			}
		}
	}
					
   /**
    * Renders bullets the same way as the enemies
    * @param x
    * @param y
    * @param z
    * @param hOffSet
    */
	public void renderProjectiles(double x, double y, double z, double hOffSet
			,int ID)
	{
		double xC 		        = (x - Player.x) * 1.9;
		double yC 			    = y + (Player.y * 0.085);
		double zC 			    = (z - Player.z) * 1.9;
		
		int spriteSize          = 16;
		
		if(ID >= 3)
		{
			spriteSize = 128;
		}
		
		double rotX      =  
				xC * cosine - zC * sine;
		double rotY      =  
				yC;
		double rotZ      =  
				zC * cosine + xC * sine;
		
		double xCenter   = WIDTH / 2;
		
		double xPixel    = ((rotX / rotZ) * HEIGHT) + xCenter;
		double yPixel    = ((rotY / rotZ) * HEIGHT) +
				(HEIGHT / Math.sin(Player.upRotate) 
						* Math.cos(Player.upRotate));
		
		double xPixelL   = xPixel - (spriteSize / rotZ);
		double xPixelR   = xPixel + (spriteSize / rotZ);
		
		double yPixelL   = yPixel - (spriteSize / rotZ);
		double yPixelR   = yPixel + (spriteSize / rotZ);
		
		int xPixL = (int)(xPixelL);
		int xPixR = (int)(xPixelR);
		int yPixL = (int)(yPixelL);
		int yPixR = (int)(yPixelR);
		
		if(xPixL < 0)
		{
			xPixL = 0;
		}
		
		if(xPixR > WIDTH)
		{
			xPixR = WIDTH;
		}
		
		if(yPixL < 0)
		{
			yPixL = 0;
		}
		
		if(yPixR > HEIGHT)
		{
			yPixR = HEIGHT;
		}
		
		rotZ *= 8;
		
		for(int yy = yPixL; yy < yPixR; yy++)
		{
			double pixelRotationY = (yy - yPixelR) / (yPixelL - yPixelR);
			int yTexture = (int)(pixelRotationY * 256);
			
			for(int xx = xPixL; xx < xPixR; xx++)
			{
				double pixelRotationX = (xx - xPixelR) / (xPixelL - xPixelR);
				int xTexture = (int)(pixelRotationX * 256);
				
				if(zBuffer[xx + yy * WIDTH] > rotZ)
				{
					int color = 0;

					if(ID == 0 || ID == 1)
					{
						color = Textures.bullet.PIXELS
							[(xTexture & 255) + (yTexture & 255) * 256];
					}
					else if(ID == 2)
					{
						color = Textures.phaser.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
					}
					else if(ID == 3)
					{
						color = Textures.defaultFireball.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
					}
					else if(ID == 4)
					{
						color = Textures.electroBall.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
					}
					else if(ID == 5)
					{
						color = Textures.giantFireball.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
					}
					else
					{
						color = Textures.electricShock.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
					}
				
					
				   /*
				    * If color is not white, render it, otherwise don't to
				    * have transparency around your image. First two ff's
				    * are the images alpha, 2nd two are red, 3rd two are
				    * green, 4th two are blue. ff is 255.
				    */
					if(color != 0xffffffff)
					{
						PIXELS[xx + yy * WIDTH] = color;
						zBuffer[xx + yy * WIDTH] = rotZ;
					}
				}
			}
		}
	}
	
	
	
	
   /**
    * Renders the walls depending on where the blocks in the map are, and
    * the players movements
    * @param xLeft
    * @param xRight
    * @param yHeight
    * @param zDepthLeft
    * @param zDepthRight
    * @param wallHeight
    */
	public void render3DWalls(double xLeft, double xRight, double yHeight,
								double zDepthLeft, double zDepthRight, double wallHeight
								, int wallID, Block block)
	{
	   /*
	    * Change in the left side in horizontal direction of the cube
	    * in respect to the players change in side to side movement
	    * given by rightSpeed;
	    */
		double xCLeft 		     = ((xLeft)- (rightSpeed * rightCorrect)) * 2;
		
	   /*
	    * Change in the Z depth of the left side of the wall due
	    * to where you are in the forward direction.
	    */
		double zCLeft 			 = ((zDepthLeft) - (fowardSpeed * forwardCorrect)) * 2;
		
	   /*
	    * Rotate the Left side x and z (up to a full circle if needed)
	    * using both cosine and sine in opposite ways for both
	    * equations to get the effect of a circle. This allows for the
	    * wall to rotate with the player.
	    */
		double rotLeftSideX      =  (xCLeft * cosine - zCLeft * sine);
		double rotLeftSideZ      =  (zCLeft * cosine + xCLeft * sine);
		
	   /*
	    * Moves corners of wall (or box I guess you could say) in 
	    * correlation to your y direction (If you jump or crouch).
	    * So that they stay in the same positions.
	    */
		double yCornerTopLeft    = 
				((-yHeight - ((0.5 / 12) * wallHeight)) + (Player.y * 0.06)) * 100;
		double yCornerBottomLeft = 
				((0.5 - yHeight) + (Player.y * 0.06)) * 100;
		
		/*
		 * Change in the right side in horizontal direction of the cube
		 * in respect to the players change in side to side movement
		 * given by rightSpeed;
		 */
		double xCRight 		     = ((xRight)- (rightSpeed * rightCorrect)) * 2;
			
		/*
		 * Change in the Z depth of the right side of the wall due
		 * to where you are in the forward direction.
		 */
		double zCRight 			 = ((zDepthRight) - (fowardSpeed * forwardCorrect)) * 2;
			
	   /*
		* Rotate the Right side x and z (up to a full circle if needed)
		* using both cosine and sine in opposite ways for both
		* equations to get the effect of a circle. This allows for the
		* wall to rotate with the player.
		*/
		double rotRightSideX      =  
				(xCRight * cosine - zCRight * sine);
		double rotRightSideZ      =  
				(zCRight * cosine + xCRight * sine);
			
	   /*
		* Moves corners of wall (or box I guess you could say) in 
		* correlation to your y direction (If you jump or crouch).
		* So that they stay in the same positions.
		*/
		double yCornerTopRight    = 
				((-yHeight - ((0.5 / 12) * wallHeight)) + (Player.y * 0.06)) * 100;
		double yCornerBottomRight = 
				((0.5 - yHeight) + (Player.y * 0.06)) * 100;
		
		double text4Modifier = 256;
		
		//The radius the wall will clip out of
		double clip = 13;
		
		
		
		
	   /*
	    * Uses cohen sutherland theroem to clip off any textures outside
	    * of the box created by the walls. 
	    */
		if(rotLeftSideZ <= clip)
		{
			double temp = (clip - rotLeftSideZ) 
					/ (rotRightSideZ - rotLeftSideZ);
			rotLeftSideZ = rotLeftSideZ + 
					(rotRightSideZ - rotLeftSideZ) * temp;
			rotLeftSideX = rotLeftSideX + 
					(rotRightSideX - rotLeftSideX) * temp;
		}
		
		if(rotRightSideZ <= clip)
		{
			double temp = (clip - rotLeftSideZ) 
					/ (rotRightSideZ - rotLeftSideZ);
			rotRightSideZ = rotLeftSideZ + 
					(rotRightSideZ - rotLeftSideZ) * temp;
			rotRightSideX = rotLeftSideX + 
					(rotRightSideX - rotLeftSideX) * temp;
		}
		
		/*
		 * Calculates the vectors from the center (The players position
		 * in respect to the wall) and places them at each end of the wall
		 * to define the range in which the textures will be rendered.
		 */
		double xPixelLeft  		  = 
				((rotLeftSideX / rotLeftSideZ) * HEIGHT + (WIDTH / 2));
		double xPixelRight		  = 
				((rotRightSideX / rotRightSideZ) * HEIGHT + (WIDTH / 2));
		
		int xPixelLeftInt         = (int) (xPixelLeft);
		int xPixelRightInt        = (int) (xPixelRight);
		
	   /*
	    * If off the left side of the screen, put it back on screen.
	    */
		if(xPixelLeftInt < 0)
		{
			xPixelLeftInt = 0;
		}
		
	   /*
	    * If off the right side of the screen, put it back on the screen.
	    */
		if(xPixelRightInt > WIDTH)
		{
			xPixelRightInt = WIDTH;
		}
		
	   /*
	    * Sets up vectors to all 4 corners of the wall to define the edges
	    * of the wall to be rendered no matter where the player is looking
	    * up or down.
	    */
		double yPixelTopLeft 	  = 
				(yCornerTopLeft 
						/ rotLeftSideZ * HEIGHT + (HEIGHT 
								/ Math.sin(Player.upRotate) 
								* Math.cos(Player.upRotate)));
		double yPixelBottomLeft   = 
				(yCornerBottomLeft 
						/ rotLeftSideZ * HEIGHT + (HEIGHT 
								/ Math.sin(Player.upRotate) 
								* Math.cos(Player.upRotate)));
		double yPixelTopRight 	  = 
				(yCornerTopRight 
						/ rotRightSideZ * HEIGHT + (HEIGHT 
								/ Math.sin(Player.upRotate) 
								* Math.cos(Player.upRotate)));
		double yPixelBottomRight  = 
				(yCornerBottomRight 
						/ rotRightSideZ * HEIGHT + (HEIGHT 
								/ Math.sin(Player.upRotate) 
								* Math.cos(Player.upRotate)));
		
		//Allows the textures to rotate with the wall.
		double texture1 = 1 / rotLeftSideZ;
		double texture2 = 1 / rotRightSideZ;
		double texture3 = 0;
		double texture4 = text4Modifier / rotRightSideZ;
		
		//Walls phase is always updated.
		block.wallPhase++;
		
	   /*
	    * While the pixels being rendered are still within the bounds
	    * of the wall
	    */
		for(int x = xPixelLeftInt; x < xPixelRightInt; x++)
		{
		   /*
		    * How much the pixels are to rotate depending on your movement
		    * . Your movement is tracked by the changed in xPixelLeft and
		    * xPixelRight from frame to frame.
		    */
			double pixelRotation = (x - xPixelLeft) /
					(xPixelLeft - xPixelRight);
			
			//Does something with textures. I don't yet understand this
			double zWall = (texture1 + (texture2 - texture1));
			
			int xTexture = (int) (((texture4) * (pixelRotation)) /
					zWall);

		   /*
		    * Figures out where the top pixel and bottom pixel are located
		    * on the screen.
		    */
			double yPixelTop     = yPixelTopLeft +
					(yPixelTopLeft - yPixelTopRight) 
					* pixelRotation;
			double yPixelBottom  = yPixelBottomLeft +
					(yPixelBottomLeft - yPixelBottomRight)
					* pixelRotation;
			
		   /*
		    * Casts them into ints to be drawn to the screen
		    */
			int yPixelTopInt     = (int) (yPixelTop);
			int yPixelBottomInt  = (int) (yPixelBottom);
			
		   /*
		    * If the wall goes out of the top of the frame, reduce it so
		    * that it stays in the frame.
		    */
			if(yPixelTopInt < 0)
			{
				yPixelTopInt = 0;
			}
			
		   /*
		    * If the wall goes below the frame, make it so that it still
		    * stays in the frame.
		    */
			if(yPixelBottomInt > HEIGHT)
			{
				yPixelBottomInt = HEIGHT;
			}
			
		   /*
		    * If the top is farther down than the bottom of the wall, 
		    * then don't render that maddness
		    */
			if(yPixelTopInt > yPixelBottomInt)
			{
				return;
			}
			
		   /*
		    * For each y pixel from the top of the wall to the bottom,
		    * render the pixels correctly depending on how you rotate
		    * upward (looking up). Also depending on the walls ID, change
		    * the texture of the wall to be so.
		    */
			for(int y = yPixelTopInt; y < yPixelBottomInt; y++)
			{
				double pixelRotationY = (y - yPixelTop) / (yPixelBottom - yPixelTop);
				int yTexture = (int) (256 * pixelRotationY);
				
				//If wall is behind another wall, break out of the loops.
				if(zBufferWall[x] > zWall)
				{
					continue;
				}
				
				int color = 0;
				
				if(wallID == 1)
				{
					color = Textures.wall1.PIXELS
					[(xTexture & 255) + (yTexture & 255) * 256];
					
					block.wallPhase = 0;
				}
				else if(wallID == 2)
				{
					color = Textures.wall2.PIXELS
					[(xTexture & 255) + (yTexture & 255) * 256];
					
					block.wallPhase = 0;
				}
				else if(wallID == 3)
				{
					color = Textures.wall3.PIXELS
					[(xTexture & 255) + (yTexture & 255) * 256];
					
					block.wallPhase = 0;
				}
				else if(wallID == 4)
				{
					color = Textures.wall4.PIXELS
					[(xTexture & 255) + (yTexture & 255) * 256];
					
					block.wallPhase = 0;
				}
				else if(wallID == 5)
				{
					color = Textures.wall5.PIXELS
					[(xTexture & 255) + (yTexture & 255) * 256];
					
					block.wallPhase = 0;
				}
				else if(wallID == 6)
				{
					color = Textures.wall6.PIXELS
					[(xTexture & 255) + (yTexture & 255) * 256];
					
					block.wallPhase = 0;
				}
				else if(wallID == 7)
				{
					color = Textures.wall7.PIXELS
					[(xTexture & 255) + (yTexture & 255) * 256];
					
					block.wallPhase = 0;
				}
				else if(wallID == 8)
				{
					color = Textures.wall8.PIXELS
					[(xTexture & 255) + (yTexture & 255) * 256];
					
					block.wallPhase = 0;
				}
				else if(wallID == 9)
				{
					color = Textures.wall9.PIXELS
					[(xTexture & 255) + (yTexture & 255) * 256];
					
					block.wallPhase = 0;
				}
				else if(wallID == 10)
				{
					color = Textures.wall10.PIXELS
					[(xTexture & 255) + (yTexture & 255) * 256];
					
					block.wallPhase = 0;
				}
				else if(wallID == 11)
				{
					color = Textures.wall11.PIXELS
					[(xTexture & 255) + (yTexture & 255) * 256];
					
					block.wallPhase = 0;
				}
				else if(wallID == 12)
				{
					color = Textures.wall12.PIXELS
					[(xTexture & 255) + (yTexture & 255) * 256];
					
					block.wallPhase = 0;
				}
				else if(wallID == 13)
				{
					if(block.wallPhase < 50)
					{
						color = Textures.wall13Phase1.PIXELS
							[(xTexture & 255) + (yTexture & 255) * 256];
					}
					else
					{
						color = Textures.wall13Phase2.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
					}
					
					if(block.wallPhase > 100)
					{
						block.wallPhase = 0;
					}
				}
				else if(wallID == 14)
				{
					color = Textures.wall14.PIXELS
							[(xTexture & 255) + (yTexture & 255) * 256];
					
					block.wallPhase = 0;
				}
				else if(wallID == 15)
				{
					int brightness = 0;
					
					if(block.wallPhase < 10)
					{
						color = Textures.wall15Phase1.PIXELS
							[(xTexture & 255) + (yTexture & 255) * 256];
						
						brightness = 25;
					}
					else if(block.wallPhase < 20)
					{
						color = Textures.wall15Phase2.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
						
						brightness = 75;
					}
					else if(block.wallPhase < 30)
					{
						color = Textures.wall15Phase3.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
						
						brightness = 150;
					}
					else if(block.wallPhase < 40)
					{
						color = Textures.wall15Phase4.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
						
						brightness = 200;
					}
					else if(block.wallPhase < 50)
					{
						color = Textures.wall15Phase5.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
						
						brightness = 255;
					}
					else if(block.wallPhase < 60)
					{
						color = Textures.wall15Phase4.PIXELS
							[(xTexture & 255) + (yTexture & 255) * 256];
						
						brightness = 200;
					}
					else if(block.wallPhase < 70)
					{
						color = Textures.wall15Phase3.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
						
						brightness = 150;
					}
					else if(block.wallPhase < 80)
					{
						color = Textures.wall15Phase2.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
						
						brightness = 75;
					}
					else if(block.wallPhase <= 90)
					{
						color = Textures.wall15Phase1.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
						
						brightness = 25;
					}
					
					if(block.wallPhase > 90)
					{
						block.wallPhase = 0;
					}	
					
					color = adjustBrightness(brightness, color, wallID);
				}
				else if(wallID == 16 || wallID == 17 || wallID == 25)
				{
					int brightness = 0;
					
					if(block.wallPhase < 10)
					{
						color = Textures.toxic1.PIXELS
							[(xTexture & 255) + (yTexture & 255) * 256];
						
						brightness = 150;
					}
					else if(block.wallPhase < 20)
					{
						color = Textures.toxic2.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
						
						brightness = 175;
					}
					else if(block.wallPhase < 30)
					{
						color = Textures.toxic3.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
						
						brightness = 200;
					}
					else if(block.wallPhase < 40)
					{
						color = Textures.toxic4.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
						
						brightness = 225;
					}
					else if(block.wallPhase < 50)
					{
						color = Textures.toxic5.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
						
						brightness = 255;
					}
					else if(block.wallPhase < 60)
					{
						color = Textures.toxic6.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
						
						brightness = 225;
					}
					else if(block.wallPhase < 70)
					{
						color = Textures.toxic7.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
						
						brightness = 200;
					}
					else if(block.wallPhase < 80)
					{
						color = Textures.toxic8.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
						
						brightness = 175;
					}
					else if(block.wallPhase < 90)
					{
						color = Textures.toxic9.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
						
						brightness = 150;
					}
					else if(block.wallPhase < 100)
					{
						color = Textures.toxic10.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
						
						brightness = 175;
					}
					else if(block.wallPhase < 110)
					{
						color = Textures.toxic11.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
						
						brightness = 200;
					}
					else if(block.wallPhase < 120)
					{
						color = Textures.toxic12.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
						
						brightness = 225;
					}
					else if(block.wallPhase < 130)
					{
						color = Textures.toxic13.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
						
						brightness = 255;
					}
					else if(block.wallPhase < 140)
					{
						color = Textures.toxic14.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
						
						brightness = 225;
					}
					else if(block.wallPhase < 150)
					{
						color = Textures.toxic15.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
						
						brightness = 200;
					}
					else if(block.wallPhase < 161)
					{
						color = Textures.toxic16.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
						
						brightness = 175;
					}
					
					if(block.wallPhase > 160)
					{
						block.wallPhase = 0;
					}
					
				    color = adjustBrightness(brightness, color, wallID);
				}
				else if(wallID == 18)
				{
					if(block.wallPhase < 10)
					{
						color = Textures.spine1.PIXELS
							[(xTexture & 255) + (yTexture & 255) * 256];
					}
					else if(block.wallPhase < 20)
					{
						color = Textures.spine2.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
					}
					else if(block.wallPhase < 30)
					{
						color = Textures.spine3.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
					}
					else if(block.wallPhase < 40)
					{
						color = Textures.spine4.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
					}
					else if(block.wallPhase < 50)
					{
						color = Textures.spine5.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
					}
					else if(block.wallPhase < 60)
					{
						color = Textures.spine6.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
					}
					else if(block.wallPhase < 70)
					{
						color = Textures.spine7.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
					}
					else if(block.wallPhase < 80)
					{
						color = Textures.spine8.PIXELS
								[(xTexture & 255) + (yTexture & 255) * 256];
					}
					
					if(block.wallPhase >= 80)
					{
						block.wallPhase = 0;
					}
				}
				//Dead electric wall
				else if(wallID == 19)
				{
					int brightness = 25;
					
					color = Textures.wall15Phase1.PIXELS
							[(xTexture & 255) + (yTexture & 255) * 256];
					
					color = adjustBrightness(brightness, color, wallID);
				}
				//MLG wall
				else if(wallID == 20)
				{
					color = Textures.mlg.PIXELS
							[(xTexture & 255) + (yTexture & 255) * 256];
				}
				//Box wall
				else if(wallID == 21)
				{
					color = Textures.box.PIXELS
							[(xTexture & 255) + (yTexture & 255) * 256];
				}
				//Wood wall
				else if(wallID == 22)
				{
					color = Textures.woodenWall.PIXELS
							[(xTexture & 255) + (yTexture & 255) * 256];
				}
				//Bloody wall
				else if(wallID == 23)
				{
					color = Textures.bloodWall.PIXELS
							[(xTexture & 255) + (yTexture & 255) * 256];
				}
				//Marble wall
				else if(wallID == 24)
				{
					color = Textures.marble.PIXELS
							[(xTexture & 255) + (yTexture & 255) * 256];
				}
				//Normal Button
				else if(wallID == 26)
				{
					color = Textures.normButton.PIXELS
							[(xTexture & 255) + (yTexture & 255) * 256];
				}
				
			   /*
			    * If color is not white, render it, otherwise don't to
			    * have transparency around your image. First two ff's
			    * are the images alpha, 2nd two are red, 3rd two are
			    * green, 4th two are blue. ff is 255.
			    */
				if(color != 0xffffffff)
				{
					PIXELS[x + y * WIDTH] = color;
					zBuffer[x + y * WIDTH] = 
							(0.4 / (texture1 - 
									(texture2 - texture1) 
									* pixelRotation)) * 0.3;
					
					//Set this pixel to being the front most wall pixel
					zBufferWall[x] =  zWall;
				}
			}
		}	
	}

   /**
    * Creates the fog that limits the render distance and creates an
    * artificial darkness
    */
	public void renderDistanceLimiter()
	{
		for (int i = 0; i < (WIDTH * HEIGHT); i++)
		{
			//Color value of this pixel in integer form
			int color      = PIXELS[i];
			
			//Brightness of color. 255 is Full brightness
			int brightness = 255;
			
			int j = Player.vision;
			
		   /*
		    * If player is nearing the end of immortality, or is not
		    * immortal.
		    */
			if(j == 0 || j > 10 && j <= 20
					|| j > 30 && j <= 40
					|| j > 50 && j <= 60
					|| j > 70 && j <= 80)
			{
			   /*
			    * The brightness of each pixel depending on its distance 
			    * from the player, and the render Distance
			    */
				brightness = (int) (renderDistance / (zBuffer[i]));
			}
			//If the player is immortal
			else
			{
			   brightness = 255;
			}
			
			//Never can be less than 0 brightness
			if(brightness < 0)
			{
				brightness = 0;
			}
			
			//Can never be brighter than 255
			if(brightness > 255)
			{
				brightness = 255;
			}
			
		   /*
		    * Or you can use 0xff. It goes from 0 - 255, and 255 = 0xff.
		    * The 255 is not technically needed as it just causes the
		    * number to stay the same, but it does matter for the int b
		    * for some reason. I think because it causes the render distance
		    * to fade to blue if not. The shifting of the ints causes the color
		    * to change.
		    * 
		    * Converts the bit value of the color into an int so that it can
		    * be easily tampered with
		    */
			int r = (color >> 16) & 255; 
			int g = (color >> 8)  & 255;
			int b =  color        & 255;
			
		   /*
		    * Divides that value by 255, then multiplies it by the
		    * brightness level of the pixel to determine how bright
		    * the reds, greens, and blues in each pixel will get.
		    */
			r     = (r * brightness) / 255;
			g     = (g * brightness) / 255;
			b     = (b * brightness) / 255;
				
			//Reset the bits of that particular pixel
			if(Player.health > 0)
			{
				int ePT = Player.environProtectionTime;
				if(ePT == 0 || ePT > 10 && ePT <= 20
						|| ePT > 30 && ePT <= 40
						|| ePT > 50 && ePT <= 60
						|| ePT > 70 && ePT <= 80)
				{
					PIXELS[i] = r << 16 | g << 8 | b;
				}
				else
				{
					PIXELS[i] = r << 8 | g << 8 | b << 8;
				}
			}
			else
			{
				PIXELS[i] = r << 16 | g << 16 | b << 16;
			}
		}
	}
	
	public void renderWallsCorrect(Block block)
	{
		currentWallID = block.wallID;
		
	   /*
		* Create a new random level when the game is first rendered as
		* game.level, then continue to keep track of the wall positions
		* every time the game renders.
		*/
		Level level = Game.level;
			
		//Corrects wall rendering for player so size looks normal.
		int test = 64;
		Block eastBlock  = level.getBlock(block.x + 1, block.z);
		Block southBlock = level.getBlock(block.x, block.z + 1);
		Block westBlock  = level.getBlock(block.x - 1, block.z);
		Block northBlock = level.getBlock(block.x, block.z - 1);
		
		
		double renderHeight = block.height;
		double renderEast   = eastBlock.height;
		double renderSouth  = southBlock.height;
		
	   /*
	    * Corrects the height of the wall that is rendered if the blocks
	    * height is less than 12. This is only because when the height
	    * is less than 12, the wall does not render the correct height
	    * in accordance to the player. For example when the wall
	    * height is 0, for some reason it will render as if it was at
	    * height 6 and etc...
	    */
		if(block.height < 12)
		{
			renderHeight = 12 - ((12 - block.height) * 2);
		}
		else if(block.height >= 48)
		{
			renderHeight += 12;
		}
		else if(block.height >= 24)
		{
			renderHeight += 4;
		}
		else if(block.height > 12)
		{
			renderHeight += 2;
		}
		
		if(eastBlock.height < 12)
		{
			renderEast = 12 - ((12 - eastBlock.height) * 2);
		}
		else if(eastBlock.height >= 48)
		{
			renderEast += 12;
		}
		else if(eastBlock.height >= 24)
		{
			renderEast += 4;
		}
		else if(eastBlock.height > 12)
		{
			renderEast += 2;
		}
		
		if(southBlock.height < 12)
		{
			renderSouth = 12 - ((12 - southBlock.height) * 2);
		}
		else if(southBlock.height >= 48)
		{
			renderSouth += 12;
		}
		else if(southBlock.height >= 24)
		{
			renderSouth += 4;
		}
		else if(southBlock.height > 12)
		{
			renderSouth += 2;
		}
		
		//IF the block being checked is solid
		if(block.isSolid)
		{		
		   /*
		    * If the block on the east side is not the same or greater
		    * height or same y value as the block currently being 
		    * rendered.
		    * 
		    * Also if the current block is not seeThrough, but the
		    * east block is, render this block still.
		    */
			if(eastBlock.height < block.height
					|| eastBlock.y != block.y
					|| eastBlock.seeThrough && !block.seeThrough)
			{
				if(Math.sin(Player.rotation) > 0)
				{
					render3DWalls((block.x + 1) * test, 
							(block.x + 1) * test, block.y / 3, 
							block.z * test, (block.z + 0.99999) 
							* test, renderHeight, block.wallID, block);
				}
				else
				{
					render3DWalls((block.x + 1.00001) * test, 
						(block.x + 1.00001) * test, block.y / 3, 
						block.z * test, (block.z + 0.99999) 
						* test, renderHeight, block.wallID, block);
				}
			}
		
		   /*
		    * Same as the east block but on the south side (positive z) 
		    */
			if(southBlock.height < block.height 
					|| southBlock.y != block.y
					|| southBlock.seeThrough && !block.seeThrough)
			{
				if(Math.cos(Player.rotation) < 0)
				{
					render3DWalls((block.x + 1) * test,
						(block.x + 0.00002) * test, block.y / 3, (block.z + 1)
						* test, (block.z + 1) 
						* test, renderHeight, block.wallID, block);
				}
				else
				{
					render3DWalls((block.x + 1) * test,
							(block.x + 0.00002) * test, block.y / 3, (block.z + 1)
							* test, (block.z + 1) 
							* test, renderHeight, block.wallID, block);
				}
			}
			
		   /*
		    * Same as others but in the north direction (negative z)
		    */
			if(northBlock.height < block.height 
					|| northBlock.y != block.y
					|| northBlock.seeThrough && !block.seeThrough)
			{
			   /*
			    * Fixes a weird graphical glitch that happens for some
			    * reason.
			    */
				if(Math.sin(Player.rotation) > 0)
				{
					render3DWalls(block.x * test,
							(block.x + 0.99999) * test, block.y / 3,
							(block.z) * test,
							(block.z) * test,
							renderHeight, block.wallID, block);
				}
				else
				{
					render3DWalls(block.x * test,
						(block.x + 1) * test, block.y / 3,
						(block.z) * test,
						(block.z) * test,
						renderHeight, block.wallID, block);
				}
			}
			
		   /*
		    * Same as others but in west (Negative x) direction.
		    */
			if(westBlock.height < block.height 
					|| westBlock.y != block.y
					|| westBlock.seeThrough && !block.seeThrough)
			{
				render3DWalls((block.x) * test,
						(block.x) * test, block.y / 3,
						(block.z + 1) * test, (block.z + 0.00001) * test,
						renderHeight, block.wallID, block);
				
			}
			
			/*
			 * USED FOR TESTING ONLY. THIS IS USED TO ATTEMPT TO DRAW THE
			 * TOPS OF BLOCKS. From what I have looked up online though,
			 * doing this is impossible unless I use OpenGL or some other
			 * type of software. I AM NOT GONNA CHEAT!
			 * 
			if(once)
			{
				once = false;
				render3DWallsTop((1) * test,
					(2) * test, block.y / 3,
					(4) * test, 4 * test,
					renderHeight, block.wallID);
			}*/

		}
	}
	
   /**
    * Adjusts the brightness of a texture based on the brightness sent
    * to this method, and the color (in integer form) of the texture.
    * 
    * WallID is only used for walls so that lava can be the toxic 
    * waste texture shifted to red. If an item calls this method, it
    * will send in 0 for wallID.
    * 
    * @param brightness
    * @param color
    * @param wallID
    * @return
    */
	public int adjustBrightness(int brightness, int color, int wallID)
	{
	   /*
	    * Or you can use 0xff. It goes from 0 - 255, and 255 = 0xff.
	    * The 255 is not technically needed as it just causes the
	    * number to stay the same, but it does matter for the int b
	    * for some reason. I think because it causes the render distance
	    * to fade to blue if not. The shifting of the ints causes the color
	    * to change.
	    * 
	    * Converts the bit value of the color into an int so that it can
	    * be easily tampered with
	    */
		int r = (color >> 16) & 255; 
		int g = (color >> 8)  & 255;
		int b =  color        & 255;
		
	   /*
	    * Divides that value by 255, then multiplies it by the
	    * brightness level of the pixel to determine how bright
	    * the reds, greens, and blues in each pixel will get.
	    */
		r     = (r * brightness) / 255;
		g     = (g * brightness) / 255;
		b     = (b * brightness) / 255;
		
		//Set colors back to their original components
		if(wallID != 17 && wallID != 25)
		{
			//Set colors back to their original components
			//If not white
			if(color != 0xffffffff)
			{
				color = r << 16 | g << 8 | b;
			}
		}
		else if (wallID == 17)
		{
			color = r << 16 | g << 16 | b << 16;
		}
		else
		{
			color = r | g | b;
		}

		
		return color;
	}
}
