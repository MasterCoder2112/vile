package com.vile.entities;

import com.vile.Game;

/**
 * Title: ExplosiveCanister
 * @author Alexander Byrd
 * Date Created: 11/11/2016
 * MY BIRTHDAY! Celebrated with explosions. Lol
 * 
 * Description:
 * An extension of Item that has a certain amount of health, and a flag
 * that states whether the canister is exploding or not at the moment.
 *
 */
public class ExplosiveCanister extends Item
{
	public int health = 10;
	private int timesCalled = 0;
	public double heightCorrect = 8;
	
	public boolean exploding = false;
	public boolean performedAction = false;
	public boolean playedSound = false;
	
   /**
    * Creates Canister Item with values sent in, then adds it to the
    * Canister arraylist in game.
    * 
    * @param value
    * @param x
    * @param y
    * @param z
    * @param ID
    */
	public ExplosiveCanister(int value, double x, double y, double z, int ID) 
	{
		super(value, x, y, z, ID);
		Game.canisters.add(this);
		
		if(y >= 0 && y < 18)
		{
			heightCorrect = 8;
		}
		else if(y >= 18 && y < 30)
		{
			heightCorrect = 9;
		}
		else if(y >= 30 && y <= 36)
		{
			heightCorrect = 9;
		}
		else if(y > 36 && y <= 48)
		{
			heightCorrect = 10;
		}
		else if(y <= 79)
		{
			heightCorrect = 10;
		}
		else
		{
			double addCorrect;
			y -= 60;
			
			addCorrect = y / 20;
			heightCorrect = 10 + (0.5 * addCorrect);
			
		}
	}
	
   /**
    * Removes this canister from everywhere in the game it may be
    * included in. This is called after the canister explodes.
    */
	public void removeCanister()
	{	
		Game.items.remove(this);
		
		Game.solidItems.remove(this);
		
		Game.canisters.remove(this);	
	}

}
