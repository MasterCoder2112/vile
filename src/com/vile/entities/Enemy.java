package com.vile.entities;

import java.util.ArrayList;






import com.vile.entities.Enemy;
import com.vile.entities.Player;
import com.vile.Display;
import com.vile.Game;
import com.vile.input.Controller;
import com.vile.levelGenerator.Block;
import com.vile.levelGenerator.Level;

/**
 * Title: Enemy
 * @author Alex Byrd
 * Date Updated: 9/16/2016
 *
 * Keeps track of the enemies status such as movement, attacking, and
 * other such things. Individual methods and such are commented
 * seperately.
 */
public class Enemy extends Entity implements Comparable
{
	//Height of enemy
	public int height       = 8;
	
	//Max height the enemy can stand on
	public double maxHeight = 0;

	//Makes sure the set height "if" is only called once initially
	private int test = 0;
	
	//Corrects height that the enemy should actually be at
	public double heightCorrect = 0;
	
	private double totalHeight = 0;
	
	//ENEMY FLAGS	
	//Is the flying enemy coming down on the player
	public boolean tracking = false;
	
	//Is the enemy stuck in a wall?
	public boolean isStuck = false;
	
	//Is the enemy a boss?
	public boolean isABoss = false;
	
	//Enemy is in sight of player
	public boolean inSight = false;
	
	//Has an enemy target instead of Player
	public boolean newTarget = false;
	
	
	//What walking phase is the enemy in, and which direction in
	//degrees is the enemy facing
	public int enemyPhase = 0;
	public double direction = 0;
	
   /**
    * Creates a new enemy with values sent in. The sets up the enemy based
    * on gamemode, fps, and enemy type. This is the default constructor.
    * @param armor
    * @param ammo
    * @param damage
    * @param x
    * @param y
    * @param z
    */
	public Enemy(double x, double y, double z, int ID) 
	{
		super(100, 0, 0, 6, 0, x, y, z, ID);
		
		speed = 1;
		
		//Normal enemy
		if(ID == 1)
		{
			health = 100;
			hasSpecial = true;
		}
		//Flyer enemy
		else if(ID == 2)
		{
			health = 60;
			hasSpecial = true;
		}
		//The Tank enemy
		else if(ID == 3)
		{
			speed  *= 0.25;
			health  = 250;
			damage *= 4;
			hasSpecial = true;
		}
		//Reaper enemy
		else if(ID == 4)
		{
			speed *= 6;
			damage = 4;
			health = 40;
		}
		//Resurrector
		else if(ID == 5)
		{
			damage = 2;
			health = 100;
			
			//Increases time it takes to use special which is resurrecting
			tickAmount = 24;
		}
		//MORGOTH
		else if(ID == 6)
		{
			speed *= 2;
			damage = 25;
			health = 2000;
			height = 60;
			isABoss = true;
			hasSpecial = true;
		}
		//VILE civilian
		else if(ID == 7)
		{
			damage = 3;
			health = 25;
		}
		//BELEGOTH
		else if(ID == 8)
		{
			speed *= 2;
			damage = 50;
			health = 5000;
			isABoss = true;
			height = 60;
			hasSpecial = true;
		}
		
		//If easy mode
		if(Display.skillMode == 1)
		{
			speed  *= 0.5;
			damage *= 0.5;
		}
		//If Bring it on mode
		else if(Display.skillMode == 3)
		{
			speed  *= 2;
			damage *= 2;
		}
		//Death cannot hurt me mode.
		else if(Display.skillMode == 4)
		{
			speed *= 3;
			damage *= 3;
		}
		//If peaceful mode
		else if(Display.skillMode == 0)
		{
			speed = 0;
			damage = 0;
		}
		
		//Sets speed to specific units the map uses
		speed /= 21.3;

		//Set initial height and see if the enemy can move
		isFreeZ(x, z);
	}
	
   /**
    * This is the specific constructor of an enemy. If you want to create
    * a specific enemy, you can call this constructor and send in the
    * specific values you want to create the enemy of your choice. 
    */
	public Enemy(int health, int armor, int ammo, int damage,
			int speed, double x, double y, double z, int ID)
	{
		super(health, armor, ammo, damage, speed, x, y, z, ID);
	}
	
   /**
    * Keeps track of the enemies movement each turn
    */
	public void move()
	{	
		//If there is a target other than the player causing infighting
		if(targetEnemy != null)
		{
			targetX = targetEnemy.xPos;
			targetY = targetEnemy.yPos;
			targetZ = targetEnemy.zPos;
		}
		else
		{
			targetX = Player.x;
			targetY = Player.y;
			targetZ = Player.z;
		}
		
		if(super.activated && !super.isFiring && !super.isAttacking)
		{
			if(ID == 2)
			{
			   /*
			    * Used to correct the players y in case the player is 
			    * crouching and the y goes below the maxHeight the player
			    * can stand on.
			    */
				double yCorrect = targetY;
				
				//If target is player
				if(targetEnemy == null)
				{
					if(yCorrect < Player.maxHeight)
					{
						yCorrect = Player.maxHeight;
					}
				}

			   /*
			    * For the flying enemy only, if the player is within a
			    * distance of 75 of the enemy, then have the enemy begin
			    * to fly up or down to match the players height in order
			    * so that it may attack the player. Because of the 
			    * different units, (playerYCorrect + 1) / 10 is used so
			    * that the enemy height matches up with the players.
			    * 
			    * In a future update when AI is improved, the enemy
			    * will only try to fly to the players y value if
			    * there are no walls in the way. If there is it will
			    * fly over them first.
			    */
				if(distance <= 5
						&& Math.abs(yPos) > (yCorrect + 1)
						&& !isStuck)
				{
					yPos += 0.5;
					tracking = true;
				}
				else if(distance <= 5
						&& Math.abs(yPos) < (1 / 10)
						&& !isStuck)
				{
					yPos = -(1/10);
					tracking = true;
				}
				else if(distance <= 5
						&& Math.abs(yPos) < (yCorrect + 1)
						|| isStuck)
				{
					yPos -= 0.5;
					tracking = true;
				}
				else if(distance <= 5
						&& Math.abs(yPos) > (yCorrect + 1)
						&& Player.y != 0
						&& !isStuck)
				{
					yPos = -(yCorrect + 1);
					tracking = true;
				}
				else if(distance > 5
						&& yPos > -48)
				{
					yPos -= 0.5;
					tracking = false;
				}
				else if(distance > 5
						&& yPos < -48
						&& !isStuck)
				{
					yPos += 0.5;
					tracking = false;
				}
				
			   /*
			    * If the enemy is negligibly close to the player, just
			    * match up the height with the players.This small
			    * correction is so small it will never be noticed if
			    * playing the game.
			    */
				if(distance <= 5
						&& Math.abs(Math.abs(yPos) - 
								((yCorrect + 1))) < 0.5)
				{
					yPos = -(yCorrect + 1);
					tracking = true;
				}
				
			   /*
			    * If the player is far enough away so that the enemy
			    * does not chase him/her, and the enemy is near its
			    * default height by less than 0.05 units, then because
			    * it is a negligible difference, just set its height
			    * to the default height of -5.
			    * 
			    * The enemy cannot be hindered or stuck by a wall either.
			    */
				if(distance > 5 && Math.abs(yPos - (-48)) < 0.25
						&& !isStuck)
				{
					yPos = -48;
					tracking = false;
				}
			}
			
		   /*
		    * For the Resurrector enemy, check to see if a corpse is
		    * in range of being resurrected, and resurrect the corpse
		    * if so.
		    * 
		    * Also do for Morgoth and Belegoth
		    */
			if(ID == 5 || ID == 6 || ID == 8)
			{
				ArrayList<Corpse> temp = new ArrayList<Corpse>();
				
				for(Corpse corpse: Game.corpses)
				{
					//Find the distance between a corpse and the enemy
					double distance = 
							Math.sqrt(((Math.abs(getX() - corpse.x))
							* (Math.abs(getX() - corpse.x)))
							+ ((Math.abs(getZ() - corpse.z))
									* (Math.abs(getZ() - corpse.z))));
					
					//If enemy is within a 1 unit range of corpse
					if(distance <= 1 && corpse.enemyID != 5 
							&& corpse.enemyID != 6 && corpse.enemyID != 0
							&& corpse.enemyID != 8)
					{	
						//Only resurrect at the end of the special 
						//attack preperation
						if(canResurrect)
						{
							canResurrect = false;
							
						   /*
						    * Reconstruct enemy depending on the ID the corpse
						    * was before it died. Also activate the new resurrected enemy.
						    */
							Enemy newEnemy = new Enemy(corpse.x, 0, corpse.z,
									corpse.enemyID);
							
							newEnemy.activated = true;
							
							Game.enemies.add(newEnemy);
							
							temp.add(corpse);
							
							Game.enemiesInMap++;
						}
						else
						{
							//This is the resurrector enemies fire.
							//So reset ticks and do this.
							if(ID == 5)
							{
								this.isFiring = true;
								tick = 0;
							}
						}
					}
				}
				
				//Every corpse that is resurrected, remove from the
				//corpses list
				for(Corpse corpse: temp)
				{
					Game.corpses.remove(corpse);
				}
				
			}
			
			/*
			 * FUTURE ENEMY AI CODE! IT IS NOT PERFECTED YET, BUT IT WILL
			 * BE EVENTUALLY
			 * 
			double distanceX = Player.x - xPos;
			double distanceZ = Player.z - zPos;
			double sinDistance = 0;
			double cosDistance = 0;
			
			double totalDistance = Math.sqrt((Math.abs(distanceX) 
					* Math.abs(distanceX)) + (Math.abs(distanceZ) 
							* Math.abs(distanceZ)));
			
			double angleInDegrees = Math.toDegrees(Math.atan2(distanceX, distanceZ));
			double angleInRadians = Math.atan2(distanceX, distanceZ);
			double directionCorrected = Math.toDegrees(direction);
			
			if(angleInDegrees < 0)
			{
				angleInDegrees = Math.abs(-180 - angleInDegrees) + 180;
			}
			
			if(directionCorrected < 0)
			{
				directionCorrected = Math.abs(-180 
						- directionCorrected) + 180;
			}
			
			if(directionCorrected < angleInDegrees - 10)
			{
				direction += (10 * Math.PI) / 180;
			}
			else if(directionCorrected < angleInDegrees)
			{
				direction = angleInRadians;
			}
			else if(directionCorrected > angleInDegrees + 10)
			{
				direction -= (10 * Math.PI) / 180;
			}
			else if(directionCorrected > angleInDegrees)
			{
				direction = angleInRadians;
			}
			
			double futureX = xPos + ((super.speed * Math.cos(direction)) +
					(super.speed * Math.sin(direction)));
			
			double futureZ = zPos + ((super.speed * Math.cos(direction)) -
					(super.speed * Math.sin(direction)));
			
			if(totalDistance > 0.5)
			{
				xPos += ((super.speed * Math.cos(direction)) +
					(super.speed * Math.sin(direction)));
				zPos += ((super.speed * Math.cos(direction)) -
					(super.speed * Math.sin(direction)));
			}
			
			if(futureX > Player.x
					&& xPos > Player.x + 0.5)
			{
				if(isFreeX(futureX, zPos))
				{
					xPos += ((super.speed * Math.cos(direction)) +
							(super.speed * Math.sin(direction)));
				}
			}
			else if(futureX < Player.x
					&& xPos < Player.x - 0.5)
			{
				if(isFreeX(futureX, zPos))
				{
					xPos += ((super.speed * Math.cos(direction)) +
							(super.speed * Math.sin(direction)));
				}
			}
			
			if(futureZ > Player.z
					&& zPos > Player.z + 0.5)
			{
				if(isFreeZ(xPos, futureZ))
				{
					zPos += ((super.speed * Math.cos(direction)) -
							(super.speed * Math.sin(direction)));
				}
			}
			else if(futureZ < Player.z
					&& zPos < Player.z - 0.5)
			{
				if(isFreeZ(xPos, futureZ))
				{
					zPos += ((super.speed * Math.cos(direction)) -
							(super.speed * Math.sin(direction)));
				}
			}*/
			
		   /*
		    * First determines what the direction the enemy needs to go to
		    * reach the player. Once it tries to move in that direction,
		    * and there is a wall blocking it, it will attempt to start
		    * going in one of the side directions, and will continue
		    * until it reaches either an opening or another wall. If it
		    * hits another wall it will change direction and start going
		    * that way. 
		    */
			if(super.xPos > targetX + super.speed)
			{
				if(isFreeX(super.xPos - super.speed, super.zPos))
				{
					super.xPos -= super.speed;
				}
				/*
				else
				{
					super.xPos += super.speed;
				}*/
			}
			else if(super.xPos < targetX - super.speed)
			{
				if(isFreeX(super.xPos + super.speed, super.zPos))
				{
					super.xPos += super.speed;
				}
				/*
				else
				{
					super.xPos -= super.speed;
				}*/
			}
			
			if(super.zPos > targetZ + super.speed)
			{
				if(isFreeZ(super.xPos, super.zPos - super.speed))
				{
					super.zPos -= super.speed;
				}
				/*
				else
				{
					super.zPos += super.speed;
				}*/
			}
			else if(super.zPos < targetZ - super.speed)
			{
				if(isFreeZ(super.xPos, super.zPos + super.speed))
				{
					super.zPos += super.speed;
				}
				/*
				else
				{
					super.zPos -= super.speed;
				}*/
			}
		   
		}
	}
	
   /**
    * Initiates an attack on the player when called.
    */
	public void attack(double yCorrect)
	{	
		//If in range, and is not attacking yet, begin attack
		//no matter the tick.
		if(distance <= 1 && !isAttacking)
		{
			tick = 0;
		}
		
	   /*
	    * Only melee attack once each tick round if
	    * within distance of 1 from the player.
	    */
		if(tick == 0 && super.distance <= 1
				&& Math.abs((this.getY()) + 
						(yCorrect)) <= 1 + this.height)
		{
			//Is melee attacking is set to true
			super.isAttacking = true;
		}
	   /*
	    * Fire at target once each 5 tick rounds if target is in sight
	    * and if the enemy is not already firing. Also the enemy has to
	    * have a special attack in order to activate it.
	    */
		else if(tick == 0
				&& super.hasSpecial && inSight
				&& !super.isFiring && tickRound == 0
				&& distance > 1 && super.activated
				&& Game.skillMode > 0)
		{
			//Enemy is in process of firing
			super.isFiring = true;
		}
	}
	
	@Override
   /**
	* Compares two enemies together depending on their distance from the
	* player, and then sorts them in increasing distance.
	*/
	public int compareTo(Object enemy) 
	{
		//Gets Integer height of the block being compared
		int comparedInteger = (int)((Enemy)(enemy)).distanceFromPlayer;
		
	   /*
	    * Compares the two blocks being compared, and sends back the
	    * result. This particular comparison would cause the list
	    * to be sorted in decending order of height. If you switched
	    * this.height and comparedInteger it would be sorted in
	    * ascending order.
	    */
		return (int)this.distanceFromPlayer - comparedInteger;
	}
	
   /**
    * Determines whether the enemy is free to move to the next space or
    * not.
    * 
    * Similar to the player, there is a method for moving in the x
    * direction, and a method for moving in the z direction in terms
    * of collision, so that collision works at every height level
    * correctly.
    * 
    * @param xx
    * @param zz
    * @return
    */
	public boolean isFreeX(double xx, double zz)
	{	
		double z = 0.3;
		
		if(xx < xPos)
		{
			z = 0.3;
		}
		else
		{
			z = -0.3;
		}
		
		//East Block
		int x0   = (int)(((xx) - z));
		
		//West Block
		int x1   = (int)(((xx) + z));
		
		//North Block
		int z0   = (int)(((zz) - z));
		
		//South Block
		int z1   = (int)(((zz) + z));
		
		int zTest = (int)(zPos);
		int xTest = (int)(xPos);
		
		Block block1 = Game.level.getBlock(x0,z0);
		Block block2 = Game.level.getBlock(x1,z0);
		Block block3 = Game.level.getBlock(x0,z1);
		Block block4 = Game.level.getBlock(x1,z1);
		Block block5 = Game.level.getBlock(xTest, zTest);
		
		//As long as not a reaper, items will stop the enemy
		//from moving past them. Reapers can go through anything
		//but walls.
		if(ID != 4)
		{
			//Go through all the enemies in the game
			for(int i = 0; i < Game.enemies.size(); i++)
			{
				Enemy temp = Game.enemies.get(i);
				
				//Distance between enemy and other enemy
				double distance = Math.sqrt(((Math.abs(temp.xPos - xx))
						* (Math.abs(temp.xPos - xx)))
						+ ((Math.abs(temp.zPos - zz))
								* (Math.abs(temp.zPos - zz))));
				
				//If close enough, don't allow the enemy to move into
				//the other enemies. Enemy can still move if 2 units above
				//The other enemy
				if(distance <= 0.5 && !this.equals(temp)
						&& Math.abs(this.yPos - temp.yPos) <= 8)
				{
					return false;
				}	
			}
			
			//Go through all the solid items in the game
			for(int i = 0; i < Game.solidItems.size(); i++)
			{
				Item temp = Game.solidItems.get(i);
				
				//Distance between item and player
				double distance = Math.sqrt(((Math.abs(temp.x - xx))
						* (Math.abs(temp.x - xx)))
						+ ((Math.abs(temp.z - zz))
								* (Math.abs(temp.z - zz))));
				
				//Difference in y
				double yDifference = yPos - Math.abs(temp.y);
				
				//If close enough, don't allow the player to move into it.
				if(distance <= 0.3 && yDifference <= temp.height
						&& yDifference >= -temp.height)
				{
					return false;
				}	
				else if(distance <= 0.3 && yDifference > temp.height)
				{
					maxHeight 
						= temp.height + Math.abs(temp.y) + 5;
					return true;
				}	
			}
		}
		
		//Distance between enemy and player
		double distance = Math.sqrt(((Math.abs(Player.x - xx))
				* (Math.abs(Player.x - xx)))
				+ ((Math.abs(Player.z - zz))
						* (Math.abs(Player.z - zz))));
		
		double playerY = Player.y;
		
		if(playerY < 0)
		{
			playerY = 0;
		}
		
		double yDifference = playerY - Math.abs(yPos);
		
		//Can't clip inside player
		if(distance <= 0.3 && yDifference <= height
				&& yDifference >= -8)
		{
			return false;
		}


	   /*
	    * Gets the total height the enemy is standing on.
	    */
		totalHeight = block5.height + block5.y;
		
		if(ID != 2)
		{
			if(totalHeight >= 0 && totalHeight < 18)
			{
				heightCorrect = 8;
			}
			else if(totalHeight >= 18 && totalHeight < 30)
			{
				heightCorrect = 9;
			}
			else if(totalHeight >= 30 && totalHeight <= 36)
			{
				heightCorrect = 9;
			}
			else if(totalHeight > 36 && totalHeight <= 48)
			{
				heightCorrect = 10;
			}
			else if(totalHeight <= 79)
			{
				heightCorrect = 10;
			}
			else
			{
				double addCorrect;
				totalHeight -= 60;
				
				addCorrect = totalHeight / 20;
				heightCorrect = 10 + (0.5 * addCorrect);
				
			}
		}
		else
		{
			heightCorrect = 10;
		}
		
		//Calculates the new height the enemy will be at when it moves
		double newHeight = -(block5.height + block5.y);

	   /*
	    * Set enemies height to the height of the block it is standing
	    * on if this is the first time the enemy is looping through
	    * this method.
	    */
		if(test == 0)
		{
			yPos = newHeight;
			test = 1;
			
			if(ID == 2)
			{
				yPos = -48;
			}
			
			speed /= ((Display.fps / 30) + 1);
		}
		
		//If not flying enemy
		if(ID != 2)
		{
			//Set height based on the height of the block the enemy is on
			if(newHeight <= yPos + (2) 
					&& newHeight >= yPos - (2)
					&& !block5.isaDoor)
			{
				yPos = newHeight;
				maxHeight = yPos;
			}
		}	
		
	   /*
	    * If block is solid, see if the enemy can pass through it or not.
	    * If the block is not solid, then see if the enemy is of a height
	    * 2 or less. For some reason 2/heightCorrect is a height of 2 in
	    * the game. Some things are just weird like that I guess. I have
	    * not figured out the math behind these weird anomolies yet.
	    * 
	    * Do the same for the other blocks too.
	    */	    
	    if(block3.isSolid)
	    {
	    	return collisionChecks(block3);
	    }
	    else
	    {
	    	if(-yPos > (2) && ID != 2)
	    	{
	    		return false;
	    	}
	    	else
	    	{
	    		isStuck = false;
	    	}
	    }
	    
	    if(block2.isSolid)
	    {
	    	return collisionChecks(block2);
	    }
	    else
	    {
	    	if(-yPos > (2) && ID != 2)
	    	{
	    		return false;
	    	}
	    	else
	    	{
	    		isStuck = false;
	    	}
	    }
	    
	    if(block4.isSolid)
	    {
	    	return collisionChecks(block4);
	    }
	    else
	    {
	    	if(-yPos > (2) && ID != 2)
	    	{
	    		return false;
	    	}
	    	else
	    	{
	    		isStuck = false;
	    	}
	    }
	    
	    if(block1.isSolid)
	    {
	    	return collisionChecks(block1);
	    }
	    else
	    {
	    	if(-yPos > (2) && ID != 2)
	    	{
	    		return false;
	    	}
	    	else
	    	{
	    		isStuck = false;
	    	}
	    }
	    
	    isStuck = false;
	    
		return true;
	}
	
   /**
    * Determines whether the enemy is free to move to the next space or
    * not.
    * 
    * Similar to the player, there is a method for moving in the x
    * direction, and a method for moving in the z direction in terms
    * of collision, so that collision works at every height level
    * correctly.
    * 
    * @param xx
    * @param zz
    * @return
    */
	public boolean isFreeZ(double xx, double zz)
	{	
		double z = 0.3;
		
		if(zz < zPos)
		{
			z = -0.3;
		}
		else
		{
			z = 0.3;
		}
		
		//East Block
		int x0   = (int)(((xx) - z));
		
		//West Block
		int x1   = (int)(((xx) + z));
		
		//North Block
		int z0   = (int)(((zz) - z));
		
		//South Block
		int z1   = (int)(((zz) + z));
		
		int zTest = (int)(zPos);
		int xTest = (int)(xPos);
		
		Block block1 = Game.level.getBlock(x0,z0);
		Block block2 = Game.level.getBlock(x1,z0);
		Block block3 = Game.level.getBlock(x0,z1);
		Block block4 = Game.level.getBlock(x1,z1);
		Block block5 = Game.level.getBlock(xTest, zTest);
		
		//As long as not a reaper, items will stop the enemy
		//from moving past them. Reapers can go through anything
		//but walls.
		if(ID != 4)
		{
			//Go through all the enemies in the game
			for(int i = 0; i < Game.enemies.size(); i++)
			{
				Enemy temp = Game.enemies.get(i);
				
				//Distance between enemy and other enemy
				double distance = Math.sqrt(((Math.abs(temp.xPos - xx))
						* (Math.abs(temp.xPos - xx)))
						+ ((Math.abs(temp.zPos - zz))
								* (Math.abs(temp.zPos - zz))));
				
				//If close enough, don't allow the enemy to move into
				//the other enemies
				if(distance <= 0.5 && !this.equals(temp)
						&& Math.abs(this.yPos - temp.yPos) <= 8)
				{
					return false;
				}	
			}
			
			//Go through all the solid items in the game
			for(int i = 0; i < Game.solidItems.size(); i++)
			{
				Item temp = Game.solidItems.get(i);
				
				//Distance between item and player
				double distance = Math.sqrt(((Math.abs(temp.x - xx))
						* (Math.abs(temp.x - xx)))
						+ ((Math.abs(temp.z - zz))
								* (Math.abs(temp.z - zz))));
				
				//Difference in y
				double yDifference = yPos - Math.abs(temp.y);
				
				//If close enough, don't allow the player to move into it.
				if(distance <= 0.3 && yDifference <= temp.height
						&& yDifference >= -temp.height)
				{
					return false;
				}	
				else if(distance <= 0.3 && yDifference > temp.height)
				{
					maxHeight 
						= temp.height + Math.abs(temp.y) + 5;
					return true;
				}	
			}
		}
		
		//Distance between enemy and player
		double distance = Math.sqrt(((Math.abs(Player.x - xx))
				* (Math.abs(Player.x - xx)))
				+ ((Math.abs(Player.z - zz))
						* (Math.abs(Player.z - zz))));
		
		double playerY = Player.y;
		
		if(playerY < 0)
		{
			playerY = 0;
		}
		
		double yDifference = playerY - Math.abs(yPos);
		
		//Can't clip inside player
		if(distance <= 0.3 && yDifference <= height
				&& yDifference >= -8)
		{
			return false;
		}


	   /*
	    * Gets the total height the enemy is standing on.
	    */
		totalHeight = block5.height + block5.y;
		
		 /*
	    * Due to graphics units being weird, this corrects
	    * where the enemy is VISUALLY based on where it 
	    * should be. For instance if the enemy was at height
	    * 100, it would be above the ceiling of the map visually
	    * but not ACTUALLY. Meaning you could go up to 100 and
	    * still get hurt but you couldn't see the enemy. This
	    * corrects the enemies height so it can be seen and
	    * matches up with negligible error to the height of
	    * the walls.
	    */
		if(ID != 2)
		{
			if(totalHeight >= 0 && totalHeight < 18)
			{
				heightCorrect = 8;
			}
			else if(totalHeight >= 18 && totalHeight < 30)
			{
				heightCorrect = 9;
			}
			else if(totalHeight >= 30 && totalHeight <= 36)
			{
				heightCorrect = 9;
			}
			else if(totalHeight > 36 && totalHeight <= 48)
			{
				heightCorrect = 10;
			}
			else if(totalHeight <= 79)
			{
				heightCorrect = 10;
			}
			else
			{
				double addCorrect;
				totalHeight -= 60;
				
				addCorrect = totalHeight / 20;
				heightCorrect = 10 + (0.5 * addCorrect);
				
			}
		}
		else
		{
			heightCorrect = 10;
		}
		
		//Calculates the new height the enemy will be at when it moves
		double newHeight = -(block5.height + block5.y);

	   /*
	    * Set enemies height to the height of the block it is standing
	    * on if this is the first time the enemy is looping through
	    * this method.
	    * 
	    * Also because this is after the game is already running
	    * finally, this is now when the speeds should be set according
	    * to the fps of the game.
	    */
		if(test == 0)
		{
			yPos = newHeight;
			test = 1;
			
			if(ID == 2)
			{
				yPos = -48;
			}
			
			/*
		    * With faster ticks, the enemies move faster, and therefore to
		    * correct the faster movements, this slows them down so that
		    * the game is still winnable.
		    */
			speed /= ((Display.fps / 30) + 1);
		}
		
		if(ID != 2)
		{
			//Set height based on the height of the block the enemy is on
			if(newHeight <= yPos + (2) 
					&& newHeight >= yPos - (2)
					&& !block5.isaDoor)
			{
				yPos = newHeight;
				maxHeight = yPos;
			}
		}	
		
		//Allows enemies to ride lifts/elevators
		if(block5.wallID == 8 && ID != 2)
		{
			yPos = -(block5.height + block5.y);
		}
		
	   /*
	    * If block is solid, see if the enemy can pass through it or not.
	    * If the block is not solid, then see if the enemy is of a height
	    * 2 or less. For some reason 2/heightCorrect is a height of 2 in
	    * the game. Some things are just weird like that I guess. I have
	    * not figured out the math behind these weird anomolies yet.
	    * 
	    * Do the same for the other blocks too.
	    */	    
	    if(block3.isSolid)
	    {
	    	return collisionChecks(block3);
	    }
	    else
	    {
	    	if(-yPos > (2) && ID != 2)
	    	{
	    		return false;
	    	}
	    	else
	    	{
	    		isStuck = false;
	    	}
	    }
	    
	    if(block2.isSolid)
	    {
	    	return collisionChecks(block2);
	    }
	    else
	    {
	    	if(-yPos > (2) && ID != 2)
	    	{
	    		return false;
	    	}
	    	else
	    	{
	    		isStuck = false;
	    	}
	    }
	    
	    if(block4.isSolid)
	    {
	    	return collisionChecks(block4);
	    }
	    else
	    {
	    	if(-yPos > (2) && ID != 2)
	    	{
	    		return false;
	    	}
	    	else
	    	{
	    		isStuck = false;
	    	}
	    }
	    
	    if(block1.isSolid)
	    {
	    	return collisionChecks(block1);
	    }
	    else
	    {
	    	if(-yPos > (2) && ID != 2)
	    	{
	    		return false;
	    	}
	    	else
	    	{
	    		isStuck = false;
	    	}
	    }
	    
	    isStuck = false;
	    
		return true;
	}
	
   /**
    * Whenever the enemy is hurt, this is the method that is called to
    * actually take the enemies health away.
    * @param damage
    */
	public void hurt(int damage, boolean soundPlayed)
	{
		super.health -= damage;
		
		if(!soundPlayed)
		{
			if(ID == 1 || ID == 2)
			{
				//Call the play audio method to play the sound
				Display.playAudioFile(Display.input1, 
						Display.enemyHit, Display.clipNum);
				
				//Update sound clip being played
				Display.clipNum++;
				
				//If the clipNum is past the length, reset it
				if(Display.clipNum == Display.enemyHit.length)
				{
					Display.clipNum = 0;
				}	
			}
			else if(ID == 3)
			{
				//Call the play audio method to play the sound
				Display.playAudioFile
						(Display.input34, Display.tankHurt,
								Display.clipNum25);
				
				//Update sound clip being played
				Display.clipNum25++;
				
				//If the clipNum is past the length, reset it
				if(Display.clipNum25 == Display.tankHurt.length)
				{
					Display.clipNum25 = 0;
				}	
			}
			else if(ID == 4 || ID == 5)
			{
				//Call the play audio method to play the sound
				Display.playAudioFile(Display.input32, Display.reaperHurt
						, Display.clipNum23);
				
				//Update sound clip being played
				Display.clipNum23++;
				
				//If the clipNum is past the length, reset it
				if(Display.clipNum23 == Display.reaperHurt.length)
				{
					Display.clipNum23 = 0;
				}	
			}
			else if(ID == 7)
			{
				//Call the play audio method to play the sound
				Display.playAudioFile(Display.input33, 
						Display.vileCivHurt, Display.clipNum24);
				
				//Update sound clip being played
				Display.clipNum24++;
				
				//If the clipNum is past the length, reset it
				if(Display.clipNum24 == Display.vileCivHurt.length)
				{
					Display.clipNum24 = 0;
				}	
			}
			else
			{
				Display.playAudioFile(Display.input19, 
						Display.bossHit, Display.clipNum12);
				Display.clipNum12++;
				
				if(Display.clipNum12 == Display.bossHit.length)
				{
					Display.clipNum12 = 0;
				}	
			}
		}
	}
	
   /**
    * Frees up code space and makes it easier to make changes to all the
    * collision checks at once just changing just one method.
    * 
    * Optimizes code.
    * @param block
    * @return
    */
	public boolean collisionChecks(Block block)
	{
	   /*
	    * If the block in front of the enemy is greater than two units
	    * higher than the enemy, or if it is more than two lower than
	    * the enemy, or the enemy is still not far enough under a block
	    * to go through it (mainly used with doors) then don't allow
	    * the enemy to move.
	    * 
	    * If the enemy is the flyer demon enemy, then only stop its
	    * passage if the block is 48 units or higher tall. Or if the
	    * flyer demon is tracking the player, it acts like a normal enemy.
	    */
		if(((block.height + block.y - 2) > 
			-yPos && -yPos + 2 > block.y)
				|| ((block.height + block.y + 2) < -yPos
						&& ID != 2))
		{
			isStuck = true;
			return false;
		}
	   /*
	    * If the block is 2 higher or lower than the enemy, or if the
	    * block itself is higher than the enemy, the enemy can pass
	    * through or onto it (enemy can't go onto doors). 
	    */
		else if(block.isSolid && Math.abs(yPos) >= (block.y + block.height - 2)
				&& Math.abs(yPos) <= (block.y + block.height)
				|| block.isSolid && Math.abs(yPos) <= (block.y + block.height + 2)
				&& Math.abs(yPos) >= (block.y + block.height))
		{
			isStuck = false;
		
		   /*
		    * Do not reset the enemies maxHeight if the block
		    * it is trying to pass into is a door because then
		    * the enemy will teleport to the top of the door.
		    * 
		    * That would not be good.
		    */
			if(!block.isaDoor &&  ID != 2)
			{			
				maxHeight = -(block.height + block.y);
			}
			
			return true;
		}
		
		isStuck = false;
		
		return true;
	}
}
