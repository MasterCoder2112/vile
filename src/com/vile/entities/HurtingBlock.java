package com.vile.entities;

import com.vile.Display;
import com.vile.input.Controller;
import com.vile.levelGenerator.Block;
import com.vile.levelGenerator.Level;

/**
 * Title: Door
 * @author Alex Byrd
 * Date Updated: 8/15/2016
 * 
 * Description:
 * The Door Entity is an unseeable entity that coorelates to a wall that
 * is supposed to act as a door. When this entity is activated it will
 * move the door block up, keep it there for a little bit, then let it
 * fall again. This will allow the player to move through the door and
 * onto the next part of the level.  
 */
public class HurtingBlock extends Entity 
{
	//Wall that door correlates to position on map
	public  int blockX;
	public  int blockZ;
	
	//Keeps track of time since last damage of player.
	public static int time = 0;
	
	//Type of hurting block. Toxic waste, lava, or eventually spikes
	public  int blockType = 0;
	
   /**
    * A constructor of a door entity, which also holds the coordinates of
    * the block the door will raise.
    * @param x
    * @param y
    * @param z
    * @param wallX
    * @param wallZ
    */
	public HurtingBlock(double x, double y, double z, int wallX, int wallZ, int type) 
	{
		super(0, 0, 0, 0, 0, x, y, z, 8);
		
		blockX = wallX;
		blockZ = wallZ;
		blockType = type;
	}
	
   /**
    * Gets the door block at the location of the door entity, and moves
    * it up to a height of 3, makes it stay there long enough for the
    * player to move through, then goes back down.
    */
	public void activate()
	{
		int z = (int)(Player.z);
		int x = (int)(Player.x); 
		
		Block temp = Level.getBlock(blockX, blockZ);
		Block playersBlock = Level.getBlock(x, z);
		
		if(playersBlock.equals(temp) && time == 0 && Player.alive
				&& Player.y <= temp.height + 1
				&& Player.environProtectionTime == 0
				&& Player.immortality == 0
				&& !Controller.godModeOn)
		{
			//Toxic waste
			if(blockType == 0)
			{
				Player.health -= 2;
			}
			//Lava
			else
			{
				Player.health -= 5;
			}
			
			Display.playAudioFile(Display.input5, Display.playerHurt, Display.clipNum5);
			
			Display.clipNum5++;
			
			if(Display.clipNum5 == Display.playerHurt.length)
			{
				Display.clipNum5 = 0;
			}
		}
		
		if(time > 51)
		{
			time = 0;
		}	
	}
}
