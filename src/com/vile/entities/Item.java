package com.vile.entities;

import com.vile.Display;
import com.vile.Game;

/**
 * Title: Item
 * @author Alex Byrd
 * Date updated: 9/16/2016
 *
 *
 * Description:
 * Creates an Item with a given itemValue, itemID, and x,y,z values.
 * Items can also be activated when the player picks
 * them up, doing certain things depending on the items id.
 * 
 * Phase time keeps track of the texture phase the item is on
 * right now. Used for animated items.
 */
public class Item
{
	public int itemID = 0;
	public int itemValue = 0;
	public double x;
	public double y;
	public double z;
	public double height = 12;
	
	public int phaseTime = 0;
	
	//Flags for items
	public boolean isSeeable = true;
	public boolean hasAmmo = false;
	public boolean activated = false;
	public boolean isSolid = false;
	
   /**
    * Instantiates an item object
    * @param value
    * @param x
    * @param y
    * @param z
    * @param ID
    */
	public Item(int value, double x, double y, double z, int ID) 
	{
		itemValue = value;
		itemID = ID;
		this.x = x;
		this.y = y;
		this.z = z;
		
		//If it is a shell item, it has a default amount of 2
		if(itemID == 3)
		{
			itemValue = 2;
			hasAmmo = true;
		}
		//If the item is a shotgun, it carries slightly more shells than
		//A normal shell casing.
		else if(itemID == 21)
		{
			itemValue = 3;
			hasAmmo = true;
		}
		//If a chargePack
		else if(itemID == 50)
		{
			itemValue = 2;
			hasAmmo = true;
		}
		//If a large charge pack
		else if(itemID == 51)
		{
			itemValue = 5;
			hasAmmo = true;
		}
		//If box of Shells
		else if(itemID == 47)
		{
			itemValue = 10;
			hasAmmo = true;
		}
		//Pistol
		else if(itemID == 55)
		{
			itemValue = 5;
			hasAmmo = true;
		}
		//If clip
		else if(itemID == 56)
		{
			itemValue = 5;
			hasAmmo = true;
		}
		//If box of bullets
		else if(itemID == 57)
		{
			itemValue = 25;
			hasAmmo = true;
		}
		
		if(itemID >= 29 && itemID <= 32 ||
				itemID == 39 ||
				itemID == 42 ||
				itemID == 43)
		{
			if(itemID == 39 || itemID == 42 || itemID == 43)
			{
				height = 6;
			}
			
			isSolid = true;
			Game.solidItems.add(this);
		}
		
		
		//If PhaseCannon Weapon
		else if(itemID == 49)
		{
			itemValue = 3;
			hasAmmo = true;
		}
		
		if(Display.skillMode <= 1)
		{
			itemValue *= 4;
		}
		else if(Display.skillMode == 2 && itemID != 56)
		{
			itemValue *= 2;
		}
		
		if(itemID == 20)
		{
			Game.secretsInMap++;
		}
		
		if(itemID >= 8 && itemID <= 15 || itemID == 22 ||
				itemID == 23 || itemID == 20 || itemID == 48
				|| itemID == 54)
		{
			isSeeable = false;
		}
	}
	
   /**
    * Activates a particular item, meaning depending on its ID it performs
    * the task it is designated to such as healing the player a certain
    * amount for health packs, or giving the player ammo if shells, etc...
    * 
    * It returns whether the item could actually be used or not. If the
    * player could be healed for instance it returns true to remove the
    * item from the map, otherwise if the player is at full health it will
    * return false as the item was not used.
    * @return
    */
	public boolean activate()
	{
		boolean itemUsed = false;
	   /*
	    * If MegaHealth, then heal the player completely.
	    */
		if(itemID == 1)
		{
			if(Player.health < Player.maxHealth)
			{
				Player.health = Player.maxHealth;
				
				//Displays that the player picked up the megahealth
				Display.itemPickup = "Picked up the MEGAHEALTH!";
				Display.itemPickupTime = 1;
				
				itemUsed = true;
			}
		}
	   /*
	    * If a healthpack, heal the player accordingly
	    */
		else if(itemID == 2)
		{
			if(Player.health >= 100)
			{
				return false;
			}

			Player.health += itemValue;
			
			if(Player.health >= 100)
			{
				Player.health = 100;
			}
			
			//Displays that the player picked up a health pack
			Display.itemPickup = "Picked up a HealthPack!";
			Display.itemPickupTime = 1;
			
			itemUsed = true;
		}
		
	   /*
	    * With anything that gives a weapon ammo, check to see if the ammo
	    * can be added or not, and then if it is, pick up the item and
	    * display what you picked up. Add the ammo to its corresponding
	    * weapon as well.
	    */
		else if(hasAmmo)
		{		
			if(itemID == 55 || itemID == 56 || itemID == 57)
			{
				itemUsed = Player.weapons[0].addAmmo(itemValue);
			}
			else if(itemID == 3 || itemID == 21 || itemID == 47)
			{
				itemUsed = Player.weapons[1].addAmmo(itemValue);
			}
			else
			{
				itemUsed = Player.weapons[2].addAmmo(itemValue);
			}
			
			if(!itemUsed)
			{
				if(itemID != 49 ||
						itemID == 49 
						&& Player.weapons[2].canBeEquipped)
				{
					return false;
				}
			}
			
			if(itemID == 3)
			{
				//Displays that the player picked up the shells
				Display.itemPickup = "Picked up some shells!";
			}
			else if(itemID == 21)
			{
				//Displays that the player picked up the shells
				Display.itemPickup = "You Found the Shotgun!";
				
				//If weapon can't already be equipped, make it so that
				//it can be, and equip it
				if(!Player.weapons[1].canBeEquipped)
				{
					Player.weapons[1].canBeEquipped = true;
					Player.weaponEquipped = 1;
				}
			}
			else if(itemID == 47)
			{
				Display.itemPickup = "You found a Shell Box!";	
			}
			else if(itemID == 49)
			{
				Display.itemPickup = "You found the Phase Cannon!";
				
				//If weapon can't already be equipped, make it so that
				//it can be, and equip it
				if(!Player.weapons[2].canBeEquipped)
				{
					Player.weapons[2].canBeEquipped = true;
					Player.weaponEquipped = 2;
				}
			}
			else if(itemID == 50)
			{
				Display.itemPickup = "You found a charge pack!";	
			}
			else if(itemID == 51)
			{
				Display.itemPickup = "You found a large charge pack!";	
			}
			else if(itemID == 55)
			{
				Display.itemPickup = "You pick up a pistol!";
				
				//If weapon cant be dual wielded yet, make it so that it 
				//is
				if(!Player.weapons[0].dualWield)
				{
					Player.weapons[0].dualWield = true;
					Player.weaponEquipped = 0;
				}	
			}
			else if(itemID == 56)
			{
				Display.itemPickup = "You pick up a clip!";	
			}
			else if(itemID == 57)
			{
				Display.itemPickup = "You found a box of bullets!";	
			}
			
			Display.itemPickupTime = 1;
		}
		
	   /*
	    * Keys
	    */
		else if(itemID == 4)
		{
			Player.hasRedKey = true;
			itemUsed = true;
			
			//Displays that the player picked up the Red key
			Display.itemPickup = "Picked up Red Keycard!";
			Display.itemPickupTime = 1;
		}
		else if(itemID == 5)
		{
			Player.hasBlueKey = true;
			itemUsed = true;
			
			//Displays that the player picked up the Blue key
			Display.itemPickup = "Picked up Blue Keycard!";
			Display.itemPickupTime = 1;
		}
		else if(itemID == 6)
		{
			Player.hasGreenKey = true;
			itemUsed = true;
			
			//Displays that the player picked up the green key
			Display.itemPickup = "Picked up Green Keycard!";
			Display.itemPickupTime = 1;
		}
		else if(itemID == 7)
		{
			Player.hasYellowKey = true;
			itemUsed = true;
			
			//Displays that the player picked up the yellow key
			Display.itemPickup = "Picked up Yellow Keycard!";
			Display.itemPickupTime = 1;
		}
		//If a secret
		else if(itemID == 20)
		{
			Game.secretsFound++;
			itemUsed = true;
			
			//Display that a secret was found 
			Display.itemPickup = "Secret Found!!!";
			Display.itemPickupTime = 1;
		}
		//Skull of resurrection
		else if(itemID == 24)
		{
			Player.resurrections++;
			itemUsed = true;
			
			Display.itemPickup = "Picked up Skull of Resurrection!";
			Display.itemPickupTime = 1;
		}
		//Environment Suit
		else if(itemID == 25)
		{
			Player.environProtectionTime = 1100;
			itemUsed = true;
			
			Display.itemPickup = "You adorn the Environment suit!";
			Display.itemPickupTime = 1;
		}
		//Goblet of Immortality
		else if(itemID == 26)
		{
			Player.immortality = 1100;
			Player.vision      = 1100;
			itemUsed = true;
			
			Display.itemPickup = "The Goblet of Immortality!";
			Display.itemPickupTime = 1;
		}
		//Adrenaline
		else if(itemID == 27)
		{
			Player.maxHealth++;
			Player.health++;
			
			itemUsed = true;
			
			Display.itemPickup = "You've gotten the adrenaline!";
			Display.itemPickupTime = 1;
		}
		//Glasses of vision
		else if(itemID == 28)
		{
			Player.vision = 1100;
			
			itemUsed = true;
			Display.itemPickup = "You've now got nightvision!";
			Display.itemPickupTime = 1;
		}
		//Chainmeal Armor
		else if(itemID == 33)
		{
			if(Player.armor < 50)
			{
				Player.armor = 50;
				
				itemUsed = true;
				Display.itemPickup = "You adorn the chainmeal armor!";
				Display.itemPickupTime = 1;
			}
			else
			{
				itemUsed = false;
			}
		}
		//Combat Armor
		else if(itemID == 34)
		{
			if(Player.armor < 100)
			{
				Player.armor = 100;
				
				itemUsed = true;
				Display.itemPickup = "You adorn the combat armor!";
				Display.itemPickupTime = 1;
			}
			else
			{
				itemUsed = false;
			}
		}
		//Argent Steel Armor
		else if(itemID == 35)
		{
			if(Player.armor < 200)
			{
				Player.armor = 200;
				
				itemUsed = true;
				Display.itemPickup = "You adorn the Argent Steel armor!";
				Display.itemPickupTime = 1;
			}
			else
			{
				itemUsed = false;
			}
		}
		//Armor Shard
		else if(itemID == 36)
		{
			if(Player.armor < 200)
			{
				Player.armor++;
			}
			
			itemUsed = true;
			Display.itemPickup = "You pick up an armor shard!";
			Display.itemPickupTime = 1;
		}
		//Health Vial
		else if(itemID == 37)
		{
			Player.health++;
			
			itemUsed = true;
			Display.itemPickup = "You pick up a health vial!";
			Display.itemPickupTime = 1;
		}
		//Barons Treasure
		else if(itemID == 38)
		{
			itemUsed = true;
			Display.itemPickup = "Barons Treasure!";
			Display.itemPickupTime = 1;
		}
		//Holy Water
		else if(itemID == 39)
		{
			Display.itemPickup = "You pick up the Holy Water";
			Display.itemPickupTime = 1;
			
			//Becomes just a normal table
			itemID = 42;
		}
		//Scepter of Deciet
		else if(itemID == 40)
		{
			itemUsed = true;
			Display.itemPickup = "Scepter of Deciet!";
			Display.itemPickupTime = 1;
		}
		//Morgoths head
		else if(itemID == 41)
		{
			itemUsed = true;
			Display.itemPickup = "Belegoth's head!";
			Display.itemPickupTime = 1;
		}
		
		return itemUsed;
	}
}
