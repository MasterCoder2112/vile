package com.vile.entities;
/**
 * @Title  Bullet
 * @author Alex Byrd
 * Date Updated: 1/16/2016
 * 
 * Description:
 * Creates a bullet object that extends the projectile class and is used
 * to move a bullet in accordance with the direction the player is looking
 * and speed of the bullet. 
 *
 */
public class Bullet extends Projectile
{
	private double rotation;
	private double upRotation;
	
   /**
    * Creates the bullet Projectile object.
    * @param damage
    * @param speed
    * @param x
    * @param y
    * @param z
    * @param ID
    * @param rotation
    */
	public Bullet(int damage, double speed, double x, double y,
			double z, int ID, double rotation) 
	{
		super(damage, speed, x, y,
				z, ID);
		
		this.rotation = rotation;
	}

   /**
    * Moves the bullet depending on where the player was looking when
    * the bullet was shot, and the speed of the bullet.
    */
	public void move()
	{
	   /*
	    * Corrects Player.rotation so that the bullet is centered on the
	    * screen to the player.
	    */
		double correction = 44.765;
		
	   /*
	    * If the bullet was just shot, set the initial movement values,
	    * if the bullet is moving, keep moving it in the same 
	    * direction using those initial values.
	    */
		if(time == 0)
		{			
			x += 
					((Math.cos(rotation - correction)) 
							+ (Math.sin(rotation - correction))) 
							* speed;
			z += 
					((Math.cos(rotation - correction)) 
							- (Math.sin(rotation - correction))) 
							* speed;
			
		   /*
		    * Correct bullet for upRotation as best as possible, though it
		    * cannot be perfect, try your best to make it perfect.
		    */
			double upCorrection = 1;
			
			//Default signSwitch, meaning its positive
			int signSwitch   = 1;
			
			if(Player.upRotate > 1.105)
			{
				signSwitch = 0;
			}
			
		   /*
		    * Adds to bullets y position in such a way that it almost
		    * seems that the bullet is going straight from your
		    * crosshair, but for some reason I cannot get it exact.
		    */
			y += ((Player.upRotate - 1.105) * 
					(1 - (Player.upRotate - 1.105) * upCorrection)) 
							* signSwitch;

			
			//Sets rotation so the bullet will continue to move in
			//a certain direction
			rotation = rotation - correction;
			
		   /*
		    * Sets bullet upRotation so that the bullet will continue the
		    * same direction upward.
		    */
			upRotation = ((Player.upRotate - 1.105) * 
					(1 - (Player.upRotate - 1.105) * upCorrection)) 
					* signSwitch;
			
			//Checks to make sure the bullet isnt in a wall now
			isFreeZ(x, z);
			isFreeX(x, z);
		}
		else
		{
			
			x += ((Math.cos(rotation)) + (Math.sin(rotation))) * speed;
			z += ((Math.cos(rotation)) - (Math.sin(rotation))) * speed;
			y += upRotation;
			
			//Checks if the bullet has hit a wall or not each tick
			isFreeZ(x, z);
			isFreeX(x, z);
			
		}

		time++;
	}
}
