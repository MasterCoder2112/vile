package com.vile.entities;

import com.vile.Display;
import com.vile.Game;
import com.vile.input.Controller;
import com.vile.levelGenerator.Block;

/**
 * @Title  Projectile
 * @author Alex Byrd
 * Date Updated: 1/16/2016
 * 
 * Description:
 * Has all the methods a projectile might need except for a movement
 * method as both bullets and enemy projectiles have different tracking
 * methods. 
 *
 */
public abstract class Projectile
{
	public int time = 0;
	public boolean hit = false;
	public int damage = 0;
	public double speed = 0;
	public double x = 0;
	public double y = 0;
	public double z = 0;
	public int ID = 0;
	
	public Projectile(int damage, double speed, double x, double y,
			double z, int ID) 
	{
		this.damage = damage;
		this.speed = speed;
		this.x = x;
		this.y = y;
		this.z = z;
		this.ID = ID;
	}
	
	/**
    * Determines whether the bullet hits a wall or
    * not
    * @param xx
    * @param yy
    * @return
    */
	public boolean isFreeZ(double xx, double zz)
	{	
		//Used to calculate correct y to be used
		double tempY = -y;
		
		double z = -0.1;
		
		if(zz < z)
		{
			z = -0;
		}
		else
		{
			z = 0;
		}
		
		Block block5 = Game.level.getBlock((int)
				(Math.round(x)), (int)(z));
		
		//Right side of block
		int x0   = (int)(((xx) - z));
		
		//Left side of block
		int x1   = (int)(((xx) + z));
		
		//Front of block facing player when spawned
		int z0   = (int)(((zz) - z));
		
		//Back of block
		int z1   = (int)(((zz) + z));
		
		Block block1 = Game.level.getBlock(x0,z0);
		Block block2 = Game.level.getBlock(x1,z0);
		Block block3 = Game.level.getBlock(x0,z1);
		Block block4 = Game.level.getBlock(x1,z1);
		
		double correct = 12;
		
		//Because of a weird large door bug, this is in place 
		//temporarily until a better method for bullets is
		//worked out
		if(block1.isaDoor && block1.y >= 3)
		{
			return true;
		}
		
		if(block2.isaDoor && block2.y >= 3)
		{
			return true;
		}
		
		if(block3.isaDoor && block3.y >= 3)
		{
			return true;
		}
		
		if(block4.isaDoor && block4.y >= 3)
		{
			return true;
		}

	   /*
	    * If the block is solid, and you are within the blocks radius,
	    * then say you hit the wall. block height is /10 because the
	    * blocks actual height for checking is purposes is always 1
	    * from the center of the block up, and down.
	    */
		if(block1.isSolid && ((block1.height - 3)/correct) + block1.y > tempY
				&& tempY >= block1.y - ((block1.height - 3)/correct))
		{
			playHitSound();
			hit = true;
			return false;
		}
		else if(block1.isSolid && tempY >= ((block1.height - 3)/correct) + block1.y
				|| block1.isSolid && tempY <= block1.y - ((block1.height - 3)/correct))
		{		
			return true;
		}
		
		if(block2.isSolid && ((block2.height - 3)/correct) + block2.y > tempY
				&& tempY >= block2.y - ((block2.height - 3)/correct))
		{
			playHitSound();
			hit = true;
			return false;
		}
		else if(block2.isSolid && tempY >= ((block2.height - 3)/correct) + block2.y
				|| block2.isSolid && tempY <= block2.y - ((block2.height - 3)/correct))
		{
			return true;
		}
		
		if(block3.isSolid && ((block3.height - 3)/correct) + block3.y > tempY
				&& tempY >= block3.y - ((block3.height - 3) / correct))
		{
			playHitSound();
			hit = true;
			return false;
		}
		else if(block3.isSolid && tempY >= (block3.height - 3 /correct) + block3.y
				|| block3.isSolid && tempY <= block3.y - ((block3.height - 3)/correct))
		{
			return true;
		}
		
		if(block4.isSolid && ((block4.height - 3) / correct) + block4.y > tempY
				&& tempY >= block4.y - ((block4.height - 3) / correct))
		{
			playHitSound();
			hit = true;	
			return false;
		}
		else if(block4.isSolid && tempY >= ((block4.height - 3) / correct) + block4.y
				|| block4.isSolid && tempY <= block4.y - ((block4.height - 3)/ correct))
		{
			return true;
		}

		return true;
	}
	
   /**
    * Determines whether the bullet hits a wall or
    * not
    * @param xx
    * @param yy
    * @return
    */
	public boolean isFreeX(double xx, double zz)
	{	
		//Used to calculate correct y to be used
		double tempY = -y;
		
		// Number used for how far away from block, block detects
		double z = -0.1;
		
		if(xx < Player.x)
		{
			z = 0;
		}
		else
		{
			z = -0;
		}
		
		Block block5 = Game.level.getBlock((int)
				(Math.round(this.x)), (int)(this.z));
		
		//Right side of block
		int x0   = (int)(((xx) - z));
		
		//Left side of block
		int x1   = (int)(((xx) + z));
		
		//Front of block facing player when spawned
		int z0   = (int)(((zz) - z));
		
		//Back of block
		int z1   = (int)(((zz) + z));
		
		Block block1 = Game.level.getBlock(x0,z0);
		Block block2 = Game.level.getBlock(x1,z0);
		Block block3 = Game.level.getBlock(x0,z1);
		Block block4 = Game.level.getBlock(x1,z1);
		
		double correct = 12;
		
		//Because of a weird large door bug, this is in place 
		//temporarily until a better method for bullets is
		//worked out
		if(block1.isaDoor && block1.y >= 3)
		{
			return true;
		}
		
		if(block2.isaDoor && block2.y >= 3)
		{
			return true;
		}
		
		if(block3.isaDoor && block3.y >= 3)
		{
			return true;
		}
		
		if(block4.isaDoor && block4.y >= 3)
		{
			return true;
		}

	   /*
	    * If the block is solid, and you are within the blocks radius,
	    * then say you hit the wall. block height is /10 because the
	    * blocks actual height for checking is purposes is always 1
	    * from the center of the block up, and down.
	    */
		if(block1.isSolid && ((block1.height - 3)/correct) + block1.y > tempY
				&& tempY >= block1.y)
		{
			playHitSound();
			hit = true;
			return false;
		}
		else if(block1.isSolid && tempY >= ((block1.height - 3)/correct) + block1.y
				|| block1.isSolid && tempY <= block1.y)
		{		
			return true;
		}
		
		if(block2.isSolid && ((block2.height - 3)/correct) + block2.y > tempY
				&& tempY >= block2.y)
		{
			playHitSound();
			hit = true;
			return false;
		}
		else if(block2.isSolid && tempY >= ((block2.height - 3)/correct) + block2.y
				|| block2.isSolid && tempY <= block2.y)
		{
			return true;
		}
		
		if(block3.isSolid && ((block3.height - 3)/correct) + block3.y > tempY
				&& tempY >= block3.y)
		{
			playHitSound();
			hit = true;
			return false;
		}
		else if(block3.isSolid && tempY >= (block3.height - 3 /correct) + block3.y
				|| block3.isSolid && tempY <= block3.y)
		{
			return true;
		}
		
		if(block4.isSolid && ((block4.height - 3) / correct) + block4.y > tempY
				&& tempY >= block4.y)
		{
			hit = true;
			playHitSound();
			return false;
		}
		else if(block4.isSolid && tempY >= ((block4.height - 3) / correct) + block4.y
				|| block4.isSolid && tempY <= block4.y)
		{
			return true;
		}

		return true;
	}
	
	public void playHitSound()
	{
		if(ID == 0 || ID == 1 || ID == 2)
		{
			//Call the play audio method to play the sound
			Display.playAudioFile(Display.input14, 
					Display.wallHit, Display.clipNum8);
			
			//Update sound clip being played
			Display.clipNum8++;
			
			//If the clipNum is past the length, reset it
			if(Display.clipNum8 == Display.wallHit.length)
			{
				Display.clipNum8 = 0;
			}
		}
		else
		{
			//Call the play audio method to play the sound
			Display.playAudioFile
					(Display.input31, Display.enemyFireHit,
							Display.clipNum22);
			
			//Update sound clip being played
			Display.clipNum22++;
			
			//If the clipNum is past the length, reset it
			if(Display.clipNum22 == Display.enemyFireHit.length)
			{
				Display.clipNum22 = 0;
			}
		}
	}

}
