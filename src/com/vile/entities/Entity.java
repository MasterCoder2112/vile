package com.vile.entities;

import com.vile.Display;
import com.vile.Game;
import com.vile.input.Controller;

/**
 * Title: Entity
 * @author Alex Byrd
 * Date Updated: 7/26/2016
 * 
 * Description:
 * Each Entity inherits these methods and values but each different type 
 * of entity has different values for these variables.
 */
public abstract class Entity 
{
	public int health     = 0;
	public int armor      = 0;
	public int ammo       = 0;
	public int damage     = 0;
	public int ID         = 0;
	public int tick       = 0;
	public int tickRound  = 0;
	public int tickAmount = 11;
	public double speed   = 0;
	public double xPos    = 0;
	public double yPos    = 0;
	public double zPos    = 0;
	public double harmed  = 0;
	public double targetX = 0;
	public double targetY = 0;
	public double targetZ = 0;
	
	//Distance From player
	public double distanceFromPlayer = -1;
	public double distance = -1;
	
	public Enemy targetEnemy = null;
	
	//Entity Flags (Many will be used in later updates)
	public boolean killable         = false;
	public boolean hasItemDrops     = false;
	public boolean hasMovement      = false;
	public boolean canActivate      = false;
	public boolean canTeleport      = false;
	
	//Is the entity activated, meaning it can move and use its AI or not
	public boolean activated        = false;
	
	//Has a special attack such as firing a projectile or not
	public boolean hasSpecial       = false;
	
	//If entity is firing a projectile or not
	public boolean isFiring = false;
	
	//If entity is melee attacking
	public boolean isAttacking = false;
	
	//If a resurrector, this tells it when it can finally resurrect the
	//corpse at the end of its firing ceremony
	public boolean canResurrect = false;
	
   /**
    * Instantiates the type of entity
    * @param health
    * @param armor
    * @param ammo
    * @param damage
    * @param speed
    * @param x
    * @param y
    * @param z
    * @param ID
    */
	public Entity(int health, int armor, int ammo, int damage, double speed
			, double x, double y, double z, int ID) 
	{
		this.health = health;
		this.armor  = armor;
		this.ammo   = ammo;
		this.damage = damage;
		this.speed  = speed;
		this.ID     = ID;
		xPos = x;
		yPos = y;
		zPos = z;
	}
	
	//Gets the x position of the entity
	public double getX()
	{
		return xPos;
	}
	
	//Gets the y position.
	public double getY()
	{
		return yPos;
	}
	
	//Gets the z position
	public double getZ()
	{
		return zPos;
	}
	
   /**
    * Continues to tick the entity and update its action
    */
	public void tick(Enemy currentEnemy)
	{
		this.tick++;
		
		//If still just recently hit, keep pose the same
		//and keep counting down.
		if(harmed > 0)
		{
			harmed--;
		}
		
		//If target enemy is dead
		if(targetEnemy != null && targetEnemy.health <= 0)
		{
			targetEnemy = null;
			targetX = Player.x;
			targetY = Player.z;
			targetZ = Player.y;
		}
		
	   /*
	    * Determines distance between the enemy and the player
	    * by finding the hypotenuse between the x direction and z
	    * from the player to the enemy being scanned.
	    */
		distanceFromPlayer = Math.sqrt(((Math.abs(this.getX() - Player.x))
				* (Math.abs(this.getX() - Player.x)))
				+ ((Math.abs(this.getZ() - Player.z))
						* (Math.abs(this.getZ() - Player.z))));
		
		//Same but for the entities current target
		distance = Math.sqrt(((Math.abs(this.getX() - targetX))
				* (Math.abs(this.getX() - targetX)))
				+ ((Math.abs(this.getZ() - targetZ))
						* (Math.abs(this.getZ() - targetZ))));
		
	   /*
	    * Halfway through each tick round, if the entity is attacking the
	    * player, then call the hurtPlayer method. And perform the given
	    * functions.
	    */
		if(this.tick >= (int)(tickAmount / 2) * ((Display.fps / 30) + 1))
		{
			if(isAttacking)
			{
			}
		}
		
		//Every so many ticks, reset time. Depends on the games fps
		if(this.tick >= tickAmount * ((Display.fps / 30) + 1))
		{
			this.tick = 0;
			tickRound++;
			
			//Every 5 sets of 10 ticks, reset the tick round count
			if(tickRound == 5)
			{
				tickRound = 0;
			}
			
			//If melee attacking, end the attack at the end of each tick
			//round and hurt the target accordingly if still in range
			if(isAttacking)
			{
				//If target is Player
				if(distanceFromPlayer <= 1 && targetEnemy == null)
				{
					Player.hurtPlayer(damage);
				}
				else if(distance <= 1 && targetEnemy != null)
				{
					targetEnemy.hurt(damage, false);
					targetEnemy.targetEnemy = currentEnemy;
				}
				
				//Call the play audio method to play the sound
				Display.playAudioFile
						(Display.input30, Display.enemyFire,
								Display.clipNum21);
				
				//Update sound clip being played
				Display.clipNum21++;
				
				//If the clipNum is past the length, reset it
				if(Display.clipNum21 == Display.enemyFire.length)
				{
					Display.clipNum21 = 0;
				}
				
				isAttacking = false;
			}
			
		   /*
			* Activates enemies special power only at the end of the
			* tick cycle. Also the enemy has to be in the process of
			* firing for this to occur.
			*/	
			if(isFiring)
			{
				this.isFiring = false;
				
				//Call the play audio method to play the sound
				Display.playAudioFile
						(Display.input30, Display.enemyFire,
								Display.clipNum21);
				
				//Update sound clip being played
				Display.clipNum21++;
				
				//If the clipNum is past the length, reset it
				if(Display.clipNum21 == Display.enemyFire.length)
				{
					Display.clipNum21 = 0;
				}
				
				//Default projectile
				int tempID = 3;
				
				//If flyer enemy, have different type of projectile
				if(ID == 2)
				{
					tempID = 4;
				}
				
				//If Tank type enemy shoot in 3 directions
				if(ID == 3)
				{
					//Create new Projectile object with given parameters
					EnemyFire temp = new EnemyFire(10, 0.1, xPos, yPos,
							zPos, 5, targetX, targetZ, 
							targetY, 0, currentEnemy);
					
					EnemyFire temp2 = new EnemyFire(10, 0.1, xPos, yPos,
							zPos, 5, targetX, targetZ, 
							targetY, Math.PI / 8, currentEnemy);
					
					EnemyFire temp3 = new EnemyFire(10, 0.1, xPos, yPos,
							zPos, 5, targetX, targetZ, 
							targetY, -Math.PI / 8, currentEnemy);
					
					//Add Projectiles to the Game events
					Game.enemyProjectiles.add(temp);
					Game.enemyProjectiles.add(temp2);
					Game.enemyProjectiles.add(temp3);
				}
				else if(ID == 5 && !canResurrect)
				{
					canResurrect = true;
				}
				//Bosses shoot in 5 directions
				else if(ID == 6 || ID == 8)
				{
					EnemyFire temp = new EnemyFire(10, 0.2, xPos, yPos,
							zPos, 6, targetX, targetZ, 
							targetY, 0, currentEnemy);
					
					EnemyFire temp2 = new EnemyFire(10, 0.2, xPos, yPos,
							zPos, 6, targetX, targetZ, 
							targetY, Math.PI / 16, currentEnemy);
					
					EnemyFire temp3 = new EnemyFire(10, 0.2, xPos, yPos,
							zPos, 6, targetX, targetZ, 
							targetY, -Math.PI / 16, currentEnemy);
					
					EnemyFire temp4 = new EnemyFire(10, 0.2, xPos, yPos,
							zPos, 6, targetX, targetZ, 
							targetY, Math.PI / 8, currentEnemy);
					
					EnemyFire temp5 = new EnemyFire(10, 0.2, xPos, yPos,
							zPos, 6, targetX, targetZ, 
							targetY, -Math.PI / 8, currentEnemy);
					
					EnemyFire temp6 = new EnemyFire(10, 0.2, xPos, yPos,
							zPos, 6, targetX, targetZ, 
							targetY, Math.PI / 4, currentEnemy);
					
					EnemyFire temp7 = new EnemyFire(10, 0.2, xPos, yPos,
							zPos, 6, targetX, targetZ, 
							targetY, -Math.PI / 4, currentEnemy);
					
					//Add Projectiles to the Game events
					Game.enemyProjectiles.add(temp);
					Game.enemyProjectiles.add(temp2);
					Game.enemyProjectiles.add(temp3);
					Game.enemyProjectiles.add(temp4);
					Game.enemyProjectiles.add(temp5);
					Game.enemyProjectiles.add(temp6);
					Game.enemyProjectiles.add(temp7);
				}
				else
				{
					int tempDamage = 5;
					
					if(ID == 2)
					{
						tempDamage = 8;
					}
					//Create new Projectile object with given parameters
					EnemyFire temp = new EnemyFire(tempDamage, 0.1, xPos, yPos,
							zPos, tempID, targetX, targetZ, 
							targetY, 0, currentEnemy);
					
					//Add Projectile to the Game events
					Game.enemyProjectiles.add(temp);
				}
			}
		}
	}

}
