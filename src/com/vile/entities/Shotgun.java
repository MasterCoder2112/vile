package com.vile.entities;

import com.vile.Display;
import com.vile.Game;

/**
 * Title: Shotgun
 * Date Created: 10/20/2016
 * @author Alexander Byrd
 * 
 * Description:
 * A type of weapon that creates a new weapon of ID 0, and has a unique
 * updateValues method that updates its phases of firing and such when
 * the weapon is being fired.
 */
public class Shotgun extends Weapon implements WeaponInterface
{
	//What sound clip is being played
	private int clipNum = 0;
	
   /**
    * Creates a new Weapon of ID 0
    */
	public Shotgun() 
	{
		super(1);
	}
	
   /**
    * Update weaponShootTime, and depending on its value update its stage
    * in the firing process of the weapon.
    */
	public void updateValues()
	{
		//IF being fired
		if(weaponShootTime != 0)
		{
			//If weapon already fired, set phase to dormant phase
			if(weaponShootTime > 25)
			{
				weaponPhase = 0;
			}
			
			//Fire weapon and play sound immediately after being fired
			if(weaponShootTime == 1)
			{
				ammo--;
				weaponPhase = 1;
				
				//Adds spray shot to shotgun
				Game.addBullet(damage, weaponID, 0.3,
						Player.rotation);
				
				Game.addBullet(damage, weaponID,
						0.3, Player.rotation + 0.07);
				
				Game.addBullet(damage, weaponID,
						0.3, Player.rotation - 0.07);
				
				Game.addBullet(damage, weaponID,
						0.3, Player.rotation + 0.05);
				
				Game.addBullet(damage, weaponID,
						0.3, Player.rotation - 0.05);
				
				Game.addBullet(damage, weaponID,
						0.3, Player.rotation + 0.1);
				
				Game.addBullet(damage, weaponID,
						0.3, Player.rotation - 0.1);
				
				Game.addBullet(damage, weaponID,
						0.3, Player.rotation + 0.025);
				
				Game.addBullet(damage, weaponID,
						0.3, Player.rotation - 0.025);
				
				Display.playAudioFile(Display.input6, 
						Display.shoot, clipNum);
				
				//Update sound clip being played
				clipNum++;
				
				//If the clipNum is past the length, reset it
				if(clipNum == Display.shoot.length)
				{
					clipNum = 0;
				}
			}
			else if(weaponShootTime == 6)
			{
				weaponPhase = 2;
			}
			else if(weaponShootTime == 11)
			{
				weaponPhase = 3;
			}
			else if(weaponShootTime == 16)
			{
				weaponPhase = 3;
			}
			else if(weaponShootTime == 21)
			{
				weaponPhase = 3;
			}
			
			weaponShootTime++;
			
			//Set weapon to fire-able again after its cool down time has
			//been reached
			if(weaponShootTime >= cooldownTime)
			{
				weaponShootTime = 0;
			}
		}
		//IF not being fired, it is in a dormant phase.
		else
		{
			weaponPhase = 0;
		}
	}

}
