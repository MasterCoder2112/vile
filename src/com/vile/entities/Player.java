package com.vile.entities;

import java.util.ArrayList;

import com.vile.Display;
import com.vile.Game;
import com.vile.input.Controller;

/**
 * Title: Player
 * @author Alex Byrd
 * Date Updated: 5/31/2016
 *
 * Keeps track of the players status. Such as position, ammo, health, etc
 * ... and also adds ammo to the player. 
 */
public class Player 
{
	public static int health      = 100;
	public static int maxHealth   = 200;
	public static double height   = 2.0;	
	public static boolean alive      = true;
	public static int armor       = 0;
	public static double x        = 0;
	public static double y        = 0;
	public static double z        = 0;
	public static double rotation = 0;
	
	//Max Height a player can stand on at moment.
	public static double maxHeight = 0;
	
	public static boolean hasRedKey = false;
	public static boolean hasBlueKey = false;
	public static boolean hasGreenKey = false;
	public static boolean hasYellowKey = false;
	
	//Used for rendering
	public static double upRotate = 1.105;
	
	//How many resurrect skulls does the player have
	public static int resurrections = 0;
	
	//How long the player is protected from toxic waste/lava
	public static int environProtectionTime = 0;
	
	//How long the player is immortal for
	public static int immortality = 0;
	
	//Enhanced Vision/night vision
	public static int vision = 0;
	
	private static final int LIMIT = 40;
	
	//0 for shotgun, 1 for phaseCannon
	public static int weaponEquipped = 0;
	
	public static Weapon[] weapons = new Weapon[3];
	
	
	
   /**
    * Reset all of a Players variables and make a new Player
    */
	public Player()
	{
		health     = 100;
		maxHealth  = 200;
		height     = 2.0;
		alive      = true;
		armor      = 0;
		maxHeight  = 0;
		x          = 1.5;
		y          = 0;
		z          = 1.5;
		rotation   = 0;
		upRotate   = 1.105;
		
		hasRedKey = false;
		hasBlueKey = false;
		hasGreenKey = false;
		hasYellowKey = false;
		
		resurrections = 0;
		environProtectionTime = 0;
		immortality = 0;
		vision = 0;
		
		weapons[0] = new Pistol();
		weapons[1] = new Shotgun();
		weapons[2] = new PhaseCannon();
		weaponEquipped = 0;
	}
	
   /**
    * Updates the players buffs by counting down the ticks until they
    * wear off.
    */
	public static void updateBuffs()
	{
	   /*
	    * Each tick, take the time the the player is protected from
	    * environmental conditions by 1.
	    */
		if(environProtectionTime > 0)
		{
			environProtectionTime--;
		}
		
	   /*
	    * Each tick, take the time the player is immortal for down by
	    * 1 tick.
	    */
		if(immortality > 0)
		{
			immortality--;
		}
		
	   /*
	    * Each tick, take the time the player is immortal for down by
	    * 1 tick.
	    */
		if(vision > 0)
		{
			vision--;
		}
	}
	
	public static void hurtPlayer(double damage)
	{
		if(Controller.godModeOn || immortality != 0)
		{
			return;
		}
		//Hurt player depending on how much damage the enemy deals
		//and on the armor the player is wearing.
		if(Player.armor > 150)
		{
			Player.health -= (int)(damage / 2);
			Player.armor -= damage;
		}
		else if(Player.armor > 100)
		{
			Player.health -= (int)(damage / 1.75);
			Player.armor -= damage * 1.5;
		}
		else if(Player.armor > 50)
		{
			Player.health -= (int)(damage / 1.5);
			Player.armor -= damage * 2;
		}
		else if(Player.armor > 0)
		{
			Player.health -= (int)(damage / 1.25);
			Player.armor -= damage * 3;
		}
		else
		{
			Player.health -= damage;
		}
		
		if(Player.armor < 0)
		{
			Player.armor = 0;
		}	
		
		//Enemy can hurt the player and the games not in
		//peaceful mode.
		if (Display.skillMode > 0) 
		{
			Display.playAudioFile(Display.input5, 
					Display.playerHurt, Display.clipNum5);
			Display.clipNum5++;
			
			if(Display.clipNum5 == Display.playerHurt.length)
			{
				Display.clipNum5 = 0;
			}
		}
	}
	
}
