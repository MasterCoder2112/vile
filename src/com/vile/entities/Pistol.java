package com.vile.entities;

import com.vile.Display;
import com.vile.Game;

/**
 * Title: Pistol
 * @author Alexander Byrd
 * Date Created: 11/26/2016
 * 
 * Description:
 * Is a weapon with unique values and rates of fire
 *
 */
public class Pistol extends Weapon implements WeaponInterface
{
	//What sound clip is being played
	private int clipNum = 0;
	
   /**
	* Creates a new Weapon of ID 2
	*/
	public Pistol() 
	{
		super(0);
	}
	
   /**
    * Update weaponShootTime, and depending on its value update its stage
    * in the firing process of the weapon.
    */
	public void updateValues()
	{
		//IF being fired
		if(weaponShootTime != 0)
		{
			//If weapon already fired, set phase to dormant phase
			if(weaponShootTime > 24)
			{
				weaponPhase = 0;
			}
			
			//Fire weapon and play sound immediately after being fired
			if(weaponShootTime == 1)
			{
				ammo--;
				weaponPhase = 1;
				
				//Adds spray shot to shotgun
				Game.addBullet(damage, weaponID, 0.3,
						Player.rotation);
				
				Display.playAudioFile(Display.input25, 
						Display.pistol, clipNum);
				
				//Update sound clip being played
				clipNum++;
				
				//If the clipNum is past the length, reset it
				if(clipNum == Display.pistol.length)
				{
					clipNum = 0;
				}
			}
			else if(weaponShootTime == 5)
			{
				weaponPhase = 2;
			}
			else if(weaponShootTime == 10)
			{
				weaponPhase = 3;
			}
			else if(weaponShootTime == 22)
			{
				weaponPhase = 0;
			}
			
			weaponShootTime++;
			
			//Set weapon to fire-able again after its cool down time has
			//been reached
			if(weaponShootTime >= cooldownTime)
			{
				weaponShootTime = 0;
			}
		}
		//IF not being fired, it is in a dormant phase.
		else
		{
			weaponPhase = 0;
		}
		
		//IF second weapon is being fired
		if(weaponShootTime2 != 0)
		{
			//If weapon already fired, set phase to dormant phase
			if(weaponShootTime2 > 24)
			{
				weaponPhase2 = 0;
			}
			
			//Fire weapon and play sound immediately after being fired
			if(weaponShootTime2 == 1)
			{
				ammo--;
				weaponPhase2 = 1;
				
				//Adds spray shot to shotgun
				Game.addBullet(damage, weaponID, 0.3,
						Player.rotation);
				
				Display.playAudioFile(Display.input25, 
						Display.pistol, clipNum);
				
				//Update sound clip being played
				clipNum++;
				
				//If the clipNum is past the length, reset it
				if(clipNum == Display.pistol.length)
				{
					clipNum = 0;
				}
			}
			else if(weaponShootTime2 == 5)
			{
				weaponPhase2 = 2;
			}
			else if(weaponShootTime2 == 10)
			{
				weaponPhase2 = 3;
			}
			else if(weaponShootTime2 == 22)
			{
				weaponPhase2 = 0;
			}
			
			weaponShootTime2++;
			
			//Set weapon to fire-able again after its cool down time has
			//been reached
			if(weaponShootTime2 >= cooldownTime)
			{
				weaponShootTime2 = 0;
			}
		}
		//IF not being fired, it is in a dormant phase.
		else
		{
			weaponPhase2 = 0;
		}
	}

}
