package com.vile.entities;

import com.vile.Display;

/**
 * @Title  EnemyFire
 * @author Alex Byrd
 * Date Updated: 1/16/2016
 * 
 * Description:
 * Creates a bullet object that extends the projectile class and is used
 * to move a bullet in accordance with the direction the player is looking
 * and speed of the bullet. 
 *
 */
public class EnemyFire extends Projectile
{
	private double targetX = 0;
	private double targetZ = 0;
	private double targetY = 0;
	private double rotation = 0;
	private double upRotation = 0;
	private double upMovement = 0;
	public Enemy sourceEnemy = null;
	
   /**
    * Creates a new Projectile object that is an enemy projectile
    * @param damage
    * @param speed
    * @param x
    * @param y
    * @param z
    * @param ID
    * @param targetX
    * @param targetZ
    * @param targetY
    */
	public EnemyFire(int damage, double speed, double x, double y,
			double z, int ID, double targetX, double targetZ, 
			double targetY, double rotChange, Enemy source) 
	{
		//Creates the projectile object
		super(damage, speed, x, y / 10,
				z, ID);
		
		//If easy mode
		if(Display.skillMode == 1)
		{
			speed  *= 0.5;
			damage *= 0.5;
		}
		//If Bring it on mode
		else if(Display.skillMode == 3)
		{
			speed  *= 2;
			damage *= 2;
		}
		//Death cannot hurt me mode.
		else if(Display.skillMode == 4)
		{
			speed *= 3;
			damage *= 3;
		}
		//If peaceful mode
		else if(Display.skillMode == 0)
		{
			speed = 0;
			damage = 0;
		}
		
		//Set values based on whats passed in
		this.targetX = targetX;
		this.targetZ = targetZ;
		this.targetY = targetY;
		
		//The enemy that was the source of the Projectile
		sourceEnemy = source;
		
		double hypot = 0;
		
		//Finds the hypotenuse of the triangle in the x and z plane
		//between the target and the enemy
		hypot = Math.sqrt(((x - targetX) * 
				(x - targetX)) + ((z - targetZ) * (z - targetZ)));
		
		//Hypotenuse in the y direction between the enemy and the target
		double upHypot = Math.sqrt((hypot * hypot) + 
				((y - targetY) * (y - targetY)));
		
		//If flying enemy projectile, things are changed up a bit.
		if(ID == 4)
		{
			if(targetY < -y)
			{
				upRotation = Math.atan((hypot * speed) 
						/((targetY - y) * speed));
				upMovement = (0.3 * Math.tan(upRotation)) * 1.3;
				upMovement = -upMovement;
			}
			else
			{
				//Angle the projectile must be shot upward
				upRotation = Math.atan((hypot * speed) 
						/((targetY - y) * speed));
				upMovement = (0.3 * Math.tan(upRotation)) * 3;
			}
		}
		else
		{
			upRotation = Math.atan(((targetY - y) * speed) 
					/ (hypot * speed));
			
			upMovement = (0.3 * Math.tan(upRotation)) / 24;
		}
		
		if(Math.abs(targetY + y) / 12 <= 1.3)
		{
			upMovement = 0;
		}
		
		//Angle that the player is in accordance to the enemy that
		//fired the projectile so that the projectile can be fired
		//in that direction
		rotation = Math.atan
				(((targetX - x) * speed) / ((targetZ - z) * speed));
		
	   /*
	    * If the player is in the 3rd or 4th quadrant of the map then add
	    * PI to rotation so that the projectile will be shot into the
	    * correct quadrant of the map and at the player.
	    */
		if(targetZ < z)
		{
			rotation += Math.PI;
		}
		
		//Change in rotation sent in if applicable
		rotation += rotChange;
	}
	
   /**
    * Moves the projectile similar to a bullet, but adds a new way that
    * the projectile should move in the y direction depending on where
    * the Player is in the air.
    */
	public void move()
	{
	   /*
	    * Corrects rotation so that the projectile is centered
	    * correctly in the graph of firing.
	    */
		double correction = 44.765;
		
	   /*
	    * If the projectile was just shot, set the initial movement values
	    * , if the projectile is moving, keep moving it in the same 
	    * direction using those initial values.
	    */
		if(time == 0)
		{			
			x += 
					((Math.cos(rotation - correction)) 
							+ (Math.sin(rotation - correction))) 
							* speed;
			z += 
					((Math.cos(rotation - correction)) 
							- (Math.sin(rotation - correction))) 
							* speed;

			
			//Sets rotation so the projectile will continue to move in
			//a certain direction
			rotation = rotation - correction;
			
			//The Projectile goes to the targetY
			if(y > targetY + speed)
			{
				y += upMovement;
			}
			else if(y < targetY - speed)
			{
				y -= upMovement;
			}
			
			//Checks to make sure the bullet isnt in a wall now
			isFreeZ(x, z);
			isFreeX(x, z);
		}
		else
		{
			time = 1;
			x += ((Math.cos(rotation)) + (Math.sin(rotation))) * speed;
			z += ((Math.cos(rotation)) - (Math.sin(rotation))) * speed;
			
			if(y > targetY + speed)
			{
				y += upMovement;
			}
			else if(y < targetY - speed)
			{
				y -= upMovement;
			}
			
			//Checks if the projectile has hit a wall or not each tick
			isFreeZ(x, z);
			isFreeX(x, z);	
		}

		time++;
	}

}
